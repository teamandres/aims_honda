Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_MF_taxAddEdit
    Private gCon As New Clsappconfiguration
    Private sKeyTax As String
    Private sKeySupplier As String

    Public Property KeyTax() As String
        Get
            Return sKeyTax
        End Get
        Set(ByVal value As String)
            sKeyTax = value
        End Set
    End Property

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub


    Private Sub frm_MF_taxAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sKeyTax <> "" Then
            loadTax()
        End If

        m_loadSupplier(cboSupplierName, cboSupplierID)
        m_selectedCboValue(cboSupplierName, cboSupplierID, sKeySupplier)
    End Sub

    Private Sub loadTax()
        Dim sSQLCmd As String = "SELECT * FROM mSalesTax "
        sSQLCmd &= "WHERE fxKeyTax = '" & sKeyTax & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtName.Text = rd.Item("fcTaxName")
                    txtDescription.Text = rd.Item("fcTaxDescription")
                    txtRate.Text = rd.Item("fdTaxRate")
                    sKeySupplier = rd.Item("fxKeySupplier").ToString
                    chkInactive.Checked = Not rd.Item("fbActive")
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Tax")
        End Try
    End Sub

    Private Sub cboSupplierName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSupplierName.SelectedIndexChanged
        cboSupplierID.SelectedIndex = cboSupplierName.SelectedIndex
        sKeySupplier = cboSupplierID.SelectedItem.ToString
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        updatetax()
    End Sub

    Private Sub updateTax()
        Dim sSQLCmd As String = "SPU_SalesTax_Update "
        sSQLCmd &= "@fxKeyTax = '" & sKeyTax & "' "
        sSQLCmd &= ",@fcTaxName = '" & txtName.Text & "'"
        sSQLCmd &= ",@fcTaxDescription = '" & txtDescription.Text & "'"
        sSQLCmd &= ",@fdTaxRate = " & txtRate.Text
        sSQLCmd &= ",@fbActive = " & IIf(chkInactive.Checked = True, 0, 1)
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"
        If sKeySupplier <> "" Then
            sSQLCmd &= ",@fxKeySupplier = '" & sKeySupplier & "'"
        End If
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully updated", "Add/Edit Sales Tax")
            frm_MF_tax.refreshForm()
            Call m_loadTax(frm_cust_CreateSalesOrder.cboTaxName, frm_cust_CreateSalesOrder.cboTaxID)
            frm_cust_CreateSalesOrder.Refresh()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Add/Edit Sales Tax")
        End Try
    End Sub
End Class