Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMember_Status

#Region "Variables"
    Private jobrankID As String
#End Region
#Region "Get Property"
    Private Property getJobrankID() As String
        Get
            Return jobrankID
        End Get
        Set(ByVal value As String)
            jobrankID = value
        End Set
    End Property
#End Region
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then
                Me.txtjobrank.Focus()
                Label1.Text = "Add New Status"
                Call cleartxt()
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(74, 6)

            ElseIf btnsave.Text = "Save" Then
                If Me.txtjobrank.Text <> "" And Me.txtjobdescription.Text <> "" Then
                    Label1.Text = "Status"
                    Call Statusvalidation()
                    Call cleartxt()
                    Me.txtjobrank.Focus()
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(230, 6)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Job Rank code desc validation get/stored proc"
    Public Sub Statusvalidation()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Membership_Status_Validate", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@fcStatusCode", SqlDbType.VarChar, 30).Value = Me.txtjobrank.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("Code already exist, please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call AddNewStatus(Me.txtjobrank.Text, Me.txtjobdescription.Text)
                Call Status()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Statusvalidation")
        End Try
    End Sub
#End Region
#Region "Insert new Status"
    Private Sub AddNewStatus(ByVal code As String, ByVal desc As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Membership_Status_Insert", _
                                      New SqlParameter("@fcStatusCode", code), _
                                      New SqlParameter("@fcStatusDesc", desc))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "AddNewStatus")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub

#End Region



    Public Sub cleartxt()
        Me.txtjobrank.Text = ""
        Me.txtjobdescription.Text = ""
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Me.txtjobrank.Focus()
                Label1.Text = "Edit Status"
                Me.btnsave.Enabled = False
                Me.txtjobrank.Text = Me.LvlJobRank.SelectedItems(0).Text
                Me.txtjobrank.Tag = Me.LvlJobRank.SelectedItems(0).Text
                Me.txtjobdescription.Text = Me.LvlJobRank.SelectedItems(0).SubItems(1).Text
                Me.txtjobdescription.Tag = Me.LvlJobRank.SelectedItems(0).SubItems(1).Text
                getJobrankID() = Me.LvlJobRank.SelectedItems(0).SubItems(2).Text
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(143, 6)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                If Me.txtjobrank.Text <> "" And Me.txtjobdescription.Text <> "" Then
                    Label1.Text = "Status"
                    Call UpdateStatus(Me.txtjobrank.Text, Me.txtjobdescription.Text, jobrankID)
                    Call Status()
                    Call cleartxt()
                    btndelete.Visible = True
                    btnupdate.Text = "Edit"
                    btnupdate.Image = My.Resources.edit1
                    btnclose.Text = "Cancel"
                    btnclose.Location = New System.Drawing.Point(230, 6)
                    btnclose.Text = "Close"
                    Me.btnsave.Enabled = True
                    Me.btndelete.Visible = True

                End If
            Else
                MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        Catch ex As Exception
        End Try
    End Sub
#Region "Update Status"
    Private Sub UpdateStatus(ByVal code As String, ByVal desc As String, ByVal statid As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Membership_Status_Update", _
                                      New SqlParameter("@fcStatusCode", code), _
                                      New SqlParameter("@fcStatusDesc", desc), _
                                      New SqlParameter("@pk_MembershipStatus", statid))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "UpdateStatus")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Status listview get/stored proc"
    Public Sub Status()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Membership_Status_Select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With Me.LvlJobRank
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Status Code", 100, HorizontalAlignment.Left)
            .Columns.Add("Status Description", 300, HorizontalAlignment.Left)
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                        .subitems.add(myreader.Item(2)) 'jobid
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Label1.Text = "Status"
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(230, 6)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        getJobrankID() = Me.LvlJobRank.SelectedItems(0).SubItems(2).Text
        Dim x As New DialogResult
        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        If x = System.Windows.Forms.DialogResult.OK Then
            Call DeleteStatus(jobrankID)
            Call Status()
        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then

        End If
    End Sub
#Region "Delete Status"
    Private Sub DeleteStatus(ByVal statid As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Membership_Status_Delete", _
                                      New SqlParameter("@pk_MembershipStatus", statid))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "DeleteStatus")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
    Private Sub Job_Rank_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call Status()
    End Sub

End Class