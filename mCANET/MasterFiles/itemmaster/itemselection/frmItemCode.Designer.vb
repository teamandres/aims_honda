<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmItemCode
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmItemCode))
        Me.txtCode = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtdescription = New System.Windows.Forms.TextBox
        Me.btn_item_cancel = New System.Windows.Forms.Button
        Me.btn_dep_saveClose = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtCode
        '
        Me.txtCode.Location = New System.Drawing.Point(97, 8)
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(252, 21)
        Me.txtCode.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Item Code"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Description"
        '
        'txtdescription
        '
        Me.txtdescription.Location = New System.Drawing.Point(15, 58)
        Me.txtdescription.Multiline = True
        Me.txtdescription.Name = "txtdescription"
        Me.txtdescription.Size = New System.Drawing.Size(334, 40)
        Me.txtdescription.TabIndex = 3
        '
        'btn_item_cancel
        '
        Me.btn_item_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_item_cancel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_item_cancel.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btn_item_cancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_item_cancel.Location = New System.Drawing.Point(265, 104)
        Me.btn_item_cancel.Name = "btn_item_cancel"
        Me.btn_item_cancel.Size = New System.Drawing.Size(84, 28)
        Me.btn_item_cancel.TabIndex = 30
        Me.btn_item_cancel.Text = "Cancel"
        Me.btn_item_cancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_item_cancel.UseVisualStyleBackColor = True
        '
        'btn_dep_saveClose
        '
        Me.btn_dep_saveClose.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_dep_saveClose.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btn_dep_saveClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_dep_saveClose.Location = New System.Drawing.Point(175, 104)
        Me.btn_dep_saveClose.Name = "btn_dep_saveClose"
        Me.btn_dep_saveClose.Size = New System.Drawing.Size(84, 28)
        Me.btn_dep_saveClose.TabIndex = 29
        Me.btn_dep_saveClose.Text = "OK"
        Me.btn_dep_saveClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_dep_saveClose.UseVisualStyleBackColor = True
        '
        'frmItemCode
        '
        Me.AcceptButton = Me.btn_dep_saveClose
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btn_item_cancel
        Me.ClientSize = New System.Drawing.Size(354, 134)
        Me.Controls.Add(Me.btn_item_cancel)
        Me.Controls.Add(Me.btn_dep_saveClose)
        Me.Controls.Add(Me.txtdescription)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtCode)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmItemCode"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Item Code"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtCode As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtdescription As System.Windows.Forms.TextBox
    Friend WithEvents btn_item_cancel As System.Windows.Forms.Button
    Friend WithEvents btn_dep_saveClose As System.Windows.Forms.Button
End Class
