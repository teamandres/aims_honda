<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_item_masterItemMeasurement
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cboUnitTypeID = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboUnitID = New System.Windows.Forms.ComboBox
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.lblAbbrevUnit = New System.Windows.Forms.Label
        Me.cboUnitTypeName = New System.Windows.Forms.ComboBox
        Me.cboUnitName = New System.Windows.Forms.ComboBox
        Me.SuspendLayout()
        '
        'cboUnitTypeID
        '
        Me.cboUnitTypeID.FormattingEnabled = True
        Me.cboUnitTypeID.Location = New System.Drawing.Point(19, 36)
        Me.cboUnitTypeID.Name = "cboUnitTypeID"
        Me.cboUnitTypeID.Size = New System.Drawing.Size(184, 21)
        Me.cboUnitTypeID.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(109, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Unit of Measure Type"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 137)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Unit of Measure"
        '
        'cboUnitID
        '
        Me.cboUnitID.FormattingEnabled = True
        Me.cboUnitID.Location = New System.Drawing.Point(19, 153)
        Me.cboUnitID.Name = "cboUnitID"
        Me.cboUnitID.Size = New System.Drawing.Size(184, 21)
        Me.cboUnitID.TabIndex = 2
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(107, 193)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 4
        Me.btnOK.Text = "Ok"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(188, 193)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescription.Location = New System.Drawing.Point(19, 63)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ReadOnly = True
        Me.txtDescription.Size = New System.Drawing.Size(244, 59)
        Me.txtDescription.TabIndex = 6
        '
        'lblAbbrevUnit
        '
        Me.lblAbbrevUnit.AutoSize = True
        Me.lblAbbrevUnit.Location = New System.Drawing.Point(209, 156)
        Me.lblAbbrevUnit.Name = "lblAbbrevUnit"
        Me.lblAbbrevUnit.Size = New System.Drawing.Size(0, 13)
        Me.lblAbbrevUnit.TabIndex = 8
        Me.lblAbbrevUnit.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboUnitTypeName
        '
        Me.cboUnitTypeName.FormattingEnabled = True
        Me.cboUnitTypeName.Location = New System.Drawing.Point(19, 36)
        Me.cboUnitTypeName.Name = "cboUnitTypeName"
        Me.cboUnitTypeName.Size = New System.Drawing.Size(244, 21)
        Me.cboUnitTypeName.TabIndex = 9
        '
        'cboUnitName
        '
        Me.cboUnitName.FormattingEnabled = True
        Me.cboUnitName.Location = New System.Drawing.Point(19, 153)
        Me.cboUnitName.Name = "cboUnitName"
        Me.cboUnitName.Size = New System.Drawing.Size(244, 21)
        Me.cboUnitName.TabIndex = 10
        '
        'frm_item_masterItemMeasurement
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(287, 227)
        Me.Controls.Add(Me.cboUnitName)
        Me.Controls.Add(Me.cboUnitTypeName)
        Me.Controls.Add(Me.lblAbbrevUnit)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboUnitID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboUnitTypeID)
        Me.Name = "frm_item_masterItemMeasurement"
        Me.Text = "Unit of Measure"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboUnitTypeID As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboUnitID As System.Windows.Forms.ComboBox
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents lblAbbrevUnit As System.Windows.Forms.Label
    Friend WithEvents cboUnitTypeName As System.Windows.Forms.ComboBox
    Friend WithEvents cboUnitName As System.Windows.Forms.ComboBox
End Class
