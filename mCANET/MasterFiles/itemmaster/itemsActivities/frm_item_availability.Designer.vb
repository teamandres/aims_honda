<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_item_availability
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboItem = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblUnitAvailable = New System.Windows.Forms.Label
        Me.lblUnitAssemblies = New System.Windows.Forms.Label
        Me.lblUnitSO = New System.Windows.Forms.Label
        Me.lblUnitOnHand = New System.Windows.Forms.Label
        Me.txtQuantity = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtReserved = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtOnSO = New System.Windows.Forms.TextBox
        Me.txtOnHand = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lblUnitPO = New System.Windows.Forms.Label
        Me.txtPending = New System.Windows.Forms.TextBox
        Me.txtOnPO = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.cboDetails = New System.Windows.Forms.ComboBox
        Me.grdDetails = New System.Windows.Forms.DataGridView
        Me.btnClose = New System.Windows.Forms.Button
        Me.lblUnit = New System.Windows.Forms.Label
        Me.btnShow = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.grdDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Item Name"
        '
        'cboItem
        '
        Me.cboItem.FormattingEnabled = True
        Me.cboItem.Location = New System.Drawing.Point(95, 12)
        Me.cboItem.Name = "cboItem"
        Me.cboItem.Size = New System.Drawing.Size(214, 21)
        Me.cboItem.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Description"
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.SystemColors.Control
        Me.txtDescription.Location = New System.Drawing.Point(95, 41)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(529, 42)
        Me.txtDescription.TabIndex = 3
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblUnitAvailable)
        Me.GroupBox1.Controls.Add(Me.lblUnitAssemblies)
        Me.GroupBox1.Controls.Add(Me.lblUnitSO)
        Me.GroupBox1.Controls.Add(Me.lblUnitOnHand)
        Me.GroupBox1.Controls.Add(Me.txtQuantity)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtReserved)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtOnSO)
        Me.GroupBox1.Controls.Add(Me.txtOnHand)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 90)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(303, 140)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Quantity Available"
        '
        'lblUnitAvailable
        '
        Me.lblUnitAvailable.AutoSize = True
        Me.lblUnitAvailable.Location = New System.Drawing.Point(264, 114)
        Me.lblUnitAvailable.Name = "lblUnitAvailable"
        Me.lblUnitAvailable.Size = New System.Drawing.Size(26, 13)
        Me.lblUnitAvailable.TabIndex = 18
        Me.lblUnitAvailable.Text = "Unit"
        Me.lblUnitAvailable.Visible = False
        '
        'lblUnitAssemblies
        '
        Me.lblUnitAssemblies.AutoSize = True
        Me.lblUnitAssemblies.Location = New System.Drawing.Point(264, 82)
        Me.lblUnitAssemblies.Name = "lblUnitAssemblies"
        Me.lblUnitAssemblies.Size = New System.Drawing.Size(26, 13)
        Me.lblUnitAssemblies.TabIndex = 20
        Me.lblUnitAssemblies.Text = "Unit"
        Me.lblUnitAssemblies.Visible = False
        '
        'lblUnitSO
        '
        Me.lblUnitSO.AutoSize = True
        Me.lblUnitSO.Location = New System.Drawing.Point(264, 56)
        Me.lblUnitSO.Name = "lblUnitSO"
        Me.lblUnitSO.Size = New System.Drawing.Size(26, 13)
        Me.lblUnitSO.TabIndex = 19
        Me.lblUnitSO.Text = "Unit"
        Me.lblUnitSO.Visible = False
        '
        'lblUnitOnHand
        '
        Me.lblUnitOnHand.AutoSize = True
        Me.lblUnitOnHand.Location = New System.Drawing.Point(264, 31)
        Me.lblUnitOnHand.Name = "lblUnitOnHand"
        Me.lblUnitOnHand.Size = New System.Drawing.Size(26, 13)
        Me.lblUnitOnHand.TabIndex = 18
        Me.lblUnitOnHand.Text = "Unit"
        Me.lblUnitOnHand.Visible = False
        '
        'txtQuantity
        '
        Me.txtQuantity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQuantity.Location = New System.Drawing.Point(177, 111)
        Me.txtQuantity.Name = "txtQuantity"
        Me.txtQuantity.Size = New System.Drawing.Size(81, 21)
        Me.txtQuantity.TabIndex = 12
        Me.txtQuantity.Text = "0.00"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 114)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(111, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Quantity Available"
        '
        'txtReserved
        '
        Me.txtReserved.Enabled = False
        Me.txtReserved.Location = New System.Drawing.Point(177, 79)
        Me.txtReserved.Name = "txtReserved"
        Me.txtReserved.Size = New System.Drawing.Size(81, 21)
        Me.txtReserved.TabIndex = 10
        Me.txtReserved.Text = "0.00"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Enabled = False
        Me.Label5.Location = New System.Drawing.Point(3, 82)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(170, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Quantity Reserved for Assemblies"
        '
        'txtOnSO
        '
        Me.txtOnSO.Location = New System.Drawing.Point(177, 53)
        Me.txtOnSO.Name = "txtOnSO"
        Me.txtOnSO.Size = New System.Drawing.Size(81, 21)
        Me.txtOnSO.TabIndex = 8
        Me.txtOnSO.Text = "0.00"
        '
        'txtOnHand
        '
        Me.txtOnHand.Location = New System.Drawing.Point(177, 28)
        Me.txtOnHand.Name = "txtOnHand"
        Me.txtOnHand.Size = New System.Drawing.Size(81, 21)
        Me.txtOnHand.TabIndex = 7
        Me.txtOnHand.Text = "0.00"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(123, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Quantity on Sales Order"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 31)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Quantity on Hand"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblUnitPO)
        Me.GroupBox2.Controls.Add(Me.txtPending)
        Me.GroupBox2.Controls.Add(Me.txtOnPO)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Location = New System.Drawing.Point(321, 90)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(303, 140)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Incoming"
        '
        'lblUnitPO
        '
        Me.lblUnitPO.AutoSize = True
        Me.lblUnitPO.Location = New System.Drawing.Point(255, 31)
        Me.lblUnitPO.Name = "lblUnitPO"
        Me.lblUnitPO.Size = New System.Drawing.Size(26, 13)
        Me.lblUnitPO.TabIndex = 18
        Me.lblUnitPO.Text = "Unit"
        Me.lblUnitPO.Visible = False
        '
        'txtPending
        '
        Me.txtPending.Location = New System.Drawing.Point(168, 53)
        Me.txtPending.Name = "txtPending"
        Me.txtPending.Size = New System.Drawing.Size(81, 21)
        Me.txtPending.TabIndex = 8
        Me.txtPending.Text = "0.00"
        Me.txtPending.Visible = False
        '
        'txtOnPO
        '
        Me.txtOnPO.Location = New System.Drawing.Point(168, 28)
        Me.txtOnPO.Name = "txtOnPO"
        Me.txtOnPO.Size = New System.Drawing.Size(81, 21)
        Me.txtOnPO.TabIndex = 7
        Me.txtOnPO.Text = "0.00"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(3, 56)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(135, 13)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Quantity on Pending Builds"
        Me.Label9.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(3, 31)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(147, 13)
        Me.Label10.TabIndex = 5
        Me.Label10.Text = "Quantity on Purchase Orders"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(9, 275)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(85, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Show Details for"
        '
        'cboDetails
        '
        Me.cboDetails.FormattingEnabled = True
        Me.cboDetails.Location = New System.Drawing.Point(95, 272)
        Me.cboDetails.Name = "cboDetails"
        Me.cboDetails.Size = New System.Drawing.Size(214, 21)
        Me.cboDetails.TabIndex = 14
        '
        'grdDetails
        '
        Me.grdDetails.AllowUserToAddRows = False
        Me.grdDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdDetails.Location = New System.Drawing.Point(12, 299)
        Me.grdDetails.Name = "grdDetails"
        Me.grdDetails.Size = New System.Drawing.Size(612, 150)
        Me.grdDetails.TabIndex = 15
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(538, 455)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(85, 23)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblUnit
        '
        Me.lblUnit.AutoSize = True
        Me.lblUnit.Location = New System.Drawing.Point(315, 15)
        Me.lblUnit.Name = "lblUnit"
        Me.lblUnit.Size = New System.Drawing.Size(26, 13)
        Me.lblUnit.TabIndex = 17
        Me.lblUnit.Text = "Unit"
        Me.lblUnit.Visible = False
        '
        'btnShow
        '
        Me.btnShow.Location = New System.Drawing.Point(12, 236)
        Me.btnShow.Name = "btnShow"
        Me.btnShow.Size = New System.Drawing.Size(158, 23)
        Me.btnShow.TabIndex = 18
        Me.btnShow.Text = "Show Details >>"
        Me.btnShow.UseVisualStyleBackColor = True
        '
        'frm_item_availability
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(635, 484)
        Me.Controls.Add(Me.btnShow)
        Me.Controls.Add(Me.lblUnit)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.grdDetails)
        Me.Controls.Add(Me.cboDetails)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboItem)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frm_item_availability"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Current Availability"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.grdDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboItem As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtOnSO As System.Windows.Forms.TextBox
    Friend WithEvents txtOnHand As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtQuantity As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtReserved As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPending As System.Windows.Forms.TextBox
    Friend WithEvents txtOnPO As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cboDetails As System.Windows.Forms.ComboBox
    Friend WithEvents grdDetails As System.Windows.Forms.DataGridView
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents lblUnit As System.Windows.Forms.Label
    Friend WithEvents lblUnitAvailable As System.Windows.Forms.Label
    Friend WithEvents lblUnitAssemblies As System.Windows.Forms.Label
    Friend WithEvents lblUnitSO As System.Windows.Forms.Label
    Friend WithEvents lblUnitOnHand As System.Windows.Forms.Label
    Friend WithEvents lblUnitPO As System.Windows.Forms.Label
    Friend WithEvents btnShow As System.Windows.Forms.Button
End Class
