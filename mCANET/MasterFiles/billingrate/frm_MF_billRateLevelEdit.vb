Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frm_MF_billRateLevelEdit
    Private gCon As New Clsappconfiguration
    Private sKeyBillRate As String
    Private sHourlyRate As Decimal
    Public Property HourlyRate() As String
        Get
            Return sHourlyRate
        End Get
        Set(ByVal value As String)
            sHourlyRate = value
        End Set
    End Property
    Public Property KeyBillRate() As String
        Get
            Return sKeyBillRate
        End Get
        Set(ByVal value As String)
            sKeyBillRate = value
        End Set
    End Property
    Private Sub loadBillRate()
        Dim sSQLCmd As String = "SELECT * FROM mBillRateLevel "
        sSQLCmd &= "WHERE fxKeyBillRate = '" & sKeyBillRate & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtBillRateName.Text = rd.Item("fcBillRateName")
                    txtHourlyRate.Text = Decimal.Parse(rd.Item("fdHourlyRate"))
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Bill Rate")
        End Try
    End Sub

    Private Sub frm_MF_billRateLevelEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If sHourlyRate = 0 Then
            loadBillRate()
        Else
            txtBillRateName.Text = ""
            txtHourlyRate.Text = sHourlyRate
        End If
    End Sub
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If sHourlyRate = 0 Then
            updateBillRateLevel()
        Else
            addBillRateLevelDuplicate()
        End If

        With frm_acc_billRateLevel
            m_loadBillingRate(.grd_BillRateLev)
        End With
        Me.Close()
    End Sub
    Private Sub addBillRateLevelDuplicate()
        Dim sSQL_CommandText As String = "usp_m_BillRateLevel_Insert "
        sSQL_CommandText &= "@fxKeyBillRate='" & Guid.NewGuid.ToString & "'"
        sSQL_CommandText &= ",@fcBillRateName='" & txtBillRateName.Text & "'"
        sSQL_CommandText &= ",@fbFixedRate='" & 1 & "'"
        sSQL_CommandText &= ",@fdHourlyRate='" & txtHourlyRate.Text & "'"
        sSQL_CommandText &= ",@fxKeyCompany='" & gCompanyID() & "'"

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, Data.CommandType.Text, sSQL_CommandText)
            MessageBox.Show("Bill Rate Level successfully updated", "Duplicate Bill Rate Level ")
        Catch
            MessageBox.Show(Err.Description, "Duplicate Bill Rate Level")
        End Try
    End Sub

    Private Sub updateBillRateLevel()
        Dim sSQLCmd As String = "usp_m_BillRateLevel_Update "
        sSQLCmd &= "@fxKeyBillRate = '" & sKeyBillRate & "' "
        sSQLCmd &= ",@fcBillRateName = '" & txtBillRateName.Text & "'"
        sSQLCmd &= ",@fdHourlyRate = '" & txtHourlyRate.Text & "'"

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully updated", "Edit Bill Rate Level")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Edit Bill Rate Level")
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class