Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_MF_customerTypeAddEdit
    Private gCon As New Clsappconfiguration
    Private sKeyCustomerType As String

    Public Property KeyCustomerType() As String
        Get
            Return Me.sKeyCustomerType
        End Get
        Set(ByVal value As String)
            sKeyCustomerType = value
        End Set
    End Property

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        updateCustomerType()
        m_customerTypeList(frm_acc_customerType.chkIncludeInactive.CheckState, frm_acc_customerType.grdCustomerType)
        m_loadCustomerType(frm_cust_masterCustomerAddEdit.cboTypeName, frm_cust_masterCustomerAddEdit.cboTypeID)
        m_chkSettings(frm_acc_customerType.chkIncludeInactive, frm_acc_customerType.SQLQuery)
    End Sub

    Private Sub updateCustomerType()
        Dim sSQLCmd As String = "usp_m_customerType_update "
        sSQLCmd &= "@fxKeyCustomerType='" & sKeyCustomerType & "',"
        sSQLCmd &= "@fcTypeName='" & txtCustomerType.Text & "',"
        sSQLCmd &= "@fbActive='" & IIf(chkInactive.Checked = True, 0, 1) & "'"
        sSQLCmd &= ",@fxKeyCompany = '" & gCompanyID() & "'"

        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Customer Type successfully updated!")
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Supplier Type")
        End Try
    End Sub

    Private Sub frm_MF_customerTypeAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadType()
    End Sub

    Private Sub loadType()

        Dim sSQLCmd As String = "SELECT fxKeyCustomerType,fcTypeName,fbActive FROM mCustomer01Type WHERE fxKeyCustomerType = '" & sKeyCustomerType & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtCustomerType.Text = rd.Item("fcTypeName")
                    chkInactive.Checked = IIf(rd.Item("fbActive") = True, False, True)
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Customer Type Master File")
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

End Class