Imports Microsoft.ApplicationBlocks.Data

Public Class frm_acc_customerType
    Private sKeyCustomerType As String
    Private gCon As New Clsappconfiguration
    Private sSQLCmdQuery As String = "SELECT * FROM mCustomer01Type WHERE fbActive <> 1 "

    Public Property SQLQuery() As String
        Get
            Return sSQLCmdQuery
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Private Sub ts_New_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_New.Click
        frm_MF_customerTypeAddEdit.Text = "Add Customer Type"
        frm_MF_customerTypeAddEdit.KeyCustomerType = Guid.NewGuid.ToString
        frm_MF_customerTypeAddEdit.MdiParent = Me.MdiParent
        frm_MF_customerTypeAddEdit.Show()
    End Sub

    Private Sub ts_Edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Edit.Click
        frm_MF_customerTypeAddEdit.Text = "Edit Customer Type"
        frm_MF_customerTypeAddEdit.KeyCustomerType = sKeyCustomerType
        frm_MF_customerTypeAddEdit.MdiParent = Me.MdiParent
        frm_MF_customerTypeAddEdit.Show()
    End Sub

    Private Sub frm_acc_customerType_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        refreshForm()
    End Sub

    Private Sub refreshForm()
        m_customerTypeList(chkIncludeInactive.CheckState, grdCustomerType)
        gridSettings()
        m_chkSettings(chkIncludeInactive, sSQLCmdQuery)
        ts_ShowInactive.Enabled = chkIncludeInactive.Enabled
    End Sub

    Public Sub gridSettings()
        With grdCustomerType
            .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            .Columns(0).Visible = False
            .Columns(1).Visible = False
            .Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
            .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns.Item(1).HeaderText = "Active"
            .Columns.Item(2).HeaderText = "Customer Name"
        End With

        If chkIncludeInactive.CheckState = CheckState.Checked Then
            grdCustomerType.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdCustomerType.Columns(1).Visible = True
        End If
    End Sub

    Private Sub grdCustomerType_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdCustomerType.CellClick
        If Me.grdCustomerType.SelectedCells.Count > 0 Then
            sKeyCustomerType = grdCustomerType.CurrentRow.Cells(0).Value.ToString

            If m_isActive("mCustomer01Type", "fxKeyCustomerType", sKeyCustomerType) Then
                ts_MkInactive.Text = "Make Inactive"
            Else
                ts_MkInactive.Text = "Make Active"
            End If
        End If
    End Sub

    Private Sub grdCustomerType_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdCustomerType.DoubleClick
        If Me.grdCustomerType.SelectedCells.Count > 0  Then
            sKeyCustomerType = grdCustomerType.CurrentRow.Cells(0).Value.ToString

            frm_MF_customerTypeAddEdit.MdiParent = Me.MdiParent
            frm_MF_customerTypeAddEdit.Text = "Edit Supplier Type"
            frm_MF_customerTypeAddEdit.KeyCustomerType = sKeyCustomerType
            frm_MF_customerTypeAddEdit.Show()
        End If
    End Sub

    Private Sub chkIncludeInactive_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.CheckStateChanged
        m_customerTypeList(chkIncludeInactive.CheckState, grdCustomerType)
        gridSettings()
    End Sub

    Private Sub customerTypeDelete()
        If MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Dim sSQLCmd As String = "DELETE FROM mCustomer01Type WHERE fxKeyCustomerType= '" & sKeyCustomerType & "'"
            Try
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                MessageBox.Show("Record successfully deleted.")
            Catch ex As Exception
                MessageBox.Show(Err.Description)
            End Try
        End If
    End Sub

    Private Sub ts_Delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Delete.Click
        customerTypeDelete()
        refreshForm()
    End Sub

    Private Sub chkIncludeInactive_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.EnabledChanged
        ts_showInactive.Enabled = chkIncludeInactive.Enabled
    End Sub

    Private Sub ts_showInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_ShowInactive.Click
        chkIncludeInactive.Checked = True
    End Sub

    Private Sub ts_MkInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_MkInactive.Click
        If ts_MkInactive.Text = "Make Active" Then
            m_mkActive("mCustomer01Type", "fxKeyCustomerType", sKeyCustomerType, True)
        Else
            m_mkActive("mCustomer01Type", "fxKeyCustomerType", sKeyCustomerType, False)
        End If
        refreshForm()
    End Sub
End Class