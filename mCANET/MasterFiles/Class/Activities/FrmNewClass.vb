Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: July 07, 2009                      ###
'                   ### Website: www.ionflux.site50.net                  ###
'                   ########################################################

Public Class FrmNewClass
#Region "Declaration"
    Inherits System.Windows.Forms.Form
    Private gcon As New Clsappconfiguration
    Dim NewRefno As String
#End Region
#Region "Functions"
    Private Function getNewRefno()
        Dim sSqlCmd As String = "select right(newid(),9)"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    Return rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Function getLevel(ByVal RefNo As String)
        Dim xLevel As String
        Dim sSqlCmd As String = "SELECT classLevel FROM dbo.mClasses WHERE ClassRefnumber='" & _
                                RefNo & "' AND fxKeyCompany ='" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    xLevel = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception
        End Try
        If xLevel = "" Or xLevel Is Nothing Then
            xLevel = 1
        Else
            xLevel += 1
        End If
        Return xLevel
    End Function
    Private Function Getparent(ByVal refno As String)
        Dim sSqlCmd As String = "SELECT classname FROM dbo.mClasses WHERE ClassRefnumber ='" & _
                                refno & "' AND fxKeyCompany ='" & gCompanyID() & "'"
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
            Try
                While rd.Read
                    Return rd.Item(0).ToString
                End While
            Catch ex As Exception
                Return Nothing
            End Try
        End Using
    End Function
#End Region
    Private Sub BtnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOK.Click
        NewRefno = getNewRefno()
        If txtParentName.Text <> "" And TxtClassName.Text <> "" And TxtClassRefno.Text <> "" Then
            Dim sSqlCmd As String = "INSERT INTO dbo.mClasses"
            sSqlCmd &= " (ClassRefnumber, fxKeyCompany, classname, classLevel, ClassParentPath, ClassDescription)"
            sSqlCmd &= " VALUES ('"
            sSqlCmd &= NewRefno & "', '"
            sSqlCmd &= gCompanyID() & "', '"
            sSqlCmd &= TxtClassName.Text & "���" & NewRefno & "', '"
            sSqlCmd &= getLevel(TxtClassRefno.Text) & "', '"
            sSqlCmd &= Getparent(TxtClassRefno.Text) & "', '"
            sSqlCmd &= txtDescription.Text & "')"
            Try
                SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, sSqlCmd)
                Me.Close()
            Catch ex As Exception

            End Try
        Else
            MsgBox("Invalid data!", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access Denied")
        End If
    End Sub
    Private Sub txtParentName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtParentName.TextChanged
        If txtParentName.Text <> "" And TxtClassName.Text <> "" And TxtClassRefno.Text <> "" Then
            BtnOK.Enabled = True
        Else
            BtnOK.Enabled = False
        End If
    End Sub
    Private Sub BtnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBrowse.Click
        FrmBrowseClass.Mode = "ADD MODE"
        FrmBrowseClass.ShowDialog()
        txtParentName.Text = FrmBrowseClass.Data1
        TxtClassRefno.Text = FrmBrowseClass.Data2
        NewRefno = getNewRefno()
        If txtParentName.Text <> "" And TxtClassName.Text <> "" And TxtClassRefno.Text <> "" Then
            BtnOK.Enabled = True
        Else
            BtnOK.Enabled = False
        End If
    End Sub
    Private Sub TxtClassName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TxtClassName.TextChanged
        If txtParentName.Text <> "" And TxtClassName.Text <> "" And TxtClassRefno.Text <> "" Then
            BtnOK.Enabled = True
        Else
            BtnOK.Enabled = False
        End If
    End Sub
    Private Sub FrmNewClass_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        TxtClassName.Text = ""
        TxtClassRefno.Text = ""
        txtParentName.Text = ""
        txtDescription.Text = ""
    End Sub

End Class