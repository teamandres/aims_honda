Public Class frm_acc_openingbalance

#Region "Declarations"

    Private gMaster As New modMasterFile

#End Region

#Region "Events"

    Private Sub frm_acc_openingbalance_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call gVerifyAccountType()
    End Sub

    Private Sub btnOka_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOka.Click
        gEndingdate = Microsoft.VisualBasic.FormatDateTime(dtasOfdate.Value, DateFormat.ShortDate)
        gEnterEndingBalance = 1
        Me.txtOpeningBal.Text = "0.00"
        Me.dtasOfdate.Value = Now.Date
        Me.Close()
    End Sub

    Private Sub btnCancela_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancela.Click
        txtOpeningBal.Text = "0.00"
        dtasOfdate.Value = Now.Date
        gEnterEndingBalance = 0
        Me.Close()
    End Sub

    Private Sub btnbOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnbOK.Click
        gEndingdate = Microsoft.VisualBasic.FormatDateTime(dtEndDate.Value, DateFormat.ShortDate)
        gEnterEndingBalance = 1
        txtendBal.Text = "0.00"
        dtEndDate.Value = Now.Date
        Me.Close()
    End Sub

    Private Sub btnbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnbCancel.Click
        txtendBal.Text = "0.00"
        dtEndDate.Value = Now.Date
        gEnterEndingBalance = 0
        Me.Close()
    End Sub

    Private Sub txtendBal_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtendBal.Validating
        txtendBal.Text = Format(CDec(txtendBal.Text), "##,##0.00")
        gEndingBal = CDec(txtendBal.Text)
    End Sub

    Private Sub txtOpeningBal_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles txtOpeningBal.Validating
        txtOpeningBal.Text = Format(CDec(txtOpeningBal.Text), "##,##0.00")
        gEndingBal = CDec(txtOpeningBal.Text)
    End Sub

#End Region

#Region "Functions/Procedures"

    Private Sub gVerifyAccountType()
        If InStr(gAcntType, "BANK", CompareMethod.Text) > 0 Or _
              InStr(gAcntType, "CREDIT", CompareMethod.Text) > 0 Then
            grpAssets.Size = New Size(40, 34)
            grpAssets.Visible = False
            grpBank.Location = New Point(6, 7)
            grpBank.Size = New Size(348, 149)
            grpBank.Visible = True
            txtendBal.Text = Format(CDec(m_GetBalanceAmount(gAccountCode)), "##,##0.00#")

        ElseIf InStr(gAcntType, "ASSET", CompareMethod.Text) > 0 Or _
                   InStr(gAcntType, "EQUITY", CompareMethod.Text) > 0 Or _
                   InStr(gAcntType, "LOAN", CompareMethod.Text) > 0 Or _
                   InStr(gAcntType, "LIABILITIES", CompareMethod.Text) > 0 Or _
                   InStr(gAcntType, "LIABILITY", CompareMethod.Text) > 0 Then
            grpBank.Size = New Size(40, 34)
            grpBank.Visible = False
            grpAssets.Location = New Point(6, 7)
            grpAssets.Size = New Size(348, 149)
            grpAssets.Visible = True
            txtOpeningBal.Text = Format(CDec(m_GetBalanceAmount(gAccountCode)), "##,##0.00#")
        End If
    End Sub

#End Region


   
  
End Class