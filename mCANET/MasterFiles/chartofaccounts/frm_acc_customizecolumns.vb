Public Class frm_acc_customizecolumns

    Private Sub btncancelcols_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncancelcols.Click
        Me.Close()
    End Sub

    Private Sub btnChoseOne_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChoseOne.Click
        lstChosenCols.Items.Add(lstAvailableCols.SelectedItem)
        lstAvailableCols.Items.Remove(lstAvailableCols.SelectedItem)
    End Sub

    Private Sub btnChoseAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChoseAll.Click
        Dim xCnt As Integer
        For xCnt = 0 To lstAvailableCols.Items.Count - 1
            lstChosenCols.Items.Add(lstAvailableCols.Items(xCnt))
        Next
        lstAvailableCols.Items.Clear()
    End Sub

    Private Sub btnRestoreAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRestoreAll.Click
        Dim xCnt As Integer
        For xCnt = 0 To lstChosenCols.Items.Count - 1
            lstAvailableCols.Items.Add(lstChosenCols.Items(xCnt))
        Next
        lstChosenCols.Items.Clear()
    End Sub

    Private Sub btnRestoreOne_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRestoreOne.Click
        lstAvailableCols.Items.Add(lstChosenCols.SelectedItem)
        lstChosenCols.Items.Remove(lstChosenCols.SelectedItem)
    End Sub

    Private Sub btnOkcols_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOkcols.Click
        If lstChosenCols.Items.Count = 0 Then
            Call defaultValue()
        Else
            Call pLoadAccount(0)
        End If
        Me.Close()
    End Sub

    Private Sub btndefaultcols_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndefaultcols.Click
        Call defaultValue()
    End Sub

    Private Sub defaultValue()
        lstChosenCols.Items.Clear()
        lstAvailableCols.Items.Clear()
        With lstAvailableCols
            .Items.Add("Active")
            .Items.Add("Account")
            .Items.Add("Account Type")
            .Items.Add("Account Code")
            .Items.Add("Balance Total")
            .Items.Add("Budget")
            .Items.Add("Budget Beginning Date")
            .Items.Add("Description")
        End With
        Call pLoadAccount(1)
    End Sub

    Private Sub pLoadAccount(ByVal intVal As Integer)
        Dim xCnt As Integer
        With frm_acc_chartaccounts.grdAccounts
            If intVal = 1 Then
                For xCnt = 1 To .Columns.Count - 1
                    .Columns(xCnt).Visible = True
                Next
            Else
                For xCnt = 1 To .Columns.Count - 1
                    .Columns(xCnt).Visible = False
                Next
                For xCnt = 0 To Me.lstChosenCols.Items.Count - 1
                    Select Case Me.lstChosenCols.Items(xCnt).ToString
                        Case "Active"
                            .Columns("Active").Visible = True
                        Case "Account"
                            .Columns("Account").Visible = True
                        Case "Account Type"
                            .Columns("Account Type").Visible = True
                        Case "Account Code"
                            .Columns("Account Code").Visible = True
                        Case "Balance Total"
                            .Columns("Balance Total").Visible = True
                        Case "Balance"
                            .Columns("Balance").Visible = True
                        Case "Budget"
                            .Columns("Budget").Visible = True
                        Case "Budget Beginning Date"
                            .Columns("Budget Beginning Date").Visible = True
                        Case "Description"
                            .Columns("Description").Visible = True
                    End Select
                Next
            End If
        End With
    End Sub

   
End Class