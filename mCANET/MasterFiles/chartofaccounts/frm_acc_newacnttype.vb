Public Class frm_acc_newacnttype

    Private gTrans As New clsTransactionFunctions
    Private gMaster As New modMasterFile

    Private Sub frm_acc_newacnttype_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtAcntType.Focus()
        gTrans.gCreateTextbox(Me, 122)
        gTrans.gClearFormTxt(Me)
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If m_segmentvalidation(gTrans.getSegment(Me, 10)) <> "exist" Then
            gMaster.gSaveAcntType(txtAcntType.Text, txtDescription.Text, _
                                gTrans.getSegment(Me, 10), Me)
        Else
            MessageBox.Show("Account Code exist, please re-define your account code", "CLICKSOFTWARE:" + Me.Text)
        End If

    End Sub

End Class