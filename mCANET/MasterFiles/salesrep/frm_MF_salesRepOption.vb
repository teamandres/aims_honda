Public Class frm_MF_salesRepOption

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If rbtnSupplier.Checked = True Then
            frm_vend_masterVendorAddEdit.KeyAddress = Guid.NewGuid.ToString
            frm_vend_masterVendorAddEdit.KeySupplier = Guid.NewGuid.ToString
            frm_vend_masterVendorAddEdit.Text = "Add Supplier"
            frm_vend_masterVendorAddEdit.Show()
            frm_MF_salesRepAddEdit.txtType.Text = "Supplier"
        ElseIf rbtnCustomer.Checked = True Then
            frm_cust_masterCustomerAddEdit.KeyAddress = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddEdit.KeyCustomer = Guid.NewGuid.ToString
            frm_cust_masterCustomerAddEdit.IsNew = True
            frm_cust_masterCustomerAddEdit.Text = "Add Customer"
            frm_cust_masterCustomerAddEdit.Show()
            frm_MF_salesRepAddEdit.txtType.Text = "Customer"
        Else
            frm_MF_otherNameMasterAddEdit.KeyAddress = Guid.NewGuid.ToString
            frm_MF_otherNameMasterAddEdit.KeyOtherName = Guid.NewGuid.ToString
            frm_MF_otherNameMasterAddEdit.Text = "Add New Name"
            frm_MF_otherNameMasterAddEdit.Show()
            frm_MF_salesRepAddEdit.txtType.Text = "Other"
        End If
        Me.Close()
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frm_MF_salesRepOption_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rbtnSupplier.Checked = True
    End Sub
End Class