<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChartOfAccounts
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmChartOfAccounts))
        Me.PanelGridContainer = New System.Windows.Forms.Panel()
        Me.gridChartOfAccounts = New System.Windows.Forms.DataGridView()
        Me.ContextMenuStripGrid = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.NewAccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditAccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteAccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PanelMenuContainer = New System.Windows.Forms.Panel()
        Me.lblFind = New System.Windows.Forms.Label()
        Me.txtFind = New System.Windows.Forms.TextBox()
        Me.MenuChartOfAccounts = New System.Windows.Forms.MenuStrip()
        Me.AccountsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteAccountsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TimZoomer = New System.Windows.Forms.Timer(Me.components)
        Me.PanelGridContainer.SuspendLayout()
        CType(Me.gridChartOfAccounts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStripGrid.SuspendLayout()
        Me.PanelMenuContainer.SuspendLayout()
        Me.MenuChartOfAccounts.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelGridContainer
        '
        Me.PanelGridContainer.Controls.Add(Me.gridChartOfAccounts)
        Me.PanelGridContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelGridContainer.Location = New System.Drawing.Point(0, 31)
        Me.PanelGridContainer.Name = "PanelGridContainer"
        Me.PanelGridContainer.Size = New System.Drawing.Size(694, 515)
        Me.PanelGridContainer.TabIndex = 0
        '
        'gridChartOfAccounts
        '
        Me.gridChartOfAccounts.AllowUserToAddRows = False
        Me.gridChartOfAccounts.AllowUserToDeleteRows = False
        Me.gridChartOfAccounts.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gridChartOfAccounts.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.gridChartOfAccounts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.gridChartOfAccounts.BackgroundColor = System.Drawing.Color.White
        Me.gridChartOfAccounts.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.gridChartOfAccounts.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.gridChartOfAccounts.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.gridChartOfAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridChartOfAccounts.ContextMenuStrip = Me.ContextMenuStripGrid
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightSkyBlue
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridChartOfAccounts.DefaultCellStyle = DataGridViewCellStyle2
        Me.gridChartOfAccounts.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gridChartOfAccounts.Location = New System.Drawing.Point(0, 0)
        Me.gridChartOfAccounts.MultiSelect = False
        Me.gridChartOfAccounts.Name = "gridChartOfAccounts"
        Me.gridChartOfAccounts.ReadOnly = True
        Me.gridChartOfAccounts.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        Me.gridChartOfAccounts.RowHeadersVisible = False
        Me.gridChartOfAccounts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gridChartOfAccounts.ShowEditingIcon = False
        Me.gridChartOfAccounts.Size = New System.Drawing.Size(694, 515)
        Me.gridChartOfAccounts.TabIndex = 0
        '
        'ContextMenuStripGrid
        '
        Me.ContextMenuStripGrid.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewAccountToolStripMenuItem, Me.EditAccountToolStripMenuItem, Me.DeleteAccountToolStripMenuItem})
        Me.ContextMenuStripGrid.Name = "ContextMenuStripGrid"
        Me.ContextMenuStripGrid.Size = New System.Drawing.Size(156, 70)
        '
        'NewAccountToolStripMenuItem
        '
        Me.NewAccountToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.NewAccountToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.NewAccountToolStripMenuItem.Name = "NewAccountToolStripMenuItem"
        Me.NewAccountToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.NewAccountToolStripMenuItem.Text = "New Account"
        '
        'EditAccountToolStripMenuItem
        '
        Me.EditAccountToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.edit
        Me.EditAccountToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.EditAccountToolStripMenuItem.Name = "EditAccountToolStripMenuItem"
        Me.EditAccountToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.EditAccountToolStripMenuItem.Text = "Edit Account"
        '
        'DeleteAccountToolStripMenuItem
        '
        Me.DeleteAccountToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.DeleteAccountToolStripMenuItem.Name = "DeleteAccountToolStripMenuItem"
        Me.DeleteAccountToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.DeleteAccountToolStripMenuItem.Text = "Delete Account"
        '
        'PanelMenuContainer
        '
        Me.PanelMenuContainer.BackColor = System.Drawing.Color.White
        Me.PanelMenuContainer.BackgroundImage = Global.CSAcctg.My.Resources.Resources.Project_Name_Panel
        Me.PanelMenuContainer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PanelMenuContainer.Controls.Add(Me.lblFind)
        Me.PanelMenuContainer.Controls.Add(Me.txtFind)
        Me.PanelMenuContainer.Controls.Add(Me.MenuChartOfAccounts)
        Me.PanelMenuContainer.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelMenuContainer.Location = New System.Drawing.Point(0, 0)
        Me.PanelMenuContainer.Name = "PanelMenuContainer"
        Me.PanelMenuContainer.Size = New System.Drawing.Size(694, 31)
        Me.PanelMenuContainer.TabIndex = 1
        '
        'lblFind
        '
        Me.lblFind.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFind.AutoSize = True
        Me.lblFind.BackColor = System.Drawing.Color.Transparent
        Me.lblFind.Location = New System.Drawing.Point(493, 5)
        Me.lblFind.Name = "lblFind"
        Me.lblFind.Size = New System.Drawing.Size(31, 15)
        Me.lblFind.TabIndex = 2
        Me.lblFind.Text = "&Find"
        '
        'txtFind
        '
        Me.txtFind.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtFind.Location = New System.Drawing.Point(530, 2)
        Me.txtFind.Name = "txtFind"
        Me.txtFind.Size = New System.Drawing.Size(161, 23)
        Me.txtFind.TabIndex = 3
        '
        'MenuChartOfAccounts
        '
        Me.MenuChartOfAccounts.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.MenuChartOfAccounts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.MenuChartOfAccounts.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuChartOfAccounts.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AccountsToolStripMenuItem, Me.PrintToolStripMenuItem})
        Me.MenuChartOfAccounts.Location = New System.Drawing.Point(0, 0)
        Me.MenuChartOfAccounts.Name = "MenuChartOfAccounts"
        Me.MenuChartOfAccounts.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
        Me.MenuChartOfAccounts.Size = New System.Drawing.Size(694, 24)
        Me.MenuChartOfAccounts.TabIndex = 0
        Me.MenuChartOfAccounts.Text = "MenuStrip1"
        '
        'AccountsToolStripMenuItem
        '
        Me.AccountsToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.AccountsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewToolStripMenuItem, Me.EditToolStripMenuItem, Me.DeleteAccountsToolStripMenuItem})
        Me.AccountsToolStripMenuItem.Name = "AccountsToolStripMenuItem"
        Me.AccountsToolStripMenuItem.ShowShortcutKeys = False
        Me.AccountsToolStripMenuItem.Size = New System.Drawing.Size(69, 20)
        Me.AccountsToolStripMenuItem.Text = "Accounts"
        '
        'NewToolStripMenuItem
        '
        Me.NewToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.add
        Me.NewToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.NewToolStripMenuItem.Name = "NewToolStripMenuItem"
        Me.NewToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.NewToolStripMenuItem.Text = "Create New Account"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.edit
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.EditToolStripMenuItem.Text = "Edit Existing Account"
        '
        'DeleteAccountsToolStripMenuItem
        '
        Me.DeleteAccountsToolStripMenuItem.Image = Global.CSAcctg.My.Resources.Resources.button_cancel2
        Me.DeleteAccountsToolStripMenuItem.Name = "DeleteAccountsToolStripMenuItem"
        Me.DeleteAccountsToolStripMenuItem.Size = New System.Drawing.Size(187, 22)
        Me.DeleteAccountsToolStripMenuItem.Text = "Delete Accounts"
        '
        'PrintToolStripMenuItem
        '
        Me.PrintToolStripMenuItem.Name = "PrintToolStripMenuItem"
        Me.PrintToolStripMenuItem.Size = New System.Drawing.Size(46, 20)
        Me.PrintToolStripMenuItem.Text = "Print"
        '
        'TimZoomer
        '
        Me.TimZoomer.Interval = 7
        '
        'frmChartOfAccounts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.CSAcctg.My.Resources.Resources.background
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(694, 546)
        Me.Controls.Add(Me.PanelGridContainer)
        Me.Controls.Add(Me.PanelMenuContainer)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuChartOfAccounts
        Me.Name = "frmChartOfAccounts"
        Me.Text = "Chart of Accounts"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.PanelGridContainer.ResumeLayout(False)
        CType(Me.gridChartOfAccounts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStripGrid.ResumeLayout(False)
        Me.PanelMenuContainer.ResumeLayout(False)
        Me.PanelMenuContainer.PerformLayout()
        Me.MenuChartOfAccounts.ResumeLayout(False)
        Me.MenuChartOfAccounts.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelGridContainer As System.Windows.Forms.Panel
    Friend WithEvents PanelMenuContainer As System.Windows.Forms.Panel
    Friend WithEvents gridChartOfAccounts As System.Windows.Forms.DataGridView
    Friend WithEvents MenuChartOfAccounts As System.Windows.Forms.MenuStrip
    Friend WithEvents AccountsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteAccountsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStripGrid As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents NewAccountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditAccountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteAccountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtFind As System.Windows.Forms.TextBox
    Friend WithEvents lblFind As System.Windows.Forms.Label
    Friend WithEvents TimZoomer As System.Windows.Forms.Timer
    Friend WithEvents PrintToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
