﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMasterFile_Accounts
    Private Active As Integer
    Public xModule As String

    Public Property GetActive() As Integer
        Get
            Return Active
        End Get
        Set(ByVal value As Integer)
            Active = value
        End Set
    End Property
#Region "DebitCreditLIST"

    Private Sub ACCOUNTREGISTER()
        Dim mycon As New Clsappconfiguration
        Dim keyCompany As New Guid(gCompanyID)
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_DebitCreate_Load",
                                                         New SqlParameter("@filter", txtAccountNo.Text),
                                                         New SqlParameter("@coid", keyCompany))
            GrdAccounts.DataSource = ds.Tables(0)
            GrdAccounts.Columns(0).Visible = False
            GrdAccounts.Columns(1).Width = 100
            GrdAccounts.Columns(1).HeaderText = "Account Code"
            GrdAccounts.Columns(1).ReadOnly = True
            GrdAccounts.Columns(2).HeaderText = "Description"
            GrdAccounts.Columns(2).ReadOnly = True
            GrdAccounts.Columns(2).Width = 270
            GrdAccounts.Columns(3).HeaderText = "Type"
            GrdAccounts.Columns(3).ReadOnly = True
            GrdAccounts.Columns(3).Width = 50
        Catch ex As Exception
            'MessageBox.Show(ex.ToString, "Account List Description", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region
#Region "ACCOUNTLIST"
    Private Sub DEBITCREDIT()
        Dim keyCompany As New Guid(gCompanyID)
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, CommandType.StoredProcedure, "usp_m_mAccountsDS_filter",
                                                     New SqlParameter("@coid", keyCompany),
                                                     New SqlParameter("@acnt_name", txtAccountNo.Text))
        Try
            With GrdAccounts
                .ClearSelection()
                .DataSource = ds.Tables(0)
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).HeaderText = "Account Code and Description"
                .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns(3).Visible = False
                .Columns(4).Visible = False
                .Columns(5).Visible = False
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
            mycon.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "AccountCode", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region

    Private Sub frmMasterFile_Accounts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Select Case xModule
            Case "DEBITCREDIT"
                DEBITCREDIT()
            Case "ACCOUNTREGISTER"
                ACCOUNTREGISTER()
        End Select
    End Sub

    Private Sub GrdAccounts_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GrdAccounts.DoubleClick
        If Active = 1 Then
            If GrdAccounts.SelectedCells.Count <> 0 Then
                For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                    frmMasterFile_DebitCredit.fxid = row.Cells(0).Value.ToString
                    frmMasterFile_DebitCredit.txtaccountcode.Text = row.Cells(1).Value.ToString
                    frmMasterFile_DebitCredit.txtaccountname.Text = row.Cells(2).Value.ToString
                Next
                Me.Close()
            End If
        ElseIf Active = 2 Then
            If GrdAccounts.SelectedCells.Count <> 0 Then
                For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                    frmMasterfile_AccountRegister.GetAccountID = row.Cells(0).Value.ToString
                    frmMasterfile_AccountRegister.txtaccountcode.Text = row.Cells(1).Value.ToString
                    frmMasterfile_AccountRegister.txtaccountname.Text = row.Cells(2).Value.ToString
                Next
                Me.Close()
            End If
        End If
    End Sub

    Private Sub txtAccountNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAccountNo.TextChanged
        Select Case xModule
            Case "DEBITCREDIT"
                DEBITCREDIT()
            Case "ACCOUNTREGISTER"
                ACCOUNTREGISTER()
        End Select
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If xModule = "DEBITCREDIT" Then
            If GrdAccounts.SelectedCells.Count <> 0 Then
                For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                    frmMasterFile_DebitCredit.fxid = row.Cells(3).Value.ToString
                    frmMasterFile_DebitCredit.txtaccountcode.Text = row.Cells(1).Value.ToString
                    frmMasterFile_DebitCredit.txtaccountname.Text = row.Cells(4).Value.ToString
                Next
                Me.Close()
            End If
        ElseIf xModule = "ACCOUNTREGISTER" Then
            If GrdAccounts.SelectedCells.Count <> 0 Then
                For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                    frmMasterfile_AccountRegister.GetAccountID = row.Cells(0).Value.ToString
                    frmMasterfile_AccountRegister.txtaccountcode.Text = row.Cells(1).Value.ToString
                    frmMasterfile_AccountRegister.txtaccountname.Text = row.Cells(2).Value.ToString
                Next
                Me.Close()
            End If
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class