Imports Microsoft.ApplicationBlocks.Data

Public Class frm_acc_otherNameMaster
    Private gCon As New Clsappconfiguration
    Private sKeyOtherName As String
    Private sKeyAddress As String
    Private sSQLCmdQuery As String = "SELECT * FROM mOtherName00Master WHERE fbActive <> 1 "

    Public Property SQLQuery() As String
        Get
            Return sSQLCmdQuery
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Private Sub ts_New_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_New.Click
        'frm_MF_otherNameMasterAddEdit.KeyOtherName = Guid.NewGuid.ToString
        'frm_MF_otherNameMasterAddEdit.KeyAddress = Guid.NewGuid.ToString
        'frm_MF_otherNameMasterAddEdit.Text = "New Name"
        'frm_MF_otherNameMasterAddEdit.Show()
        Dim x As New frm_MF_otherNameMasterAddEdit
        x.Show()
    End Sub

    Private Sub ts_Edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Edit.Click
        frm_MF_otherNameMasterAddEdit.KeyOtherName = sKeyOtherName
        frm_MF_otherNameMasterAddEdit.KeyAddress = sKeyAddress
        frm_MF_otherNameMasterAddEdit.Text = "Edit Name"
        frm_MF_otherNameMasterAddEdit.Show()
    End Sub

    Private Sub frm_acc_otherNameMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       refreshForm
    End Sub

    Private Sub grdSettings()
        Try
            With grdOtherName
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader
                .Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns(4).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns.Item(2).HeaderText = "Active"
                .Columns.Item(3).HeaderText = "Name"
                .Columns.Item(4).HeaderText = "Notes"
                If chkIncludeInactive.CheckState = CheckState.Checked Then
                    .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .Columns(2).Visible = True
                End If
            End With
        Catch
            MessageBox.Show(Err.Description, "Other Name")
        End Try
    End Sub

    Private Sub grdOtherName_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdOtherName.CellClick
        If Me.grdOtherName.SelectedRows.Count > 0 AndAlso _
                          Not Me.grdOtherName.SelectedRows(0).Index = _
                          Me.grdOtherName.Rows.Count - 1 Then
            sKeyOtherName = grdOtherName.CurrentRow.Cells(0).Value.ToString
            sKeyAddress = grdOtherName.CurrentRow.Cells(1).Value.ToString

            If m_isActive("mOtherName00Master", "fxKeyOtherName", sKeyOtherName) Then
                ts_MkInactive.Text = "Make Inactive"
            Else
                ts_MkInactive.Text = "Make Active"
            End If
        End If
    End Sub

    Private Sub grdOtherName_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdOtherName.DoubleClick
        If Me.grdOtherName.SelectedRows.Count > 0 AndAlso _
                           Not Me.grdOtherName.SelectedRows(0).Index = _
                           Me.grdOtherName.Rows.Count - 1 Then
            frm_MF_otherNameMasterAddEdit.MdiParent = Me.MdiParent
            frm_MF_otherNameMasterAddEdit.KeyOtherName = sKeyOtherName
            frm_MF_otherNameMasterAddEdit.KeyAddress = sKeyAddress
            frm_MF_otherNameMasterAddEdit.Text = "Edit Name"
            frm_MF_otherNameMasterAddEdit.Show()
        End If
    End Sub

    Private Sub ts_showInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_showInactive.Click
        chkIncludeInactive.CheckState = CheckState.Checked
    End Sub

    Private Sub chkIncludeInactive_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.CheckStateChanged
        m_OtherNameList(chkIncludeInactive.CheckState, grdOtherName)
        grdSettings()
    End Sub

    Private Sub chkIncludeInactive_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeInactive.EnabledChanged
        ts_showInactive.Enabled = chkIncludeInactive.Enabled
    End Sub

    Private Sub ts_MkInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_MkInactive.Click
        If ts_MkInactive.Text = "Make Active" Then
            m_mkActive("mOtherName00Master", "fxKeyOtherName", sKeyOtherName, True)
        Else
            m_mkActive("mOtherName00Master", "fxKeyOtherName", sKeyOtherName, False)
        End If
       refreshForm
    End Sub

    Private Sub ts_Delete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_Delete.Click
        otherNameDelete()
        refreshForm()
    End Sub

    Private Sub otherNameDelete()
        If MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Dim sSQLCmd As String = "DELETE FROM mOtherName00Master WHERE fxKeyOtherName= '" & sKeyOtherName & "'"
            Try
                SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
                MessageBox.Show("Record successfully deleted.")
            Catch ex As Exception
                MessageBox.Show(Err.Description)
            End Try
        End If
    End Sub

    Private Sub refreshForm()
        m_OtherNameList(chkIncludeInactive.CheckState, grdOtherName)
        grdSettings()
        m_chkSettings(chkIncludeInactive, sSQLCmdQuery)
        ts_showInactive.Enabled = chkIncludeInactive.Enabled
    End Sub
End Class