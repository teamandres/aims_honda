Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_MF_termsAddEdit
    Private gCon As New Clsappconfiguration
    Private sKeyTerms As String
    Private sFormOrigin As String

    Public Property Keyterms() As String
        Get
            Return sKeyTerms
        End Get
        Set(ByVal value As String)
            sKeyTerms = value
        End Set
    End Property
    Public Property KeyFormOrigin() As String
        Get
            Return sFormOrigin
        End Get
        Set(ByVal value As String)
            sFormOrigin = value
        End Set
    End Property

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub addEditTerms()
        Dim interest As Decimal = 0.0
        If rbtnStandard.Checked = True Then
            interest = CDec(txtInt1.Text)
        ElseIf rbtnDate.Checked = True Then
            interest = CDec(txtInt2.Text)
        End If

        Dim sSQLCmd As String = "SPU_Terms_Update "
        sSQLCmd &= "@fxKeyTerms = '" & sKeyTerms & "'"
        sSQLCmd &= ",@fcTermsName = '" & txtTermsName.Text & "'"
        sSQLCmd &= ",@fbStandard =" & IIf(rbtnStandard.Checked = True, 1, 0)
        sSQLCmd &= ",@fnDayNetDue =" & IIf(rbtnStandard.Checked = True, txtStdDaysDue.Text, txtDtDaysDue.Text)
        sSQLCmd &= ",@fdDiscountPercentage =" & IIf(rbtnStandard.Checked = True, txtStdPercentage.Text, txtDtPercentage.Text)
        sSQLCmd &= ",@fnDayDiscount =" & IIf(rbtnStandard.Checked = True, txtStdDaysDiscount.Text, txtDtDaysDiscount.Text)
        sSQLCmd &= ",@fnDayNxDue =" & IIf(rbtnStandard.Checked = True, 0, txtDtDaysNxDue.Text)
        sSQLCmd &= ",@fbActive =" & IIf(chkInactive.Checked = True, 0, 1)
        sSQLCmd &= ",@fxKeyCompany ='" & gCompanyID() & "' "
        sSQLCmd &= ",@fdInterest='" & interest & "' "
        Try
            SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSQLCmd)
            MsgBox("Record successfully updated", MsgBoxStyle.Information, "Add/Edit Terms")
            m_termsList(frm_acc_terms.chkIncludeInactive.CheckState, frm_acc_terms.grdTerms)
            m_loadTerms(frm_vend_masterVendorAddEdit.cboTermsName, frm_vend_masterVendorAddEdit.cboTermsID)
            m_chkSettings(frm_acc_terms.chkIncludeInactive, frm_acc_terms.SQLQuery)
            m_loadTerms(frm_cust_CreateInvoice.cboTermsName, frm_cust_CreateInvoice.cboTermsID)
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Add/Edit Terms")
        End Try

    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        addEditTerms()
        If sFormOrigin = "frm_vend_EnterBills" Then
            With frm_vend_EnterBills
                m_GetTermss(.cboTerms_Multicombo, frm_vend_EnterBills)
            End With
        End If
    End Sub

    Private Sub loadTerm()
        Dim sSQLCmd As String = "SELECT * FROM mTerms WHERE fxKeyTerms='" & sKeyTerms & "'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtTermsName.Text = rd.Item("fcTermsName")
                    chkInactive.Checked = IIf(rd.Item("fbActive") = True, False, True)
                    If rd.Item("fbStandard") = True Then
                        rbtnStandard.Checked = True
                        txtStdDaysDue.Text = rd.Item("fnDayNetDue")
                        txtStdPercentage.Text = rd.Item("fdDiscountPercentage")
                        txtStdDaysDiscount.Text = rd.Item("fnDayDiscount")
                    Else
                        rbtnDate.Checked = True
                        txtDtDaysDue.Text = rd.Item("fnDayNetDue")
                        txtDtPercentage.Text = rd.Item("fdDiscountPercentage")
                        txtDtDaysDiscount.Text = rd.Item("fnDayDiscount")
                        txtDtDaysNxDue.Text = rd.Item("fnDayNxDue")
                    End If
                End If
            End Using
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Exclamation, "Load Terms")
        End Try
    End Sub

    Private Sub frm_MF_termsAddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rbtnStandard.Checked = True
        ctrlSettings()
        loadTerm()
    End Sub

    Private Sub ctrlSettings()
        If rbtnStandard.Checked = True Then
            txtStdDaysDiscount.Enabled = True
            txtStdDaysDue.Enabled = True
            txtStdPercentage.Enabled = True

            txtDtDaysDiscount.Enabled = False
            txtDtDaysDue.Enabled = False
            txtDtPercentage.Enabled = False
            txtDtDaysNxDue.Enabled = False
        Else
            txtStdDaysDiscount.Enabled = False
            txtStdDaysDue.Enabled = False
            txtStdPercentage.Enabled = False

            txtDtDaysDiscount.Enabled = True
            txtDtDaysDue.Enabled = True
            txtDtPercentage.Enabled = True
            txtDtDaysNxDue.Enabled = True
        End If
    End Sub

    Private Sub rbtnStandard_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnStandard.CheckedChanged
        ctrlSettings()
    End Sub

    Private Sub Label18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label18.Click

    End Sub
End Class