<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_segment_newEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_segment_newEdit))
        Me.txtnumSegment = New System.Windows.Forms.TextBox
        Me.btn_mSegment_create = New System.Windows.Forms.Button
        Me.gSegment = New System.Windows.Forms.DataGridView
        Me.gcolDesc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gColSep = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.txtsegmentmask = New System.Windows.Forms.TextBox
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        CType(Me.gSegment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtnumSegment
        '
        Me.txtnumSegment.Location = New System.Drawing.Point(229, 26)
        Me.txtnumSegment.Name = "txtnumSegment"
        Me.txtnumSegment.Size = New System.Drawing.Size(34, 21)
        Me.txtnumSegment.TabIndex = 1
        Me.txtnumSegment.Text = "0"
        Me.txtnumSegment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btn_mSegment_create
        '
        Me.btn_mSegment_create.Image = CType(resources.GetObject("btn_mSegment_create.Image"), System.Drawing.Image)
        Me.btn_mSegment_create.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_mSegment_create.Location = New System.Drawing.Point(269, 21)
        Me.btn_mSegment_create.Name = "btn_mSegment_create"
        Me.btn_mSegment_create.Size = New System.Drawing.Size(64, 28)
        Me.btn_mSegment_create.TabIndex = 9
        Me.btn_mSegment_create.Text = "Go .."
        Me.btn_mSegment_create.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_mSegment_create.UseVisualStyleBackColor = True
        '
        'gSegment
        '
        Me.gSegment.AllowUserToAddRows = False
        Me.gSegment.AllowUserToDeleteRows = False
        Me.gSegment.AllowUserToOrderColumns = True
        Me.gSegment.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.gSegment.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised
        Me.gSegment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gSegment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.gcolDesc, Me.gColSep})
        Me.gSegment.Location = New System.Drawing.Point(15, 80)
        Me.gSegment.Name = "gSegment"
        Me.gSegment.Size = New System.Drawing.Size(216, 188)
        Me.gSegment.TabIndex = 10
        '
        'gcolDesc
        '
        Me.gcolDesc.HeaderText = "Segments"
        Me.gcolDesc.Name = "gcolDesc"
        Me.gcolDesc.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gcolDesc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.gcolDesc.Width = 70
        '
        'gColSep
        '
        Me.gColSep.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.gColSep.HeaderText = "Separator"
        Me.gColSep.Items.AddRange(New Object() {" ", "-", "/", ".", ":", "+", "|", "\", "=", ";", "#", "&", "*"})
        Me.gColSep.Name = "gColSep"
        Me.gColSep.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gColSep.Width = 70
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox1.Location = New System.Drawing.Point(14, 12)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(229, 30)
        Me.TextBox1.TabIndex = 11
        Me.TextBox1.Text = "Please define the number of Segments you want to use in your company..."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Blue
        Me.Label2.Location = New System.Drawing.Point(10, 277)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 13)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Segment Mask :"
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBox2.ForeColor = System.Drawing.Color.Red
        Me.TextBox2.Location = New System.Drawing.Point(14, 64)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(266, 16)
        Me.TextBox2.TabIndex = 14
        Me.TextBox2.Text = "Define the number of Character per Segment."
        '
        'txtsegmentmask
        '
        Me.txtsegmentmask.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.txtsegmentmask.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtsegmentmask.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsegmentmask.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtsegmentmask.Location = New System.Drawing.Point(14, 293)
        Me.txtsegmentmask.Multiline = True
        Me.txtsegmentmask.Name = "txtsegmentmask"
        Me.txtsegmentmask.Size = New System.Drawing.Size(316, 39)
        Me.txtsegmentmask.TabIndex = 15
        '
        'btnCancel
        '
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(250, 342)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(80, 28)
        Me.btnCancel.TabIndex = 34
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Enabled = False
        Me.btnSave.Image = CType(resources.GetObject("btnSave.Image"), System.Drawing.Image)
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.Location = New System.Drawing.Point(164, 342)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(80, 28)
        Me.btnSave.TabIndex = 33
        Me.btnSave.Text = "&Save"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'frm_segment_newEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(343, 382)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.txtsegmentmask)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.gSegment)
        Me.Controls.Add(Me.btn_mSegment_create)
        Me.Controls.Add(Me.txtnumSegment)
        Me.Controls.Add(Me.TextBox1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(351, 416)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(351, 416)
        Me.Name = "frm_segment_newEdit"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Define Segment"
        CType(Me.gSegment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtnumSegment As System.Windows.Forms.TextBox
    Friend WithEvents btn_mSegment_create As System.Windows.Forms.Button
    Friend WithEvents gSegment As System.Windows.Forms.DataGridView
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents txtsegmentmask As System.Windows.Forms.TextBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents gcolDesc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gColSep As System.Windows.Forms.DataGridViewComboBoxColumn
End Class
