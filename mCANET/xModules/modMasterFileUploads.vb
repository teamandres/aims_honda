Imports System.Data.OleDb

Public Class modMasterFileUploads
    Public excelConnectionString As String

    Public Sub New(ByVal FilePath As String)
        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & _
                                   FilePath & ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'"
    End Sub

    Public Function retrieveDataFromExcel(ByVal FilePath As String) As DataSet

        Dim ExcelDataset = New DataSet
        Dim excelConnection As New OleDbConnection(excelConnectionString)
        excelConnection.Open()

        Try
            Dim TableSchema As DataTable = excelConnection.GetSchema("Tables")
            Dim sqlCommand As String = String.Format("SELECT * FROM [{0}]", TableSchema.Rows(0)("TABLE_NAME"))
            Dim exceladapter As New OleDbDataAdapter(sqlCommand, excelConnection)

            exceladapter.Fill(ExcelDataset, "Source Data")
        Catch ex As OleDbException
            If ex.Message = "External table is not in the expected format." Then
                MsgBox("Please check the format of your Excel File." _
                        & vbNewLine & "Resave it to MS Excel 2003 Format." _
                        , MsgBoxStyle.Exclamation, "Browse SR File")
            End If
        End Try

        excelConnection.Close()

        Return ExcelDataset
    End Function
End Class
