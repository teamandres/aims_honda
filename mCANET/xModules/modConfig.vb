Imports System.IO

Module modConfig
    Public Sub getCurDir()
        strCurrentDir = Directory.GetCurrentDirectory
        '===================================================
        '1. to get directory (no file name of executable file)
        '       strCurrentDir = Directory.GetCurrentDirectory or
        '       strCurrentDir = Application.StartupPath
        '2. to get directory (with file name of executable file)
        '       strCurrentFileName = Application.ExecutablePath
        '===================================================
        strCurrentFileName = Application.ExecutablePath
    End Sub

    Public Function GetFileContents(ByVal FullPath As String, _
       Optional ByRef ErrInfo As String = "") As String

        Dim strContents As String
        Dim objReader As StreamReader
        Try
            objReader = New StreamReader(FullPath)
            strContents = objReader.ReadToEnd()
            objReader.Close()
            Return strContents
        Catch Ex As Exception
            Return ErrInfo = Ex.Message
        End Try
    End Function

    Public Function SaveTextToFile(ByVal strData As String, _
     ByVal FullPath As String, _
       Optional ByVal ErrInfo As String = "") As Boolean

        'Dim Contents As String
        Dim bReturn As Boolean = False
        Dim objReader As StreamWriter
        Try
            objReader = New StreamWriter(FullPath)
            objReader.Write(strData)
            objReader.Close()
            bReturn = True
        Catch Ex As Exception
            ErrInfo = Ex.Message

        End Try
        Return bReturn
    End Function

End Module
