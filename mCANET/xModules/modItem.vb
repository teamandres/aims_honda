Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Module modItem

    Private gCon As New Clsappconfiguration

    Public Sub m_loadItemGroup(ByVal cboName As ComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQLCmd As String = "SELECT fcItemGroupName "
        sSQLCmd &= " FROM mItem01Group WHERE fbActive = 1 "
        sSQLCmd &= " AND fxKeyCompany = '" & gCompanyID() & "'"
        sSQLCmd &= " ORDER BY fcItemGroupName"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item("fcItemGroupName"))
                End While
            End Using
            Dim xCnt As Integer
            cboName.Items.Clear()
            cboName.Items.Add("")
            cboName.Items.Add("create")
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboName.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Item Group")
        End Try
    End Sub

    Public Sub m_loadItemCode(ByVal cboName As ComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQLCmd As String = "select fcItemCode from dbo.mItemCode where  fxKeyCompany ='" & gCompanyID() & "' Order by fcItemCode asc"
        'Dim sSQLCmd As String = "select fcItemCode from dbo.mItemMaster where  fxKeyCompany ='" & gCompanyID() & "' Order by fcItemCode asc"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item("fcItemCode"))
                End While
            End Using
            Dim xCnt As Integer
            cboName.Items.Clear()
            cboName.Items.Add("")
            cboName.Items.Add("create")
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboName.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Item Code")
        End Try
    End Sub

    Public Sub m_loadItemModel(ByVal cboName As ComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQLCmd As String = "select fcItemDescription from dbo.mItemCode where  fxKeyCompany ='" & gCompanyID() & "' Order by fcItemDescription asc"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item("fcItemDescription"))
                End While
            End Using
            Dim xCnt As Integer
            cboName.Items.Clear()
            cboName.Items.Add("")
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboName.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Item Model")
        End Try
    End Sub

    Public Sub m_loadItemCategory(ByVal cboName As ComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQLCmd As String = "select fcCategoryCode from dbo.mItemCategory where fxKeyCompany='" & gCompanyID() & "' Order by fcCategoryCode asc"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item("fcCategoryCode"))
                End While
            End Using
            Dim xCnt As Integer
            cboName.Items.Clear()
            cboName.Items.Add("")
            cboName.Items.Add("create")
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboName.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Item Category")
        End Try
    End Sub

    Public Sub m_loadItemCategoryDescription(ByVal cboName As ComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQLCmd As String = "select fcCategoryDescription from dbo.mItemCategory where fxKeyCompany='" & gCompanyID() & "' Order by fcCategoryCode asc"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item("fcCategoryDescription"))
                End While
            End Using
            Dim xCnt As Integer
            cboName.Items.Clear()
            cboName.Items.Add("")
            cboName.Items.Add("create")
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboName.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Item Category")
        End Try
    End Sub

    Public Sub m_loadItemBrand(ByVal cboName As ComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQLCmd As String = "select fcBrandCode from dbo.mItemBrand where fxKeyCompany='" & gCompanyID() & "' order by fcBrandCode asc "
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item("fcBrandCode"))
                End While
            End Using
            Dim xCnt As Integer
            cboName.Items.Clear()
            cboName.Items.Add("")
            cboName.Items.Add("create")
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboName.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Item Brand ")
        End Try
    End Sub

    Public Sub m_loadItemColor(ByVal cboName As ComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQLCmd As String = "select distinct fcColorDescription from dbo.mItemColor where fxKeyCompany='" & gCompanyID() & "' Order by fcColorDescription asc"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item("fcColorDescription"))
                End While
            End Using
            Dim xCnt As Integer
            cboName.Items.Clear()
            cboName.Items.Add("")
            cboName.Items.Add("create")
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboName.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Item Color ")
        End Try
    End Sub

    Public Sub m_loadItemSize(ByVal cboName As ComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQLCmd As String = "select distinct fcSizeCode from dbo.mItemSize where fxKeyCompany='" & gCompanyID() & "' Order by fcSizeCode asc"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item("fcSizeCode"))
                End While
            End Using
            Dim xCnt As Integer
            cboName.Items.Clear()
            cboName.Items.Add("")
            cboName.Items.Add("create")
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboName.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Item Size")
        End Try
    End Sub

    Public Sub m_loadItemUnitMeasurement(ByVal cboName As ComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQLCmd As String = "select fcUnitCode from dbo.mItemUnit where fxKeyCompany = '" & gCompanyID() & "' Order by fcUnitCode asc"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item("fcUnitCode"))
                End While
            End Using
            Dim xCnt As Integer
            cboName.Items.Clear()
            cboName.Items.Add("")
            cboName.Items.Add("create")
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboName.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Unit of Measurement")
        End Try
    End Sub

    Public Sub m_loadTaxCode(ByVal cboName As ComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQLCmd As String = "select fcTaxName from mSalesTax where fxKeyCompany ='" & gCompanyID() & "' Order by fcTaxName asc"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item("fcTaxName"))
                End While
            End Using
            Dim xCnt As Integer
            cboName.Items.Clear()
            cboName.Items.Add("")
            cboName.Items.Add("create")
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboName.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Tax Code")
        End Try
    End Sub

    Public Sub m_loadSuppliers(ByVal cboName As ComboBox)
        Dim cboTemp As New ComboBox

        Dim sSQLCmd As String = "select fcSupplierName from mSupplier00Master where fxKeyCompany='" & gCompanyID() & "' Order by fcSupplierName asc"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSQLCmd)
                While rd.Read
                    cboTemp.Items.Add(rd.Item("fcSupplierName"))
                End While
            End Using
            Dim xCnt As Integer
            cboName.Items.Clear()
            cboName.Items.Add("")
            For xCnt = 0 To cboTemp.Items.Count - 1
                cboName.Items.Add(cboTemp.Items(xCnt))
            Next
        Catch ex As Exception
            MessageBox.Show(Err.Description, "CLICKSOFTWARE: Load Supplier")
        End Try
    End Sub

    Public Sub m_loadItemDetails()

    End Sub


End Module
