Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared

Public Class frmAPSummary

    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        If rdbReference.Checked = False And rdbAccount.Checked = False Then
            MsgBox("Please Indicate whether PER REFERENCE or PER ACCOUNT.", MsgBoxStyle.Information)
            rdbReference.Focus()
        Else
            If Me.rdbReference.Checked = True Then
                Call perReference()
            ElseIf Me.rdbAccount.Checked = True Then
                Call perAccounts()
            End If
        End If
    End Sub

    Private Sub perReference()
        ' rptsummary.Refresh()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\accountpayabale_perReference.rpt")

        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@fxKeyCompany", gCompanyID())
        rptsummary.SetParameterValue("@frmdate", Microsoft.VisualBasic.FormatDateTime(DateTimePicker1.Value, DateFormat.ShortDate))
        rptsummary.SetParameterValue("@todate", Microsoft.VisualBasic.FormatDateTime(DateTimePicker2.Value, DateFormat.ShortDate))

        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary
    End Sub

    Private Sub perAccounts()
        'rptsummary.Refresh()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\accountpayable_peraccounts.rpt")

        Logon(rptsummary, con.Server, con.Database, con.Username, "sa")
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@fxKeyCompany", gCompanyID())
        rptsummary.SetParameterValue("@frmdate", Microsoft.VisualBasic.FormatDateTime(DateTimePicker1.Value, DateFormat.ShortDate))
        rptsummary.SetParameterValue("@todate", Microsoft.VisualBasic.FormatDateTime(DateTimePicker2.Value, DateFormat.ShortDate))

        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary
    End Sub


    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub frmAPSummary_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub
End Class