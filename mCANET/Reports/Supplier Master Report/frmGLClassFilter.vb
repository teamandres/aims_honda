Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient


'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: Aug 12, 2009                       ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################

Public Class frmGLClassFilter
#Region "Declaration"
    Private gcon As New Clsappconfiguration
    Private LevelDept As Integer = 0
    Private DiggerDept As Integer = 0
    Private ClassDesciptionForToolTip As String
    Private ClassParentForToolTip As String
    Private ClassNameForToolTip As String
    Public Result As String
#End Region
#Region "Functions and Subs"
    Private Function LoadClasses() As DataSet
        Dim sSqlGetClassesCmd As String = "SELECT * FROM dbo.mClasses WHERE fxKeyCompany ='" & _
                                                gCompanyID() & "' ORDER BY classLevel"
        Try
            Return SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSqlGetClassesCmd)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
    Private Sub loadChosenClasses()
        Dim xDBName As String
        Dim xName(0) As String
        Dim sSqlCmd As String = "SELECT ClassName FROM dbo.mTemporaryClassStorage WHERE fxKeyCompany ='" & gCompanyID() & "'"
        LstClass.Items.Clear()
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    xDBName = rd.Item(0).ToString
                    xName = Split(xDBName, "���")
                    With LstClass.Items.Add(xName(0).ToString)
                        .SubItems.Add(xDBName)
                    End With
                End While
            End Using
        Catch ex As Exception
        End Try
    End Sub
    Private Sub ClassLoadToGrid()
        With DGVClasses
            Dim DGVClasses As DataTable = LoadClasses().Tables(0)
            .DataSource = DGVClasses.DefaultView
        End With
    End Sub
    Private Sub getLevelDeptValue()
        LevelDept = 0
        Dim sSqlCmd As String = "SELECT classLevel FROM dbo.mClasses WHERE fxKeyCompany ='" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    If LevelDept < rd.Item(0).ToString Then
                        LevelDept = rd.Item(0).ToString
                    End If
                End While
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Private Sub DeleteClass(ByVal Refno As String)
        Dim SqlDeleteClass As String = "DELETE FROM dbo.mClasses where ClassRefnumber ='" & Refno & _
                                        "' AND fxKeyCompany ='" & gCompanyID() & "'"
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, SqlDeleteClass)
        Catch ex As Exception
            MsgBox(ex, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "frmClasses-delete class")
        End Try
    End Sub
    Private Sub EvaluateNodeName()
        DGVClasses.SelectAll()
        Dim xRow As Integer = 0
        Dim UncleanName As String
        Dim ClassName() As String
        For Each selectedRow As DataGridViewRow In DGVClasses.SelectedRows
            xRow = selectedRow.Index
            TVClass.SelectedNode = GetNode(DGVClasses.Item(2, xRow).Value, TVClass.Nodes)
            If TVClass.SelectedNode Is Nothing Then
                DeleteClass(DGVClasses.Item(0, xRow).Value)
            Else
                UncleanName = TVClass.SelectedNode.Text
                ClassName = Split(UncleanName, "���")
                TVClass.SelectedNode.Text = ClassName(0)
            End If
        Next
        TVClass.SelectedNode = GetNode(DGVClasses.Item(2, xRow).Value, TVClass.Nodes)
        DGVClasses.Refresh()
        TVClass.CollapseAll()
    End Sub
    Private Sub ClassesFromGridToTree()
        Dim diggerDept As Integer = 1
        Dim xRow As Integer = 0
        TVClass.Nodes.Clear()
        Do While diggerDept <= LevelDept
            If diggerDept = 1 Then
                'establishing first level
                DGVClasses.SelectAll()
                For Each selectedRow As DataGridViewRow In DGVClasses.SelectedRows
                    xRow = selectedRow.Index
                    If diggerDept = DGVClasses.Item(3, xRow).Value Then
                        TVClass.Nodes.Add(DGVClasses.Item(2, xRow).Value)
                        TVClass.SelectedNode = GetNode(DGVClasses.Item(2, xRow).Value, TVClass.Nodes)
                        TVClass.SelectedNode.Tag = DGVClasses.Item(0, xRow).Value
                    End If
                Next
                xRow = 0
                DGVClasses.Refresh()
                '#############################
            Else

                DGVClasses.SelectAll()
                For Each SelectedRow As DataGridViewRow In DGVClasses.SelectedRows
                    xRow = SelectedRow.Index
                    'MsgBox(DGVClasses.Item(2, xRow).Value)
                    If diggerDept = DGVClasses.Item(3, xRow).Value Then
                        TVClass.SelectedNode = GetNode(DGVClasses.Item(4, xRow).Value, TVClass.Nodes)
                        If TVClass.SelectedNode Is Nothing Then
                            DeleteClass(DGVClasses.Item(0, xRow).Value)
                        Else
                            TVClass.SelectedNode.Nodes.Add(DGVClasses.Item(2, xRow).Value)
                            TVClass.SelectedNode = GetNode(DGVClasses.Item(2, xRow).Value, TVClass.Nodes)
                            TVClass.SelectedNode.Tag = DGVClasses.Item(0, xRow).Value
                        End If
                    End If
                Next
                DGVClasses.Refresh()
            End If
            diggerDept += 1
        Loop

        EvaluateNodeName()
    End Sub
    Private Function GetNode(ByVal text As String, ByVal parentCollection As TreeNodeCollection) As TreeNode
        Dim ret As TreeNode
        Dim child As TreeNode
        For Each child In parentCollection 'step through the parentcollection
            If child.Text = text Then
                ret = child
            ElseIf child.GetNodeCount(False) > 0 Then ' if there is child items then call this function recursively
                ret = GetNode(text, child.Nodes)
            End If
            If Not ret Is Nothing Then Exit For 'if something was found, exit out of the for loop
        Next
        Return ret
    End Function
    Private Sub getClassDetailsForToolTip(ByVal ClassReferenceNumber As String)
        Dim dbParent As String
        Dim xParent(0) As String
        Dim dbName As String
        Dim xName(0) As String
        Dim sSqlGetDetail As String = _
                    "SELECT ClassDescription, ClassParentPath, classname FROM dbo.mClasses WHERE ClassRefnumber = '" & _
                    ClassReferenceNumber & "' AND fxKeyCompany ='" & gCompanyID() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlGetDetail)
                While rd.Read
                    ClassDesciptionForToolTip = rd.Item(0).ToString
                    dbParent = rd.Item(1).ToString
                    If dbParent Is Nothing Or dbParent = "" Or dbParent = " " Then
                        ClassParentForToolTip = "N/A"
                    Else
                        xParent = Split(dbParent, "���")
                        ClassParentForToolTip = xParent(0)
                    End If
                    dbName = rd.Item(2).ToString
                    xName = Split(dbName, "���")
                    ClassNameForToolTip = xName(0)
                End While
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Private Sub ChosenClassesTomTemporaryClassStorage()
        Dim sSqlDeleteCmd As String = "DELETE FROM dbo.mTemporaryClassStorage WHERE fxKeyCompany ='" & gCompanyID() & "'"
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, sSqlDeleteCmd)
        Catch ex As Exception
        End Try
        For Each ChosenClass As ListViewItem In LstClass.Items
            Dim xRefno(0) As String
            xRefno = Split(LstClass.Items(ChosenClass.Index).SubItems(1).Text, "���")
            Dim sSqlInsertClasses As String = "INSERT INTO dbo.mTemporaryClassStorage "
            sSqlInsertClasses &= "(ClassName, ClassRefno, fxKeyCompany) VALUES ('" & LstClass.Items(ChosenClass.Index).SubItems(1).Text & "', '" & xRefno(1).ToString & "', '" & gCompanyID() & "')"
            Try
                SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, sSqlInsertClasses)
            Catch ex As Exception
            End Try
        Next
    End Sub
    Private Sub EraseClassAtTemporaryClassStorage()
        Dim sSqlDelete As String = "DELETE FROM dbo.mTemporaryClassStorage WHERE fxKeyCompany = '" & gCompanyID() & "'"
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, sSqlDelete)
        Catch ex As Exception
        End Try
    End Sub
    Private Sub AddSigleClass(ByVal SelectedClass As String)
        Dim xselectedItem(0) As String
        Dim xItem As ListViewItem
        xselectedItem = Split(SelectedClass, "���")
        If SelectedClass Is Nothing Then
            MsgBox("Please, select a class.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "No Class selected")
        Else
            If LstClass.Items.Count = 0 Then
                With LstClass.Items.Add(xselectedItem(0).ToString)
                    .SubItems.Add(SelectedClass)
                End With
            Else
                xItem = LstClass.FindItemWithText(SelectedClass, True, 0)
                If xItem Is Nothing Then
                    With LstClass.Items.Add(xselectedItem(0).ToString)
                        .SubItems.Add(SelectedClass)
                    End With
                End If
            End If
        End If
    End Sub
    Private Sub RemoveSingleClass()
        Dim xItem As ListViewItem
        If LstClass.SelectedItems.Count = 0 Then
            MsgBox("Please select a Class to remove.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "No Class selected")
        Else
            xItem = LstClass.SelectedItems.Item(0)
            LstClass.Items.Remove(xItem)
        End If
    End Sub
    Private Sub AddMultipleClass(ByVal ClassName As String)
        Dim VirtualGrid As New ListView
        Dim digger As Integer = 0
        getLevelDeptValue() 'set the level dept
        VirtualGrid.Items.Add(ClassName) 'add the selected class to virtual list
        Do While digger < LevelDept '<============ para magloop kaba collect ng base
            For Each item As ListViewItem In VirtualGrid.Items
                Dim sSqlCmd As String = "SELECT classname FROM dbo.mClasses WHERE fxKeyCompany ='" & gCompanyID() & _
                                        "' AND ClassParentPath = '" & item.Text & "'"
                Try
                    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                        While rd.Read
                            Dim searchString As New ListViewItem
                            searchString = VirtualGrid.FindItemWithText(rd.Item(0).ToString)
                            If searchString Is Nothing Then
                                VirtualGrid.Items.Add(rd.Item(0).ToString)
                            End If
                        End While
                    End Using
                Catch ex As Exception

                End Try
            Next
            digger += 1
        Loop
        '##################Add all collected Class to the real list (lstClass)##########################
        For Each realItem As ListViewItem In VirtualGrid.Items
            Dim xdbName As String = realItem.Text
            Dim xClassName(0) As String
            Dim searchString2 As New ListViewItem
            xClassName = Split(xdbName, "���")
            If LstClass.Items.Count <> 0 Then
                searchString2 = LstClass.FindItemWithText(realItem.Text, True, 0)
            Else
                With LstClass.Items.Add(xClassName(0).ToString)
                    .SubItems.Add(xdbName)
                End With
            End If
            If searchString2 Is Nothing Then
                With LstClass.Items.Add(xClassName(0))
                    .SubItems.Add(xdbName)
                End With
            End If
        Next
        '################################################################################################
    End Sub
    Public Sub RemoveMultipleClass(ByVal ClassName As String)
        Dim virtualList As New ListView
        Dim digger As Integer = 0
        getLevelDeptValue()
        virtualList.Items.Add(ClassName)
        Do While digger < LevelDept
            For Each Item As ListViewItem In virtualList.Items
                Dim sSqlCmd As String = "SELECT classname FROM dbo.mClasses WHERE fxKeyCompany ='" & gCompanyID() & _
                                        "' AND ClassParentPath = '" & Item.Text & "'"
                Try
                    Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                        While rd.Read
                            Dim seachVariable As New ListViewItem
                            seachVariable = virtualList.FindItemWithText(rd.Item(0).ToString, True, 0)
                            If seachVariable Is Nothing Then
                                virtualList.Items.Add(rd.Item(0).ToString)
                            End If
                        End While
                    End Using
                Catch ex As Exception
                End Try
            Next
            digger += 1
        Loop

        For Each CollectedItem As ListViewItem In virtualList.Items
            Dim searchVariable2 As ListViewItem
            searchVariable2 = LstClass.FindItemWithText(CollectedItem.Text)
            If searchVariable2 Is Nothing Then
            Else
                LstClass.Items.Remove(searchVariable2)
            End If
        Next
    End Sub
#End Region
    Private Sub frmGLClassFilter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Result = "OK"
        ClassLoadToGrid()
        getLevelDeptValue()
        ClassesFromGridToTree()
        loadChosenClasses()
        ChkSubClasses.Checked = True
    End Sub
    Private Sub TVClass_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TVClass.AfterSelect
        getClassDetailsForToolTip(TVClass.SelectedNode.Tag)
        On Error GoTo subTerminated
        If TVClass.SelectedNode.Tag <> Nothing Or TVClass.SelectedNode.Tag <> "" Then
            getClassDetailsForToolTip(TVClass.SelectedNode.Tag)
            TVClass.SelectedNode.ToolTipText = "Class Name:  " & ClassNameForToolTip & _
                        vbCr & "Reference #:  " & TVClass.SelectedNode.Tag & vbCr & _
                        "Subclass of:   " & ClassParentForToolTip & vbCr & _
                        "Description:   " & ClassDesciptionForToolTip
        End If
subTerminated:
    End Sub
    Private Sub TVClass_NodeMouseHover(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeNodeMouseHoverEventArgs) Handles TVClass.NodeMouseHover
        On Error GoTo subTerminated
        getClassDetailsForToolTip(TVClass.SelectedNode.Tag)
        If TVClass.SelectedNode.Tag <> Nothing Or TVClass.SelectedNode.Tag <> "" Then
            TVClass.SelectedNode.ToolTipText = "Class Name:  " & ClassNameForToolTip & _
                        vbCr & "Reference #:  " & TVClass.SelectedNode.Tag & vbCr & _
                        "Subclass of:   " & ClassParentForToolTip & vbCr & _
                        "Description:   " & ClassDesciptionForToolTip
        End If
subTerminated:
    End Sub
    Private Sub BtnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOK.Click
        If LstClass.Items.Count = 0 Then
            MsgBox("Please select atleast 1 class.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "No Class/es has been loaded")
        Else
            ChosenClassesTomTemporaryClassStorage()
            Result = "OK"
            Me.Close()
        End If
    End Sub
    Private Sub ChkSubClasses_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkSubClasses.CheckedChanged
        If ChkSubClasses.Checked = True Then
            BtnSingleAdd.Text = "Add Class and subclasses"
            btnSingleRemove.Text = "Remove Class and subclasses"
        Else
            BtnSingleAdd.Text = "Add Class"
            btnSingleRemove.Text = "Remove Class"
        End If
    End Sub
    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        EraseClassAtTemporaryClassStorage()
        Result = "Cancel"
    End Sub
    Private Sub BtnSingleAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSingleAdd.Click
        If TVClass.SelectedNode Is Nothing Then
        Else
            If ChkSubClasses.Checked = False Then
                AddSigleClass(TVClass.SelectedNode.Text & "���" & TVClass.SelectedNode.Tag)
            Else
                AddMultipleClass(TVClass.SelectedNode.Text & "���" & TVClass.SelectedNode.Tag)
            End If
        End If
    End Sub
    Private Sub btnSingleRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSingleRemove.Click
        If LstClass.SelectedItems.Count = 0 Then
            MsgBox("Please select a Class to remove.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "No Class selected")
        Else
            If ChkSubClasses.Checked = False Then
                RemoveSingleClass()
            Else
                RemoveMultipleClass(LstClass.SelectedItems.Item(0).SubItems(1).Text)
            End If
        End If
    End Sub
    Private Sub LstClass_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles LstClass.DoubleClick
        If LstClass.SelectedItems.Count = 0 Then
            MsgBox("Please select a Class to remove.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "No Class selected")
        Else
            If ChkSubClasses.Checked = False Then
                RemoveSingleClass()
            Else
                RemoveMultipleClass(LstClass.SelectedItems.Item(0).SubItems(1).Text)
            End If
        End If
    End Sub
End Class