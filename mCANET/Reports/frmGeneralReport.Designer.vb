<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGeneralReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.crvGeneralReport = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SuspendLayout()
        '
        'crvGeneralReport
        '
        Me.crvGeneralReport.ActiveViewIndex = -1
        Me.crvGeneralReport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvGeneralReport.DisplayGroupTree = False
        Me.crvGeneralReport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvGeneralReport.Location = New System.Drawing.Point(0, 0)
        Me.crvGeneralReport.Name = "crvGeneralReport"
        Me.crvGeneralReport.SelectionFormula = ""
        Me.crvGeneralReport.ShowGroupTreeButton = False
        Me.crvGeneralReport.Size = New System.Drawing.Size(606, 501)
        Me.crvGeneralReport.TabIndex = 0
        Me.crvGeneralReport.ViewTimeSelectionFormula = ""
        '
        'frmGeneralReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(606, 501)
        Me.Controls.Add(Me.crvGeneralReport)
        Me.Name = "frmGeneralReport"
        Me.Text = "frmGeneralReport"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvGeneralReport As CrystalDecisions.Windows.Forms.CrystalReportViewer
End Class
