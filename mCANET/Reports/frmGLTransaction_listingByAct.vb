Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared

Public Class frmGLTransaction_listingByAct

    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        'rptsummary.Refresh()
        If frmMain.LblClassActivator.Visible = True And ChkClassFilter.Checked = True Then
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\gl_transaactionlistingByAccountforClassing.rpt")
        Else
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\gl_transaactionlistingByAccount.rpt")
        End If
        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
        rptsummary.SetParameterValue("@frmMth", cboMth1.SelectedItem)
        rptsummary.SetParameterValue("@toMth", cboMth2.SelectedItem)
        rptsummary.SetParameterValue("@year", cboYear.SelectedItem)
        rptsummary.SetParameterValue("@fxKeyCompany", gCompanyID())
        rptsummary.SetParameterValue("@fcAccount", cboAccount.SelectedValue)
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary

    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub frmGLTransaction_listingByAct_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub frmGLTransaction_listingByAct_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If frmMain.LblClassActivator.Visible = True Then
            ChkClassFilter.Visible = True
        Else
            ChkClassFilter.Visible = False
        End If
        loadAccts()
    End Sub

    Private Sub loadAccts()
        Dim dtAccounts As DataTable = m_displayAccountsDS().Tables(0)
        Dim dtNames As DataTable = m_DisplayNamesJV().Tables(0)

        With cboAccount
            .Items.Clear()
            ' .HeaderText = "Accounts"
            '.DataPropertyName = "fcDescription"
            .Name = "cAccounts"
            .DataSource = dtAccounts.DefaultView
            .DisplayMember = "fcDescription"
            .ValueMember = "fcDescription"
            .DropDownWidth = 200
            '.AutoComplete = True
            '.DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
            .MaxDropDownItems = 10
            '.Resizable = DataGridViewTriState.True
        End With
    End Sub

    Private Sub ChkClassFilter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkClassFilter.CheckedChanged
        If ChkClassFilter.Checked = True Then
            frmGLClassFilter.ShowDialog()
            If frmGLClassFilter.Result = "Cancel" Then
                ChkClassFilter.Checked = False
            End If
        End If
    End Sub

    Private Sub BtnShowPane_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnShowPane.Click
        If BtnShowPane.Text = "Hide Search Pane" Then
            Panel1.Height = 0
            BtnShowPane.Top = 3
            BtnShowPane.Text = "Show Search Pane"
        Else
            BtnShowPane.Text = "Hide Search Pane"
            BtnShowPane.Top = 76
            Panel1.Height = 73
        End If
    End Sub
End Class