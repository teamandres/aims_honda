Public Class frmCustomerTransactionReport

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        pCallCompany(frmInvoiceList)
        If rbtnTransaction1.Checked = True Then
            frmInvoiceList.LblType.Text = ""
            frmInvoiceList.Text = "Customer Transactions List"
            frmInvoiceList.LblTransactionKind.Text = "Customer Transactions List"
        Else
            If rbtnTransaction2.Checked = True Then
                frmInvoiceList.LblType.Text = "Invoice"
                frmInvoiceList.Text = "Invoice List"
                frmInvoiceList.LblTransactionKind.Text = "Invoice List"
            Else
                If rbtnTransaction3.Checked = True Then
                    frmInvoiceList.LblType.Text = "Credit Memo"
                    frmInvoiceList.Text = "Credit Memo List"
                    frmInvoiceList.LblTransactionKind.Text = "Credit Memo List"
                Else
                    frmInvoiceList.LblType.Text = "Debit Memo"
                    frmInvoiceList.Text = "Debit Memo List"
                    frmInvoiceList.LblTransactionKind.Text = "Debit Memo List"
                End If
            End If
        End If



    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class