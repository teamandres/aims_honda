﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCOAVerification
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LstAcnts = New System.Windows.Forms.ListBox()
        Me.btnAcnt = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(287, 30)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "The following account(s) has no parent account." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Double-click an account to go to" & _
            " Chart of Accounts." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'LstAcnts
        '
        Me.LstAcnts.FormattingEnabled = True
        Me.LstAcnts.ItemHeight = 15
        Me.LstAcnts.Location = New System.Drawing.Point(15, 52)
        Me.LstAcnts.Name = "LstAcnts"
        Me.LstAcnts.Size = New System.Drawing.Size(284, 244)
        Me.LstAcnts.TabIndex = 1
        '
        'btnAcnt
        '
        Me.btnAcnt.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnAcnt.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnAcnt.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAcnt.Location = New System.Drawing.Point(224, 302)
        Me.btnAcnt.Name = "btnAcnt"
        Me.btnAcnt.Size = New System.Drawing.Size(75, 29)
        Me.btnAcnt.TabIndex = 2
        Me.btnAcnt.Text = "Close"
        Me.btnAcnt.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAcnt.UseVisualStyleBackColor = True
        '
        'btnRefresh
        '
        Me.btnRefresh.Location = New System.Drawing.Point(15, 301)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(75, 30)
        Me.btnRefresh.TabIndex = 3
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'frmCOAVerification
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnAcnt
        Me.ClientSize = New System.Drawing.Size(315, 336)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.btnAcnt)
        Me.Controls.Add(Me.LstAcnts)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmCOAVerification"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Unallocated Account(s) found!"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LstAcnts As System.Windows.Forms.ListBox
    Friend WithEvents btnAcnt As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
End Class
