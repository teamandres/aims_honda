Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmClosingEntryVoucher

    Private rptsummary As New ReportDocument
    Private gCon As New Clsappconfiguration

    Private pkEntryID As String
    Public Property GetpkEntryID() As String
        Get
            Return pkEntryID
        End Get
        Set(ByVal value As String)
            pkEntryID = value
        End Set
    End Property

    Private Sub frmClosingEntryVoucher_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub frmClosingEntryVoucher_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bgwLoadReport.RunWorkerAsync()
    End Sub

    Private Sub LoadReport()
        Try
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\ClosingEntryVoucher.rpt")
            Logon(rptsummary, gCon.Server, gCon.Database, gCon.Username, Clsappconfiguration.GetDecryptedData(gCon.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@pkeyID", GetpkEntryID())
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub bgwLoadReport_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwLoadReport.DoWork
        Call LoadReport()
    End Sub

    Private Sub bgwLoadReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwLoadReport.RunWorkerCompleted
        Me.crvClosingEntry.Visible = True
        Me.crvClosingEntry.ReportSource = rptsummary
        picLoading.Visible = False
    End Sub
End Class