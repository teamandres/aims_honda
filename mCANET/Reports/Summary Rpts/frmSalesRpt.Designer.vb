<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSalesRpt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSalesRpt))
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnPreview = New System.Windows.Forms.Button
        Me.rdPerInvoice = New System.Windows.Forms.RadioButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.ChkSubClasses = New System.Windows.Forms.CheckBox
        Me.LstClass = New System.Windows.Forms.ListView
        Me.ClassName = New System.Windows.Forms.ColumnHeader
        Me.ClassRefNo = New System.Windows.Forms.ColumnHeader
        Me.CboClassing = New MTGCComboBox
        Me.txtClassRefno = New System.Windows.Forms.TextBox
        Me.ChckClassing = New System.Windows.Forms.CheckBox
        Me.rdPerBrand = New System.Windows.Forms.RadioButton
        Me.ListViewScope = New System.Windows.Forms.ListView
        Me.ClassRealName = New System.Windows.Forms.ColumnHeader
        Me.ClassExRefno = New System.Windows.Forms.ColumnHeader
        Me.cboMonth = New System.Windows.Forms.ComboBox
        Me.cboYear = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboBrand = New System.Windows.Forms.ComboBox
        Me.btnCancel = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(197, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Month"
        '
        'btnPreview
        '
        Me.btnPreview.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.Location = New System.Drawing.Point(251, 105)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(87, 28)
        Me.btnPreview.TabIndex = 2
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'rdPerInvoice
        '
        Me.rdPerInvoice.AutoSize = True
        Me.rdPerInvoice.Checked = True
        Me.rdPerInvoice.Location = New System.Drawing.Point(6, 37)
        Me.rdPerInvoice.Name = "rdPerInvoice"
        Me.rdPerInvoice.Size = New System.Drawing.Size(90, 17)
        Me.rdPerInvoice.TabIndex = 3
        Me.rdPerInvoice.TabStop = True
        Me.rdPerInvoice.Text = "Per Invoice"
        Me.rdPerInvoice.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ChkSubClasses)
        Me.GroupBox1.Controls.Add(Me.LstClass)
        Me.GroupBox1.Controls.Add(Me.CboClassing)
        Me.GroupBox1.Controls.Add(Me.txtClassRefno)
        Me.GroupBox1.Controls.Add(Me.ChckClassing)
        Me.GroupBox1.Controls.Add(Me.rdPerBrand)
        Me.GroupBox1.Controls.Add(Me.rdPerInvoice)
        Me.GroupBox1.Location = New System.Drawing.Point(7, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(182, 131)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Type"
        '
        'ChkSubClasses
        '
        Me.ChkSubClasses.AutoSize = True
        Me.ChkSubClasses.BackColor = System.Drawing.Color.Transparent
        Me.ChkSubClasses.Checked = True
        Me.ChkSubClasses.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ChkSubClasses.Enabled = False
        Me.ChkSubClasses.Location = New System.Drawing.Point(43, 103)
        Me.ChkSubClasses.Name = "ChkSubClasses"
        Me.ChkSubClasses.Size = New System.Drawing.Size(139, 17)
        Me.ChkSubClasses.TabIndex = 15
        Me.ChkSubClasses.Text = "Include Sub classes"
        Me.ChkSubClasses.UseVisualStyleBackColor = False
        Me.ChkSubClasses.Visible = False
        '
        'LstClass
        '
        Me.LstClass.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ClassName, Me.ClassRefNo})
        Me.LstClass.Location = New System.Drawing.Point(22, 189)
        Me.LstClass.Name = "LstClass"
        Me.LstClass.Size = New System.Drawing.Size(121, 65)
        Me.LstClass.TabIndex = 13
        Me.LstClass.UseCompatibleStateImageBehavior = False
        Me.LstClass.View = System.Windows.Forms.View.Details
        Me.LstClass.Visible = False
        '
        'ClassName
        '
        Me.ClassName.Text = "ClassName"
        Me.ClassName.Width = 98
        '
        'ClassRefNo
        '
        Me.ClassRefNo.Text = "ClassRefNo"
        '
        'CboClassing
        '
        Me.CboClassing.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.CboClassing.ArrowColor = System.Drawing.Color.Black
        Me.CboClassing.BindedControl = CType(resources.GetObject("CboClassing.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.CboClassing.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.CboClassing.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.CboClassing.ColumnNum = 2
        Me.CboClassing.ColumnWidth = "200;1"
        Me.CboClassing.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.CboClassing.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.CboClassing.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.CboClassing.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.CboClassing.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.CboClassing.DisplayMember = "Text"
        Me.CboClassing.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CboClassing.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.CboClassing.DropDownForeColor = System.Drawing.Color.Black
        Me.CboClassing.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.CboClassing.DropDownWidth = 201
        Me.CboClassing.Enabled = False
        Me.CboClassing.GridLineColor = System.Drawing.Color.LightGray
        Me.CboClassing.GridLineHorizontal = False
        Me.CboClassing.GridLineVertical = False
        Me.CboClassing.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.CboClassing.Location = New System.Drawing.Point(22, 75)
        Me.CboClassing.ManagingFastMouseMoving = True
        Me.CboClassing.ManagingFastMouseMovingInterval = 30
        Me.CboClassing.Name = "CboClassing"
        Me.CboClassing.SelectedItem = Nothing
        Me.CboClassing.SelectedValue = Nothing
        Me.CboClassing.Size = New System.Drawing.Size(154, 22)
        Me.CboClassing.TabIndex = 8
        '
        'txtClassRefno
        '
        Me.txtClassRefno.Enabled = False
        Me.txtClassRefno.Location = New System.Drawing.Point(22, 162)
        Me.txtClassRefno.Name = "txtClassRefno"
        Me.txtClassRefno.Size = New System.Drawing.Size(143, 21)
        Me.txtClassRefno.TabIndex = 7
        Me.txtClassRefno.Visible = False
        '
        'ChckClassing
        '
        Me.ChckClassing.AutoSize = True
        Me.ChckClassing.Location = New System.Drawing.Point(22, 58)
        Me.ChckClassing.Name = "ChckClassing"
        Me.ChckClassing.Size = New System.Drawing.Size(131, 17)
        Me.ChckClassing.TabIndex = 5
        Me.ChckClassing.Text = "Group by Classing"
        Me.ChckClassing.UseVisualStyleBackColor = True
        Me.ChckClassing.Visible = False
        '
        'rdPerBrand
        '
        Me.rdPerBrand.AutoSize = True
        Me.rdPerBrand.Location = New System.Drawing.Point(6, 14)
        Me.rdPerBrand.Name = "rdPerBrand"
        Me.rdPerBrand.Size = New System.Drawing.Size(82, 17)
        Me.rdPerBrand.TabIndex = 4
        Me.rdPerBrand.Text = "Per Brand"
        Me.rdPerBrand.UseVisualStyleBackColor = True
        '
        'ListViewScope
        '
        Me.ListViewScope.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ClassRealName, Me.ClassExRefno})
        Me.ListViewScope.Location = New System.Drawing.Point(7, 164)
        Me.ListViewScope.Name = "ListViewScope"
        Me.ListViewScope.Size = New System.Drawing.Size(199, 97)
        Me.ListViewScope.TabIndex = 14
        Me.ListViewScope.UseCompatibleStateImageBehavior = False
        Me.ListViewScope.View = System.Windows.Forms.View.Details
        Me.ListViewScope.Visible = False
        '
        'ClassRealName
        '
        Me.ClassRealName.Text = "ClassRealName"
        Me.ClassRealName.Width = 112
        '
        'ClassExRefno
        '
        Me.ClassExRefno.Text = "Refno"
        '
        'cboMonth
        '
        Me.cboMonth.FormattingEnabled = True
        Me.cboMonth.Items.AddRange(New Object() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"})
        Me.cboMonth.Location = New System.Drawing.Point(244, 12)
        Me.cboMonth.Name = "cboMonth"
        Me.cboMonth.Size = New System.Drawing.Size(187, 21)
        Me.cboMonth.TabIndex = 5
        '
        'cboYear
        '
        Me.cboYear.FormattingEnabled = True
        Me.cboYear.Items.AddRange(New Object() {"2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020"})
        Me.cboYear.Location = New System.Drawing.Point(244, 44)
        Me.cboYear.Name = "cboYear"
        Me.cboYear.Size = New System.Drawing.Size(118, 21)
        Me.cboYear.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(200, 47)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Year"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(197, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Brand"
        '
        'cboBrand
        '
        Me.cboBrand.Enabled = False
        Me.cboBrand.FormattingEnabled = True
        Me.cboBrand.Items.AddRange(New Object() {"Havaianas", "David & Goliath", "T-Box", "Cia Maritima", "Pininho"})
        Me.cboBrand.Location = New System.Drawing.Point(244, 78)
        Me.cboBrand.Name = "cboBrand"
        Me.cboBrand.Size = New System.Drawing.Size(187, 21)
        Me.cboBrand.TabIndex = 9
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(344, 105)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(87, 28)
        Me.btnCancel.TabIndex = 10
        Me.btnCancel.Text = "Close"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frmSalesRpt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(438, 141)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.ListViewScope)
        Me.Controls.Add(Me.cboBrand)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cboYear)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboMonth)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(426, 175)
        Me.Name = "frmSalesRpt"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Sales Report"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents rdPerInvoice As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rdPerBrand As System.Windows.Forms.RadioButton
    Friend WithEvents cboMonth As System.Windows.Forms.ComboBox
    Friend WithEvents cboYear As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboBrand As System.Windows.Forms.ComboBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents ChckClassing As System.Windows.Forms.CheckBox
    Friend WithEvents CboClassing As MTGCComboBox
    Friend WithEvents txtClassRefno As System.Windows.Forms.TextBox
    Friend WithEvents LstClass As System.Windows.Forms.ListView
    Friend WithEvents ClassName As System.Windows.Forms.ColumnHeader
    Friend WithEvents ClassRefNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents ListViewScope As System.Windows.Forms.ListView
    Friend WithEvents ClassRealName As System.Windows.Forms.ColumnHeader
    Friend WithEvents ClassExRefno As System.Windows.Forms.ColumnHeader
    Friend WithEvents ChkSubClasses As System.Windows.Forms.CheckBox
End Class
