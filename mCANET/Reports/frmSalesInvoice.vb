﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Public Class frmSalesInvoice
    Private con As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private Sub frmSalesInvoice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        rptsummary.Close()
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Private Sub LoadReport()
        If rbDaily.Checked = True Then
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\SalesInvoice.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@coid", gCompanyID())
            rptsummary.SetParameterValue("@fdTransDate", dtp1.Text)
            rptsummary.SetParameterValue("@coid", gCompanyID(), "SalesInvoiceDRCR")
            rptsummary.SetParameterValue("@fdTransDate", dtp1.Text, "SalesInvoiceDRCR")
        Else
            rptsummary.Load(Application.StartupPath & "\Accounting Reports\SalesJournalMonthly.rpt")
            Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@coid", gCompanyID())
            rptsummary.SetParameterValue("@fdTransDate", dtp1.Text)
            rptsummary.SetParameterValue("@coid", gCompanyID(), "SJMonthly")
            rptsummary.SetParameterValue("@fdTransDate", dtp1.Text, "SJMonthly")
        End If
        
    End Sub

    Private Sub bgwProcessReport_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub PreviewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviewToolStripMenuItem.Click
        picLoading.Visible = True
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub

    Private Sub RBDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbDaily.CheckedChanged
        If RBDaily.Checked = True Then
            dtp1.Enabled = True
            RBMonthly.Checked = False
            dtp2.Enabled = False
        End If
    End Sub

    Private Sub RBMonthly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RBMonthly.CheckedChanged
        If RBMonthly.Checked = True Then
            RBDaily.Checked = False
            dtp1.Enabled = False
            dtp2.Enabled = True
        End If
    End Sub
End Class