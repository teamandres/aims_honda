﻿Imports System.Windows.Forms
Imports System.Data.SqlClient
Imports System.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports Microsoft.Reporting.WinForms
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports System.IO
Imports CrystalDecisions.Shared
Public Class frmInterestReport
    Private con As New Clsappconfiguration
    Private Sub frmInterestReport_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        AuditTrail_Save("INTEREST COMPUTATION", "Preview Interest Distribution Report")
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
        'Me.RptVwer.RefreshReport()
        'LoadReport()
    End Sub

    'Private Sub LoadReport()
    '    RptVwer.SetDisplayMode(DisplayMode.PrintLayout)
    '    RptVwer.ZoomMode = ZoomMode.Percent
    '    RptVwer.ZoomPercent = 100

    '    RptVwer.Reset()
    '    RptVwer.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local
    '    RptVwer.LocalReport.ReportPath = Environment.CurrentDirectory + "\Accounting Reports\SSRS Reports\InterestDistribution.rdl"

    '    'Dim paramList As New Generic.List(Of ReportParameter)

    '    'paramList.Add(New ReportParameter("startDate", "4/1/2014", False))
    '    'paramList.Add(New ReportParameter("ENDDate", "6/30/2014", False))
    '    'paramList.Add(New ReportParameter("Rate", 0.03, False))
    '    'paramList.Add(New ReportParameter("acntName", "Regular Savings", False))
    '    'paramList.Add(New ReportParameter("minimumBal", 500, False))
    '    'RptVwer.LocalReport.SetParameters(paramList)

    '    Dim dsSummary As DataSet = PrintDividend()
    '    Dim rDs As ReportDataSource = New ReportDataSource("DataSet1", dsSummary.Tables(0))
    '    RptVwer.LocalReport.DataSources.Clear()
    '    RptVwer.LocalReport.DataSources.Add(rDs)
    '    RptVwer.ServerReport.Refresh()
    '    RptVwer.RefreshReport()
    'End Sub

    Private Sub CloseToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private rptsummary As New ReportDocument

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Sub LoadReport()
        rptsummary.Load(Application.StartupPath & "\Accounting Reports\InterestDistribution.rpt")
        'rptsummary.Load(Application.StartupPath & "\Accounting Reports\SubReportCDJDaily.rpt")
        Logon(rptsummary, con.Server, con.Database, con.Username, Clsappconfiguration.GetDecryptedData(con.Password))
        rptsummary.Refresh()
    End Sub
    Private Sub bgwProcessReport_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.CrystalReportViewer1.Visible = True
        Me.CrystalReportViewer1.ReportSource = rptsummary
        'picLoading.Visible = False
    End Sub
End Class