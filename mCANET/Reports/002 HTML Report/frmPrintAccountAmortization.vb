﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmPrintAccountAmortization

    Private mycon As New Clsappconfiguration

    Private strWriter As String
    Public refNo As String
    Public AccountUsed As String

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub HeaderDetails()
        strWriter += "<table><tr>"
        strWriter += "<td><b>ID No. :</b></td><td><small>" + frmVerification.lblIDno.Text + "</small></td></tr>"
        strWriter += "<tr><td><b>Name :</b></td><td><small>" + frmVerification.lblClientname.Text + "</small></td></tr>"
        strWriter += "<tr><td><b>Account Title:</b></td><td><small>" + AccountUsed + "</small></td></tr>"
        strWriter += "<tr><td><b>Ref. No :</b></td><td><small>" + refNo + "</small></td></tr>"
        strWriter += "</table>"
    End Sub

    '============ Load Account Amortization @vince 7-15-15 8:00pm =======================
    Private Sub LoadAccount_Amortization(ByVal refno As String)
        Try
            'Load CSS
            strWriter += "<head><style type='text/css'>b{ font-size:12px; } small{ font-size:12px; } </style></head>"

            'Load Header
            strWriter += "<body style='font-family:verdana;padding:3px;'><center><h3 style='color:#2983a6'>Account Amortization Schedule</h3></center><br><br><hr>"

            HeaderDetails()
            PrepareAccountColumns(refno)
            Dim xRow As Integer = 0
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "_Display_AccountDynamicAmortization",
                                                              New SqlParameter("@refNo", refno))
            If rd.HasRows Then
                While rd.Read
                    Dim xCol As Integer = 0 'starting columns 0,1 is default
                    Dim xColTotal As Decimal = 0
                    strWriter += "<tr>" 'generate new row
                    strWriter += "<td><small>" + rd(0).ToString + "</small></td>"
                    strWriter += "<td><small>" + CDate(rd(1)).ToString("MMMM dd,yyyy") + "</small></td>"
                    'rd(2)
                    Dim colval As String() = rd(3).Split(New Char() {","c})
                    Dim cv As String
                    xCol = 2 'starting index 0,1 is used by payno and date
                    For Each cv In colval
                        strWriter += "<td><small>" + Format(CDec(cv), "##,##0.00") + "</small></td>"
                        xColTotal += CDec(cv)
                        xCol += 1
                    Next
                    strWriter += "<td><small>" + Format(xColTotal, "##,##0.00") + "</small></td></tr>"
                    xRow += 1 'add row
                End While
                strWriter += "</table></body>"
            End If
            webWriter.DocumentText = strWriter
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub PrepareAccountColumns(ByVal key As String)
        Try

            Dim xIndex As Integer = 0 'use as column name index
            strWriter += "<table border='1'><tr><td style='background-color:#2983a6;color:white;'><b>Pay No.</b></td><td style='background-color:#2983a6;color:white;'><b>Date</b></td>"

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "_Display_AccountDynamicColumns",
                                                              New SqlParameter("@refNo", key))
            If rd.HasRows Then
                While rd.Read
                    strWriter += "<td style='background-color:#2983a6;color:white;'><b>" + rd(0).ToString + "</b></td>"
                    xIndex += 1
                End While
            End If
            strWriter += "<td style='background-color:#2983a6;color:white;'><b>Total</b></td></tr>"
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    '====================================================================

    Private Sub frmPrintAccountAmortization_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        strWriter = ""
        LoadAccount_Amortization(refNo)
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        webWriter.ShowPrintPreviewDialog()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        webWriter.ShowPrintDialog()
    End Sub

End Class