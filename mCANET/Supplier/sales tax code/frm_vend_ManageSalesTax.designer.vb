<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_vend_ManageSalesTax
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_vend_ManageSalesTax))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnPreference = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.btnPay = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnTaxRevSummary = New System.Windows.Forms.Button
        Me.btnTaxLiabilityReport = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.linkView = New System.Windows.Forms.LinkLabel
        Me.linkOpen = New System.Windows.Forms.LinkLabel
        Me.linkAdjust = New System.Windows.Forms.LinkLabel
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnPreference)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(401, 71)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Get Started"
        '
        'btnPreference
        '
        Me.btnPreference.Location = New System.Drawing.Point(28, 33)
        Me.btnPreference.Name = "btnPreference"
        Me.btnPreference.Size = New System.Drawing.Size(94, 23)
        Me.btnPreference.TabIndex = 1
        Me.btnPreference.Text = "Preferences"
        Me.btnPreference.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(25, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(151, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Setup sales tax collection"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnPay)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Panel1)
        Me.GroupBox2.Controls.Add(Me.btnTaxRevSummary)
        Me.GroupBox2.Controls.Add(Me.btnTaxLiabilityReport)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 89)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(401, 147)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Pay Sales Tax"
        '
        'btnPay
        '
        Me.btnPay.Location = New System.Drawing.Point(27, 116)
        Me.btnPay.Name = "btnPay"
        Me.btnPay.Size = New System.Drawing.Size(95, 23)
        Me.btnPay.TabIndex = 9
        Me.btnPay.Text = "Pay Sales Tax"
        Me.btnPay.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(24, 100)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(263, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Pay your sales tax suppliers in CS-Accounting"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Location = New System.Drawing.Point(28, 88)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(358, 4)
        Me.Panel1.TabIndex = 7
        '
        'btnTaxRevSummary
        '
        Me.btnTaxRevSummary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTaxRevSummary.Image = CType(resources.GetObject("btnTaxRevSummary.Image"), System.Drawing.Image)
        Me.btnTaxRevSummary.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnTaxRevSummary.Location = New System.Drawing.Point(190, 58)
        Me.btnTaxRevSummary.Name = "btnTaxRevSummary"
        Me.btnTaxRevSummary.Size = New System.Drawing.Size(182, 23)
        Me.btnTaxRevSummary.TabIndex = 6
        Me.btnTaxRevSummary.Text = "Sales Tax Revenue Summary"
        Me.btnTaxRevSummary.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnTaxRevSummary.UseVisualStyleBackColor = True
        '
        'btnTaxLiabilityReport
        '
        Me.btnTaxLiabilityReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTaxLiabilityReport.Image = CType(resources.GetObject("btnTaxLiabilityReport.Image"), System.Drawing.Image)
        Me.btnTaxLiabilityReport.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.btnTaxLiabilityReport.Location = New System.Drawing.Point(28, 58)
        Me.btnTaxLiabilityReport.Name = "btnTaxLiabilityReport"
        Me.btnTaxLiabilityReport.Size = New System.Drawing.Size(123, 23)
        Me.btnTaxLiabilityReport.TabIndex = 5
        Me.btnTaxLiabilityReport.Text = "Sales Tax Liability"
        Me.btnTaxLiabilityReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnTaxLiabilityReport.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(25, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(297, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Run the following reports to help fill out your sales tax forms"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(25, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(142, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Prepare sales tax forms"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(37, 250)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(74, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Related Tasks"
        '
        'linkView
        '
        Me.linkView.AutoSize = True
        Me.linkView.Location = New System.Drawing.Point(55, 263)
        Me.linkView.Name = "linkView"
        Me.linkView.Size = New System.Drawing.Size(108, 13)
        Me.linkView.TabIndex = 3
        Me.linkView.TabStop = True
        Me.linkView.Text = "View Sales Tax Items"
        '
        'linkOpen
        '
        Me.linkOpen.AutoSize = True
        Me.linkOpen.Location = New System.Drawing.Point(55, 283)
        Me.linkOpen.Name = "linkOpen"
        Me.linkOpen.Size = New System.Drawing.Size(129, 13)
        Me.linkOpen.TabIndex = 4
        Me.linkOpen.TabStop = True
        Me.linkOpen.Text = "Open Sales Tax Code List"
        '
        'linkAdjust
        '
        Me.linkAdjust.AutoSize = True
        Me.linkAdjust.Location = New System.Drawing.Point(55, 304)
        Me.linkAdjust.Name = "linkAdjust"
        Me.linkAdjust.Size = New System.Drawing.Size(109, 13)
        Me.linkAdjust.TabIndex = 5
        Me.linkAdjust.TabStop = True
        Me.linkAdjust.Text = "Adjust Sales Tax Due"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(338, 325)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 11
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(260, 325)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 10
        Me.btnOK.Text = "&OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'frm_vend_ManageSalesTax
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(425, 360)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.linkAdjust)
        Me.Controls.Add(Me.linkOpen)
        Me.Controls.Add(Me.linkView)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frm_vend_ManageSalesTax"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Manage Sales Tax"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnPreference As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnTaxLiabilityReport As System.Windows.Forms.Button
    Friend WithEvents btnTaxRevSummary As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnPay As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents linkView As System.Windows.Forms.LinkLabel
    Friend WithEvents linkOpen As System.Windows.Forms.LinkLabel
    Friend WithEvents linkAdjust As System.Windows.Forms.LinkLabel
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
End Class
