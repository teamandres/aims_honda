Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frm_vend_masterVendorNote
    Private gCOn As New Clsappconfiguration
    Private sKeySupplier As String

    Public Property KeySupplier() As String
        Get
            Return sKeySupplier
        End Get
        Set(ByVal value As String)
            sKeySupplier = value
        End Set
    End Property

    Private Sub noteEdit()
        Dim sSQLCmd As String = "UPDATE mSupplier00Master SET fcNote = '" & txtNote.Text & "'"
        sSQLCmd &= " WHERE fxKeySupplier ='" & sKeySupplier & "'"

        Try
            SqlHelper.ExecuteDataset(gCOn.cnstring, CommandType.Text, sSQLCmd)
            MessageBox.Show("Record successfully updated!", "Edit Note")
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Edit Note")
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        noteEdit()
        m_supplierList(frm_vend_masterVendor.grdSupplier1, frm_vend_masterVendor.cboView.SelectedItem, frm_vend_masterVendor.cbofilter.ToString())
        frm_vend_masterVendor.loadSuppplierInfo()
        Me.Close()
    End Sub

    Private Sub btnDateStamp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDateStamp.Click
        txtNote.Text = txtNote.Text & vbCrLf & Format(Now, "MM/dd/yyyy") & ": "
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frm_vend_masterVendorNote_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtNote.Text = ""
        If frm_vend_masterVendor.txtNotes.Text <> "" Then
            loadNotes()
        End If
    End Sub

    Private Sub loadNotes()
        Dim sSQLCmd As String = "Select fcNote FROM mSupplier00Master WHERE fxKeySupplier ='" & sKeySupplier & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCOn.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    txtNote.Text = rd.Item("fcNote")
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(Err.Description, "Load Notes")
        End Try
    End Sub
End Class