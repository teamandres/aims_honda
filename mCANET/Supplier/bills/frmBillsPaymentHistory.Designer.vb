<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBillsPaymentHistory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim lblSupplier As System.Windows.Forms.Label
        Dim lblAPVNo As System.Windows.Forms.Label
        Dim lblAmountDue As System.Windows.Forms.Label
        Dim lblBalance As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBillsPaymentHistory))
        Me.panelTop = New System.Windows.Forms.Panel
        Me.txtbalance = New System.Windows.Forms.TextBox
        Me.txtAmountDue = New System.Windows.Forms.TextBox
        Me.txtRefNo = New System.Windows.Forms.TextBox
        Me.txtSupplier = New System.Windows.Forms.TextBox
        Me.grdPaymentHistory = New System.Windows.Forms.DataGridView
        Me.PanelBottom = New System.Windows.Forms.Panel
        Me.btnClose = New System.Windows.Forms.Button
        lblSupplier = New System.Windows.Forms.Label
        lblAPVNo = New System.Windows.Forms.Label
        lblAmountDue = New System.Windows.Forms.Label
        lblBalance = New System.Windows.Forms.Label
        Me.panelTop.SuspendLayout()
        CType(Me.grdPaymentHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblSupplier
        '
        lblSupplier.AutoSize = True
        lblSupplier.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblSupplier.Location = New System.Drawing.Point(37, 9)
        lblSupplier.Name = "lblSupplier"
        lblSupplier.Size = New System.Drawing.Size(56, 15)
        lblSupplier.TabIndex = 0
        lblSupplier.Text = "Supplier:"
        '
        'lblAPVNo
        '
        lblAPVNo.AutoSize = True
        lblAPVNo.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblAPVNo.Location = New System.Drawing.Point(42, 35)
        lblAPVNo.Name = "lblAPVNo"
        lblAPVNo.Size = New System.Drawing.Size(51, 15)
        lblAPVNo.TabIndex = 1
        lblAPVNo.Text = "Ref No.:"
        '
        'lblAmountDue
        '
        lblAmountDue.AutoSize = True
        lblAmountDue.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblAmountDue.Location = New System.Drawing.Point(12, 60)
        lblAmountDue.Name = "lblAmountDue"
        lblAmountDue.Size = New System.Drawing.Size(81, 15)
        lblAmountDue.TabIndex = 2
        lblAmountDue.Text = "Amount Due:"
        '
        'lblBalance
        '
        lblBalance.AutoSize = True
        lblBalance.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        lblBalance.Location = New System.Drawing.Point(273, 59)
        lblBalance.Name = "lblBalance"
        lblBalance.Size = New System.Drawing.Size(52, 15)
        lblBalance.TabIndex = 6
        lblBalance.Text = "Balance:"
        '
        'panelTop
        '
        Me.panelTop.Controls.Add(Me.txtbalance)
        Me.panelTop.Controls.Add(lblBalance)
        Me.panelTop.Controls.Add(Me.txtAmountDue)
        Me.panelTop.Controls.Add(Me.txtRefNo)
        Me.panelTop.Controls.Add(Me.txtSupplier)
        Me.panelTop.Controls.Add(lblAmountDue)
        Me.panelTop.Controls.Add(lblAPVNo)
        Me.panelTop.Controls.Add(lblSupplier)
        Me.panelTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelTop.Location = New System.Drawing.Point(0, 0)
        Me.panelTop.Name = "panelTop"
        Me.panelTop.Size = New System.Drawing.Size(499, 85)
        Me.panelTop.TabIndex = 0
        '
        'txtbalance
        '
        Me.txtbalance.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbalance.Location = New System.Drawing.Point(331, 57)
        Me.txtbalance.Name = "txtbalance"
        Me.txtbalance.ReadOnly = True
        Me.txtbalance.Size = New System.Drawing.Size(156, 23)
        Me.txtbalance.TabIndex = 7
        Me.txtbalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAmountDue
        '
        Me.txtAmountDue.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountDue.Location = New System.Drawing.Point(99, 59)
        Me.txtAmountDue.Name = "txtAmountDue"
        Me.txtAmountDue.ReadOnly = True
        Me.txtAmountDue.Size = New System.Drawing.Size(156, 23)
        Me.txtAmountDue.TabIndex = 5
        Me.txtAmountDue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRefNo
        '
        Me.txtRefNo.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRefNo.Location = New System.Drawing.Point(99, 35)
        Me.txtRefNo.Name = "txtRefNo"
        Me.txtRefNo.ReadOnly = True
        Me.txtRefNo.Size = New System.Drawing.Size(156, 23)
        Me.txtRefNo.TabIndex = 4
        '
        'txtSupplier
        '
        Me.txtSupplier.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSupplier.Location = New System.Drawing.Point(99, 9)
        Me.txtSupplier.Name = "txtSupplier"
        Me.txtSupplier.ReadOnly = True
        Me.txtSupplier.Size = New System.Drawing.Size(156, 23)
        Me.txtSupplier.TabIndex = 3
        '
        'grdPaymentHistory
        '
        Me.grdPaymentHistory.AllowUserToAddRows = False
        Me.grdPaymentHistory.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.YellowGreen
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.grdPaymentHistory.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.grdPaymentHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdPaymentHistory.BackgroundColor = System.Drawing.Color.White
        Me.grdPaymentHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdPaymentHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdPaymentHistory.Location = New System.Drawing.Point(0, 85)
        Me.grdPaymentHistory.Name = "grdPaymentHistory"
        Me.grdPaymentHistory.ReadOnly = True
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.YellowGreen
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdPaymentHistory.RowHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.grdPaymentHistory.RowHeadersVisible = False
        Me.grdPaymentHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdPaymentHistory.ShowEditingIcon = False
        Me.grdPaymentHistory.Size = New System.Drawing.Size(499, 216)
        Me.grdPaymentHistory.TabIndex = 0
        '
        'PanelBottom
        '
        Me.PanelBottom.Controls.Add(Me.btnClose)
        Me.PanelBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelBottom.Location = New System.Drawing.Point(0, 301)
        Me.PanelBottom.Name = "PanelBottom"
        Me.PanelBottom.Size = New System.Drawing.Size(499, 37)
        Me.PanelBottom.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.button_cancel
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(417, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(79, 29)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmBillsPaymentHistory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(499, 338)
        Me.Controls.Add(Me.grdPaymentHistory)
        Me.Controls.Add(Me.PanelBottom)
        Me.Controls.Add(Me.panelTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmBillsPaymentHistory"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Payment History"
        Me.panelTop.ResumeLayout(False)
        Me.panelTop.PerformLayout()
        CType(Me.grdPaymentHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents panelTop As System.Windows.Forms.Panel
    Friend WithEvents txtbalance As System.Windows.Forms.TextBox
    Friend WithEvents txtAmountDue As System.Windows.Forms.TextBox
    Friend WithEvents txtRefNo As System.Windows.Forms.TextBox
    Friend WithEvents txtSupplier As System.Windows.Forms.TextBox
    Friend WithEvents grdPaymentHistory As System.Windows.Forms.DataGridView
    Friend WithEvents PanelBottom As System.Windows.Forms.Panel
    Friend WithEvents btnClose As System.Windows.Forms.Button
End Class
