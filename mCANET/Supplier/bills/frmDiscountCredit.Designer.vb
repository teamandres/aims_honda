<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDiscountCredit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.tbDiscount = New System.Windows.Forms.TabPage
        Me.tbsCredit = New System.Windows.Forms.TabPage
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtDiscount = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.cSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.cDates = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cRefNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cCreditAmt = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cAmtUse = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cCreditBal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.TabControl1.SuspendLayout()
        Me.tbDiscount.SuspendLayout()
        Me.tbsCredit.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tbDiscount)
        Me.TabControl1.Controls.Add(Me.tbsCredit)
        Me.TabControl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(9, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(473, 212)
        Me.TabControl1.TabIndex = 0
        '
        'tbDiscount
        '
        Me.tbDiscount.Controls.Add(Me.ComboBox1)
        Me.tbDiscount.Controls.Add(Me.TextBox5)
        Me.tbDiscount.Controls.Add(Me.Label6)
        Me.tbDiscount.Controls.Add(Me.Label5)
        Me.tbDiscount.Controls.Add(Me.TextBox4)
        Me.tbDiscount.Controls.Add(Me.TextBox3)
        Me.tbDiscount.Controls.Add(Me.Label4)
        Me.tbDiscount.Controls.Add(Me.Label3)
        Me.tbDiscount.Controls.Add(Me.txtDiscount)
        Me.tbDiscount.Controls.Add(Me.Label2)
        Me.tbDiscount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbDiscount.Location = New System.Drawing.Point(4, 22)
        Me.tbDiscount.Name = "tbDiscount"
        Me.tbDiscount.Padding = New System.Windows.Forms.Padding(3)
        Me.tbDiscount.Size = New System.Drawing.Size(465, 186)
        Me.tbDiscount.TabIndex = 0
        Me.tbDiscount.Text = "Discount"
        Me.tbDiscount.UseVisualStyleBackColor = True
        '
        'tbsCredit
        '
        Me.tbsCredit.Controls.Add(Me.btnClear)
        Me.tbsCredit.Controls.Add(Me.DataGridView1)
        Me.tbsCredit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbsCredit.Location = New System.Drawing.Point(4, 22)
        Me.tbsCredit.Name = "tbsCredit"
        Me.tbsCredit.Padding = New System.Windows.Forms.Padding(3)
        Me.tbsCredit.Size = New System.Drawing.Size(465, 186)
        Me.tbsCredit.TabIndex = 1
        Me.tbsCredit.Text = "Credits"
        Me.tbsCredit.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Discount Date"
        '
        'txtDiscount
        '
        Me.txtDiscount.Location = New System.Drawing.Point(117, 23)
        Me.txtDiscount.Name = "txtDiscount"
        Me.txtDiscount.Size = New System.Drawing.Size(121, 21)
        Me.txtDiscount.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 53)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(36, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Terms"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 80)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(102, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Suggested Discount"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(117, 50)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(121, 21)
        Me.TextBox3.TabIndex = 6
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(117, 77)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(121, 21)
        Me.TextBox4.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 107)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(101, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Amount of Discount"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 134)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(90, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Discount Account"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(117, 104)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(121, 21)
        Me.TextBox5.TabIndex = 10
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(117, 131)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(204, 21)
        Me.ComboBox1.TabIndex = 11
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cSelect, Me.cDates, Me.cRefNo, Me.cCreditAmt, Me.cAmtUse, Me.cCreditBal})
        Me.DataGridView1.Location = New System.Drawing.Point(6, 26)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(453, 130)
        Me.DataGridView1.TabIndex = 0
        '
        'cSelect
        '
        Me.cSelect.HeaderText = "Select"
        Me.cSelect.Name = "cSelect"
        Me.cSelect.Width = 50
        '
        'cDates
        '
        Me.cDates.HeaderText = "Date"
        Me.cDates.Name = "cDates"
        Me.cDates.Width = 60
        '
        'cRefNo
        '
        Me.cRefNo.HeaderText = "cRefno"
        Me.cRefNo.Name = "cRefNo"
        Me.cRefNo.Width = 60
        '
        'cCreditAmt
        '
        Me.cCreditAmt.HeaderText = "Credit Amt."
        Me.cCreditAmt.Name = "cCreditAmt"
        Me.cCreditAmt.Width = 80
        '
        'cAmtUse
        '
        Me.cAmtUse.HeaderText = "Amt. To Use"
        Me.cAmtUse.Name = "cAmtUse"
        Me.cAmtUse.Width = 80
        '
        'cCreditBal
        '
        Me.cCreditBal.HeaderText = "Credit Balance"
        Me.cCreditBal.Name = "cCreditBal"
        Me.cCreditBal.Width = 80
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(366, 160)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(93, 23)
        Me.btnClear.TabIndex = 1
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(283, 230)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(93, 23)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(379, 230)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(93, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frmDiscountCredit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(488, 258)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.TabControl1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDiscountCredit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Discount & Credit"
        Me.TabControl1.ResumeLayout(False)
        Me.tbDiscount.ResumeLayout(False)
        Me.tbDiscount.PerformLayout()
        Me.tbsCredit.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tbDiscount As System.Windows.Forms.TabPage
    Friend WithEvents tbsCredit As System.Windows.Forms.TabPage
    Friend WithEvents txtDiscount As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents cSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents cDates As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cRefNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cCreditAmt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cAmtUse As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cCreditBal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
