<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_vend_CreatePurchaseOrder
    Inherits System.Windows.Forms.Form


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_vend_CreatePurchaseOrder))
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboSupplierName = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtSupplierAddress = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.dtDate = New System.Windows.Forms.DateTimePicker
        Me.txtPONo = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtShipAddress = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtSupplierMessage = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.chkPrint = New System.Windows.Forms.CheckBox
        Me.chkEmail = New System.Windows.Forms.CheckBox
        Me.txtMemo = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.cboSupplierID = New System.Windows.Forms.ComboBox
        Me.chkClose = New System.Windows.Forms.CheckBox
        Me.grdPO = New System.Windows.Forms.DataGridView
        Me.MtcboShipname = New MTGCComboBox
        Me.MtcboShipAddress = New MTGCComboBox
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnPreview = New System.Windows.Forms.Button
        Me.btnTrigger = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.TransLbl = New CSAcctg.modTransparentLabel
        CType(Me.grdPO, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Supplier"
        '
        'cboSupplierName
        '
        Me.cboSupplierName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboSupplierName.FormattingEnabled = True
        Me.cboSupplierName.Location = New System.Drawing.Point(59, 7)
        Me.cboSupplierName.Name = "cboSupplierName"
        Me.cboSupplierName.Size = New System.Drawing.Size(172, 21)
        Me.cboSupplierName.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Ship To"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(2, 45)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(156, 23)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Purchase Order"
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.BackColor = System.Drawing.Color.DarkBlue
        Me.Label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label4.Location = New System.Drawing.Point(6, 77)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(225, 25)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Supplier"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSupplierAddress
        '
        Me.txtSupplierAddress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSupplierAddress.Location = New System.Drawing.Point(6, 102)
        Me.txtSupplierAddress.Multiline = True
        Me.txtSupplierAddress.Name = "txtSupplierAddress"
        Me.txtSupplierAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSupplierAddress.Size = New System.Drawing.Size(225, 68)
        Me.txtSupplierAddress.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.BackColor = System.Drawing.Color.DarkBlue
        Me.Label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label5.Location = New System.Drawing.Point(16, 33)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 15)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Date"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.BackColor = System.Drawing.Color.DarkBlue
        Me.Label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label6.Location = New System.Drawing.Point(131, 33)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(105, 15)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "P.O. No."
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'dtDate
        '
        Me.dtDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtDate.Location = New System.Drawing.Point(16, 47)
        Me.dtDate.Name = "dtDate"
        Me.dtDate.Size = New System.Drawing.Size(109, 21)
        Me.dtDate.TabIndex = 10
        '
        'txtPONo
        '
        Me.txtPONo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPONo.Location = New System.Drawing.Point(131, 47)
        Me.txtPONo.Name = "txtPONo"
        Me.txtPONo.Size = New System.Drawing.Size(105, 21)
        Me.txtPONo.TabIndex = 11
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.BackColor = System.Drawing.Color.DarkBlue
        Me.Label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label7.Location = New System.Drawing.Point(16, 77)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(220, 25)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Ship To"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtShipAddress
        '
        Me.txtShipAddress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtShipAddress.Location = New System.Drawing.Point(16, 102)
        Me.txtShipAddress.Multiline = True
        Me.txtShipAddress.Name = "txtShipAddress"
        Me.txtShipAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtShipAddress.Size = New System.Drawing.Size(220, 68)
        Me.txtShipAddress.TabIndex = 13
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.Location = New System.Drawing.Point(3, 304)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(155, 24)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Supplier Message"
        '
        'txtSupplierMessage
        '
        Me.txtSupplierMessage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtSupplierMessage.Location = New System.Drawing.Point(6, 318)
        Me.txtSupplierMessage.Multiline = True
        Me.txtSupplierMessage.Name = "txtSupplierMessage"
        Me.txtSupplierMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSupplierMessage.Size = New System.Drawing.Size(298, 70)
        Me.txtSupplierMessage.TabIndex = 17
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(378, 305)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(88, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Total Amount:"
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(465, 305)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(139, 13)
        Me.lblTotal.TabIndex = 19
        Me.lblTotal.Text = "0.00"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkPrint
        '
        Me.chkPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkPrint.AutoSize = True
        Me.chkPrint.Location = New System.Drawing.Point(12, 394)
        Me.chkPrint.Name = "chkPrint"
        Me.chkPrint.Size = New System.Drawing.Size(102, 17)
        Me.chkPrint.TabIndex = 20
        Me.chkPrint.Text = "To be printed"
        Me.chkPrint.UseVisualStyleBackColor = True
        '
        'chkEmail
        '
        Me.chkEmail.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkEmail.AutoSize = True
        Me.chkEmail.Location = New System.Drawing.Point(124, 394)
        Me.chkEmail.Name = "chkEmail"
        Me.chkEmail.Size = New System.Drawing.Size(107, 17)
        Me.chkEmail.TabIndex = 21
        Me.chkEmail.Text = "To be emailed"
        Me.chkEmail.UseVisualStyleBackColor = True
        '
        'txtMemo
        '
        Me.txtMemo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMemo.Location = New System.Drawing.Point(381, 337)
        Me.txtMemo.Multiline = True
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMemo.Size = New System.Drawing.Size(221, 87)
        Me.txtMemo.TabIndex = 23
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(378, 321)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(41, 13)
        Me.Label11.TabIndex = 22
        Me.Label11.Text = "Memo"
        '
        'cboSupplierID
        '
        Me.cboSupplierID.FormattingEnabled = True
        Me.cboSupplierID.Location = New System.Drawing.Point(59, 6)
        Me.cboSupplierID.Name = "cboSupplierID"
        Me.cboSupplierID.Size = New System.Drawing.Size(169, 21)
        Me.cboSupplierID.TabIndex = 29
        Me.cboSupplierID.Visible = False
        '
        'chkClose
        '
        Me.chkClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkClose.AutoSize = True
        Me.chkClose.Location = New System.Drawing.Point(239, 394)
        Me.chkClose.Name = "chkClose"
        Me.chkClose.Size = New System.Drawing.Size(65, 17)
        Me.chkClose.TabIndex = 34
        Me.chkClose.Text = "Closed"
        Me.chkClose.UseVisualStyleBackColor = True
        '
        'grdPO
        '
        Me.grdPO.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdPO.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdPO.BackgroundColor = System.Drawing.SystemColors.Window
        Me.grdPO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdPO.GridColor = System.Drawing.Color.Black
        Me.grdPO.Location = New System.Drawing.Point(6, 193)
        Me.grdPO.Name = "grdPO"
        Me.grdPO.Size = New System.Drawing.Size(598, 105)
        Me.grdPO.TabIndex = 36
        '
        'MtcboShipname
        '
        Me.MtcboShipname.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MtcboShipname.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboShipname.ArrowColor = System.Drawing.Color.Black
        Me.MtcboShipname.BindedControl = CType(resources.GetObject("MtcboShipname.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.MtcboShipname.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.MtcboShipname.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.MtcboShipname.ColumnNum = 3
        Me.MtcboShipname.ColumnWidth = "250; 0; 100"
        Me.MtcboShipname.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboShipname.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.MtcboShipname.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.MtcboShipname.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.MtcboShipname.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.MtcboShipname.DisplayMember = "Text"
        Me.MtcboShipname.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.MtcboShipname.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.MtcboShipname.DropDownForeColor = System.Drawing.Color.Black
        Me.MtcboShipname.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.MtcboShipname.DropDownWidth = 370
        Me.MtcboShipname.GridLineColor = System.Drawing.Color.LightGray
        Me.MtcboShipname.GridLineHorizontal = False
        Me.MtcboShipname.GridLineVertical = True
        Me.MtcboShipname.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.MtcboShipname.Location = New System.Drawing.Point(67, 6)
        Me.MtcboShipname.ManagingFastMouseMoving = True
        Me.MtcboShipname.ManagingFastMouseMovingInterval = 30
        Me.MtcboShipname.MaxDropDownItems = 15
        Me.MtcboShipname.Name = "MtcboShipname"
        Me.MtcboShipname.SelectedItem = Nothing
        Me.MtcboShipname.SelectedValue = Nothing
        Me.MtcboShipname.Size = New System.Drawing.Size(169, 22)
        Me.MtcboShipname.TabIndex = 66
        '
        'MtcboShipAddress
        '
        Me.MtcboShipAddress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MtcboShipAddress.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboShipAddress.ArrowColor = System.Drawing.Color.Black
        Me.MtcboShipAddress.BindedControl = CType(resources.GetObject("MtcboShipAddress.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.MtcboShipAddress.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.MtcboShipAddress.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.MtcboShipAddress.ColumnNum = 3
        Me.MtcboShipAddress.ColumnWidth = "121; 0; 200"
        Me.MtcboShipAddress.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboShipAddress.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.MtcboShipAddress.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.MtcboShipAddress.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.MtcboShipAddress.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.MtcboShipAddress.DisplayMember = "Text"
        Me.MtcboShipAddress.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.MtcboShipAddress.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.MtcboShipAddress.DropDownForeColor = System.Drawing.Color.Black
        Me.MtcboShipAddress.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.MtcboShipAddress.DropDownWidth = 341
        Me.MtcboShipAddress.GridLineColor = System.Drawing.Color.LightGray
        Me.MtcboShipAddress.GridLineHorizontal = True
        Me.MtcboShipAddress.GridLineVertical = False
        Me.MtcboShipAddress.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.MtcboShipAddress.Location = New System.Drawing.Point(67, 79)
        Me.MtcboShipAddress.ManagingFastMouseMoving = True
        Me.MtcboShipAddress.ManagingFastMouseMovingInterval = 30
        Me.MtcboShipAddress.MaxDropDownItems = 15
        Me.MtcboShipAddress.Name = "MtcboShipAddress"
        Me.MtcboShipAddress.SelectedItem = Nothing
        Me.MtcboShipAddress.SelectedValue = Nothing
        Me.MtcboShipAddress.Size = New System.Drawing.Size(167, 22)
        Me.MtcboShipAddress.TabIndex = 66
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 1)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtSupplierAddress)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboSupplierName)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboSupplierID)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.MtcboShipAddress)
        Me.SplitContainer1.Panel2.Controls.Add(Me.dtDate)
        Me.SplitContainer1.Panel2.Controls.Add(Me.MtcboShipname)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtPONo)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtShipAddress)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label7)
        Me.SplitContainer1.Size = New System.Drawing.Size(611, 186)
        Me.SplitContainer1.SplitterDistance = 364
        Me.SplitContainer1.TabIndex = 67
        Me.SplitContainer1.TabStop = False
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(516, 430)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 30)
        Me.btnClose.TabIndex = 28
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPreview.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.Location = New System.Drawing.Point(11, 430)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(87, 30)
        Me.btnPreview.TabIndex = 24
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnTrigger
        '
        Me.btnTrigger.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnTrigger.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTrigger.Image = Global.CSAcctg.My.Resources.Resources.reload
        Me.btnTrigger.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnTrigger.Location = New System.Drawing.Point(106, 430)
        Me.btnTrigger.Name = "btnTrigger"
        Me.btnTrigger.Size = New System.Drawing.Size(87, 30)
        Me.btnTrigger.TabIndex = 37
        Me.btnTrigger.Text = "Refresh"
        Me.btnTrigger.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnTrigger.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.Location = New System.Drawing.Point(423, 430)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 30)
        Me.btnSave.TabIndex = 26
        Me.btnSave.Text = "S&ave "
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'TransLbl
        '
        Me.TransLbl.AutoSize = True
        Me.TransLbl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TransLbl.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TransLbl.ForeColor = System.Drawing.Color.Silver
        Me.TransLbl.Location = New System.Drawing.Point(0, 0)
        Me.TransLbl.Name = "TransLbl"
        Me.TransLbl.quadrantMode = False
        Me.TransLbl.rotationAngle = -45
        Me.TransLbl.Size = New System.Drawing.Size(382, 75)
        Me.TransLbl.TabIndex = 68
        Me.TransLbl.Text = "Status here"
        Me.TransLbl.Visible = False
        '
        'frm_vend_CreatePurchaseOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(611, 462)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.TransLbl)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.btnTrigger)
        Me.Controls.Add(Me.grdPO)
        Me.Controls.Add(Me.chkClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.txtMemo)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.chkEmail)
        Me.Controls.Add(Me.chkPrint)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtSupplierMessage)
        Me.Controls.Add(Me.Label8)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(619, 496)
        Me.Name = "frm_vend_CreatePurchaseOrder"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Create Purchase Order"
        CType(Me.grdPO, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboSupplierName As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtSupplierAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dtDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtPONo As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtShipAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtSupplierMessage As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents chkPrint As System.Windows.Forms.CheckBox
    Friend WithEvents chkEmail As System.Windows.Forms.CheckBox
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents cboSupplierID As System.Windows.Forms.ComboBox
    Friend WithEvents chkClose As System.Windows.Forms.CheckBox
    Friend WithEvents grdPO As System.Windows.Forms.DataGridView
    Friend WithEvents btnTrigger As System.Windows.Forms.Button
    Friend WithEvents MtcboShipname As MTGCComboBox
    Friend WithEvents MtcboShipAddress As MTGCComboBox
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents TransLbl As CSAcctg.modTransparentLabel
End Class
