Public Class frm_acc_accountregistry

    Private Sub frm_acc_accountregistry_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call load_accountregister_list()
    End Sub

    Private Sub ts_undefund_goto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_undefund_goto.Click
        frm_undefund_goto.Show()
    End Sub

    Private Sub ts_undeFunds_Print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_undeFunds_Print.Click
        frm_undefund_print.ShowDialog()
    End Sub

    Private Sub load_accountregister_list()
        With lst_undefund_lstundepositedfunds
            .Clear()
            .View = View.Details
            .CheckBoxes = True
            .Columns.Add("Date", 70, HorizontalAlignment.Left)
            .Columns.Add("Ref Type", 70, HorizontalAlignment.Left)
            .Columns.Add("Payee", 130, HorizontalAlignment.Left)
            .Columns.Add("Account", 110, HorizontalAlignment.Left)
            .Columns.Add("Memo", 110, HorizontalAlignment.Left)
            .Columns.Add("Decrease", 70, HorizontalAlignment.Left)
            .Columns.Add("Increase", 70, HorizontalAlignment.Left)
            .Columns.Add("Balance", 70, HorizontalAlignment.Left)
        End With
    End Sub

   
End Class