<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_accountregistry
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_accountregistry))
        Me.ts_acc_makedeposit = New System.Windows.Forms.ToolStrip
        Me.ts_undefund_goto = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_undeFunds_Print = New System.Windows.Forms.ToolStripButton
        Me.ts_spliter1 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_spliter2 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_undefund_edittrans = New System.Windows.Forms.ToolStripButton
        Me.ts_undefund_quickreport = New System.Windows.Forms.ToolStripButton
        Me.lst_undefund_lstundepositedfunds = New System.Windows.Forms.ListView
        Me.Label1 = New System.Windows.Forms.Label
        Me.txt_undefund_endingfunds = New System.Windows.Forms.TextBox
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.btn_goto_Ok = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.ts_acc_makedeposit.SuspendLayout()
        Me.SuspendLayout()
        '
        'ts_acc_makedeposit
        '
        Me.ts_acc_makedeposit.BackColor = System.Drawing.Color.White
        Me.ts_acc_makedeposit.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_undefund_goto, Me.ToolStripSeparator1, Me.ToolStripSeparator2, Me.ts_undeFunds_Print, Me.ts_spliter1, Me.ts_spliter2, Me.ts_undefund_edittrans, Me.ts_undefund_quickreport})
        Me.ts_acc_makedeposit.Location = New System.Drawing.Point(0, 0)
        Me.ts_acc_makedeposit.Name = "ts_acc_makedeposit"
        Me.ts_acc_makedeposit.Size = New System.Drawing.Size(711, 25)
        Me.ts_acc_makedeposit.TabIndex = 2
        Me.ts_acc_makedeposit.Text = "ToolStrip1"
        '
        'ts_undefund_goto
        '
        Me.ts_undefund_goto.Image = CType(resources.GetObject("ts_undefund_goto.Image"), System.Drawing.Image)
        Me.ts_undefund_goto.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_undefund_goto.Name = "ts_undefund_goto"
        Me.ts_undefund_goto.Size = New System.Drawing.Size(61, 22)
        Me.ts_undefund_goto.Text = "Go to.."
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'ts_undeFunds_Print
        '
        Me.ts_undeFunds_Print.Image = CType(resources.GetObject("ts_undeFunds_Print.Image"), System.Drawing.Image)
        Me.ts_undeFunds_Print.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_undeFunds_Print.Name = "ts_undeFunds_Print"
        Me.ts_undeFunds_Print.Size = New System.Drawing.Size(49, 22)
        Me.ts_undeFunds_Print.Text = "Print"
        '
        'ts_spliter1
        '
        Me.ts_spliter1.Name = "ts_spliter1"
        Me.ts_spliter1.Size = New System.Drawing.Size(6, 25)
        '
        'ts_spliter2
        '
        Me.ts_spliter2.Name = "ts_spliter2"
        Me.ts_spliter2.Size = New System.Drawing.Size(6, 25)
        '
        'ts_undefund_edittrans
        '
        Me.ts_undefund_edittrans.Image = CType(resources.GetObject("ts_undefund_edittrans.Image"), System.Drawing.Image)
        Me.ts_undefund_edittrans.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_undefund_edittrans.Name = "ts_undefund_edittrans"
        Me.ts_undefund_edittrans.Size = New System.Drawing.Size(104, 22)
        Me.ts_undefund_edittrans.Text = "Edit Transaction"
        '
        'ts_undefund_quickreport
        '
        Me.ts_undefund_quickreport.Image = CType(resources.GetObject("ts_undefund_quickreport.Image"), System.Drawing.Image)
        Me.ts_undefund_quickreport.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_undefund_quickreport.Name = "ts_undefund_quickreport"
        Me.ts_undefund_quickreport.Size = New System.Drawing.Size(89, 22)
        Me.ts_undefund_quickreport.Text = "Quick Report"
        '
        'lst_undefund_lstundepositedfunds
        '
        Me.lst_undefund_lstundepositedfunds.Location = New System.Drawing.Point(2, 28)
        Me.lst_undefund_lstundepositedfunds.Name = "lst_undefund_lstundepositedfunds"
        Me.lst_undefund_lstundepositedfunds.Size = New System.Drawing.Size(706, 213)
        Me.lst_undefund_lstundepositedfunds.TabIndex = 3
        Me.lst_undefund_lstundepositedfunds.UseCompatibleStateImageBehavior = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(523, 252)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Ending Balance"
        '
        'txt_undefund_endingfunds
        '
        Me.txt_undefund_endingfunds.Location = New System.Drawing.Point(608, 248)
        Me.txt_undefund_endingfunds.Name = "txt_undefund_endingfunds"
        Me.txt_undefund_endingfunds.Size = New System.Drawing.Size(100, 20)
        Me.txt_undefund_endingfunds.TabIndex = 5
        Me.txt_undefund_endingfunds.Text = "0.00"
        Me.txt_undefund_endingfunds.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(7, 284)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(61, 17)
        Me.CheckBox1.TabIndex = 6
        Me.CheckBox1.Text = "1 - Line"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(106, 285)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Sort by"
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(149, 281)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(191, 21)
        Me.ComboBox1.TabIndex = 8
        '
        'btn_goto_Ok
        '
        Me.btn_goto_Ok.Location = New System.Drawing.Point(5, 245)
        Me.btn_goto_Ok.Name = "btn_goto_Ok"
        Me.btn_goto_Ok.Size = New System.Drawing.Size(93, 23)
        Me.btn_goto_Ok.TabIndex = 29
        Me.btn_goto_Ok.Text = "Split"
        Me.btn_goto_Ok.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(519, 286)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(93, 23)
        Me.Button1.TabIndex = 30
        Me.Button1.Text = "Record"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(615, 286)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(93, 23)
        Me.Button2.TabIndex = 31
        Me.Button2.Text = "Restore"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'frm_acc_accountregistry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(711, 315)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btn_goto_Ok)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.txt_undefund_endingfunds)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lst_undefund_lstundepositedfunds)
        Me.Controls.Add(Me.ts_acc_makedeposit)
        Me.Name = "frm_acc_accountregistry"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Account Register"
        Me.ts_acc_makedeposit.ResumeLayout(False)
        Me.ts_acc_makedeposit.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ts_acc_makedeposit As System.Windows.Forms.ToolStrip
    Friend WithEvents ts_undefund_goto As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_undeFunds_Print As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_spliter1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_spliter2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_undefund_edittrans As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_undefund_quickreport As System.Windows.Forms.ToolStripButton
    Friend WithEvents lst_undefund_lstundepositedfunds As System.Windows.Forms.ListView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_undefund_endingfunds As System.Windows.Forms.TextBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents btn_goto_Ok As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
End Class
