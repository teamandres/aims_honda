<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_dep_depositslip
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btn_dep_depSlipClear = New System.Windows.Forms.Button
        Me.btn_dep_depSlipPreview = New System.Windows.Forms.Button
        Me.btn_dep_depSlipCancel = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(12, 23)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(256, 17)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "Include cash in total number of deposited items."
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(12, 58)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(403, 17)
        Me.CheckBox2.TabIndex = 1
        Me.CheckBox2.Text = "Combine checks with the same number and same customer as one deposit item"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Location = New System.Drawing.Point(12, 92)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(400, 1)
        Me.Panel1.TabIndex = 13
        '
        'btn_dep_depSlipClear
        '
        Me.btn_dep_depSlipClear.Location = New System.Drawing.Point(319, 17)
        Me.btn_dep_depSlipClear.Name = "btn_dep_depSlipClear"
        Me.btn_dep_depSlipClear.Size = New System.Drawing.Size(93, 23)
        Me.btn_dep_depSlipClear.TabIndex = 25
        Me.btn_dep_depSlipClear.Text = "Help"
        Me.btn_dep_depSlipClear.UseVisualStyleBackColor = True
        '
        'btn_dep_depSlipPreview
        '
        Me.btn_dep_depSlipPreview.Location = New System.Drawing.Point(220, 105)
        Me.btn_dep_depSlipPreview.Name = "btn_dep_depSlipPreview"
        Me.btn_dep_depSlipPreview.Size = New System.Drawing.Size(93, 23)
        Me.btn_dep_depSlipPreview.TabIndex = 26
        Me.btn_dep_depSlipPreview.Text = "Preview"
        Me.btn_dep_depSlipPreview.UseVisualStyleBackColor = True
        '
        'btn_dep_depSlipCancel
        '
        Me.btn_dep_depSlipCancel.Location = New System.Drawing.Point(319, 105)
        Me.btn_dep_depSlipCancel.Name = "btn_dep_depSlipCancel"
        Me.btn_dep_depSlipCancel.Size = New System.Drawing.Size(93, 23)
        Me.btn_dep_depSlipCancel.TabIndex = 27
        Me.btn_dep_depSlipCancel.Text = "Cancel"
        Me.btn_dep_depSlipCancel.UseVisualStyleBackColor = True
        '
        'frm_dep_depositslip
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(428, 139)
        Me.Controls.Add(Me.btn_dep_depSlipCancel)
        Me.Controls.Add(Me.btn_dep_depSlipPreview)
        Me.Controls.Add(Me.btn_dep_depSlipClear)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.CheckBox2)
        Me.Controls.Add(Me.CheckBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frm_dep_depositslip"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Print Deposit Slips"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btn_dep_depSlipClear As System.Windows.Forms.Button
    Friend WithEvents btn_dep_depSlipPreview As System.Windows.Forms.Button
    Friend WithEvents btn_dep_depSlipCancel As System.Windows.Forms.Button
End Class
