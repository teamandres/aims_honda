<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_makedeposit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_makedeposit))
        Me.ts_acc_makedeposit = New System.Windows.Forms.ToolStrip
        Me.ts_dep_previous = New System.Windows.Forms.ToolStripButton
        Me.ts_dep_next = New System.Windows.Forms.ToolStripButton
        Me.ts_dep_print = New System.Windows.Forms.ToolStripDropDownButton
        Me.ts_dep_depositSlip = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_dep_depositSummary = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_spliter1 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_spliter2 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_dep_payment = New System.Windows.Forms.ToolStripButton
        Me.ts_dep_history = New System.Windows.Forms.ToolStripButton
        Me.ts_dep_journal = New System.Windows.Forms.ToolStripButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboDepositTo = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.dtDeposit = New System.Windows.Forms.DateTimePicker
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtMemo = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.cboAssetAct = New System.Windows.Forms.ComboBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtCBMemo = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtCBAmt = New System.Windows.Forms.TextBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label12 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.btn_dep_Clear = New System.Windows.Forms.Button
        Me.btn_dep_saveNew = New System.Windows.Forms.Button
        Me.btn_dep_saveClose = New System.Windows.Forms.Button
        Me.gDeposits = New System.Windows.Forms.DataGridView
        Me.lbl_dep_subtotaldeposit = New System.Windows.Forms.TextBox
        Me.lbl_dep_totaldeposit = New System.Windows.Forms.TextBox
        Me.ts_acc_makedeposit.SuspendLayout()
        CType(Me.gDeposits, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ts_acc_makedeposit
        '
        Me.ts_acc_makedeposit.BackColor = System.Drawing.Color.White
        Me.ts_acc_makedeposit.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_dep_previous, Me.ts_dep_next, Me.ts_dep_print, Me.ts_spliter1, Me.ts_spliter2, Me.ts_dep_payment, Me.ts_dep_history, Me.ts_dep_journal})
        Me.ts_acc_makedeposit.Location = New System.Drawing.Point(0, 0)
        Me.ts_acc_makedeposit.Name = "ts_acc_makedeposit"
        Me.ts_acc_makedeposit.Size = New System.Drawing.Size(819, 25)
        Me.ts_acc_makedeposit.TabIndex = 1
        Me.ts_acc_makedeposit.Text = "ToolStrip1"
        '
        'ts_dep_previous
        '
        Me.ts_dep_previous.Image = CType(resources.GetObject("ts_dep_previous.Image"), System.Drawing.Image)
        Me.ts_dep_previous.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_dep_previous.Name = "ts_dep_previous"
        Me.ts_dep_previous.Size = New System.Drawing.Size(72, 22)
        Me.ts_dep_previous.Text = "Previous"
        '
        'ts_dep_next
        '
        Me.ts_dep_next.Image = CType(resources.GetObject("ts_dep_next.Image"), System.Drawing.Image)
        Me.ts_dep_next.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_dep_next.Name = "ts_dep_next"
        Me.ts_dep_next.Size = New System.Drawing.Size(51, 22)
        Me.ts_dep_next.Text = "Next"
        '
        'ts_dep_print
        '
        Me.ts_dep_print.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_dep_depositSlip, Me.ts_dep_depositSummary})
        Me.ts_dep_print.Image = CType(resources.GetObject("ts_dep_print.Image"), System.Drawing.Image)
        Me.ts_dep_print.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_dep_print.Name = "ts_dep_print"
        Me.ts_dep_print.Size = New System.Drawing.Size(61, 22)
        Me.ts_dep_print.Text = "Print"
        '
        'ts_dep_depositSlip
        '
        Me.ts_dep_depositSlip.Name = "ts_dep_depositSlip"
        Me.ts_dep_depositSlip.Size = New System.Drawing.Size(168, 22)
        Me.ts_dep_depositSlip.Text = "Deposit Slip"
        '
        'ts_dep_depositSummary
        '
        Me.ts_dep_depositSummary.Name = "ts_dep_depositSummary"
        Me.ts_dep_depositSummary.Size = New System.Drawing.Size(168, 22)
        Me.ts_dep_depositSummary.Text = "Deposit Summary"
        '
        'ts_spliter1
        '
        Me.ts_spliter1.Name = "ts_spliter1"
        Me.ts_spliter1.Size = New System.Drawing.Size(6, 25)
        Me.ts_spliter1.Visible = False
        '
        'ts_spliter2
        '
        Me.ts_spliter2.Name = "ts_spliter2"
        Me.ts_spliter2.Size = New System.Drawing.Size(6, 25)
        Me.ts_spliter2.Visible = False
        '
        'ts_dep_payment
        '
        Me.ts_dep_payment.Image = CType(resources.GetObject("ts_dep_payment.Image"), System.Drawing.Image)
        Me.ts_dep_payment.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_dep_payment.Name = "ts_dep_payment"
        Me.ts_dep_payment.Size = New System.Drawing.Size(74, 22)
        Me.ts_dep_payment.Text = "Payment"
        Me.ts_dep_payment.Visible = False
        '
        'ts_dep_history
        '
        Me.ts_dep_history.Image = CType(resources.GetObject("ts_dep_history.Image"), System.Drawing.Image)
        Me.ts_dep_history.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_dep_history.Name = "ts_dep_history"
        Me.ts_dep_history.Size = New System.Drawing.Size(65, 22)
        Me.ts_dep_history.Text = "History"
        Me.ts_dep_history.Visible = False
        '
        'ts_dep_journal
        '
        Me.ts_dep_journal.Image = CType(resources.GetObject("ts_dep_journal.Image"), System.Drawing.Image)
        Me.ts_dep_journal.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_dep_journal.Name = "ts_dep_journal"
        Me.ts_dep_journal.Size = New System.Drawing.Size(65, 22)
        Me.ts_dep_journal.Text = "Journal"
        Me.ts_dep_journal.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Deposit To"
        '
        'cboDepositTo
        '
        Me.cboDepositTo.FormattingEnabled = True
        Me.cboDepositTo.Location = New System.Drawing.Point(82, 41)
        Me.cboDepositTo.Name = "cboDepositTo"
        Me.cboDepositTo.Size = New System.Drawing.Size(182, 21)
        Me.cboDepositTo.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(294, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Date"
        '
        'dtDeposit
        '
        Me.dtDeposit.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtDeposit.Location = New System.Drawing.Point(336, 41)
        Me.dtDeposit.Name = "dtDeposit"
        Me.dtDeposit.Size = New System.Drawing.Size(125, 21)
        Me.dtDeposit.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(486, 45)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Memo"
        '
        'txtMemo
        '
        Me.txtMemo.BackColor = System.Drawing.Color.White
        Me.txtMemo.Location = New System.Drawing.Point(534, 41)
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.ReadOnly = True
        Me.txtMemo.Size = New System.Drawing.Size(154, 21)
        Me.txtMemo.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(10, 77)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(640, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Click Payments to select customer payments that you have received. List any other" & _
            " amounts to deposit below."
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(582, 295)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(104, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Deposit SubTotal"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Location = New System.Drawing.Point(14, 310)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(564, 1)
        Me.Panel1.TabIndex = 12
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(10, 320)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(567, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "To get cash back from this deposit, enter the amount below. Indicate the account " & _
            "where you want"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(10, 335)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(302, 13)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "this money to go, such as your Petty Cash account."
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(10, 366)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(113, 13)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "Cash back goes to"
        '
        'cboAssetAct
        '
        Me.cboAssetAct.FormattingEnabled = True
        Me.cboAssetAct.Location = New System.Drawing.Point(14, 382)
        Me.cboAssetAct.Name = "cboAssetAct"
        Me.cboAssetAct.Size = New System.Drawing.Size(164, 21)
        Me.cboAssetAct.TabIndex = 16
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(198, 366)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(107, 13)
        Me.Label10.TabIndex = 17
        Me.Label10.Text = "Cash back memo"
        '
        'txtCBMemo
        '
        Me.txtCBMemo.AcceptsReturn = True
        Me.txtCBMemo.Location = New System.Drawing.Point(202, 382)
        Me.txtCBMemo.Name = "txtCBMemo"
        Me.txtCBMemo.Size = New System.Drawing.Size(188, 21)
        Me.txtCBMemo.TabIndex = 18
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(408, 366)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(115, 13)
        Me.Label11.TabIndex = 19
        Me.Label11.Text = "Cash back Amount"
        '
        'txtCBAmt
        '
        Me.txtCBAmt.Location = New System.Drawing.Point(412, 382)
        Me.txtCBAmt.Name = "txtCBAmt"
        Me.txtCBAmt.Size = New System.Drawing.Size(132, 21)
        Me.txtCBAmt.TabIndex = 20
        Me.txtCBAmt.Text = "0.00"
        Me.txtCBAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Location = New System.Drawing.Point(14, 413)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(564, 1)
        Me.Panel2.TabIndex = 21
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(576, 411)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(93, 16)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "Deposit Total"
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Location = New System.Drawing.Point(574, 430)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(233, 1)
        Me.Panel3.TabIndex = 22
        '
        'btn_dep_Clear
        '
        Me.btn_dep_Clear.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_dep_Clear.Location = New System.Drawing.Point(699, 444)
        Me.btn_dep_Clear.Name = "btn_dep_Clear"
        Me.btn_dep_Clear.Size = New System.Drawing.Size(108, 23)
        Me.btn_dep_Clear.TabIndex = 24
        Me.btn_dep_Clear.Text = "Clear"
        Me.btn_dep_Clear.UseVisualStyleBackColor = True
        '
        'btn_dep_saveNew
        '
        Me.btn_dep_saveNew.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_dep_saveNew.Location = New System.Drawing.Point(588, 444)
        Me.btn_dep_saveNew.Name = "btn_dep_saveNew"
        Me.btn_dep_saveNew.Size = New System.Drawing.Size(108, 23)
        Me.btn_dep_saveNew.TabIndex = 25
        Me.btn_dep_saveNew.Text = "Save && New"
        Me.btn_dep_saveNew.UseVisualStyleBackColor = True
        '
        'btn_dep_saveClose
        '
        Me.btn_dep_saveClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_dep_saveClose.Location = New System.Drawing.Point(477, 444)
        Me.btn_dep_saveClose.Name = "btn_dep_saveClose"
        Me.btn_dep_saveClose.Size = New System.Drawing.Size(108, 23)
        Me.btn_dep_saveClose.TabIndex = 26
        Me.btn_dep_saveClose.Text = "Save && Close"
        Me.btn_dep_saveClose.UseVisualStyleBackColor = True
        '
        'gDeposits
        '
        Me.gDeposits.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gDeposits.Location = New System.Drawing.Point(14, 95)
        Me.gDeposits.Name = "gDeposits"
        Me.gDeposits.Size = New System.Drawing.Size(791, 195)
        Me.gDeposits.TabIndex = 27
        '
        'lbl_dep_subtotaldeposit
        '
        Me.lbl_dep_subtotaldeposit.BackColor = System.Drawing.Color.White
        Me.lbl_dep_subtotaldeposit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lbl_dep_subtotaldeposit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_dep_subtotaldeposit.Location = New System.Drawing.Point(681, 294)
        Me.lbl_dep_subtotaldeposit.Name = "lbl_dep_subtotaldeposit"
        Me.lbl_dep_subtotaldeposit.ReadOnly = True
        Me.lbl_dep_subtotaldeposit.Size = New System.Drawing.Size(122, 14)
        Me.lbl_dep_subtotaldeposit.TabIndex = 28
        Me.lbl_dep_subtotaldeposit.Text = "0.00"
        Me.lbl_dep_subtotaldeposit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lbl_dep_totaldeposit
        '
        Me.lbl_dep_totaldeposit.BackColor = System.Drawing.Color.White
        Me.lbl_dep_totaldeposit.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lbl_dep_totaldeposit.Font = New System.Drawing.Font("Tahoma", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_dep_totaldeposit.Location = New System.Drawing.Point(681, 411)
        Me.lbl_dep_totaldeposit.Name = "lbl_dep_totaldeposit"
        Me.lbl_dep_totaldeposit.ReadOnly = True
        Me.lbl_dep_totaldeposit.Size = New System.Drawing.Size(122, 16)
        Me.lbl_dep_totaldeposit.TabIndex = 29
        Me.lbl_dep_totaldeposit.Text = "0.00"
        Me.lbl_dep_totaldeposit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'frm_acc_makedeposit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(819, 479)
        Me.Controls.Add(Me.lbl_dep_totaldeposit)
        Me.Controls.Add(Me.lbl_dep_subtotaldeposit)
        Me.Controls.Add(Me.gDeposits)
        Me.Controls.Add(Me.btn_dep_saveClose)
        Me.Controls.Add(Me.btn_dep_saveNew)
        Me.Controls.Add(Me.btn_dep_Clear)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.txtCBAmt)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtCBMemo)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cboAssetAct)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtMemo)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dtDeposit)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboDepositTo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ts_acc_makedeposit)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_acc_makedeposit"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Make Deposits"
        Me.ts_acc_makedeposit.ResumeLayout(False)
        Me.ts_acc_makedeposit.PerformLayout()
        CType(Me.gDeposits, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ts_acc_makedeposit As System.Windows.Forms.ToolStrip
    Friend WithEvents ts_dep_previous As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_dep_next As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_dep_print As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ts_dep_depositSlip As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_dep_depositSummary As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_spliter1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_spliter2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_dep_payment As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_dep_history As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_dep_journal As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboDepositTo As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtDeposit As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cboAssetAct As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtCBMemo As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtCBAmt As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btn_dep_Clear As System.Windows.Forms.Button
    Friend WithEvents btn_dep_saveNew As System.Windows.Forms.Button
    Friend WithEvents btn_dep_saveClose As System.Windows.Forms.Button
    Friend WithEvents gDeposits As System.Windows.Forms.DataGridView
    Friend WithEvents lbl_dep_subtotaldeposit As System.Windows.Forms.TextBox
    Friend WithEvents lbl_dep_totaldeposit As System.Windows.Forms.TextBox
End Class
