Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports CrystalDecisions.Shared
Public Class DebitAdvice
    Private gcon As New Clsappconfiguration
    Private rptSummary As New ReportDocument
#Region "Properties"
    Private withdrawabledate As Date
    Public Property getwitdate() As Date
        Get
            Return withdrawabledate
        End Get
        Set(ByVal value As Date)
            withdrawabledate = value
        End Set
    End Property
    Private audit As String
    Public Property getaudit() As String
        Get
            Return audit
        End Get
        Set(ByVal value As String)
            audit = value
        End Set
    End Property
    Private approve As String
    Public Property getapprove() As String
        Get
            Return approve
        End Get
        Set(ByVal value As String)
            approve = value
        End Set
    End Property
    Private AccountNo As String
    Public Property getAcctNo() As String
        Get
            Return AccountNo
        End Get
        Set(ByVal value As String)
            AccountNo = value
        End Set
    End Property
    Private AmountNetPay As Decimal
    Public Property getAmount() As Decimal
        Get
            Return AmountNetPay
        End Get
        Set(ByVal value As Decimal)
            AmountNetPay = value
        End Set
    End Property
#End Region

    Private Sub DebitAdvice_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptSummary.Close()
    End Sub



    Private Sub DebitAdvice_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadBankAdviceReport()
    End Sub
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
    Private Sub LoadBankAdviceReport()
        Dim reportPath As String = Application.StartupPath & "\Accounting Reports\Bank_Advice.rpt"
        rptSummary.Load(reportPath)
        Logon(rptSummary, gcon.Server, gcon.Database, gcon.Username, Clsappconfiguration.GetDecryptedData(gcon.Password))
        rptSummary.Refresh()
        With rptSummary
            .SetParameterValue("@date", getwitdate())
            .SetParameterValue("@audit", getaudit())
            .SetParameterValue("@approve", getapprove())
            .SetParameterValue("@acc#", getAcctNo())
            .SetParameterValue("@amnt", getAmount())
        End With
        Me.crvReport.Visible = True
        Me.crvReport.ReportSource = rptSummary



    End Sub
End Class