<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_acc_transferfunds
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_acc_transferfunds))
        Me.ts_acc_transferfund = New System.Windows.Forms.ToolStrip
        Me.ts_trans_previous = New System.Windows.Forms.ToolStripButton
        Me.ts_trans_next = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_trans_journal = New System.Windows.Forms.ToolStripButton
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtAcntBalTo = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtAcntBalFrom = New System.Windows.Forms.TextBox
        Me.txtTransferAmt = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label5 = New System.Windows.Forms.Label
        Me.cboTransferTo = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cboTransferFrom = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.dteTransferred = New System.Windows.Forms.DateTimePicker
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnClear = New System.Windows.Forms.Button
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtMemo = New System.Windows.Forms.TextBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.btnSaveNew = New System.Windows.Forms.Button
        Me.btnSaveClose = New System.Windows.Forms.Button
        Me.ts_acc_transferfund.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ts_acc_transferfund
        '
        Me.ts_acc_transferfund.BackColor = System.Drawing.Color.White
        Me.ts_acc_transferfund.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_trans_previous, Me.ts_trans_next, Me.ToolStripSeparator1, Me.ToolStripSeparator2, Me.ts_trans_journal})
        Me.ts_acc_transferfund.Location = New System.Drawing.Point(0, 0)
        Me.ts_acc_transferfund.Name = "ts_acc_transferfund"
        Me.ts_acc_transferfund.Size = New System.Drawing.Size(647, 25)
        Me.ts_acc_transferfund.TabIndex = 1
        Me.ts_acc_transferfund.Text = "ToolStrip1"
        '
        'ts_trans_previous
        '
        Me.ts_trans_previous.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ts_trans_previous.Image = CType(resources.GetObject("ts_trans_previous.Image"), System.Drawing.Image)
        Me.ts_trans_previous.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_trans_previous.Name = "ts_trans_previous"
        Me.ts_trans_previous.Size = New System.Drawing.Size(76, 22)
        Me.ts_trans_previous.Text = "Previous"
        '
        'ts_trans_next
        '
        Me.ts_trans_next.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ts_trans_next.Image = CType(resources.GetObject("ts_trans_next.Image"), System.Drawing.Image)
        Me.ts_trans_next.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_trans_next.Name = "ts_trans_next"
        Me.ts_trans_next.Size = New System.Drawing.Size(53, 22)
        Me.ts_trans_next.Text = "Next"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        Me.ToolStripSeparator1.Visible = False
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        Me.ToolStripSeparator2.Visible = False
        '
        'ts_trans_journal
        '
        Me.ts_trans_journal.Image = CType(resources.GetObject("ts_trans_journal.Image"), System.Drawing.Image)
        Me.ts_trans_journal.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_trans_journal.Name = "ts_trans_journal"
        Me.ts_trans_journal.Size = New System.Drawing.Size(65, 22)
        Me.ts_trans_journal.Text = "Journal"
        Me.ts_trans_journal.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txtAcntBalTo)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.txtAcntBalFrom)
        Me.Panel1.Controls.Add(Me.txtTransferAmt)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.cboTransferTo)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.cboTransferFrom)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.dteTransferred)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(5, 31)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(637, 196)
        Me.Panel1.TabIndex = 2
        '
        'txtAcntBalTo
        '
        Me.txtAcntBalTo.BackColor = System.Drawing.Color.White
        Me.txtAcntBalTo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAcntBalTo.Location = New System.Drawing.Point(458, 106)
        Me.txtAcntBalTo.Name = "txtAcntBalTo"
        Me.txtAcntBalTo.ReadOnly = True
        Me.txtAcntBalTo.Size = New System.Drawing.Size(136, 14)
        Me.txtAcntBalTo.TabIndex = 14
        Me.txtAcntBalTo.Text = "0.00"
        Me.txtAcntBalTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(357, 106)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(101, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Account Balance"
        '
        'txtAcntBalFrom
        '
        Me.txtAcntBalFrom.BackColor = System.Drawing.Color.White
        Me.txtAcntBalFrom.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAcntBalFrom.Location = New System.Drawing.Point(458, 74)
        Me.txtAcntBalFrom.Name = "txtAcntBalFrom"
        Me.txtAcntBalFrom.ReadOnly = True
        Me.txtAcntBalFrom.Size = New System.Drawing.Size(136, 14)
        Me.txtAcntBalFrom.TabIndex = 12
        Me.txtAcntBalFrom.Text = "0.00"
        Me.txtAcntBalFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTransferAmt
        '
        Me.txtTransferAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTransferAmt.Location = New System.Drawing.Point(462, 155)
        Me.txtTransferAmt.Name = "txtTransferAmt"
        Me.txtTransferAmt.Size = New System.Drawing.Size(136, 21)
        Me.txtTransferAmt.TabIndex = 11
        Me.txtTransferAmt.Text = "0.00"
        Me.txtTransferAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(357, 159)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(103, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Transfer Amount"
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Location = New System.Drawing.Point(190, 143)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(408, 1)
        Me.Panel2.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(357, 73)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(101, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Account Balance"
        '
        'cboTransferTo
        '
        Me.cboTransferTo.FormattingEnabled = True
        Me.cboTransferTo.Location = New System.Drawing.Point(134, 103)
        Me.cboTransferTo.Name = "cboTransferTo"
        Me.cboTransferTo.Size = New System.Drawing.Size(205, 21)
        Me.cboTransferTo.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 106)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Transfer Fund To"
        '
        'cboTransferFrom
        '
        Me.cboTransferFrom.FormattingEnabled = True
        Me.cboTransferFrom.Location = New System.Drawing.Point(134, 69)
        Me.cboTransferFrom.Name = "cboTransferFrom"
        Me.cboTransferFrom.Size = New System.Drawing.Size(205, 21)
        Me.cboTransferFrom.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(119, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Transfer Fund From"
        '
        'dteTransferred
        '
        Me.dteTransferred.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteTransferred.Location = New System.Drawing.Point(50, 30)
        Me.dteTransferred.Name = "dteTransferred"
        Me.dteTransferred.Size = New System.Drawing.Size(117, 21)
        Me.dteTransferred.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Date"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(103, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "TRANSFER FUNDS"
        '
        'btnClear
        '
        Me.btnClear.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Location = New System.Drawing.Point(533, 279)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(108, 23)
        Me.btnClear.TabIndex = 13
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(14, 240)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Memo"
        '
        'txtMemo
        '
        Me.txtMemo.Location = New System.Drawing.Point(56, 237)
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.Size = New System.Drawing.Size(409, 21)
        Me.txtMemo.TabIndex = 12
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Location = New System.Drawing.Point(196, 269)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(408, 1)
        Me.Panel3.TabIndex = 14
        '
        'btnSaveNew
        '
        Me.btnSaveNew.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveNew.Location = New System.Drawing.Point(421, 279)
        Me.btnSaveNew.Name = "btnSaveNew"
        Me.btnSaveNew.Size = New System.Drawing.Size(108, 23)
        Me.btnSaveNew.TabIndex = 15
        Me.btnSaveNew.Text = "Save && New"
        Me.btnSaveNew.UseVisualStyleBackColor = True
        '
        'btnSaveClose
        '
        Me.btnSaveClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSaveClose.Location = New System.Drawing.Point(309, 279)
        Me.btnSaveClose.Name = "btnSaveClose"
        Me.btnSaveClose.Size = New System.Drawing.Size(108, 23)
        Me.btnSaveClose.TabIndex = 16
        Me.btnSaveClose.Text = "Save && Close"
        Me.btnSaveClose.UseVisualStyleBackColor = True
        '
        'frm_acc_transferfunds
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(647, 310)
        Me.Controls.Add(Me.btnSaveClose)
        Me.Controls.Add(Me.btnSaveNew)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.txtMemo)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ts_acc_transferfund)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_acc_transferfunds"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Transfer Fund Between Accounts"
        Me.ts_acc_transferfund.ResumeLayout(False)
        Me.ts_acc_transferfund.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ts_acc_transferfund As System.Windows.Forms.ToolStrip
    Friend WithEvents ts_trans_previous As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_trans_next As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_trans_journal As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dteTransferred As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboTransferTo As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboTransferFrom As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btnSaveNew As System.Windows.Forms.Button
    Friend WithEvents btnSaveClose As System.Windows.Forms.Button
    Friend WithEvents txtAcntBalTo As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtAcntBalFrom As System.Windows.Forms.TextBox
    Friend WithEvents txtTransferAmt As System.Windows.Forms.TextBox
End Class
