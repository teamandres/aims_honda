﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmCOAFilter
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring
    Public xModule As String
    Dim isdebit As Boolean

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
    Private Sub DebitCredit_Focus()
        isdebit = grdCoAList.SelectedRows(0).Cells(5).Value.ToString()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Call SelectedColumnVAR()
    End Sub

    Private Sub frmCOAFilter_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call loadAccounts()
        txtSearch.Text = ""
        Me.ActiveControl = txtSearch
    End Sub

    Private Sub loadAccounts()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "usp_m_mAccountsDS_load",
                                       New SqlParameter("@coid", gCompanyID))
        grdCoAList.DataSource = ds.Tables(0)

        With grdCoAList
            .Columns("fcDescription").Visible = False
            .Columns("acnt_code").Width = 80
            .Columns("acnt_code").HeaderText = "Code"
            .Columns("acnt_code").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("acnt_id").Visible = False
            .Columns("acnt_name").Width = 420
            .Columns("acnt_name").HeaderText = "Account Title"
            .Columns("acnt_name").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("code_description").Visible = False
            .Columns("isdebit").Visible = False
        End With
    End Sub

    Private Sub SelectedColumnVAR()
        Select Case xModule
            Case "Entry"
                Me.DialogResult = Windows.Forms.DialogResult.OK
            Case "GL Report"
                frmGeneralLedgerDetailedV2.txtAccountTitle.Text = grdCoAList.SelectedRows(0).Cells(1).Value.ToString() + " | " + grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
                frmGeneralLedgerDetailedV2.acnt_id = grdCoAList.SelectedRows(0).Cells(3).Value.ToString
                frmGeneralLedgerDetailedV2.acnt_code = grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                frmGeneralLedgerDetailedV2.acnt_name = grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
                Me.Close()
            Case "Account Analysis"
                frmAccountAnalysis.txtAccountTitle.Text = grdCoAList.SelectedRows(0).Cells(1).Value.ToString() + " | " + grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
                frmAccountAnalysis.acnt_id = grdCoAList.SelectedRows(0).Cells(3).Value.ToString
                frmAccountAnalysis.acnt_code = grdCoAList.SelectedRows(0).Cells(1).Value.ToString()
                frmAccountAnalysis.acnt_name = grdCoAList.SelectedRows(0).Cells(4).Value.ToString()
                Me.Close()
        End Select
    End Sub

    Private Sub grdCoAList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles grdCoAList.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                SelectedColumnVAR()
            Case Keys.Escape
                Me.Close()
        End Select
        'If e.KeyCode = Keys.Enter Then
        '    SelectedColumnVAR()
        'End If

        'If e.KeyCode = Keys.Escape Then
        '    Me.Close()
        'End If
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.SuppressKeyPress = True
                Call SelectedColumnVAR()
            Case Keys.Escape
                Me.Close()
        End Select

        'If e.KeyCode = Keys.Enter Then
        '    Call SelectedColumnVAR()
        'End If

        'If e.KeyCode = Keys.Escape Then
        '    Me.Close()
        'End If
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If txtSearch.Text = "" Then
            Call loadAccounts()
        Else
            Call filterAccounts()
        End If
    End Sub

    Private Sub filterAccounts()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(cs, CommandType.StoredProcedure, "usp_m_mAccountsDS_filter",
                                       New SqlParameter("@coid", gCompanyID),
                                       New SqlParameter("@acnt_name", txtSearch.Text))
        grdCoAList.DataSource = ds.Tables(0)

        With grdCoAList
            .Columns("fcDescription").Visible = False
            .Columns("acnt_code").Width = 80
            .Columns("acnt_code").HeaderText = "Code"
            .Columns("acnt_code").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("acnt_id").Visible = False
            .Columns("acnt_name").Width = 420
            .Columns("acnt_name").HeaderText = "Account Title"
            .Columns("acnt_name").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("code_description").Visible = False
            .Columns("isdebit").Visible = False
        End With
    End Sub

    'Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    '    If txtSearch.Text = "" Then
    '        Call loadAccounts()
    '    Else
    '        Call filterAccounts()
    '    End If
    'End Sub
End Class