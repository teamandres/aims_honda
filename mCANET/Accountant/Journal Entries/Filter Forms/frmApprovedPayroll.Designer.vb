﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmApprovedPayroll
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtname = New System.Windows.Forms.TextBox()
        Me.dgvApprovedLoan = New System.Windows.Forms.DataGridView()
        Me.btncancel = New System.Windows.Forms.Button()
        Me.btnselect = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.dgvApprovedLoan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtname
        '
        Me.txtname.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.txtname.Location = New System.Drawing.Point(59, 5)
        Me.txtname.Name = "txtname"
        Me.txtname.Size = New System.Drawing.Size(172, 20)
        Me.txtname.TabIndex = 25
        '
        'dgvApprovedLoan
        '
        Me.dgvApprovedLoan.AllowUserToAddRows = False
        Me.dgvApprovedLoan.AllowUserToDeleteRows = False
        Me.dgvApprovedLoan.AllowUserToResizeColumns = False
        Me.dgvApprovedLoan.AllowUserToResizeRows = False
        Me.dgvApprovedLoan.BackgroundColor = System.Drawing.Color.White
        Me.dgvApprovedLoan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvApprovedLoan.Location = New System.Drawing.Point(14, 31)
        Me.dgvApprovedLoan.Name = "dgvApprovedLoan"
        Me.dgvApprovedLoan.ReadOnly = True
        Me.dgvApprovedLoan.RowHeadersVisible = False
        Me.dgvApprovedLoan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvApprovedLoan.Size = New System.Drawing.Size(257, 201)
        Me.dgvApprovedLoan.TabIndex = 24
        '
        'btncancel
        '
        Me.btncancel.Image = Global.CSAcctg.My.Resources.Resources.reload
        Me.btncancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btncancel.Location = New System.Drawing.Point(202, 239)
        Me.btncancel.Name = "btncancel"
        Me.btncancel.Size = New System.Drawing.Size(69, 29)
        Me.btncancel.TabIndex = 23
        Me.btncancel.Text = "Cancel"
        Me.btncancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btncancel.UseVisualStyleBackColor = True
        '
        'btnselect
        '
        Me.btnselect.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btnselect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnselect.Location = New System.Drawing.Point(120, 239)
        Me.btnselect.Name = "btnselect"
        Me.btnselect.Size = New System.Drawing.Size(70, 29)
        Me.btnselect.TabIndex = 22
        Me.btnselect.Text = "Select"
        Me.btnselect.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnselect.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSearch.Location = New System.Drawing.Point(326, -1)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(67, 30)
        Me.btnSearch.TabIndex = 26
        Me.btnSearch.Text = "Search"
        Me.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSearch.UseVisualStyleBackColor = True
        Me.btnSearch.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "Search"
        '
        'frmApprovedPayroll
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(283, 274)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtname)
        Me.Controls.Add(Me.dgvApprovedLoan)
        Me.Controls.Add(Me.btncancel)
        Me.Controls.Add(Me.btnselect)
        Me.Controls.Add(Me.btnSearch)
        Me.Name = "frmApprovedPayroll"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Payroll for Release"
        CType(Me.dgvApprovedLoan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtname As System.Windows.Forms.TextBox
    Friend WithEvents dgvApprovedLoan As System.Windows.Forms.DataGridView
    Friend WithEvents btncancel As System.Windows.Forms.Button
    Friend WithEvents btnselect As System.Windows.Forms.Button
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
