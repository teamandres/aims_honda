﻿Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.FileSystem
Imports System.Data
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.OleDb

Public Class frmCollections_Integrator

    Dim filenym As String
    Dim fpath As String
    Private con As New Clsappconfiguration
    Public curRow As Integer = 0

    Private Sub CloseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseToolStripMenuItem.Click
        If MsgBox("Are you sure?", vbYesNo + MsgBoxStyle.Question, "Exit Collections Integrator") = vbYes Then
            Me.Close()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub frmCollections_Integrator_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormMode("FirstLoad")
        ActiveControl = dgvList
    End Sub

    Public Function isPrepareColumns() As Boolean
        Try
            dgvList.DataSource = Nothing
            dgvList.Columns.Clear()
            dgvList.Rows.Clear()

            Dim crvno As New DataGridViewTextBoxColumn
            Dim soano As New DataGridViewTextBoxColumn
            Dim orno As New DataGridViewTextBoxColumn
            Dim collectionDate As New DataGridViewTextBoxColumn
            Dim checkDate As New DataGridViewTextBoxColumn
            Dim cardname As New DataGridViewTextBoxColumn
            Dim cardnumber As New DataGridViewTextBoxColumn
            Dim acctDesc As New DataGridViewTextBoxColumn
            Dim acctCode As New DataGridViewTextBoxColumn
            Dim decription As New DataGridViewTextBoxColumn
            Dim debit As New DataGridViewTextBoxColumn
            Dim credit As New DataGridViewTextBoxColumn
            Dim fxkey As New DataGridViewTextBoxColumn
            With crvno
                .Name = "crvno"
                .HeaderText = "CRV No."
            End With
            With soano
                .Name = "soano"
                .HeaderText = "SOA No."
            End With
            With orno
                .Name = "orno"
                .HeaderText = "OR No."
            End With
            With collectionDate
                .Name = "collectionDate"
                .HeaderText = "Collection Date"
            End With
            With checkDate
                .Name = "checkDate"
                .HeaderText = "Check Date"
            End With
            With cardname
                .Name = "cardname"
                .HeaderText = "Card Name"
            End With
            With cardnumber
                .Name = "cardnumber"
                .HeaderText = "Card Number"
            End With
            With acctDesc
                .Name = "acctDesc"
                .HeaderText = "Acct. Description"
            End With
            With acctCode
                .Name = "acctCode"
                .HeaderText = "Acct. Code"
            End With
            With decription
                .Name = "description"
                .HeaderText = "Description"
            End With
            With debit
                .Name = "debit"
                .HeaderText = "Debit"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .DefaultCellStyle.Format = "N2"
            End With
            With credit
                .Name = "credit"
                .HeaderText = "Credit"
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .DefaultCellStyle.Format = "N2"
            End With
            With fxkey
                .Name = "fxkey"
                .HeaderText = "fxkey"
                .Visible = False
            End With
            With dgvList
                .Columns.Add(crvno)
                .Columns.Add(soano)
                .Columns.Add(orno)
                .Columns.Add(collectionDate)
                .Columns.Add(checkDate)
                .Columns.Add(cardname)
                .Columns.Add(cardnumber)
                .Columns.Add(acctDesc)
                .Columns.Add(acctCode)
                .Columns.Add(decription)
                .Columns.Add(debit)
                .Columns.Add(credit)
                .Columns.Add(fxkey)
            End With
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub FormMode(ByVal mode As String)
        Select Case mode
            Case "FirstLoad"
                Panel2.Enabled = False
                Panel3.Enabled = False
            Case "New"
                Panel2.Enabled = True
                Panel3.Enabled = True
        End Select
    End Sub

    Private Sub NewToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewToolStripMenuItem.Click
        If isPrepareColumns() Then
            curRow = 0
            'dgvList.Rows.Add()
            'dgvList.Rows(curRow).Cells(10).Value = "0.00"
            'dgvList.Rows(curRow).Cells(11).Value = "0.00"
            'dgvList.Rows(curRow).Cells(12).Value = System.Guid.NewGuid.ToString
            xAddRow()
            FormMode("New")
            ClearText()
            xmode = "New"
        End If
    End Sub

    Private Sub ClearText()
        txtAmount.Text = "0.00"
        txtBank.Text = ""
        txtCollectionNo.Text = ""
        txtCredit.Text = "0.00"
        txtDebit.Text = "0.00"
        txtDifference.Text = "0.00"
        txtParticulars.Text = ""
        txtPAth.Text = ""
        txtPayee.Text = ""
        txtPreparedBy.Text = ""
    End Sub

    Private Sub ShowBrowse(ByVal text As String, ByVal dtype As String, ByVal x As Integer)
        frmBDoc.doctype = dtype
        frmBDoc.cellPos = x
        frmBDoc.Text = text
        frmBDoc.StartPosition = FormStartPosition.CenterScreen
        frmBDoc.ShowDialog()
    End Sub

    Private Sub dgvList_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvList.CellValueChanged
        Dim totdebit As Decimal = 0.0
        Dim totcredit As Decimal = 0.0
        For i As Integer = 0 To dgvList.RowCount - 1
            With dgvList
                totdebit = totdebit + CDec(.Item("debit", i).Value)
                totcredit = totcredit + CDec(.Item("credit", i).Value)
            End With
        Next
        txtDebit.Text = Format(totdebit, "##,##0.00")
        txtCredit.Text = Format(totcredit, "##,##0.00")
        txtDifference.Text = Format((totdebit - totcredit), "##,##0.00")
        txtAmount.Text = Format(totdebit, "##,##0.00")
        FormatNumValues()
    End Sub

    'Private Sub FormatDateValues()
    '    For i As Integer = 0 To dgvList.RowCount - 1
    '        With dgvList
    '            .Item("collectionDate", i).Value = .Item("collectionDate", i).Value.ToString("MMMM dd,yyyy")
    '            .Item("checkDate", i).Value = .Item("checkDate", i).Value.ToString("MMMM dd,yyyy")
    '        End With
    '    Next
    'End Sub

    Private Sub FormatNumValues()
        For i As Integer = 0 To dgvList.RowCount - 1
            With dgvList
                .Item("debit", i).Value = Format(CDec(.Item("debit", i).Value), "##,##0.00")
                .Item("credit", i).Value = Format(CDec(.Item("credit", i).Value), "##,##0.00")
            End With
        Next
    End Sub

    Private Sub dgvList_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvList.KeyDown
        Select Case dgvList.CurrentCellAddress.X
            Case 0
                If e.KeyCode = Keys.Enter Then
                    ShowBrowse("Browse CRV No.", "OR", 0)
                End If
            Case 1
                If e.KeyCode = Keys.Enter Then
                    ShowBrowse("Browse SOA No.", "SOA", 1)
                End If
            Case 2
                If e.KeyCode = Keys.Enter Then
                    ShowBrowse("Browse OR No.", "OR", 2)
                End If
            Case 3 'Collection Date
                If e.KeyCode = Keys.Enter Then
                    MonthCalendar1.Left = dgvList.GetCellDisplayRectangle(dgvList.CurrentCell.ColumnIndex,
                   dgvList.CurrentCell.RowIndex, False).Left + dgvList.Left
                    MonthCalendar1.Top = dgvList.GetCellDisplayRectangle(dgvList.CurrentCell.ColumnIndex,
                    dgvList.CurrentCell.RowIndex, False).Bottom + dgvList.Top
                    MonthCalendar1.Visible = True
                    MonthCalendar1.Select()
                End If
            Case 4 'Check Date
                If e.KeyCode = Keys.Enter Then
                    MonthCalendar1.Left = dgvList.GetCellDisplayRectangle(dgvList.CurrentCell.ColumnIndex,
                   dgvList.CurrentCell.RowIndex, False).Left + dgvList.Left
                    MonthCalendar1.Top = dgvList.GetCellDisplayRectangle(dgvList.CurrentCell.ColumnIndex,
                    dgvList.CurrentCell.RowIndex, False).Bottom + dgvList.Top
                    MonthCalendar1.Visible = True
                    MonthCalendar1.Select()
                End If
            Case 5
                If e.KeyCode = Keys.Enter Then
                    frmBCard.Text = "Card Name"
                    frmBCard.StartPosition = FormStartPosition.CenterScreen
                    frmBCard.ShowDialog()
                End If
            Case 6
                If e.KeyCode = Keys.Enter Then
                    frmBCard.Text = "Card Number"
                    frmBCard.StartPosition = FormStartPosition.CenterScreen
                    frmBCard.ShowDialog()
                End If
            Case 7
                If e.KeyCode = Keys.Enter Then
                    frmBAccounts.StartPosition = FormStartPosition.CenterScreen
                    frmBAccounts.ShowDialog()
                End If
            Case 8
                If e.KeyCode = Keys.Enter Then
                    frmBAccounts.StartPosition = FormStartPosition.CenterScreen
                    frmBAccounts.ShowDialog()
                End If
            Case 10
                If e.KeyCode = Keys.Enter Then
                    xAddRow()
                End If
            Case 11
                If e.KeyCode = Keys.Enter Then
                    xAddRow()
                End If
            Case Else
                MonthCalendar1.Visible = False
        End Select
    End Sub

    Private Sub xAddRow()
        dgvList.Rows.Add()
        For i As Integer = 0 To 10 - 1
            dgvList.Rows(curRow).Cells(i).Value = " "
        Next
        dgvList.Rows(curRow).Cells(10).Value = "0.00"
        dgvList.Rows(curRow).Cells(11).Value = "0.00"
        dgvList.Rows(curRow).Cells(12).Value = System.Guid.NewGuid.ToString
        curRow += 1
    End Sub

    Private Sub MonthCalendar1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MonthCalendar1.KeyDown
        If e.KeyCode = Keys.Enter Then
            dgvList.SelectedCells(0).Value = MonthCalendar1.SelectionRange.Start.ToString("MM/dd/yyyy")
            MonthCalendar1.Visible = False
        End If
        If e.KeyCode = Keys.Escape Then
            MonthCalendar1.Visible = False
        End If
    End Sub

    Private Sub SearchToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SearchToolStripMenuItem.Click
        frmBSearch.StartPosition = FormStartPosition.CenterScreen
        frmBSearch.ShowDialog()
    End Sub

    Private Sub btnExportLoanDeduction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportLoanDeduction.Click
        frmExportLoanDeductions.StartPosition = FormStartPosition.CenterScreen
        frmExportLoanDeductions.ShowDialog()
    End Sub

    Private Sub SaveToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveToolStripMenuItem.Click
        If txtCollectionNo.Text <> "" Then
            If isHeaderSaved() Then
                If isDetailSaved() Then
                    dgvList.SelectionMode = DataGridViewSelectionMode.CellSelect
                    MsgBox("Your data has been successfully saved!", vbInformation, "Success")
                    xmode = ""
                End If
            End If
        Else
            MsgBox("Collection No. is Required to continue.", vbInformation, "Oops.")
        End If
    End Sub

    Private Function isHeaderSaved() As Boolean
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Collections_InsertUpdate_Header",
                                       New SqlParameter("@fcPreparedBy", txtPreparedBy.Text),
                                       New SqlParameter("@fbDate", dtDate.Value),
                                       New SqlParameter("@fcPayee", txtPayee.Text),
                                       New SqlParameter("@fcParticulars", txtParticulars.Text),
                                       New SqlParameter("@pkCollectionNo", txtCollectionNo.Text),
                                       New SqlParameter("@fcBank", txtBank.Text),
                                       New SqlParameter("@fdAmount", CDec(txtAmount.Text)))
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function isDetailSaved() As Boolean
        Try
            dgvList.SelectionMode = DataGridViewSelectionMode.FullRowSelect
            If xmode = "Upload" Then
                For i As Integer = 0 To dgvList.RowCount - 1
                    With dgvList
                        Dim crvno As String = IIf(.Item("crvno", i).Value Is DBNull.Value, "", .Item("crvno", i).Value.ToString)
                        Dim soano As String = IIf(.Item("soano", i).Value Is DBNull.Value, "", .Item("soano", i).Value.ToString)
                        Dim orno As String = IIf(.Item("orno", i).Value Is DBNull.Value, "", .Item("orno", i).Value.ToString)
                        Dim collectiondate As String = .Item("collectionDate", i).Value.ToString
                        Dim checkDate As String = .Item("checkDate", i).Value.ToString
                        Dim cardname As String = .Item("cardname", i).Value.ToString
                        Dim cardnumber As String = .Item("cardnumber", i).Value.ToString
                        Dim acctDesc As String = .Item("acctDesc", i).Value.ToString
                        Dim acctCode As String = .Item("acctCode", i).Value.ToString
                        Dim description As String = .Item("description", i).Value.ToString
                        Dim debit As String = .Item("debit", i).Value.ToString
                        Dim credit As String = .Item("credit", i).Value.ToString
                        SqlHelper.ExecuteNonQuery(con.cnstring, "_Collections_InsertUpdate_Details",
                                                  New SqlParameter("@fkCollectionNo", txtCollectionNo.Text),
                                                  New SqlParameter("@fcCRVNo", crvno),
                                                  New SqlParameter("@fcSOANo", soano),
                                                  New SqlParameter("@fcORNo", orno),
                                                  New SqlParameter("@fbCollectionDate", collectiondate),
                                                  New SqlParameter("@fbCheckDate", checkDate),
                                                  New SqlParameter("@fcCardName", cardname),
                                                  New SqlParameter("@fcCardNumber", cardnumber),
                                                  New SqlParameter("@Acct_Desc", acctDesc),
                                                  New SqlParameter("@Acct_Code", acctCode),
                                                  New SqlParameter("@fcDescription", description),
                                                  New SqlParameter("@fdDebit", CDec(debit)),
                                                  New SqlParameter("@fdCredit", CDec(credit)),
                                                  New SqlParameter("@fxKey", System.Guid.NewGuid.ToString))
                    End With
                Next
            Else
                For i As Integer = 0 To dgvList.RowCount - 1
                    With dgvList
                        Dim crvno As String = IIf(.Item("crvno", i).Value Is DBNull.Value, "", .Item("crvno", i).Value.ToString)
                        Dim soano As String = IIf(.Item("soano", i).Value Is DBNull.Value, "", .Item("soano", i).Value.ToString)
                        Dim orno As String = IIf(.Item("orno", i).Value Is DBNull.Value, "", .Item("orno", i).Value.ToString)
                        Dim collectiondate As String = .Item("collectionDate", i).Value.ToString
                        Dim checkDate As String = .Item("checkDate", i).Value.ToString
                        Dim cardname As String = .Item("cardname", i).Value.ToString
                        Dim cardnumber As String = .Item("cardnumber", i).Value.ToString
                        Dim acctDesc As String = .Item("acctDesc", i).Value.ToString
                        Dim acctCode As String = .Item("acctCode", i).Value.ToString
                        Dim description As String = IIf(.Item("description", i).Value Is DBNull.Value, "", .Item("description", i).Value.ToString)
                        Dim debit As String = IIf(.Item("debit", i).Value Is DBNull.Value, "0.00", .Item("debit", i).Value.ToString)
                        Dim credit As String = IIf(.Item("credit", i).Value Is DBNull.Value, "0.00", .Item("credit", i).Value.ToString)
                        Dim pkkey As String = .Item("fxkey", i).Value.ToString
                        SqlHelper.ExecuteNonQuery(con.cnstring, "_Collections_InsertUpdate_Details",
                                                  New SqlParameter("@fkCollectionNo", txtCollectionNo.Text),
                                                  New SqlParameter("@fcCRVNo", crvno),
                                                  New SqlParameter("@fcSOANo", soano),
                                                  New SqlParameter("@fcORNo", orno),
                                                  New SqlParameter("@fbCollectionDate", collectiondate),
                                                  New SqlParameter("@fbCheckDate", checkDate),
                                                  New SqlParameter("@fcCardName", cardname),
                                                  New SqlParameter("@fcCardNumber", cardnumber),
                                                  New SqlParameter("@Acct_Desc", acctDesc),
                                                  New SqlParameter("@Acct_Code", acctCode),
                                                  New SqlParameter("@fcDescription", description),
                                                  New SqlParameter("@fdDebit", CDec(debit)),
                                                  New SqlParameter("@fdCredit", CDec(credit)),
                                                  New SqlParameter("@fxKey", pkkey))
                    End With
                Next
            End If
            Return True
        Catch ex As Exception
            Throw
            'MsgBox(ex.Message)
            Return False
        End Try
    End Function

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Try
            OpenFileDialog1.Filter = "Microsoft Excel (*.xlsx)|*.xlsx|Microsoft Excel 97-2003 (*.xls)|*.xls"
            OpenFileDialog1.ShowDialog()
            filenym = OpenFileDialog1.FileName
            fpath = Path.GetFullPath(filenym)
            txtPAth.Text = fpath
            dgvList.Columns.Clear()
            Import()
        Catch ex As Exception
            MsgBox("User Cancelled!", vbInformation, "...")
        End Try
    End Sub

    Dim xmode As String = ""
    Private Sub Import()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim DtSet As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPAth.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)
        MyCommand.TableMappings.Add("Table", "Net-informations.com")
        DtSet = New System.Data.DataSet
        MyCommand.Fill(DtSet)
        dgvList.DataSource = DtSet.Tables(0)
        MyConnection.Close()
        xmode = "Upload"
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        If txtCollectionNo.Text <> "" Then
            If MsgBox("Are you sure you want to delete this collection?", vbYesNo + MsgBoxStyle.Question, "Confirmation") = vbYes Then
                If isDeleted() Then
                    MsgBox("Delete Success!", vbInformation, "Success")
                    If isPrepareColumns() Then
                        curRow = 0
                        dgvList.Rows.Add()
                        dgvList.Rows(curRow).Cells(10).Value = "0.00"
                        dgvList.Rows(curRow).Cells(11).Value = "0.00"
                        dgvList.Rows(curRow).Cells(12).Value = System.Guid.NewGuid.ToString
                        FormMode("New")
                        ClearText()
                    End If
                End If
            Else
                Exit Sub
            End If
        Else
            MsgBox("No data to be deleted!", vbInformation, "Oops.")
        End If
    End Sub

    Private Function isDeleted() As Boolean
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Collections_Delete",
                           New SqlParameter("@pkCollectionNo", txtCollectionNo.Text))
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Sub ViewHeader()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(con.cnstring, "_Collections_ViewRecord",
                                       New SqlParameter("@pkCollectionNo", txtCollectionNo.Text))
        While rd.Read
            txtPreparedBy.Text = rd(0)
            dtDate.Value = rd(1)
            txtPayee.Text = rd(2)
            txtParticulars.Text = rd(3)
            txtBank.Text = rd(4)
            txtAmount.Text = rd(5)
        End While
    End Sub

    Public Sub ViewDetails()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(con.cnstring, "_Collections_ViewRecord2",
                                     New SqlParameter("@pkCollectionNo", txtCollectionNo.Text))
        While rd.Read
            With dgvList
                .Rows.Add()
                .Item("crvno", curRow).Value = rd(0)
                .Item("soano", curRow).Value = rd(1)
                .Item("orno", curRow).Value = rd(2)
                .Item("collectionDate", curRow).Value = rd(3)
                .Item("checkDate", curRow).Value = rd(4)
                .Item("cardname", curRow).Value = rd(5)
                .Item("cardnumber", curRow).Value = rd(6)
                .Item("acctDesc", curRow).Value = rd(7)
                .Item("acctCode", curRow).Value = rd(8)
                .Item("description", curRow).Value = rd(9)
                .Item("debit", curRow).Value = rd(10)
                .Item("credit", curRow).Value = rd(11)
                .Item("fxkey", curRow).Value = rd(12)
                curRow += 1
            End With
        End While
    End Sub

End Class