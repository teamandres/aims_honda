﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCollectionsBilling
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.btnImport = New System.Windows.Forms.Button()
        Me.txtPath = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnGetBilling = New System.Windows.Forms.Button()
        Me.txtBilling = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.dgvPayment = New System.Windows.Forms.DataGridView()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btnDefaultAcct = New System.Windows.Forms.Button()
        Me.txtDefaulAcct = New System.Windows.Forms.TextBox()
        Me.btnPrintBillingD = New System.Windows.Forms.Button()
        Me.btnPrintDiff = New System.Windows.Forms.Button()
        Me.btnGenerateEntry = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtOverUnder = New System.Windows.Forms.TextBox()
        Me.txtCollectionTot = New System.Windows.Forms.TextBox()
        Me.txtTotApplied = New System.Windows.Forms.TextBox()
        Me.txtTotAmount = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.dgvDetails = New System.Windows.Forms.DataGridView()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.btnPrintNotfoundC = New System.Windows.Forms.Button()
        Me.txttotCollection = New System.Windows.Forms.TextBox()
        Me.txtTotBilling = New System.Windows.Forms.TextBox()
        Me.txtPastDue = New System.Windows.Forms.TextBox()
        Me.txtCurrentDue = New System.Windows.Forms.TextBox()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblBilldate = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvPayment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.dgvDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.lblBilldate)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.btnCalculate)
        Me.Panel1.Controls.Add(Me.btnImport)
        Me.Panel1.Controls.Add(Me.txtPath)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.btnGetBilling)
        Me.Panel1.Controls.Add(Me.txtBilling)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1004, 83)
        Me.Panel1.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.Desktop
        Me.Label5.Location = New System.Drawing.Point(437, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(108, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Billing Payment"
        '
        'btnCalculate
        '
        Me.btnCalculate.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.btnCalculate.Location = New System.Drawing.Point(921, 52)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(75, 23)
        Me.btnCalculate.TabIndex = 6
        Me.btnCalculate.Text = "Calculate"
        Me.btnCalculate.UseVisualStyleBackColor = True
        Me.btnCalculate.Visible = False
        '
        'btnImport
        '
        Me.btnImport.Location = New System.Drawing.Point(789, 52)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(126, 23)
        Me.btnImport.TabIndex = 5
        Me.btnImport.Text = "Import Collections"
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'txtPath
        '
        Me.txtPath.Location = New System.Drawing.Point(511, 54)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.ReadOnly = True
        Me.txtPath.Size = New System.Drawing.Size(272, 21)
        Me.txtPath.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(437, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "File Path :"
        '
        'btnGetBilling
        '
        Me.btnGetBilling.Location = New System.Drawing.Point(360, 52)
        Me.btnGetBilling.Name = "btnGetBilling"
        Me.btnGetBilling.Size = New System.Drawing.Size(75, 23)
        Me.btnGetBilling.TabIndex = 2
        Me.btnGetBilling.Text = "Get Billing"
        Me.btnGetBilling.UseVisualStyleBackColor = True
        '
        'txtBilling
        '
        Me.txtBilling.Location = New System.Drawing.Point(82, 54)
        Me.txtBilling.Name = "txtBilling"
        Me.txtBilling.ReadOnly = True
        Me.txtBilling.Size = New System.Drawing.Size(272, 21)
        Me.txtBilling.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 62)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Billing :"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.dgvPayment)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 83)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1004, 170)
        Me.Panel2.TabIndex = 1
        '
        'dgvPayment
        '
        Me.dgvPayment.AllowUserToAddRows = False
        Me.dgvPayment.AllowUserToDeleteRows = False
        Me.dgvPayment.AllowUserToResizeColumns = False
        Me.dgvPayment.AllowUserToResizeRows = False
        Me.dgvPayment.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvPayment.BackgroundColor = System.Drawing.Color.White
        Me.dgvPayment.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvPayment.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPayment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvPayment.Location = New System.Drawing.Point(0, 0)
        Me.dgvPayment.Name = "dgvPayment"
        Me.dgvPayment.ReadOnly = True
        Me.dgvPayment.RowHeadersVisible = False
        Me.dgvPayment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPayment.Size = New System.Drawing.Size(1004, 170)
        Me.dgvPayment.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.White
        Me.Panel3.Controls.Add(Me.btnDefaultAcct)
        Me.Panel3.Controls.Add(Me.txtDefaulAcct)
        Me.Panel3.Controls.Add(Me.btnPrintBillingD)
        Me.Panel3.Controls.Add(Me.btnPrintDiff)
        Me.Panel3.Controls.Add(Me.btnGenerateEntry)
        Me.Panel3.Controls.Add(Me.btnClose)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(0, 462)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1004, 30)
        Me.Panel3.TabIndex = 2
        '
        'btnDefaultAcct
        '
        Me.btnDefaultAcct.Location = New System.Drawing.Point(318, 3)
        Me.btnDefaultAcct.Name = "btnDefaultAcct"
        Me.btnDefaultAcct.Size = New System.Drawing.Size(227, 23)
        Me.btnDefaultAcct.TabIndex = 5
        Me.btnDefaultAcct.Text = "Default Account for Over Payment"
        Me.btnDefaultAcct.UseVisualStyleBackColor = True
        '
        'txtDefaulAcct
        '
        Me.txtDefaulAcct.Location = New System.Drawing.Point(551, 6)
        Me.txtDefaulAcct.Name = "txtDefaulAcct"
        Me.txtDefaulAcct.ReadOnly = True
        Me.txtDefaulAcct.Size = New System.Drawing.Size(221, 21)
        Me.txtDefaulAcct.TabIndex = 4
        '
        'btnPrintBillingD
        '
        Me.btnPrintBillingD.Location = New System.Drawing.Point(187, 3)
        Me.btnPrintBillingD.Name = "btnPrintBillingD"
        Me.btnPrintBillingD.Size = New System.Drawing.Size(90, 23)
        Me.btnPrintBillingD.TabIndex = 3
        Me.btnPrintBillingD.Text = "Print Details"
        Me.btnPrintBillingD.UseVisualStyleBackColor = True
        '
        'btnPrintDiff
        '
        Me.btnPrintDiff.Location = New System.Drawing.Point(12, 4)
        Me.btnPrintDiff.Name = "btnPrintDiff"
        Me.btnPrintDiff.Size = New System.Drawing.Size(169, 23)
        Me.btnPrintDiff.TabIndex = 2
        Me.btnPrintDiff.Text = "Print Over/Under Payment"
        Me.btnPrintDiff.UseVisualStyleBackColor = True
        '
        'btnGenerateEntry
        '
        Me.btnGenerateEntry.Location = New System.Drawing.Point(778, 4)
        Me.btnGenerateEntry.Name = "btnGenerateEntry"
        Me.btnGenerateEntry.Size = New System.Drawing.Size(133, 23)
        Me.btnGenerateEntry.TabIndex = 1
        Me.btnGenerateEntry.Text = "Generate Entry"
        Me.btnGenerateEntry.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(917, 4)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.White
        Me.Panel4.Controls.Add(Me.txtOverUnder)
        Me.Panel4.Controls.Add(Me.txtCollectionTot)
        Me.Panel4.Controls.Add(Me.txtTotApplied)
        Me.Panel4.Controls.Add(Me.txtTotAmount)
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 253)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1004, 29)
        Me.Panel4.TabIndex = 3
        '
        'txtOverUnder
        '
        Me.txtOverUnder.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtOverUnder.ForeColor = System.Drawing.Color.Red
        Me.txtOverUnder.Location = New System.Drawing.Point(673, 5)
        Me.txtOverUnder.Name = "txtOverUnder"
        Me.txtOverUnder.ReadOnly = True
        Me.txtOverUnder.Size = New System.Drawing.Size(160, 21)
        Me.txtOverUnder.TabIndex = 4
        Me.txtOverUnder.Text = "0.00"
        Me.txtOverUnder.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCollectionTot
        '
        Me.txtCollectionTot.Location = New System.Drawing.Point(839, 5)
        Me.txtCollectionTot.Name = "txtCollectionTot"
        Me.txtCollectionTot.ReadOnly = True
        Me.txtCollectionTot.Size = New System.Drawing.Size(162, 21)
        Me.txtCollectionTot.TabIndex = 3
        Me.txtCollectionTot.Text = "0.00"
        Me.txtCollectionTot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotApplied
        '
        Me.txtTotApplied.Location = New System.Drawing.Point(507, 5)
        Me.txtTotApplied.Name = "txtTotApplied"
        Me.txtTotApplied.ReadOnly = True
        Me.txtTotApplied.Size = New System.Drawing.Size(160, 21)
        Me.txtTotApplied.TabIndex = 2
        Me.txtTotApplied.Text = "0.00"
        Me.txtTotApplied.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotAmount
        '
        Me.txtTotAmount.Location = New System.Drawing.Point(335, 5)
        Me.txtTotAmount.Name = "txtTotAmount"
        Me.txtTotAmount.ReadOnly = True
        Me.txtTotAmount.Size = New System.Drawing.Size(166, 21)
        Me.txtTotAmount.TabIndex = 1
        Me.txtTotAmount.Text = "0.00"
        Me.txtTotAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(274, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Totals :"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.Controls.Add(Me.dgvDetails)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(0, 282)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1004, 142)
        Me.Panel5.TabIndex = 4
        '
        'dgvDetails
        '
        Me.dgvDetails.AllowUserToAddRows = False
        Me.dgvDetails.AllowUserToDeleteRows = False
        Me.dgvDetails.AllowUserToResizeColumns = False
        Me.dgvDetails.AllowUserToResizeRows = False
        Me.dgvDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvDetails.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetails.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvDetails.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDetails.Location = New System.Drawing.Point(0, 0)
        Me.dgvDetails.Name = "dgvDetails"
        Me.dgvDetails.ReadOnly = True
        Me.dgvDetails.RowHeadersVisible = False
        Me.dgvDetails.Size = New System.Drawing.Size(1004, 142)
        Me.dgvDetails.TabIndex = 1
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.White
        Me.Panel6.Controls.Add(Me.btnPrintNotfoundC)
        Me.Panel6.Controls.Add(Me.txttotCollection)
        Me.Panel6.Controls.Add(Me.txtTotBilling)
        Me.Panel6.Controls.Add(Me.txtPastDue)
        Me.Panel6.Controls.Add(Me.txtCurrentDue)
        Me.Panel6.Controls.Add(Me.btnNew)
        Me.Panel6.Controls.Add(Me.Label4)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel6.Location = New System.Drawing.Point(0, 424)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(1004, 38)
        Me.Panel6.TabIndex = 5
        '
        'btnPrintNotfoundC
        '
        Me.btnPrintNotfoundC.Location = New System.Drawing.Point(110, 12)
        Me.btnPrintNotfoundC.Name = "btnPrintNotfoundC"
        Me.btnPrintNotfoundC.Size = New System.Drawing.Size(167, 23)
        Me.btnPrintNotfoundC.TabIndex = 7
        Me.btnPrintNotfoundC.Text = "Print Not Found Client"
        Me.btnPrintNotfoundC.UseVisualStyleBackColor = True
        Me.btnPrintNotfoundC.Visible = False
        '
        'txttotCollection
        '
        Me.txttotCollection.Location = New System.Drawing.Point(882, 3)
        Me.txttotCollection.Name = "txttotCollection"
        Me.txttotCollection.ReadOnly = True
        Me.txttotCollection.Size = New System.Drawing.Size(119, 21)
        Me.txttotCollection.TabIndex = 6
        Me.txttotCollection.Text = "0.00"
        Me.txttotCollection.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotBilling
        '
        Me.txtTotBilling.Location = New System.Drawing.Point(757, 3)
        Me.txtTotBilling.Name = "txtTotBilling"
        Me.txtTotBilling.ReadOnly = True
        Me.txtTotBilling.Size = New System.Drawing.Size(119, 21)
        Me.txtTotBilling.TabIndex = 5
        Me.txtTotBilling.Text = "0.00"
        Me.txtTotBilling.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPastDue
        '
        Me.txtPastDue.Location = New System.Drawing.Point(632, 3)
        Me.txtPastDue.Name = "txtPastDue"
        Me.txtPastDue.ReadOnly = True
        Me.txtPastDue.Size = New System.Drawing.Size(119, 21)
        Me.txtPastDue.TabIndex = 4
        Me.txtPastDue.Text = "0.00"
        Me.txtPastDue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCurrentDue
        '
        Me.txtCurrentDue.Location = New System.Drawing.Point(507, 3)
        Me.txtCurrentDue.Name = "txtCurrentDue"
        Me.txtCurrentDue.ReadOnly = True
        Me.txtCurrentDue.Size = New System.Drawing.Size(119, 21)
        Me.txtCurrentDue.TabIndex = 3
        Me.txtCurrentDue.Text = "0.00"
        Me.txtCurrentDue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnNew
        '
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.ForeColor = System.Drawing.Color.Blue
        Me.btnNew.Location = New System.Drawing.Point(12, 12)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(95, 23)
        Me.btnNew.TabIndex = 2
        Me.btnNew.Text = "New"
        Me.btnNew.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(446, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Totals :"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(26, 23)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(81, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Billing Date :"
        '
        'lblBilldate
        '
        Me.lblBilldate.AutoSize = True
        Me.lblBilldate.Location = New System.Drawing.Point(114, 22)
        Me.lblBilldate.Name = "lblBilldate"
        Me.lblBilldate.Size = New System.Drawing.Size(0, 13)
        Me.lblBilldate.TabIndex = 9
        '
        'frmCollectionsBilling
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1004, 492)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmCollectionsBilling"
        Me.Text = "Billing Payment"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvPayment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        CType(Me.dgvDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnGetBilling As System.Windows.Forms.Button
    Friend WithEvents txtBilling As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btnCalculate As System.Windows.Forms.Button
    Friend WithEvents dgvPayment As System.Windows.Forms.DataGridView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnPrintBillingD As System.Windows.Forms.Button
    Friend WithEvents btnPrintDiff As System.Windows.Forms.Button
    Friend WithEvents btnGenerateEntry As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtOverUnder As System.Windows.Forms.TextBox
    Friend WithEvents txtCollectionTot As System.Windows.Forms.TextBox
    Friend WithEvents txtTotApplied As System.Windows.Forms.TextBox
    Friend WithEvents txtTotAmount As System.Windows.Forms.TextBox
    Friend WithEvents btnDefaultAcct As System.Windows.Forms.Button
    Friend WithEvents txtDefaulAcct As System.Windows.Forms.TextBox
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents dgvDetails As System.Windows.Forms.DataGridView
    Friend WithEvents txttotCollection As System.Windows.Forms.TextBox
    Friend WithEvents txtTotBilling As System.Windows.Forms.TextBox
    Friend WithEvents txtPastDue As System.Windows.Forms.TextBox
    Friend WithEvents txtCurrentDue As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnPrintNotfoundC As System.Windows.Forms.Button
    Friend WithEvents lblBilldate As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
End Class
