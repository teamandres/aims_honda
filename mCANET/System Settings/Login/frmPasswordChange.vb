Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading
Public Class frmPasswordChange
    Inherits System.Windows.Forms.Form
    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        If Me.txtnewpassword.Text = Me.txtconfirmpassword.Text Then
            Call changepassword()
        Else
            MessageBox.Show("Password Mismatch", "Mismatch", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1)
        End If
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub
    Private Sub changepassword()
        If strSysCurrentPwd = txtoldpassword.Text Then
            Dim EncrytedPassword As String = Clsappconfiguration.GetEncryptedData(Me.txtconfirmpassword.Text)
            'Dim myadapter As New SqlDataAdapter
            'Dim mydataset As New DataSet
            'Dim appRdr As New System.Configuration.AppSettingsReader
            'Dim myconnection As New Clsappconfiguration
            'Dim cmd As New SqlCommand("SYS_ChangePassword_Update", myconnection.sqlconn)
            Try
                'cmd.CommandType = CommandType.StoredProcedure
                'cmd.Parameters.Clear()
                'cmd.Parameters.Add("@fxKeyUser", SqlDbType.Int).Value = intSysCurrentId
                'cmd.Parameters.Add("@Password", SqlDbType.VarChar, 50).Value = Clsappconfiguration.GetEncryptedData(Me.txtconfirmpassword.Text)
                'strSysCurrentPwd = Me.txtconfirmpassword.Text
                'myconnection.sqlconn.Open()
                'cmd.ExecuteNonQuery()
                Dim mycon As New Clsappconfiguration
                SqlHelper.ExecuteNonQuery(mycon.sqlconn, CommandType.StoredProcedure, "spu_Users_UpdatePassword", _
                                           New SqlParameter("@fxKeyUser", intSysCurrentId),
                                           New SqlParameter("@fcPassword", EncrytedPassword))

                MessageBox.Show("Password successfully updated!", "Change Password Form", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
                mycon.sqlconn.Close()
            Catch
                MsgBox(Err.Description, MsgBoxStyle.Critical, "Change Password Form")
            Finally
                Me.Close()
            End Try
        Else
            MessageBox.Show("Invalid Password", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1)
        End If

    End Sub
    Private Sub cleartxt()
        txtoldpassword.Text = ""
        txtnewpassword.Text = ""
    End Sub
    Public Sub incrpyte_password()
        Dim PassInt As Integer
        Dim i As Integer


        PassInt = GetIntegerFromPassword(txtnewpassword.Text)
        Label2.Text = ""

        For i = 1 To Len(txtnewpassword.Text)

            Label2.Text = String.Concat(Label2.Text, CStr(Chr(CInt(Asc(Mid(txtnewpassword.Text, i, 1)) * PassInt / 2 / 3 / 4 / 5 / 6))))

        Next i
    End Sub
    Function GetIntegerFromPassword(ByVal Password As String) As Integer
        Dim i As Integer
        Dim Acumm As Integer
        'I'm going to pass across the entire String for to check wath chars it have
        For i = 1 To Len(Password)
            Acumm = Acumm + Asc(Mid(Password, i, 1))
        Next

        'So Acumm now is an Integer, equal to the password.
        'return it
        Return Acumm

    End Function


    'Private Sub getoldpass()

    '    With Me

    '        '-----check if useraccount is valid-----'

    '        Dim myreader As SqlDataReader
    '        Dim myadapter As New SqlDataAdapter
    '        Dim mydataset As New DataSet
    '        Dim appRdr As New System.Configuration.AppSettingsReader
    '        Dim myconnection As New Clsappconfiguration
    '        Dim cmd As New SqlCommand("USP_GET_EMPLOYEEID", myconnection.sqlconn)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        cmd.Parameters.Clear()
    '        cmd.Parameters.Add("@SelectedOption", SqlDbType.Int, ParameterDirection.Input).Value = "1"
    '        cmd.Parameters.Add("@EmployeeID", SqlDbType.Int, ParameterDirection.Input).Value = employeeid

    '        myconnection.sqlconn.Open()
    '        myreader = cmd.ExecuteReader()

    '        Try

    '            While myreader.Read()

    '                dbpword = myreader.Item(0)

    '            End While

    '        Finally

    '            myreader.Close()
    '            myconnection.sqlconn.Close()

    '        End Try

    '        If .txtoldpassword.Text = dbpword Then
    '            Call changepassword()
    '            Me.Close()
    '        Else
    '            MessageBox.Show("Invalid Password", "Invalid Password", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
    '        End If

    '    End With

    'End Sub

    'Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    '    If Me.Opacity < 1 Then
    '        Me.Opacity = Me.Opacity + 0.06
    '        Me.Refresh()
    '    End If

    '    If Me.Opacity = 6.0 Then
    '        Timer1.Enabled = False
    '    End If
    'End Sub

    Private Sub frmPasswordChange_load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.Opacity = 0.6
        lblUserName.Text = strSysCurrentUserName
        lblFullName.Text = strSysCurrentFullName
        lblDescription.Text = strSysCurrentDesc

    End Sub
End Class
