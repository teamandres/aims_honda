<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUploadItemMaster
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvUpload = New System.Windows.Forms.DataGridView
        Me.btnUpload = New System.Windows.Forms.Button
        Me.btnTextFile = New System.Windows.Forms.Button
        Me.bgwUpload = New System.ComponentModel.BackgroundWorker
        Me.pbUpload = New System.Windows.Forms.ProgressBar
        Me.btnClose = New System.Windows.Forms.Button
        CType(Me.dgvUpload, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvUpload
        '
        Me.dgvUpload.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUpload.Location = New System.Drawing.Point(12, 12)
        Me.dgvUpload.Name = "dgvUpload"
        Me.dgvUpload.Size = New System.Drawing.Size(738, 440)
        Me.dgvUpload.TabIndex = 0
        '
        'btnUpload
        '
        Me.btnUpload.Location = New System.Drawing.Point(122, 458)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(104, 23)
        Me.btnUpload.TabIndex = 3
        Me.btnUpload.Text = "Upload"
        Me.btnUpload.UseVisualStyleBackColor = True
        '
        'btnTextFile
        '
        Me.btnTextFile.Location = New System.Drawing.Point(12, 458)
        Me.btnTextFile.Name = "btnTextFile"
        Me.btnTextFile.Size = New System.Drawing.Size(104, 23)
        Me.btnTextFile.TabIndex = 4
        Me.btnTextFile.Text = "Open Text File"
        Me.btnTextFile.UseVisualStyleBackColor = True
        '
        'bgwUpload
        '
        '
        'pbUpload
        '
        Me.pbUpload.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.pbUpload.Location = New System.Drawing.Point(562, 458)
        Me.pbUpload.MarqueeAnimationSpeed = 20
        Me.pbUpload.Name = "pbUpload"
        Me.pbUpload.Size = New System.Drawing.Size(188, 23)
        Me.pbUpload.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.pbUpload.TabIndex = 5
        Me.pbUpload.Visible = False
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(232, 458)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(104, 23)
        Me.btnClose.TabIndex = 6
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmUploadItemMaster
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(762, 491)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.pbUpload)
        Me.Controls.Add(Me.btnTextFile)
        Me.Controls.Add(Me.btnUpload)
        Me.Controls.Add(Me.dgvUpload)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUploadItemMaster"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Upload Item Master"
        CType(Me.dgvUpload, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvUpload As System.Windows.Forms.DataGridView
    Friend WithEvents btnUpload As System.Windows.Forms.Button
    Friend WithEvents btnTextFile As System.Windows.Forms.Button
    Friend WithEvents bgwUpload As System.ComponentModel.BackgroundWorker
    Friend WithEvents pbUpload As System.Windows.Forms.ProgressBar
    Friend WithEvents btnClose As System.Windows.Forms.Button

End Class
