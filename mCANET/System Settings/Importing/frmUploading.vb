Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports Microsoft.ApplicationBlocks.Data

Public Class frmUploadItemMaster

    Dim gcon As New Clsappconfiguration

    Private Function GetDataTableFromFile(ByVal filePath As String, _
                                     ByVal separator As Char, _
                                     ByVal numberOfFields As Integer, _
                                     Optional ByVal tableName As String = "myTable") _
                                     As DataTable
        Dim dt As DataTable = Nothing
        Dim sr As System.IO.StreamReader = Nothing
        Dim columnFields As System.IO.StreamReader = Nothing
        Dim count As Integer

        If System.IO.File.Exists(filePath) Then
            Try
                dt = New DataTable(tableName)

                'Add columns to datatable
                columnFields = New System.IO.StreamReader(filePath)
                Dim header() As String = Nothing

                'Insert the Column Header
                header = columnFields.ReadLine().Split(separator)
                For i As Integer = 0 To header.Length - 1
                    dt.Columns.Add(New DataColumn(header(i)))
                Next

                'Open text file
                sr = New System.IO.StreamReader(filePath)
                Dim fields() As String
                While sr.Peek <> -1

                    fields = sr.ReadLine().Split(separator)
                    Dim row As DataRow = dt.NewRow()

                    'Assign values to each fields on the new row
                    If count <> 0 Then
                        For j As Integer = 0 To fields.Length - 1
                            row.Item(j) = fields(j)
                        Next
                        'Add the new row to datatable
                        dt.Rows.Add(row)
                    End If

                    count += 1
                End While

            Catch ex As Exception
                'If any error occurs during the process, display it
                MsgBox(ex.Message)
                Return Nothing

            Finally
                'Close the stream
                If Not IsNothing(sr) Then
                    sr.Close()
                End If
            End Try
        Else
            MsgBox("File Not Found")
        End If
        Return dt
    End Function

    Private Function getHeaderFromSource(ByVal filepath As String, _
                                         ByVal separator As String) As String()

        Dim columnFields = New System.IO.StreamReader(filepath)
        Dim header() As String = Nothing

        header = columnFields.ReadLine().Split(separator)

        Return header

    End Function

    Private Function getHeaderFromDestination() As String()
        Dim sSQLcmd As String = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = 'mTemporaryUpload'"
        Dim Header(20) As String
        Dim index As Integer = 0

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLcmd)
                For index = 0 To Header.Length - 1
                    rd.Read()
                    Header(index) = rd.Item(0).ToString
                Next
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return Header
    End Function

    Private Sub updateItemCodeTable()
        Dim sSQLcmd As String = "SELECT fcItemCode,fcDescription FROM dbo.mTemporaryUpload"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLcmd)
                While rd.Read
                    m_saveitemcode(rd.Item(0), rd.Item(1), Me)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
    End Sub

    Private Sub updateItemBrandTable()
        Dim sSQLcmd As String = "SELECT fcDepartment,fcDepartment FROM dbo.mTemporaryUpload"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLcmd)
                While rd.Read
                    m_saveitembrand(rd.Item(0), rd.Item(1), Me)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub updateItemCategoryTable()
        Dim sSQLcmd As String = "SELECT fcCategory,fcCategory FROM dbo.mTemporaryUpload"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLcmd)
                While rd.Read
                    m_saveitemcategory(rd.Item(0), rd.Item(1), Me)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub updateItemSizeTable()
        Dim sSQLcmd As String = "SELECT fcProductSize,LEFT(fcProductSize,2) + '/' + RIGHT(fcProductSize,1) FROM dbo.mTemporaryUpload"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLcmd)
                While rd.Read
                    If rd.Item(0) = "NONE" Then
                        m_saveitemsize(rd.Item(0), "NONE", Me)
                    Else
                        m_saveitemsize(rd.Item(0), rd.Item(1), Me)
                    End If

                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub updateColorTable()
        Dim sSQLcmd As String = "SELECT	LEFT(fcColor,4), SUBSTRING(fcColor,6,LEN(fcColor)-5) "
        sSQLcmd &= "FROM mTemporaryUpload"
        Try
            Using rd = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLcmd)
                While rd.Read
                    m_saveitemcolor(rd.Item(0), rd.Item(1), Me)
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub updateItemMaster()
        Dim sSQLcmd As String = "SELECT fcItemCode, fcDescription, fcDepartment, fcCategory, fcSubcategory, "
        sSQLcmd &= "fcStyle, fcProductSize, LEFT(fcColor,4), fdUnitPrice "
        sSQLcmd &= "FROM mTemporaryUpload"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLcmd)
                While rd.Read
                    m_saveItemMaster(rd.Item(0), rd.Item(1), rd.Item(2), rd.Item(3), rd.Item(4), _
                                     rd.Item(5), rd.Item(6), rd.Item(7), CDec(rd.Item(8)))
                End While
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click

        pbUpload.Visible = True
        btnClose.Enabled = False
        bgwUpload.RunWorkerAsync()

    End Sub

    Private Sub btnTextFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTextFile.Click
        Dim txtFileToTable As DataTable = GetDataTableFromFile("C:\ItemMasterFile.csv", ",", 21)
        dgvUpload.DataSource = txtFileToTable
        btnUpload.Enabled = True
    End Sub

    Private Sub frmUploadItemMaster_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If btnClose.Enabled = False Then
            MsgBox("Please do not close while uploading." & vbNewLine & "This will cause damage to your database.", MsgBoxStyle.Exclamation)
            e.Cancel = True
        End If
    End Sub

    Private Sub frmUploadItemMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnUpload.Enabled = False
    End Sub

    Private Sub bgwUpload_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwUpload.DoWork
        Dim SourceTable As DataTable = GetDataTableFromFile("C:\ItemMasterFile.csv", ",", 21)
        gcon.sqlconn.Open()

        Using CopyDatabase As SqlBulkCopy = New SqlBulkCopy(gcon.sqlconn)

            Dim worker As System.ComponentModel.BackgroundWorker = DirectCast(sender, System.ComponentModel.BackgroundWorker)
            Dim sourceHeader() As String = getHeaderFromSource("C:\ItemMasterFile.csv", ",")
            Dim destinationHeader() As String = getHeaderFromDestination()
            Dim index As Integer = 0

            CopyDatabase.DestinationTableName = "dbo.mTemporaryUpload"
            For index = 0 To sourceHeader.Length - 1
                Dim mapColumn As New SqlBulkCopyColumnMapping(sourceHeader(index), destinationHeader(index))
                CopyDatabase.ColumnMappings.Add(mapColumn)
            Next

            Try
                CopyDatabase.WriteToServer(SourceTable)
                updateItemCodeTable()
                updateItemBrandTable()
                updateItemCategoryTable()
                updateItemSizeTable()
                updateColorTable()
                updateItemMaster()
                MsgBox("Uploading Successful!", MsgBoxStyle.Exclamation, "Upload")
            Catch ex As Exception
                MsgBox("Error in Uploading")
            End Try
        End Using
        gcon.sqlconn.Close()
    End Sub

    Private Sub bgwUpload_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwUpload.RunWorkerCompleted
        btnClose.Enabled = True
        pbUpload.Visible = False
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
        Me.Dispose()
    End Sub
End Class
