﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading, System.IO

Public Class frmGenerateTemplate
    Private gCon As New Clsappconfiguration()
    Dim c As New Clsappconfiguration
    Dim cs = c.cnstring

    Private Sub LoadAccountType()
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("spu_CIMS_DebitCreditAccount_List", gCon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Document")
            With cboAcntType
                .ValueMember = "fxkey_AccountID"
                .DisplayMember = "fcAccountName"
                .DataSource = ds.Tables(0)
                '.SelectedIndex = -1
                .Text = "Select"
            End With
            gCon.sqlconn.Close()
        Catch ex As Exception

        End Try

    End Sub

    Private Sub LoadAccounts()
        grdList.Rows.Clear()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "spu_CIMS_AccountRegister_ListToBeExport", _
                                   New SqlParameter("@AccountTitle", cboAcntType.Text))
        Try

            While rd.Read
                Dim row As String() = New String() {rd.Item(0).ToString, rd.Item(1).ToString, rd.Item(2).ToString, rd.Item(3).ToString, rd.Item(4).ToString, Format(CDec(rd.Item(5).ToString), "##,##0.00"), Format(CDec(rd.Item(6).ToString), "##,##0.00")}
                grdList.Rows.Add(row)
            End While
            rd.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmGenerateTemplate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call LoadAccountType()
        Call LoadAccounts()
    End Sub

    Private Sub btnGenerateTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerateTemplate.Click
        SaveFileDialog1.Filter = "Microsoft Excel (*.xlsx)|*.xlsx|Microsoft Excel 97-2003 (*.xls)|*.xls"
        SaveFileDialog1.ShowDialog()
        If DialogResult.OK Then
            GenerateTemplate()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub GenerateTemplate()
        Dim xlApp As Microsoft.Office.Interop.Excel.Application
        Dim xlWorkBook As Microsoft.Office.Interop.Excel.Workbook
        Dim xlWorkSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value
        Dim i As Integer
        Dim j As Integer

        xlApp = New Microsoft.Office.Interop.Excel.Application
        xlWorkBook = xlApp.Workbooks.Add(misValue)
        xlWorkSheet = xlWorkBook.Sheets("sheet1")


        For i = 0 To grdList.RowCount - 1
            For j = 0 To grdList.ColumnCount - 1
                For k As Integer = 1 To grdList.Columns.Count
                    xlWorkSheet.Cells(1, k) = grdList.Columns(k - 1).HeaderText
                    xlWorkSheet.Cells(i + 2, j + 1) = grdList(j, i).Value
                Next
            Next
        Next

        xlWorkSheet.SaveAs(Path.GetFullPath(SaveFileDialog1.FileName))
        xlWorkBook.Close()
        xlApp.Quit()

        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)

        Dim res As MsgBoxResult
        res = MsgBox("Process completed, Would you like to open file?", MsgBoxStyle.YesNo)
        If (res = MsgBoxResult.Yes) Then
            Process.Start(Path.GetFullPath(SaveFileDialog1.FileName))
        End If
    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub cboAcntType_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAcntType.TextChanged
        Call LoadAccounts()
    End Sub
End Class