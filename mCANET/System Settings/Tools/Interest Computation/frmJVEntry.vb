﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmJVEntry
    Private gCon As New Clsappconfiguration()
    Public xType As String
    Public xMode As String

    Private Sub frmJVEntry_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Select Case xMode
            Case "JV"
                ListDocNum()
            Case "CHV"
                ListCHVNum()
            Case "CHVUsed"
                ListCHVUsed()
        End Select

    End Sub

    Private Sub ListDocNum()
        Try
            Dim mycon As New Clsappconfiguration
            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, "SELECT TransKey, JVNo FROM tbl_ComputationList WHERE coid='" & gCompanyID() & "' AND TransType='" & xType & "' AND JVNo LIKE '%' + '" & txtSearch.Text & "' + '%' AND Posted = 1  ORDER BY JVNo")

            grdList.DataSource = ds.Tables(0)

            With grdList
                .Columns("TransKey").Visible = False
                .Columns("JVNo").HeaderText = "Document Number"
                .Columns("JVNo").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("JVNo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns("JVNo").Width = 260
            End With
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub ListCHVNum()
        Try
            Dim mycon As New Clsappconfiguration
            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, "SELECT fcDocNumber FROM mDocNumber WHERE fkCoid = '" & gCompanyID() & "' AND fcDocType = 'CHV' AND fcDocNumber LIKE '%' + '" & txtSearch.Text & "' + '%' AND fdDateUsed IS NULL ORDER BY fcDocNumber")

            grdList.DataSource = ds.Tables(0)

            With grdList
                .Columns("fcDocNumber").HeaderText = "Document Number"
                .Columns("fcDocNumber").SortMode = DataGridViewColumnSortMode.NotSortable
                .Columns("fcDocNumber").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .Columns("fcDocNumber").Width = 260
            End With
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub ListCHVUsed()
        Dim ds As DataSet

        ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, "SELECT fxKeyJVNo, fiEntryNo FROM tJVEntry WHERE fxKeyCompany = '" & gCompanyID() & "' AND fkDoctype = 'CHV' AND fiEntryNo LIKE '%' + '" & txtSearch.Text & "' + '%' ORDER BY fiEntryNo ASC")

        grdList.DataSource = ds.Tables(0)

        With grdList
            .Columns("fxKeyJVNo").Visible = False
            .Columns("fiEntryNo").HeaderText = "Document Number"
            .Columns("fiEntryNo").SortMode = DataGridViewColumnSortMode.NotSortable
            .Columns("fiEntryNo").Width = 260
        End With
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.TextChanged
        Select Case xMode
            Case "JV"
                ListDocNum()
            Case "CHV"
                ListCHVNum()
            Case "CHVUsed"
                ListCHVUsed()
        End Select
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(sender As System.Object, e As System.EventArgs) Handles btnSelect.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub
End Class