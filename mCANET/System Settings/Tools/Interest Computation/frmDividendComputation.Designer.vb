﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDividendComputation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.grdList = New System.Windows.Forms.DataGridView()
        Me.memKey = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.memID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cAcntRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cJan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cFeb = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cApr = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cMay = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cJun = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cJul = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cAug = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cSep = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cOct = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNov = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDec = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cAverage = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cDividend = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.txtAccount = New System.Windows.Forms.TextBox()
        Me.btnAccount = New System.Windows.Forms.Button()
        Me.btnCompute = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtDividendFactor = New System.Windows.Forms.TextBox()
        Me.txtAllocatedIncome = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpDate = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTotalMember = New System.Windows.Forms.TextBox()
        Me.txtTotalDividend = New System.Windows.Forms.TextBox()
        Me.txtTotalAverageShare = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.pbStatus = New System.Windows.Forms.ProgressBar()
        Me.bgwPrint = New System.ComponentModel.BackgroundWorker()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cboPostingType = New System.Windows.Forms.ComboBox()
        Me.dtpPostingDate = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.btnPost = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtJVNo = New System.Windows.Forms.TextBox()
        Me.btnSearchJVno = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCreditAccount = New System.Windows.Forms.TextBox()
        Me.btnSearchCredit = New System.Windows.Forms.Button()
        Me.txtDebitAccount = New System.Windows.Forms.TextBox()
        Me.btnSearchDebit = New System.Windows.Forms.Button()
        Me.bgwPost = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.grdList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'grdList
        '
        Me.grdList.AllowUserToAddRows = False
        Me.grdList.AllowUserToDeleteRows = False
        Me.grdList.AllowUserToResizeColumns = False
        Me.grdList.AllowUserToResizeRows = False
        Me.grdList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdList.BackgroundColor = System.Drawing.Color.White
        Me.grdList.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Consolas", 8.25!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.grdList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.memKey, Me.memID, Me.cName, Me.cAcntRef, Me.cJan, Me.cFeb, Me.cMar, Me.cApr, Me.cMay, Me.cJun, Me.cJul, Me.cAug, Me.cSep, Me.cOct, Me.cNov, Me.colDec, Me.cAverage, Me.cDividend})
        Me.grdList.Location = New System.Drawing.Point(12, 26)
        Me.grdList.Name = "grdList"
        Me.grdList.Size = New System.Drawing.Size(841, 197)
        Me.grdList.TabIndex = 0
        '
        'memKey
        '
        Me.memKey.Frozen = True
        Me.memKey.HeaderText = "Key"
        Me.memKey.Name = "memKey"
        Me.memKey.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.memKey.Visible = False
        '
        'memID
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.memID.DefaultCellStyle = DataGridViewCellStyle2
        Me.memID.Frozen = True
        Me.memID.HeaderText = "ID"
        Me.memID.Name = "memID"
        Me.memID.ReadOnly = True
        Me.memID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cName
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.cName.DefaultCellStyle = DataGridViewCellStyle3
        Me.cName.Frozen = True
        Me.cName.HeaderText = "Name"
        Me.cName.Name = "cName"
        Me.cName.ReadOnly = True
        Me.cName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.cName.Width = 240
        '
        'cAcntRef
        '
        Me.cAcntRef.Frozen = True
        Me.cAcntRef.HeaderText = "Account Ref."
        Me.cAcntRef.Name = "cAcntRef"
        Me.cAcntRef.ReadOnly = True
        Me.cAcntRef.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cJan
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.cJan.DefaultCellStyle = DataGridViewCellStyle4
        Me.cJan.HeaderText = "JAN"
        Me.cJan.Name = "cJan"
        Me.cJan.ReadOnly = True
        Me.cJan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cFeb
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.cFeb.DefaultCellStyle = DataGridViewCellStyle5
        Me.cFeb.HeaderText = "FEB"
        Me.cFeb.Name = "cFeb"
        Me.cFeb.ReadOnly = True
        Me.cFeb.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cMar
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.cMar.DefaultCellStyle = DataGridViewCellStyle6
        Me.cMar.HeaderText = "MAR"
        Me.cMar.Name = "cMar"
        Me.cMar.ReadOnly = True
        Me.cMar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cApr
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.cApr.DefaultCellStyle = DataGridViewCellStyle7
        Me.cApr.HeaderText = "APR"
        Me.cApr.Name = "cApr"
        Me.cApr.ReadOnly = True
        Me.cApr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cMay
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.cMay.DefaultCellStyle = DataGridViewCellStyle8
        Me.cMay.HeaderText = "MAY"
        Me.cMay.Name = "cMay"
        Me.cMay.ReadOnly = True
        Me.cMay.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cJun
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.cJun.DefaultCellStyle = DataGridViewCellStyle9
        Me.cJun.HeaderText = "JUN"
        Me.cJun.Name = "cJun"
        Me.cJun.ReadOnly = True
        Me.cJun.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cJul
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.cJul.DefaultCellStyle = DataGridViewCellStyle10
        Me.cJul.HeaderText = "JUL"
        Me.cJul.Name = "cJul"
        Me.cJul.ReadOnly = True
        Me.cJul.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cAug
        '
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.cAug.DefaultCellStyle = DataGridViewCellStyle11
        Me.cAug.HeaderText = "AUG"
        Me.cAug.Name = "cAug"
        Me.cAug.ReadOnly = True
        Me.cAug.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cSep
        '
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.cSep.DefaultCellStyle = DataGridViewCellStyle12
        Me.cSep.HeaderText = "SEP"
        Me.cSep.Name = "cSep"
        Me.cSep.ReadOnly = True
        Me.cSep.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cOct
        '
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.cOct.DefaultCellStyle = DataGridViewCellStyle13
        Me.cOct.HeaderText = "OCT"
        Me.cOct.Name = "cOct"
        Me.cOct.ReadOnly = True
        Me.cOct.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cNov
        '
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.cNov.DefaultCellStyle = DataGridViewCellStyle14
        Me.cNov.HeaderText = "NOV"
        Me.cNov.Name = "cNov"
        Me.cNov.ReadOnly = True
        Me.cNov.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colDec
        '
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colDec.DefaultCellStyle = DataGridViewCellStyle15
        Me.colDec.HeaderText = "DEC"
        Me.colDec.Name = "colDec"
        Me.colDec.ReadOnly = True
        Me.colDec.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cAverage
        '
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.cAverage.DefaultCellStyle = DataGridViewCellStyle16
        Me.cAverage.HeaderText = "Average"
        Me.cAverage.Name = "cAverage"
        Me.cAverage.ReadOnly = True
        Me.cAverage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'cDividend
        '
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.cDividend.DefaultCellStyle = DataGridViewCellStyle17
        Me.cDividend.HeaderText = "Dividend"
        Me.cDividend.Name = "cDividend"
        Me.cDividend.ReadOnly = True
        Me.cDividend.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.btnPrint)
        Me.GroupBox1.Controls.Add(Me.txtAccount)
        Me.GroupBox1.Controls.Add(Me.btnAccount)
        Me.GroupBox1.Controls.Add(Me.btnCompute)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtDividendFactor)
        Me.GroupBox1.Controls.Add(Me.txtAllocatedIncome)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.dtpDate)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 305)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(841, 83)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Computation Parameters"
        '
        'btnPrint
        '
        Me.btnPrint.Image = Global.CSAcctg.My.Resources.Resources.printer
        Me.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPrint.Location = New System.Drawing.Point(709, 48)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(126, 24)
        Me.btnPrint.TabIndex = 42
        Me.btnPrint.Text = "Print Dividend"
        Me.btnPrint.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'txtAccount
        '
        Me.txtAccount.BackColor = System.Drawing.Color.White
        Me.txtAccount.Location = New System.Drawing.Point(126, 24)
        Me.txtAccount.Name = "txtAccount"
        Me.txtAccount.ReadOnly = True
        Me.txtAccount.Size = New System.Drawing.Size(263, 20)
        Me.txtAccount.TabIndex = 40
        '
        'btnAccount
        '
        Me.btnAccount.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnAccount.Location = New System.Drawing.Point(393, 22)
        Me.btnAccount.Name = "btnAccount"
        Me.btnAccount.Size = New System.Drawing.Size(25, 25)
        Me.btnAccount.TabIndex = 41
        Me.btnAccount.UseVisualStyleBackColor = True
        '
        'btnCompute
        '
        Me.btnCompute.Image = Global.CSAcctg.My.Resources.Resources.Calculator
        Me.btnCompute.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCompute.Location = New System.Drawing.Point(709, 22)
        Me.btnCompute.Name = "btnCompute"
        Me.btnCompute.Size = New System.Drawing.Size(126, 24)
        Me.btnCompute.TabIndex = 13
        Me.btnCompute.Text = "Compute Dividend"
        Me.btnCompute.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCompute.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 27)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(115, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Account to Compute"
        '
        'txtDividendFactor
        '
        Me.txtDividendFactor.BackColor = System.Drawing.Color.White
        Me.txtDividendFactor.Location = New System.Drawing.Point(544, 49)
        Me.txtDividendFactor.Name = "txtDividendFactor"
        Me.txtDividendFactor.ReadOnly = True
        Me.txtDividendFactor.Size = New System.Drawing.Size(145, 20)
        Me.txtDividendFactor.TabIndex = 7
        Me.txtDividendFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAllocatedIncome
        '
        Me.txtAllocatedIncome.Location = New System.Drawing.Point(544, 24)
        Me.txtAllocatedIncome.MaxLength = 18
        Me.txtAllocatedIncome.Name = "txtAllocatedIncome"
        Me.txtAllocatedIncome.Size = New System.Drawing.Size(145, 20)
        Me.txtAllocatedIncome.TabIndex = 6
        Me.txtAllocatedIncome.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(443, 52)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(97, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Dividend Factor"
        '
        'dtpDate
        '
        Me.dtpDate.CustomFormat = "yyyy"
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDate.Location = New System.Drawing.Point(126, 49)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(83, 20)
        Me.dtpDate.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(437, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(103, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Allocated Income"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Year to Compute"
        '
        'txtTotalMember
        '
        Me.txtTotalMember.BackColor = System.Drawing.Color.White
        Me.txtTotalMember.Location = New System.Drawing.Point(143, 23)
        Me.txtTotalMember.Name = "txtTotalMember"
        Me.txtTotalMember.ReadOnly = True
        Me.txtTotalMember.Size = New System.Drawing.Size(145, 20)
        Me.txtTotalMember.TabIndex = 15
        Me.txtTotalMember.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalDividend
        '
        Me.txtTotalDividend.BackColor = System.Drawing.Color.White
        Me.txtTotalDividend.Location = New System.Drawing.Point(404, 23)
        Me.txtTotalDividend.Name = "txtTotalDividend"
        Me.txtTotalDividend.ReadOnly = True
        Me.txtTotalDividend.Size = New System.Drawing.Size(145, 20)
        Me.txtTotalDividend.TabIndex = 16
        Me.txtTotalDividend.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalAverageShare
        '
        Me.txtTotalAverageShare.BackColor = System.Drawing.Color.White
        Me.txtTotalAverageShare.Location = New System.Drawing.Point(688, 23)
        Me.txtTotalAverageShare.Name = "txtTotalAverageShare"
        Me.txtTotalAverageShare.ReadOnly = True
        Me.txtTotalAverageShare.Size = New System.Drawing.Size(145, 20)
        Me.txtTotalAverageShare.TabIndex = 17
        Me.txtTotalAverageShare.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(7, 26)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(133, 13)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Total Member Included"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(308, 26)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(91, 13)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Total Dividend"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(564, 26)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(121, 13)
        Me.Label9.TabIndex = 21
        Me.Label9.Text = "Total Average Share"
        '
        'pbStatus
        '
        Me.pbStatus.Location = New System.Drawing.Point(12, 7)
        Me.pbStatus.Name = "pbStatus"
        Me.pbStatus.Size = New System.Drawing.Size(195, 13)
        Me.pbStatus.TabIndex = 23
        Me.pbStatus.Visible = False
        '
        'bgwPrint
        '
        Me.bgwPrint.WorkerReportsProgress = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.txtTotalMember)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.txtTotalDividend)
        Me.GroupBox2.Controls.Add(Me.txtTotalAverageShare)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 236)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(841, 56)
        Me.GroupBox2.TabIndex = 24
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Totals"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(213, 7)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblStatus.TabIndex = 25
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.cboPostingType)
        Me.GroupBox3.Controls.Add(Me.dtpPostingDate)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.btnPost)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.txtJVNo)
        Me.GroupBox3.Controls.Add(Me.btnSearchJVno)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.txtCreditAccount)
        Me.GroupBox3.Controls.Add(Me.btnSearchCredit)
        Me.GroupBox3.Controls.Add(Me.txtDebitAccount)
        Me.GroupBox3.Controls.Add(Me.btnSearchDebit)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 397)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(841, 82)
        Me.GroupBox3.TabIndex = 26
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Posting Parameters"
        '
        'cboPostingType
        '
        Me.cboPostingType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPostingType.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboPostingType.FormattingEnabled = True
        Me.cboPostingType.Items.AddRange(New Object() {"Per SL", "GL"})
        Me.cboPostingType.Location = New System.Drawing.Point(709, 18)
        Me.cboPostingType.Name = "cboPostingType"
        Me.cboPostingType.Size = New System.Drawing.Size(121, 21)
        Me.cboPostingType.TabIndex = 54
        '
        'dtpPostingDate
        '
        Me.dtpPostingDate.CustomFormat = "MMMM dd, yyyy"
        Me.dtpPostingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPostingDate.Location = New System.Drawing.Point(544, 19)
        Me.dtpPostingDate.Name = "dtpPostingDate"
        Me.dtpPostingDate.Size = New System.Drawing.Size(145, 20)
        Me.dtpPostingDate.TabIndex = 53
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(461, 22)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(79, 13)
        Me.Label11.TabIndex = 52
        Me.Label11.Text = "Posting Date"
        '
        'btnPost
        '
        Me.btnPost.Image = Global.CSAcctg.My.Resources.Resources.Save
        Me.btnPost.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPost.Location = New System.Drawing.Point(709, 43)
        Me.btnPost.Name = "btnPost"
        Me.btnPost.Size = New System.Drawing.Size(121, 24)
        Me.btnPost.TabIndex = 51
        Me.btnPost.Text = "Save Dividend"
        Me.btnPost.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPost.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(497, 47)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(43, 13)
        Me.Label10.TabIndex = 50
        Me.Label10.Text = "JV No."
        '
        'txtJVNo
        '
        Me.txtJVNo.BackColor = System.Drawing.Color.White
        Me.txtJVNo.Location = New System.Drawing.Point(544, 44)
        Me.txtJVNo.Name = "txtJVNo"
        Me.txtJVNo.ReadOnly = True
        Me.txtJVNo.Size = New System.Drawing.Size(114, 20)
        Me.txtJVNo.TabIndex = 48
        '
        'btnSearchJVno
        '
        Me.btnSearchJVno.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnSearchJVno.Location = New System.Drawing.Point(663, 42)
        Me.btnSearchJVno.Name = "btnSearchJVno"
        Me.btnSearchJVno.Size = New System.Drawing.Size(25, 25)
        Me.btnSearchJVno.TabIndex = 49
        Me.btnSearchJVno.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(29, 49)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 13)
        Me.Label7.TabIndex = 47
        Me.Label7.Text = "Credit Account"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(35, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 13)
        Me.Label2.TabIndex = 46
        Me.Label2.Text = "Debit Account"
        '
        'txtCreditAccount
        '
        Me.txtCreditAccount.BackColor = System.Drawing.Color.White
        Me.txtCreditAccount.Location = New System.Drawing.Point(126, 45)
        Me.txtCreditAccount.Name = "txtCreditAccount"
        Me.txtCreditAccount.ReadOnly = True
        Me.txtCreditAccount.Size = New System.Drawing.Size(263, 20)
        Me.txtCreditAccount.TabIndex = 44
        '
        'btnSearchCredit
        '
        Me.btnSearchCredit.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnSearchCredit.Location = New System.Drawing.Point(393, 42)
        Me.btnSearchCredit.Name = "btnSearchCredit"
        Me.btnSearchCredit.Size = New System.Drawing.Size(25, 25)
        Me.btnSearchCredit.TabIndex = 45
        Me.btnSearchCredit.UseVisualStyleBackColor = True
        '
        'txtDebitAccount
        '
        Me.txtDebitAccount.BackColor = System.Drawing.Color.White
        Me.txtDebitAccount.Location = New System.Drawing.Point(126, 19)
        Me.txtDebitAccount.Name = "txtDebitAccount"
        Me.txtDebitAccount.ReadOnly = True
        Me.txtDebitAccount.Size = New System.Drawing.Size(263, 20)
        Me.txtDebitAccount.TabIndex = 42
        '
        'btnSearchDebit
        '
        Me.btnSearchDebit.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnSearchDebit.Location = New System.Drawing.Point(393, 16)
        Me.btnSearchDebit.Name = "btnSearchDebit"
        Me.btnSearchDebit.Size = New System.Drawing.Size(25, 25)
        Me.btnSearchDebit.TabIndex = 43
        Me.btnSearchDebit.UseVisualStyleBackColor = True
        '
        'bgwPost
        '
        Me.bgwPost.WorkerReportsProgress = True
        Me.bgwPost.WorkerSupportsCancellation = True
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        Me.BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle18
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle19
        Me.DataGridViewTextBoxColumn3.Frozen = True
        Me.DataGridViewTextBoxColumn3.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 240
        '
        'DataGridViewTextBoxColumn4
        '
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle20
        Me.DataGridViewTextBoxColumn4.Frozen = True
        Me.DataGridViewTextBoxColumn4.HeaderText = "JAN"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle21
        Me.DataGridViewTextBoxColumn5.HeaderText = "FEB"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn6
        '
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle22
        Me.DataGridViewTextBoxColumn6.HeaderText = "MAR"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn7
        '
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle23
        Me.DataGridViewTextBoxColumn7.HeaderText = "APR"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn8
        '
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle24
        Me.DataGridViewTextBoxColumn8.HeaderText = "MAY"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn9
        '
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn9.DefaultCellStyle = DataGridViewCellStyle25
        Me.DataGridViewTextBoxColumn9.HeaderText = "JUN"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn10
        '
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn10.DefaultCellStyle = DataGridViewCellStyle26
        Me.DataGridViewTextBoxColumn10.HeaderText = "JUL"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn11
        '
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn11.DefaultCellStyle = DataGridViewCellStyle27
        Me.DataGridViewTextBoxColumn11.HeaderText = "AUG"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn12
        '
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn12.DefaultCellStyle = DataGridViewCellStyle28
        Me.DataGridViewTextBoxColumn12.HeaderText = "SEP"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn13
        '
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn13.DefaultCellStyle = DataGridViewCellStyle29
        Me.DataGridViewTextBoxColumn13.HeaderText = "OCT"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn14
        '
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn14.DefaultCellStyle = DataGridViewCellStyle30
        Me.DataGridViewTextBoxColumn14.HeaderText = "NOV"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn15
        '
        DataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn15.DefaultCellStyle = DataGridViewCellStyle31
        Me.DataGridViewTextBoxColumn15.HeaderText = "DEC"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn16
        '
        DataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn16.DefaultCellStyle = DataGridViewCellStyle32
        Me.DataGridViewTextBoxColumn16.HeaderText = "Average"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn17
        '
        DataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn17.DefaultCellStyle = DataGridViewCellStyle33
        Me.DataGridViewTextBoxColumn17.HeaderText = "Divided"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn18
        '
        DataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn18.DefaultCellStyle = DataGridViewCellStyle34
        Me.DataGridViewTextBoxColumn18.HeaderText = "Dividend"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'frmDividendComputation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(865, 489)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.pbStatus)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.grdList)
        Me.Font = New System.Drawing.Font("Consolas", 8.25!)
        Me.Name = "frmDividendComputation"
        Me.Text = "Dividend Computation"
        CType(Me.grdList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grdList As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnCompute As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtDividendFactor As System.Windows.Forms.TextBox
    Friend WithEvents txtAllocatedIncome As System.Windows.Forms.TextBox
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTotalMember As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalDividend As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalAverageShare As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pbStatus As System.Windows.Forms.ProgressBar
    Friend WithEvents bgwPrint As System.ComponentModel.BackgroundWorker
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtAccount As System.Windows.Forms.TextBox
    Friend WithEvents btnAccount As System.Windows.Forms.Button
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCreditAccount As System.Windows.Forms.TextBox
    Friend WithEvents btnSearchCredit As System.Windows.Forms.Button
    Friend WithEvents txtDebitAccount As System.Windows.Forms.TextBox
    Friend WithEvents btnSearchDebit As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtJVNo As System.Windows.Forms.TextBox
    Friend WithEvents btnSearchJVno As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnPost As System.Windows.Forms.Button
    Friend WithEvents dtpPostingDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents memKey As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents memID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cAcntRef As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cJan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cFeb As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cMar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cApr As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cMay As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cJun As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cJul As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cAug As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cSep As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cOct As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cNov As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cAverage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cDividend As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents cboPostingType As System.Windows.Forms.ComboBox
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bgwPost As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
End Class
