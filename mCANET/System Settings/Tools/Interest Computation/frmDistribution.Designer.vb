﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDistribution
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.grdList = New System.Windows.Forms.DataGridView()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtDividend = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPatronage = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtEntryNo = New System.Windows.Forms.TextBox()
        Me.dtpPostingDate = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnGetDetails = New System.Windows.Forms.Button()
        Me.btnCheckVoucher = New System.Windows.Forms.Button()
        Me.btnPatronage = New System.Windows.Forms.Button()
        Me.btnDividend = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.pbStatus = New System.Windows.Forms.ProgressBar()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.txtTotalDebit = New System.Windows.Forms.TextBox()
        Me.txtTotalCredit = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.memKey = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.memID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.memName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.acntRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.acntKey = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.acntCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.acntTitle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.debit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.credit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.grdList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grdList
        '
        Me.grdList.AllowUserToAddRows = False
        Me.grdList.AllowUserToDeleteRows = False
        Me.grdList.AllowUserToResizeRows = False
        Me.grdList.BackgroundColor = System.Drawing.Color.White
        Me.grdList.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grdList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grdList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.grdList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.memKey, Me.memID, Me.memName, Me.acntRef, Me.acntKey, Me.acntCode, Me.acntTitle, Me.debit, Me.credit})
        Me.grdList.Location = New System.Drawing.Point(10, 34)
        Me.grdList.Name = "grdList"
        Me.grdList.RowHeadersVisible = False
        Me.grdList.Size = New System.Drawing.Size(815, 240)
        Me.grdList.TabIndex = 0
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(14, 296)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(55, 13)
        Me.Label10.TabIndex = 53
        Me.Label10.Text = "Dividend"
        '
        'txtDividend
        '
        Me.txtDividend.BackColor = System.Drawing.Color.White
        Me.txtDividend.Location = New System.Drawing.Point(74, 293)
        Me.txtDividend.Name = "txtDividend"
        Me.txtDividend.ReadOnly = True
        Me.txtDividend.Size = New System.Drawing.Size(131, 20)
        Me.txtDividend.TabIndex = 51
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 322)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = "Patronage"
        '
        'txtPatronage
        '
        Me.txtPatronage.BackColor = System.Drawing.Color.White
        Me.txtPatronage.Location = New System.Drawing.Point(74, 319)
        Me.txtPatronage.Name = "txtPatronage"
        Me.txtPatronage.ReadOnly = True
        Me.txtPatronage.Size = New System.Drawing.Size(131, 20)
        Me.txtPatronage.TabIndex = 54
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(254, 296)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 13)
        Me.Label2.TabIndex = 59
        Me.Label2.Text = "CHV No."
        '
        'txtEntryNo
        '
        Me.txtEntryNo.BackColor = System.Drawing.Color.White
        Me.txtEntryNo.Location = New System.Drawing.Point(304, 293)
        Me.txtEntryNo.Name = "txtEntryNo"
        Me.txtEntryNo.ReadOnly = True
        Me.txtEntryNo.Size = New System.Drawing.Size(131, 20)
        Me.txtEntryNo.TabIndex = 57
        '
        'dtpPostingDate
        '
        Me.dtpPostingDate.CustomFormat = "MMMM dd, yyyy"
        Me.dtpPostingDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpPostingDate.Location = New System.Drawing.Point(304, 319)
        Me.dtpPostingDate.Name = "dtpPostingDate"
        Me.dtpPostingDate.Size = New System.Drawing.Size(162, 20)
        Me.dtpPostingDate.TabIndex = 61
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(270, 322)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 62
        Me.Label3.Text = "Date"
        '
        'btnGetDetails
        '
        Me.btnGetDetails.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGetDetails.Image = Global.CSAcctg.My.Resources.Resources.apply
        Me.btnGetDetails.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGetDetails.Location = New System.Drawing.Point(74, 347)
        Me.btnGetDetails.Name = "btnGetDetails"
        Me.btnGetDetails.Size = New System.Drawing.Size(130, 27)
        Me.btnGetDetails.TabIndex = 60
        Me.btnGetDetails.Text = "Get Details"
        Me.btnGetDetails.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGetDetails.UseVisualStyleBackColor = True
        '
        'btnCheckVoucher
        '
        Me.btnCheckVoucher.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnCheckVoucher.Location = New System.Drawing.Point(441, 290)
        Me.btnCheckVoucher.Name = "btnCheckVoucher"
        Me.btnCheckVoucher.Size = New System.Drawing.Size(25, 25)
        Me.btnCheckVoucher.TabIndex = 58
        Me.btnCheckVoucher.UseVisualStyleBackColor = True
        '
        'btnPatronage
        '
        Me.btnPatronage.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnPatronage.Location = New System.Drawing.Point(210, 317)
        Me.btnPatronage.Name = "btnPatronage"
        Me.btnPatronage.Size = New System.Drawing.Size(25, 25)
        Me.btnPatronage.TabIndex = 55
        Me.btnPatronage.UseVisualStyleBackColor = True
        '
        'btnDividend
        '
        Me.btnDividend.Image = Global.CSAcctg.My.Resources.Resources.Find
        Me.btnDividend.Location = New System.Drawing.Point(210, 290)
        Me.btnDividend.Name = "btnDividend"
        Me.btnDividend.Size = New System.Drawing.Size(25, 25)
        Me.btnDividend.TabIndex = 52
        Me.btnDividend.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = Global.CSAcctg.My.Resources.Resources.Save
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(304, 347)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(130, 27)
        Me.btnSave.TabIndex = 63
        Me.btnSave.Text = "Save Entry"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(211, 11)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(0, 13)
        Me.lblStatus.TabIndex = 65
        '
        'pbStatus
        '
        Me.pbStatus.Location = New System.Drawing.Point(10, 8)
        Me.pbStatus.Name = "pbStatus"
        Me.pbStatus.Size = New System.Drawing.Size(195, 20)
        Me.pbStatus.TabIndex = 64
        Me.pbStatus.Visible = False
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        Me.BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'txtTotalDebit
        '
        Me.txtTotalDebit.BackColor = System.Drawing.Color.White
        Me.txtTotalDebit.Location = New System.Drawing.Point(603, 280)
        Me.txtTotalDebit.Name = "txtTotalDebit"
        Me.txtTotalDebit.ReadOnly = True
        Me.txtTotalDebit.Size = New System.Drawing.Size(110, 20)
        Me.txtTotalDebit.TabIndex = 66
        Me.txtTotalDebit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalCredit
        '
        Me.txtTotalCredit.BackColor = System.Drawing.Color.White
        Me.txtTotalCredit.Location = New System.Drawing.Point(712, 280)
        Me.txtTotalCredit.Name = "txtTotalCredit"
        Me.txtTotalCredit.ReadOnly = True
        Me.txtTotalCredit.Size = New System.Drawing.Size(113, 20)
        Me.txtTotalCredit.TabIndex = 67
        Me.txtTotalCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(554, 283)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 68
        Me.Label4.Text = "Totals"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "memKey"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 80
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Account Ref."
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "acntKey"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 60
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "Title"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 200
        '
        'DataGridViewTextBoxColumn8
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn8.HeaderText = "Debit"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Width = 110
        '
        'DataGridViewTextBoxColumn9
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn9.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn9.HeaderText = "Credit"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn9.Width = 110
        '
        'memKey
        '
        Me.memKey.HeaderText = "memKey"
        Me.memKey.Name = "memKey"
        Me.memKey.Visible = False
        '
        'memID
        '
        Me.memID.HeaderText = "ID"
        Me.memID.Name = "memID"
        Me.memID.ReadOnly = True
        Me.memID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.memID.Width = 80
        '
        'memName
        '
        Me.memName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.memName.HeaderText = "Name"
        Me.memName.Name = "memName"
        Me.memName.ReadOnly = True
        Me.memName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'acntRef
        '
        Me.acntRef.HeaderText = "Account Ref."
        Me.acntRef.Name = "acntRef"
        Me.acntRef.ReadOnly = True
        Me.acntRef.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'acntKey
        '
        Me.acntKey.HeaderText = "acntKey"
        Me.acntKey.Name = "acntKey"
        Me.acntKey.Visible = False
        '
        'acntCode
        '
        Me.acntCode.HeaderText = "Code"
        Me.acntCode.Name = "acntCode"
        Me.acntCode.ReadOnly = True
        Me.acntCode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.acntCode.Width = 60
        '
        'acntTitle
        '
        Me.acntTitle.HeaderText = "Title"
        Me.acntTitle.Name = "acntTitle"
        Me.acntTitle.ReadOnly = True
        Me.acntTitle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.acntTitle.Width = 200
        '
        'debit
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.debit.DefaultCellStyle = DataGridViewCellStyle2
        Me.debit.HeaderText = "Debit"
        Me.debit.Name = "debit"
        Me.debit.ReadOnly = True
        Me.debit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.debit.Width = 110
        '
        'credit
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.credit.DefaultCellStyle = DataGridViewCellStyle3
        Me.credit.HeaderText = "Credit"
        Me.credit.Name = "credit"
        Me.credit.ReadOnly = True
        Me.credit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.credit.Width = 110
        '
        'frmDistribution
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(833, 383)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtTotalCredit)
        Me.Controls.Add(Me.txtTotalDebit)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.pbStatus)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dtpPostingDate)
        Me.Controls.Add(Me.btnGetDetails)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtEntryNo)
        Me.Controls.Add(Me.btnCheckVoucher)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtPatronage)
        Me.Controls.Add(Me.btnPatronage)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtDividend)
        Me.Controls.Add(Me.btnDividend)
        Me.Controls.Add(Me.grdList)
        Me.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmDistribution"
        Me.Text = "Distribution"
        CType(Me.grdList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grdList As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtDividend As System.Windows.Forms.TextBox
    Friend WithEvents btnDividend As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtPatronage As System.Windows.Forms.TextBox
    Friend WithEvents btnPatronage As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtEntryNo As System.Windows.Forms.TextBox
    Friend WithEvents btnCheckVoucher As System.Windows.Forms.Button
    Friend WithEvents btnGetDetails As System.Windows.Forms.Button
    Friend WithEvents dtpPostingDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents pbStatus As System.Windows.Forms.ProgressBar
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents txtTotalDebit As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalCredit As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents memKey As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents memID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents memName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents acntRef As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents acntKey As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents acntCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents acntTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents debit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents credit As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
