<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cust_masterCustomerAddEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cust_masterCustomerAddEdit))
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblBalance = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.dteOpeningBalDate = New System.Windows.Forms.DateTimePicker
        Me.txtCustomerName = New System.Windows.Forms.TextBox
        Me.txtOpeningBal = New System.Windows.Forms.TextBox
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnDeleteShipAdd = New System.Windows.Forms.Button
        Me.btnEditShipAdd = New System.Windows.Forms.Button
        Me.btnAddShip = New System.Windows.Forms.Button
        Me.cboShipToName = New System.Windows.Forms.ComboBox
        Me.cboShipToID = New System.Windows.Forms.ComboBox
        Me.txtShipAddress = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.btnCopy = New System.Windows.Forms.Button
        Me.btnEditBillAdd = New System.Windows.Forms.Button
        Me.txtBillAddress = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtMiddleInitial = New System.Windows.Forms.TextBox
        Me.Label28 = New System.Windows.Forms.Label
        Me.txtCC = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtEmail = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtAltContact = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtAltPhone = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtFAX = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtPhone = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtContact = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtLastName = New System.Windows.Forms.TextBox
        Me.txtFirstName = New System.Windows.Forms.TextBox
        Me.txtSalutation = New System.Windows.Forms.TextBox
        Me.txtCoName = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.cboPriceLvlName = New System.Windows.Forms.ComboBox
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.cboTaxItemName = New System.Windows.Forms.ComboBox
        Me.cboTaxCodeName = New System.Windows.Forms.ComboBox
        Me.txtResaleNo = New System.Windows.Forms.TextBox
        Me.Label30 = New System.Windows.Forms.Label
        Me.cboTaxItemID = New System.Windows.Forms.ComboBox
        Me.Label29 = New System.Windows.Forms.Label
        Me.cboTaxCodeID = New System.Windows.Forms.ComboBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.grdDefinedField = New System.Windows.Forms.DataGridView
        Me.btnDefineField = New System.Windows.Forms.Button
        Me.cboPriceLvlID = New System.Windows.Forms.ComboBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cboRepName = New System.Windows.Forms.ComboBox
        Me.cboTermsName = New System.Windows.Forms.ComboBox
        Me.cboTypeName = New System.Windows.Forms.ComboBox
        Me.cboTypeID = New System.Windows.Forms.ComboBox
        Me.cboSendMethod = New System.Windows.Forms.ComboBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.cboRepID = New System.Windows.Forms.ComboBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.cboTermsID = New System.Windows.Forms.ComboBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.cboPaymentMethodName = New System.Windows.Forms.ComboBox
        Me.Label38 = New System.Windows.Forms.Label
        Me.txtYear = New System.Windows.Forms.TextBox
        Me.txtMonth = New System.Windows.Forms.TextBox
        Me.Label37 = New System.Windows.Forms.Label
        Me.txtZipCode = New System.Windows.Forms.TextBox
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtAddress = New System.Windows.Forms.TextBox
        Me.Label35 = New System.Windows.Forms.Label
        Me.txtNameOnCard = New System.Windows.Forms.TextBox
        Me.Label34 = New System.Windows.Forms.Label
        Me.txtCreditCardNo = New System.Windows.Forms.TextBox
        Me.Label33 = New System.Windows.Forms.Label
        Me.cboPaymentMethodID = New System.Windows.Forms.ComboBox
        Me.txtCreditLimit = New System.Windows.Forms.TextBox
        Me.Label32 = New System.Windows.Forms.Label
        Me.txtAcctNo = New System.Windows.Forms.TextBox
        Me.Label31 = New System.Windows.Forms.Label
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.cboJobTypeName = New System.Windows.Forms.ComboBox
        Me.txtJobDescription = New System.Windows.Forms.TextBox
        Me.cboJobTypeID = New System.Windows.Forms.ComboBox
        Me.Label44 = New System.Windows.Forms.Label
        Me.Label43 = New System.Windows.Forms.Label
        Me.dteEndDate = New System.Windows.Forms.DateTimePicker
        Me.Label42 = New System.Windows.Forms.Label
        Me.dteEndDateTemp = New System.Windows.Forms.DateTimePicker
        Me.Label41 = New System.Windows.Forms.Label
        Me.dteStartDate = New System.Windows.Forms.DateTimePicker
        Me.Label40 = New System.Windows.Forms.Label
        Me.cboJobStat = New System.Windows.Forms.ComboBox
        Me.Label39 = New System.Windows.Forms.Label
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.chkInactive = New System.Windows.Forms.CheckBox
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.grdDefinedField, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "CustomerName"
        '
        'lblBalance
        '
        Me.lblBalance.AutoSize = True
        Me.lblBalance.Location = New System.Drawing.Point(12, 47)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(87, 13)
        Me.lblBalance.TabIndex = 1
        Me.lblBalance.Text = "Opening Balance"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(246, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "as of"
        '
        'dteOpeningBalDate
        '
        Me.dteOpeningBalDate.Location = New System.Drawing.Point(282, 44)
        Me.dteOpeningBalDate.Name = "dteOpeningBalDate"
        Me.dteOpeningBalDate.Size = New System.Drawing.Size(196, 21)
        Me.dteOpeningBalDate.TabIndex = 3
        '
        'txtCustomerName
        '
        Me.txtCustomerName.Location = New System.Drawing.Point(107, 17)
        Me.txtCustomerName.Name = "txtCustomerName"
        Me.txtCustomerName.Size = New System.Drawing.Size(371, 21)
        Me.txtCustomerName.TabIndex = 4
        '
        'txtOpeningBal
        '
        Me.txtOpeningBal.Location = New System.Drawing.Point(107, 44)
        Me.txtOpeningBal.Name = "txtOpeningBal"
        Me.txtOpeningBal.Size = New System.Drawing.Size(133, 21)
        Me.txtOpeningBal.TabIndex = 5
        Me.txtOpeningBal.Text = "0.00"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Location = New System.Drawing.Point(15, 84)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(467, 367)
        Me.TabControl1.TabIndex = 6
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox3)
        Me.TabPage1.Controls.Add(Me.txtMiddleInitial)
        Me.TabPage1.Controls.Add(Me.Label28)
        Me.TabPage1.Controls.Add(Me.txtCC)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.Controls.Add(Me.txtEmail)
        Me.TabPage1.Controls.Add(Me.Label14)
        Me.TabPage1.Controls.Add(Me.txtAltContact)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.txtAltPhone)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.txtFAX)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.txtPhone)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.txtContact)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.txtLastName)
        Me.TabPage1.Controls.Add(Me.txtFirstName)
        Me.TabPage1.Controls.Add(Me.txtSalutation)
        Me.TabPage1.Controls.Add(Me.txtCoName)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(459, 341)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Address Info"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnDeleteShipAdd)
        Me.GroupBox3.Controls.Add(Me.btnEditShipAdd)
        Me.GroupBox3.Controls.Add(Me.btnAddShip)
        Me.GroupBox3.Controls.Add(Me.cboShipToName)
        Me.GroupBox3.Controls.Add(Me.cboShipToID)
        Me.GroupBox3.Controls.Add(Me.txtShipAddress)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.btnCopy)
        Me.GroupBox3.Controls.Add(Me.btnEditBillAdd)
        Me.GroupBox3.Controls.Add(Me.txtBillAddress)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 187)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(447, 148)
        Me.GroupBox3.TabIndex = 29
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Address"
        '
        'btnDeleteShipAdd
        '
        Me.btnDeleteShipAdd.Location = New System.Drawing.Point(376, 121)
        Me.btnDeleteShipAdd.Name = "btnDeleteShipAdd"
        Me.btnDeleteShipAdd.Size = New System.Drawing.Size(58, 21)
        Me.btnDeleteShipAdd.TabIndex = 35
        Me.btnDeleteShipAdd.Text = "Delete"
        Me.btnDeleteShipAdd.UseVisualStyleBackColor = True
        '
        'btnEditShipAdd
        '
        Me.btnEditShipAdd.Location = New System.Drawing.Point(319, 121)
        Me.btnEditShipAdd.Name = "btnEditShipAdd"
        Me.btnEditShipAdd.Size = New System.Drawing.Size(51, 21)
        Me.btnEditShipAdd.TabIndex = 34
        Me.btnEditShipAdd.Text = "Edit"
        Me.btnEditShipAdd.UseVisualStyleBackColor = True
        '
        'btnAddShip
        '
        Me.btnAddShip.Location = New System.Drawing.Point(272, 121)
        Me.btnAddShip.Name = "btnAddShip"
        Me.btnAddShip.Size = New System.Drawing.Size(41, 21)
        Me.btnAddShip.TabIndex = 33
        Me.btnAddShip.Text = "Add"
        Me.btnAddShip.UseVisualStyleBackColor = True
        '
        'cboShipToName
        '
        Me.cboShipToName.FormattingEnabled = True
        Me.cboShipToName.Location = New System.Drawing.Point(272, 12)
        Me.cboShipToName.Name = "cboShipToName"
        Me.cboShipToName.Size = New System.Drawing.Size(167, 21)
        Me.cboShipToName.TabIndex = 32
        '
        'cboShipToID
        '
        Me.cboShipToID.FormattingEnabled = True
        Me.cboShipToID.Location = New System.Drawing.Point(290, 12)
        Me.cboShipToID.Name = "cboShipToID"
        Me.cboShipToID.Size = New System.Drawing.Size(123, 21)
        Me.cboShipToID.TabIndex = 31
        '
        'txtShipAddress
        '
        Me.txtShipAddress.Location = New System.Drawing.Point(272, 38)
        Me.txtShipAddress.Multiline = True
        Me.txtShipAddress.Name = "txtShipAddress"
        Me.txtShipAddress.Size = New System.Drawing.Size(167, 80)
        Me.txtShipAddress.TabIndex = 30
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(227, 15)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(39, 13)
        Me.Label16.TabIndex = 29
        Me.Label16.Text = "ShipTo"
        '
        'btnCopy
        '
        Me.btnCopy.Location = New System.Drawing.Point(180, 61)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(64, 21)
        Me.btnCopy.TabIndex = 28
        Me.btnCopy.Text = "Copy >>"
        Me.btnCopy.UseVisualStyleBackColor = True
        '
        'btnEditBillAdd
        '
        Me.btnEditBillAdd.Location = New System.Drawing.Point(9, 121)
        Me.btnEditBillAdd.Name = "btnEditBillAdd"
        Me.btnEditBillAdd.Size = New System.Drawing.Size(41, 21)
        Me.btnEditBillAdd.TabIndex = 27
        Me.btnEditBillAdd.Text = "Edit"
        Me.btnEditBillAdd.UseVisualStyleBackColor = True
        '
        'txtBillAddress
        '
        Me.txtBillAddress.Location = New System.Drawing.Point(9, 38)
        Me.txtBillAddress.Multiline = True
        Me.txtBillAddress.Name = "txtBillAddress"
        Me.txtBillAddress.Size = New System.Drawing.Size(162, 80)
        Me.txtBillAddress.TabIndex = 26
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 15)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(34, 13)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "Bill To"
        '
        'txtMiddleInitial
        '
        Me.txtMiddleInitial.Location = New System.Drawing.Point(199, 32)
        Me.txtMiddleInitial.Name = "txtMiddleInitial"
        Me.txtMiddleInitial.Size = New System.Drawing.Size(43, 21)
        Me.txtMiddleInitial.TabIndex = 28
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(168, 36)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(27, 13)
        Me.Label28.TabIndex = 27
        Me.Label28.Text = "M.I."
        '
        'txtCC
        '
        Me.txtCC.Location = New System.Drawing.Point(309, 161)
        Me.txtCC.Name = "txtCC"
        Me.txtCC.Size = New System.Drawing.Size(136, 21)
        Me.txtCC.TabIndex = 23
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(246, 161)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(19, 13)
        Me.Label15.TabIndex = 22
        Me.Label15.Text = "Cc"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(309, 135)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(136, 21)
        Me.txtEmail.TabIndex = 21
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(246, 135)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(31, 13)
        Me.Label14.TabIndex = 20
        Me.Label14.Text = "Email"
        '
        'txtAltContact
        '
        Me.txtAltContact.Location = New System.Drawing.Point(309, 109)
        Me.txtAltContact.Name = "txtAltContact"
        Me.txtAltContact.Size = New System.Drawing.Size(136, 21)
        Me.txtAltContact.TabIndex = 19
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(246, 112)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(65, 13)
        Me.Label13.TabIndex = 18
        Me.Label13.Text = "Alt. Contact"
        '
        'txtAltPhone
        '
        Me.txtAltPhone.Location = New System.Drawing.Point(309, 84)
        Me.txtAltPhone.Name = "txtAltPhone"
        Me.txtAltPhone.Size = New System.Drawing.Size(136, 21)
        Me.txtAltPhone.TabIndex = 17
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(246, 85)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(57, 13)
        Me.Label12.TabIndex = 16
        Me.Label12.Text = "Alt. Phone"
        '
        'txtFAX
        '
        Me.txtFAX.Location = New System.Drawing.Point(309, 60)
        Me.txtFAX.Name = "txtFAX"
        Me.txtFAX.Size = New System.Drawing.Size(136, 21)
        Me.txtFAX.TabIndex = 15
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(246, 64)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(26, 13)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = "FAX"
        '
        'txtPhone
        '
        Me.txtPhone.Location = New System.Drawing.Point(309, 36)
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.Size = New System.Drawing.Size(136, 21)
        Me.txtPhone.TabIndex = 13
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(246, 40)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(37, 13)
        Me.Label10.TabIndex = 12
        Me.Label10.Text = "Phone"
        '
        'txtContact
        '
        Me.txtContact.Location = New System.Drawing.Point(309, 12)
        Me.txtContact.Name = "txtContact"
        Me.txtContact.Size = New System.Drawing.Size(136, 21)
        Me.txtContact.TabIndex = 11
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(246, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(45, 13)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Contact"
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(106, 82)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(136, 21)
        Me.txtLastName.TabIndex = 8
        '
        'txtFirstName
        '
        Me.txtFirstName.Location = New System.Drawing.Point(106, 56)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(136, 21)
        Me.txtFirstName.TabIndex = 7
        '
        'txtSalutation
        '
        Me.txtSalutation.Location = New System.Drawing.Point(106, 32)
        Me.txtSalutation.Name = "txtSalutation"
        Me.txtSalutation.Size = New System.Drawing.Size(43, 21)
        Me.txtSalutation.TabIndex = 6
        '
        'txtCoName
        '
        Me.txtCoName.Location = New System.Drawing.Point(106, 8)
        Me.txtCoName.Name = "txtCoName"
        Me.txtCoName.Size = New System.Drawing.Size(136, 21)
        Me.txtCoName.TabIndex = 5
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 89)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(57, 13)
        Me.Label7.TabIndex = 3
        Me.Label7.Text = "Last Name"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 63)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "First Name"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 39)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(60, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Mr./Ms./..."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 15)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(82, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Company Name"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.cboPriceLvlName)
        Me.TabPage2.Controls.Add(Me.GroupBox4)
        Me.TabPage2.Controls.Add(Me.GroupBox2)
        Me.TabPage2.Controls.Add(Me.cboPriceLvlID)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(459, 341)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Additional Info"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'cboPriceLvlName
        '
        Me.cboPriceLvlName.FormattingEnabled = True
        Me.cboPriceLvlName.Location = New System.Drawing.Point(311, 9)
        Me.cboPriceLvlName.Name = "cboPriceLvlName"
        Me.cboPriceLvlName.Size = New System.Drawing.Size(134, 21)
        Me.cboPriceLvlName.TabIndex = 27
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cboTaxItemName)
        Me.GroupBox4.Controls.Add(Me.cboTaxCodeName)
        Me.GroupBox4.Controls.Add(Me.txtResaleNo)
        Me.GroupBox4.Controls.Add(Me.Label30)
        Me.GroupBox4.Controls.Add(Me.cboTaxItemID)
        Me.GroupBox4.Controls.Add(Me.Label29)
        Me.GroupBox4.Controls.Add(Me.cboTaxCodeID)
        Me.GroupBox4.Controls.Add(Me.Label19)
        Me.GroupBox4.Location = New System.Drawing.Point(7, 12)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(214, 104)
        Me.GroupBox4.TabIndex = 17
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Sales Tax Info."
        '
        'cboTaxItemName
        '
        Me.cboTaxItemName.FormattingEnabled = True
        Me.cboTaxItemName.Location = New System.Drawing.Point(62, 50)
        Me.cboTaxItemName.Name = "cboTaxItemName"
        Me.cboTaxItemName.Size = New System.Drawing.Size(146, 21)
        Me.cboTaxItemName.TabIndex = 27
        '
        'cboTaxCodeName
        '
        Me.cboTaxCodeName.FormattingEnabled = True
        Me.cboTaxCodeName.Location = New System.Drawing.Point(62, 23)
        Me.cboTaxCodeName.Name = "cboTaxCodeName"
        Me.cboTaxCodeName.Size = New System.Drawing.Size(146, 21)
        Me.cboTaxCodeName.TabIndex = 27
        '
        'txtResaleNo
        '
        Me.txtResaleNo.Location = New System.Drawing.Point(62, 76)
        Me.txtResaleNo.Name = "txtResaleNo"
        Me.txtResaleNo.Size = New System.Drawing.Size(146, 21)
        Me.txtResaleNo.TabIndex = 19
        Me.txtResaleNo.Text = "0"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(6, 79)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(59, 13)
        Me.Label30.TabIndex = 18
        Me.Label30.Text = "Resale No."
        '
        'cboTaxItemID
        '
        Me.cboTaxItemID.FormattingEnabled = True
        Me.cboTaxItemID.Location = New System.Drawing.Point(62, 50)
        Me.cboTaxItemID.Name = "cboTaxItemID"
        Me.cboTaxItemID.Size = New System.Drawing.Size(146, 21)
        Me.cboTaxItemID.TabIndex = 17
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(6, 53)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(50, 13)
        Me.Label29.TabIndex = 16
        Me.Label29.Text = "Tax Item"
        '
        'cboTaxCodeID
        '
        Me.cboTaxCodeID.FormattingEnabled = True
        Me.cboTaxCodeID.Location = New System.Drawing.Point(62, 23)
        Me.cboTaxCodeID.Name = "cboTaxCodeID"
        Me.cboTaxCodeID.Size = New System.Drawing.Size(146, 21)
        Me.cboTaxCodeID.TabIndex = 15
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(6, 26)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(53, 13)
        Me.Label19.TabIndex = 14
        Me.Label19.Text = "Tax Code"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.grdDefinedField)
        Me.GroupBox2.Controls.Add(Me.btnDefineField)
        Me.GroupBox2.Location = New System.Drawing.Point(233, 38)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(214, 254)
        Me.GroupBox2.TabIndex = 16
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Custom Fields"
        '
        'grdDefinedField
        '
        Me.grdDefinedField.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdDefinedField.Location = New System.Drawing.Point(6, 20)
        Me.grdDefinedField.Name = "grdDefinedField"
        Me.grdDefinedField.Size = New System.Drawing.Size(202, 201)
        Me.grdDefinedField.TabIndex = 26
        '
        'btnDefineField
        '
        Me.btnDefineField.Location = New System.Drawing.Point(6, 227)
        Me.btnDefineField.Name = "btnDefineField"
        Me.btnDefineField.Size = New System.Drawing.Size(88, 21)
        Me.btnDefineField.TabIndex = 25
        Me.btnDefineField.Text = "&Define Fields"
        Me.btnDefineField.UseVisualStyleBackColor = True
        '
        'cboPriceLvlID
        '
        Me.cboPriceLvlID.FormattingEnabled = True
        Me.cboPriceLvlID.Location = New System.Drawing.Point(311, 9)
        Me.cboPriceLvlID.Name = "cboPriceLvlID"
        Me.cboPriceLvlID.Size = New System.Drawing.Size(134, 21)
        Me.cboPriceLvlID.TabIndex = 15
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(230, 12)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(58, 13)
        Me.Label22.TabIndex = 14
        Me.Label22.Text = "Price Level"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cboRepName)
        Me.GroupBox1.Controls.Add(Me.cboTermsName)
        Me.GroupBox1.Controls.Add(Me.cboTypeName)
        Me.GroupBox1.Controls.Add(Me.cboTypeID)
        Me.GroupBox1.Controls.Add(Me.cboSendMethod)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.cboRepID)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.cboTermsID)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Location = New System.Drawing.Point(7, 122)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(214, 170)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Category"
        '
        'cboRepName
        '
        Me.cboRepName.FormattingEnabled = True
        Me.cboRepName.Location = New System.Drawing.Point(58, 76)
        Me.cboRepName.Name = "cboRepName"
        Me.cboRepName.Size = New System.Drawing.Size(150, 21)
        Me.cboRepName.TabIndex = 18
        '
        'cboTermsName
        '
        Me.cboTermsName.FormattingEnabled = True
        Me.cboTermsName.Location = New System.Drawing.Point(58, 49)
        Me.cboTermsName.Name = "cboTermsName"
        Me.cboTermsName.Size = New System.Drawing.Size(150, 21)
        Me.cboTermsName.TabIndex = 18
        '
        'cboTypeName
        '
        Me.cboTypeName.FormattingEnabled = True
        Me.cboTypeName.Location = New System.Drawing.Point(58, 22)
        Me.cboTypeName.Name = "cboTypeName"
        Me.cboTypeName.Size = New System.Drawing.Size(150, 21)
        Me.cboTypeName.TabIndex = 13
        '
        'cboTypeID
        '
        Me.cboTypeID.FormattingEnabled = True
        Me.cboTypeID.Location = New System.Drawing.Point(58, 22)
        Me.cboTypeID.Name = "cboTypeID"
        Me.cboTypeID.Size = New System.Drawing.Size(150, 21)
        Me.cboTypeID.TabIndex = 18
        '
        'cboSendMethod
        '
        Me.cboSendMethod.FormattingEnabled = True
        Me.cboSendMethod.Location = New System.Drawing.Point(58, 124)
        Me.cboSendMethod.Name = "cboSendMethod"
        Me.cboSendMethod.Size = New System.Drawing.Size(150, 21)
        Me.cboSendMethod.TabIndex = 19
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(6, 105)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(119, 13)
        Me.Label18.TabIndex = 18
        Me.Label18.Text = "Preferred Send Method"
        '
        'cboRepID
        '
        Me.cboRepID.FormattingEnabled = True
        Me.cboRepID.Location = New System.Drawing.Point(58, 76)
        Me.cboRepID.Name = "cboRepID"
        Me.cboRepID.Size = New System.Drawing.Size(150, 21)
        Me.cboRepID.TabIndex = 17
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(6, 79)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(26, 13)
        Me.Label17.TabIndex = 16
        Me.Label17.Text = "Rep"
        '
        'cboTermsID
        '
        Me.cboTermsID.FormattingEnabled = True
        Me.cboTermsID.Location = New System.Drawing.Point(58, 49)
        Me.cboTermsID.Name = "cboTermsID"
        Me.cboTermsID.Size = New System.Drawing.Size(150, 21)
        Me.cboTermsID.TabIndex = 15
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(6, 52)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(36, 13)
        Me.Label21.TabIndex = 14
        Me.Label21.Text = "Terms"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(6, 25)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(31, 13)
        Me.Label20.TabIndex = 10
        Me.Label20.Text = "Type"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox5)
        Me.TabPage3.Controls.Add(Me.txtCreditLimit)
        Me.TabPage3.Controls.Add(Me.Label32)
        Me.TabPage3.Controls.Add(Me.txtAcctNo)
        Me.TabPage3.Controls.Add(Me.Label31)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(459, 341)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Payment Info"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.cboPaymentMethodName)
        Me.GroupBox5.Controls.Add(Me.Label38)
        Me.GroupBox5.Controls.Add(Me.txtYear)
        Me.GroupBox5.Controls.Add(Me.txtMonth)
        Me.GroupBox5.Controls.Add(Me.Label37)
        Me.GroupBox5.Controls.Add(Me.txtZipCode)
        Me.GroupBox5.Controls.Add(Me.Label36)
        Me.GroupBox5.Controls.Add(Me.txtAddress)
        Me.GroupBox5.Controls.Add(Me.Label35)
        Me.GroupBox5.Controls.Add(Me.txtNameOnCard)
        Me.GroupBox5.Controls.Add(Me.Label34)
        Me.GroupBox5.Controls.Add(Me.txtCreditCardNo)
        Me.GroupBox5.Controls.Add(Me.Label33)
        Me.GroupBox5.Controls.Add(Me.cboPaymentMethodID)
        Me.GroupBox5.Location = New System.Drawing.Point(18, 91)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(399, 173)
        Me.GroupBox5.TabIndex = 20
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Preferred Payment Method"
        '
        'cboPaymentMethodName
        '
        Me.cboPaymentMethodName.FormattingEnabled = True
        Me.cboPaymentMethodName.Location = New System.Drawing.Point(6, 20)
        Me.cboPaymentMethodName.Name = "cboPaymentMethodName"
        Me.cboPaymentMethodName.Size = New System.Drawing.Size(222, 21)
        Me.cboPaymentMethodName.TabIndex = 30
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(323, 59)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(11, 13)
        Me.Label38.TabIndex = 29
        Me.Label38.Text = "/"
        '
        'txtYear
        '
        Me.txtYear.Location = New System.Drawing.Point(337, 56)
        Me.txtYear.MaxLength = 4
        Me.txtYear.Name = "txtYear"
        Me.txtYear.Size = New System.Drawing.Size(40, 21)
        Me.txtYear.TabIndex = 28
        '
        'txtMonth
        '
        Me.txtMonth.Location = New System.Drawing.Point(293, 56)
        Me.txtMonth.MaxLength = 2
        Me.txtMonth.Name = "txtMonth"
        Me.txtMonth.Size = New System.Drawing.Size(24, 21)
        Me.txtMonth.TabIndex = 27
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(234, 56)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(55, 13)
        Me.Label37.TabIndex = 26
        Me.Label37.Text = "Exp. Date"
        '
        'txtZipCode
        '
        Me.txtZipCode.Location = New System.Drawing.Point(98, 137)
        Me.txtZipCode.Name = "txtZipCode"
        Me.txtZipCode.Size = New System.Drawing.Size(130, 21)
        Me.txtZipCode.TabIndex = 25
        Me.txtZipCode.Text = "0"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(10, 140)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(49, 13)
        Me.Label36.TabIndex = 24
        Me.Label36.Text = "Zip Code"
        '
        'txtAddress
        '
        Me.txtAddress.Location = New System.Drawing.Point(98, 110)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(279, 21)
        Me.txtAddress.TabIndex = 23
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(10, 113)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(46, 13)
        Me.Label35.TabIndex = 22
        Me.Label35.Text = "Address"
        '
        'txtNameOnCard
        '
        Me.txtNameOnCard.Location = New System.Drawing.Point(98, 83)
        Me.txtNameOnCard.Name = "txtNameOnCard"
        Me.txtNameOnCard.Size = New System.Drawing.Size(279, 21)
        Me.txtNameOnCard.TabIndex = 21
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(10, 86)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(75, 13)
        Me.Label34.TabIndex = 20
        Me.Label34.Text = "Name on Card"
        '
        'txtCreditCardNo
        '
        Me.txtCreditCardNo.Location = New System.Drawing.Point(98, 56)
        Me.txtCreditCardNo.Name = "txtCreditCardNo"
        Me.txtCreditCardNo.Size = New System.Drawing.Size(130, 21)
        Me.txtCreditCardNo.TabIndex = 19
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(10, 59)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(82, 13)
        Me.Label33.TabIndex = 18
        Me.Label33.Text = "Credit Card No."
        '
        'cboPaymentMethodID
        '
        Me.cboPaymentMethodID.FormattingEnabled = True
        Me.cboPaymentMethodID.Location = New System.Drawing.Point(6, 20)
        Me.cboPaymentMethodID.Name = "cboPaymentMethodID"
        Me.cboPaymentMethodID.Size = New System.Drawing.Size(222, 21)
        Me.cboPaymentMethodID.TabIndex = 0
        '
        'txtCreditLimit
        '
        Me.txtCreditLimit.Location = New System.Drawing.Point(88, 44)
        Me.txtCreditLimit.Name = "txtCreditLimit"
        Me.txtCreditLimit.Size = New System.Drawing.Size(130, 21)
        Me.txtCreditLimit.TabIndex = 19
        Me.txtCreditLimit.Text = "0.00"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(16, 47)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(60, 13)
        Me.Label32.TabIndex = 18
        Me.Label32.Text = "Credit Limit"
        '
        'txtAcctNo
        '
        Me.txtAcctNo.Location = New System.Drawing.Point(88, 15)
        Me.txtAcctNo.Name = "txtAcctNo"
        Me.txtAcctNo.Size = New System.Drawing.Size(130, 21)
        Me.txtAcctNo.TabIndex = 17
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(16, 18)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(66, 13)
        Me.Label31.TabIndex = 16
        Me.Label31.Text = "Account No."
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.cboJobTypeName)
        Me.TabPage4.Controls.Add(Me.txtJobDescription)
        Me.TabPage4.Controls.Add(Me.cboJobTypeID)
        Me.TabPage4.Controls.Add(Me.Label44)
        Me.TabPage4.Controls.Add(Me.Label43)
        Me.TabPage4.Controls.Add(Me.dteEndDate)
        Me.TabPage4.Controls.Add(Me.Label42)
        Me.TabPage4.Controls.Add(Me.dteEndDateTemp)
        Me.TabPage4.Controls.Add(Me.Label41)
        Me.TabPage4.Controls.Add(Me.dteStartDate)
        Me.TabPage4.Controls.Add(Me.Label40)
        Me.TabPage4.Controls.Add(Me.cboJobStat)
        Me.TabPage4.Controls.Add(Me.Label39)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(459, 341)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Job Info"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'cboJobTypeName
        '
        Me.cboJobTypeName.FormattingEnabled = True
        Me.cboJobTypeName.Location = New System.Drawing.Point(273, 41)
        Me.cboJobTypeName.Name = "cboJobTypeName"
        Me.cboJobTypeName.Size = New System.Drawing.Size(146, 21)
        Me.cboJobTypeName.TabIndex = 12
        '
        'txtJobDescription
        '
        Me.txtJobDescription.Location = New System.Drawing.Point(273, 97)
        Me.txtJobDescription.Multiline = True
        Me.txtJobDescription.Name = "txtJobDescription"
        Me.txtJobDescription.Size = New System.Drawing.Size(146, 120)
        Me.txtJobDescription.TabIndex = 11
        '
        'cboJobTypeID
        '
        Me.cboJobTypeID.FormattingEnabled = True
        Me.cboJobTypeID.Location = New System.Drawing.Point(273, 41)
        Me.cboJobTypeID.Name = "cboJobTypeID"
        Me.cboJobTypeID.Size = New System.Drawing.Size(146, 21)
        Me.cboJobTypeID.TabIndex = 10
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(270, 25)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(51, 13)
        Me.Label44.TabIndex = 9
        Me.Label44.Text = "Job Type"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(270, 80)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(80, 13)
        Me.Label43.TabIndex = 8
        Me.Label43.Text = "Job Description"
        '
        'dteEndDate
        '
        Me.dteEndDate.Location = New System.Drawing.Point(18, 196)
        Me.dteEndDate.Name = "dteEndDate"
        Me.dteEndDate.Size = New System.Drawing.Size(203, 21)
        Me.dteEndDate.TabIndex = 7
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(15, 180)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(51, 13)
        Me.Label42.TabIndex = 6
        Me.Label42.Text = "End Date"
        '
        'dteEndDateTemp
        '
        Me.dteEndDateTemp.Location = New System.Drawing.Point(18, 146)
        Me.dteEndDateTemp.Name = "dteEndDateTemp"
        Me.dteEndDateTemp.Size = New System.Drawing.Size(200, 21)
        Me.dteEndDateTemp.TabIndex = 5
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(15, 130)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(74, 13)
        Me.Label41.TabIndex = 4
        Me.Label41.Text = "Projected End"
        '
        'dteStartDate
        '
        Me.dteStartDate.Location = New System.Drawing.Point(18, 96)
        Me.dteStartDate.Name = "dteStartDate"
        Me.dteStartDate.Size = New System.Drawing.Size(200, 21)
        Me.dteStartDate.TabIndex = 3
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(15, 80)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(57, 13)
        Me.Label40.TabIndex = 2
        Me.Label40.Text = "Start Date"
        '
        'cboJobStat
        '
        Me.cboJobStat.FormattingEnabled = True
        Me.cboJobStat.Location = New System.Drawing.Point(18, 41)
        Me.cboJobStat.Name = "cboJobStat"
        Me.cboJobStat.Size = New System.Drawing.Size(200, 21)
        Me.cboJobStat.TabIndex = 1
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(15, 25)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(58, 13)
        Me.Label39.TabIndex = 0
        Me.Label39.Text = "Job Status"
        '
        'btnOK
        '
        Me.btnOK.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btnOK.Location = New System.Drawing.Point(325, 459)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 29)
        Me.btnOK.TabIndex = 7
        Me.btnOK.Text = "&Save"
        Me.btnOK.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.btnCancel.Location = New System.Drawing.Point(403, 459)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 29)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'chkInactive
        '
        Me.chkInactive.AutoSize = True
        Me.chkInactive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkInactive.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkInactive.Location = New System.Drawing.Point(413, 71)
        Me.chkInactive.Name = "chkInactive"
        Me.chkInactive.Size = New System.Drawing.Size(65, 17)
        Me.chkInactive.TabIndex = 9
        Me.chkInactive.Text = "Inactive"
        Me.chkInactive.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkInactive.UseVisualStyleBackColor = True
        '
        'frm_cust_masterCustomerAddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(495, 492)
        Me.Controls.Add(Me.chkInactive)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.txtOpeningBal)
        Me.Controls.Add(Me.txtCustomerName)
        Me.Controls.Add(Me.dteOpeningBalDate)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblBalance)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_cust_masterCustomerAddEdit"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "New Customer"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.grdDefinedField, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dteOpeningBalDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtCustomerName As System.Windows.Forms.TextBox
    Friend WithEvents txtOpeningBal As System.Windows.Forms.TextBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents txtSalutation As System.Windows.Forms.TextBox
    Friend WithEvents txtCoName As System.Windows.Forms.TextBox
    Friend WithEvents txtContact As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtCC As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtAltContact As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtAltPhone As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtFAX As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtPhone As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboTermsID As System.Windows.Forms.ComboBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cboTypeName As System.Windows.Forms.ComboBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cboPriceLvlID As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents btnDefineField As System.Windows.Forms.Button
    Friend WithEvents chkInactive As System.Windows.Forms.CheckBox
    Friend WithEvents txtMiddleInitial As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnEditBillAdd As System.Windows.Forms.Button
    Friend WithEvents txtBillAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtShipAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents btnCopy As System.Windows.Forms.Button
    Friend WithEvents cboShipToID As System.Windows.Forms.ComboBox
    Friend WithEvents cboSendMethod As System.Windows.Forms.ComboBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents cboRepID As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtResaleNo As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents cboTaxItemID As System.Windows.Forms.ComboBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents cboTaxCodeID As System.Windows.Forms.ComboBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents txtCreditLimit As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtAcctNo As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtZipCode As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtNameOnCard As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtCreditCardNo As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents cboPaymentMethodID As System.Windows.Forms.ComboBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents txtYear As System.Windows.Forms.TextBox
    Friend WithEvents txtMonth As System.Windows.Forms.TextBox
    Friend WithEvents txtJobDescription As System.Windows.Forms.TextBox
    Friend WithEvents cboJobTypeID As System.Windows.Forms.ComboBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents dteEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents dteEndDateTemp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents dteStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents cboJobStat As System.Windows.Forms.ComboBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents grdDefinedField As System.Windows.Forms.DataGridView
    Friend WithEvents cboTypeID As System.Windows.Forms.ComboBox
    Friend WithEvents cboTermsName As System.Windows.Forms.ComboBox
    Friend WithEvents cboRepName As System.Windows.Forms.ComboBox
    Friend WithEvents cboPriceLvlName As System.Windows.Forms.ComboBox
    Friend WithEvents cboPaymentMethodName As System.Windows.Forms.ComboBox
    Friend WithEvents cboJobTypeName As System.Windows.Forms.ComboBox
    Friend WithEvents cboTaxCodeName As System.Windows.Forms.ComboBox
    Friend WithEvents cboTaxItemName As System.Windows.Forms.ComboBox
    Friend WithEvents cboShipToName As System.Windows.Forms.ComboBox
    Friend WithEvents btnDeleteShipAdd As System.Windows.Forms.Button
    Friend WithEvents btnEditShipAdd As System.Windows.Forms.Button
    Friend WithEvents btnAddShip As System.Windows.Forms.Button
End Class
