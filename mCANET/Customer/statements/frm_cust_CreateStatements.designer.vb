<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cust_CreateStatements
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtDays = New System.Windows.Forms.TextBox
        Me.chkIncludeTrans = New System.Windows.Forms.CheckBox
        Me.rbtnAsOfStatementDte = New System.Windows.Forms.RadioButton
        Me.rbtnStatementPeriod = New System.Windows.Forms.RadioButton
        Me.dteStatementPeriodTo = New System.Windows.Forms.DateTimePicker
        Me.Label3 = New System.Windows.Forms.Label
        Me.dteStatementPeriodFrom = New System.Windows.Forms.DateTimePicker
        Me.dteStatementDate = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.btnViewSelected = New System.Windows.Forms.Button
        Me.rbtnPreferred = New System.Windows.Forms.RadioButton
        Me.rbtnCustomerType = New System.Windows.Forms.RadioButton
        Me.rbtnOneCustomer = New System.Windows.Forms.RadioButton
        Me.rbtnMulCustomer = New System.Windows.Forms.RadioButton
        Me.rbtnAllCustomer = New System.Windows.Forms.RadioButton
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnAssessCharges = New System.Windows.Forms.Button
        Me.chkInactive = New System.Windows.Forms.CheckBox
        Me.txtAmount = New System.Windows.Forms.TextBox
        Me.chkNoAcct = New System.Windows.Forms.CheckBox
        Me.chkLessBal = New System.Windows.Forms.CheckBox
        Me.chkZeroBal = New System.Windows.Forms.CheckBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.chkPrintDueDate = New System.Windows.Forms.CheckBox
        Me.chkPrintbyZIP = New System.Windows.Forms.CheckBox
        Me.chkShowInvoice = New System.Windows.Forms.CheckBox
        Me.cboCreatePer = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.btnPreview = New System.Windows.Forms.Button
        Me.btnPrint = New System.Windows.Forms.Button
        Me.btnEmail = New System.Windows.Forms.Button
        Me.btnHelp = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtDays)
        Me.GroupBox1.Controls.Add(Me.chkIncludeTrans)
        Me.GroupBox1.Controls.Add(Me.rbtnAsOfStatementDte)
        Me.GroupBox1.Controls.Add(Me.rbtnStatementPeriod)
        Me.GroupBox1.Controls.Add(Me.dteStatementPeriodTo)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.dteStatementPeriodFrom)
        Me.GroupBox1.Controls.Add(Me.dteStatementDate)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(10, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(365, 127)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Statement Date && Type"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(242, 95)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(97, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "days past due date"
        '
        'txtDays
        '
        Me.txtDays.Location = New System.Drawing.Point(208, 92)
        Me.txtDays.Name = "txtDays"
        Me.txtDays.Size = New System.Drawing.Size(28, 20)
        Me.txtDays.TabIndex = 9
        Me.txtDays.Text = "30"
        '
        'chkIncludeTrans
        '
        Me.chkIncludeTrans.AutoSize = True
        Me.chkIncludeTrans.Location = New System.Drawing.Point(35, 94)
        Me.chkIncludeTrans.Name = "chkIncludeTrans"
        Me.chkIncludeTrans.Size = New System.Drawing.Size(167, 17)
        Me.chkIncludeTrans.TabIndex = 8
        Me.chkIncludeTrans.Text = "Include only transactions over"
        Me.chkIncludeTrans.UseVisualStyleBackColor = True
        '
        'rbtnAsOfStatementDte
        '
        Me.rbtnAsOfStatementDte.AutoSize = True
        Me.rbtnAsOfStatementDte.Location = New System.Drawing.Point(14, 71)
        Me.rbtnAsOfStatementDte.Name = "rbtnAsOfStatementDte"
        Me.rbtnAsOfStatementDte.Size = New System.Drawing.Size(226, 17)
        Me.rbtnAsOfStatementDte.TabIndex = 7
        Me.rbtnAsOfStatementDte.TabStop = True
        Me.rbtnAsOfStatementDte.Text = "All open transactions as of Statement Date"
        Me.rbtnAsOfStatementDte.UseVisualStyleBackColor = True
        '
        'rbtnStatementPeriod
        '
        Me.rbtnStatementPeriod.AutoSize = True
        Me.rbtnStatementPeriod.Location = New System.Drawing.Point(14, 48)
        Me.rbtnStatementPeriod.Name = "rbtnStatementPeriod"
        Me.rbtnStatementPeriod.Size = New System.Drawing.Size(129, 17)
        Me.rbtnStatementPeriod.TabIndex = 6
        Me.rbtnStatementPeriod.TabStop = True
        Me.rbtnStatementPeriod.Text = "Statement Perio&d from"
        Me.rbtnStatementPeriod.UseVisualStyleBackColor = True
        '
        'dteStatementPeriodTo
        '
        Me.dteStatementPeriodTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteStatementPeriodTo.Location = New System.Drawing.Point(268, 45)
        Me.dteStatementPeriodTo.Name = "dteStatementPeriodTo"
        Me.dteStatementPeriodTo.Size = New System.Drawing.Size(87, 20)
        Me.dteStatementPeriodTo.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(242, 49)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(20, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "To"
        '
        'dteStatementPeriodFrom
        '
        Me.dteStatementPeriodFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteStatementPeriodFrom.Location = New System.Drawing.Point(149, 45)
        Me.dteStatementPeriodFrom.Name = "dteStatementPeriodFrom"
        Me.dteStatementPeriodFrom.Size = New System.Drawing.Size(87, 20)
        Me.dteStatementPeriodFrom.TabIndex = 3
        '
        'dteStatementDate
        '
        Me.dteStatementDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteStatementDate.Location = New System.Drawing.Point(149, 21)
        Me.dteStatementDate.Name = "dteStatementDate"
        Me.dteStatementDate.Size = New System.Drawing.Size(87, 20)
        Me.dteStatementDate.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(32, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "&Statement Date"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnViewSelected)
        Me.GroupBox2.Controls.Add(Me.rbtnPreferred)
        Me.GroupBox2.Controls.Add(Me.rbtnCustomerType)
        Me.GroupBox2.Controls.Add(Me.rbtnOneCustomer)
        Me.GroupBox2.Controls.Add(Me.rbtnMulCustomer)
        Me.GroupBox2.Controls.Add(Me.rbtnAllCustomer)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 146)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(363, 166)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Customer"
        '
        'btnViewSelected
        '
        Me.btnViewSelected.Location = New System.Drawing.Point(84, 134)
        Me.btnViewSelected.Name = "btnViewSelected"
        Me.btnViewSelected.Size = New System.Drawing.Size(154, 23)
        Me.btnViewSelected.TabIndex = 13
        Me.btnViewSelected.Text = "View Selected Customers"
        Me.btnViewSelected.UseVisualStyleBackColor = True
        '
        'rbtnPreferred
        '
        Me.rbtnPreferred.AutoSize = True
        Me.rbtnPreferred.Location = New System.Drawing.Point(12, 111)
        Me.rbtnPreferred.Name = "rbtnPreferred"
        Me.rbtnPreferred.Size = New System.Drawing.Size(135, 17)
        Me.rbtnPreferred.TabIndex = 11
        Me.rbtnPreferred.TabStop = True
        Me.rbtnPreferred.Text = "Preferred Send Method"
        Me.rbtnPreferred.UseVisualStyleBackColor = True
        '
        'rbtnCustomerType
        '
        Me.rbtnCustomerType.AutoSize = True
        Me.rbtnCustomerType.Location = New System.Drawing.Point(12, 88)
        Me.rbtnCustomerType.Name = "rbtnCustomerType"
        Me.rbtnCustomerType.Size = New System.Drawing.Size(113, 17)
        Me.rbtnCustomerType.TabIndex = 10
        Me.rbtnCustomerType.TabStop = True
        Me.rbtnCustomerType.Text = "Customers of Type"
        Me.rbtnCustomerType.UseVisualStyleBackColor = True
        '
        'rbtnOneCustomer
        '
        Me.rbtnOneCustomer.AutoSize = True
        Me.rbtnOneCustomer.Location = New System.Drawing.Point(12, 65)
        Me.rbtnOneCustomer.Name = "rbtnOneCustomer"
        Me.rbtnOneCustomer.Size = New System.Drawing.Size(92, 17)
        Me.rbtnOneCustomer.TabIndex = 9
        Me.rbtnOneCustomer.TabStop = True
        Me.rbtnOneCustomer.Text = "One Customer"
        Me.rbtnOneCustomer.UseVisualStyleBackColor = True
        '
        'rbtnMulCustomer
        '
        Me.rbtnMulCustomer.AutoSize = True
        Me.rbtnMulCustomer.Location = New System.Drawing.Point(12, 42)
        Me.rbtnMulCustomer.Name = "rbtnMulCustomer"
        Me.rbtnMulCustomer.Size = New System.Drawing.Size(113, 17)
        Me.rbtnMulCustomer.TabIndex = 8
        Me.rbtnMulCustomer.TabStop = True
        Me.rbtnMulCustomer.Text = "Multiple Custo&mers"
        Me.rbtnMulCustomer.UseVisualStyleBackColor = True
        '
        'rbtnAllCustomer
        '
        Me.rbtnAllCustomer.AutoSize = True
        Me.rbtnAllCustomer.Location = New System.Drawing.Point(12, 19)
        Me.rbtnAllCustomer.Name = "rbtnAllCustomer"
        Me.rbtnAllCustomer.Size = New System.Drawing.Size(88, 17)
        Me.rbtnAllCustomer.TabIndex = 7
        Me.rbtnAllCustomer.TabStop = True
        Me.rbtnAllCustomer.Text = "&All Customers"
        Me.rbtnAllCustomer.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnAssessCharges)
        Me.GroupBox3.Controls.Add(Me.chkInactive)
        Me.GroupBox3.Controls.Add(Me.txtAmount)
        Me.GroupBox3.Controls.Add(Me.chkNoAcct)
        Me.GroupBox3.Controls.Add(Me.chkLessBal)
        Me.GroupBox3.Controls.Add(Me.chkZeroBal)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.chkPrintDueDate)
        Me.GroupBox3.Controls.Add(Me.chkPrintbyZIP)
        Me.GroupBox3.Controls.Add(Me.chkShowInvoice)
        Me.GroupBox3.Controls.Add(Me.cboCreatePer)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Location = New System.Drawing.Point(381, 12)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(253, 300)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Additional Options"
        '
        'btnAssessCharges
        '
        Me.btnAssessCharges.Location = New System.Drawing.Point(73, 268)
        Me.btnAssessCharges.Name = "btnAssessCharges"
        Me.btnAssessCharges.Size = New System.Drawing.Size(131, 23)
        Me.btnAssessCharges.TabIndex = 12
        Me.btnAssessCharges.Text = "Assess Fi&nance Charges"
        Me.btnAssessCharges.UseVisualStyleBackColor = True
        '
        'chkInactive
        '
        Me.chkInactive.AutoSize = True
        Me.chkInactive.Location = New System.Drawing.Point(9, 240)
        Me.chkInactive.Name = "chkInactive"
        Me.chkInactive.Size = New System.Drawing.Size(129, 17)
        Me.chkInactive.TabIndex = 11
        Me.chkInactive.Text = "for inactive customers"
        Me.chkInactive.UseVisualStyleBackColor = True
        '
        'txtAmount
        '
        Me.txtAmount.Location = New System.Drawing.Point(155, 192)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(65, 20)
        Me.txtAmount.TabIndex = 10
        Me.txtAmount.Text = "0.00"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkNoAcct
        '
        Me.chkNoAcct.AutoSize = True
        Me.chkNoAcct.Location = New System.Drawing.Point(9, 217)
        Me.chkNoAcct.Name = "chkNoAcct"
        Me.chkNoAcct.Size = New System.Drawing.Size(138, 17)
        Me.chkNoAcct.TabIndex = 8
        Me.chkNoAcct.Text = "with no account activity"
        Me.chkNoAcct.UseVisualStyleBackColor = True
        '
        'chkLessBal
        '
        Me.chkLessBal.AutoSize = True
        Me.chkLessBal.Location = New System.Drawing.Point(9, 194)
        Me.chkLessBal.Name = "chkLessBal"
        Me.chkLessBal.Size = New System.Drawing.Size(140, 17)
        Me.chkLessBal.TabIndex = 7
        Me.chkLessBal.Text = "with a balance less than"
        Me.chkLessBal.UseVisualStyleBackColor = True
        '
        'chkZeroBal
        '
        Me.chkZeroBal.AutoSize = True
        Me.chkZeroBal.Location = New System.Drawing.Point(9, 171)
        Me.chkZeroBal.Name = "chkZeroBal"
        Me.chkZeroBal.Size = New System.Drawing.Size(118, 17)
        Me.chkZeroBal.TabIndex = 6
        Me.chkZeroBal.Text = "with a &zero balance"
        Me.chkZeroBal.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 153)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(134, 13)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Do NOT create statements"
        '
        'chkPrintDueDate
        '
        Me.chkPrintDueDate.AutoSize = True
        Me.chkPrintDueDate.Location = New System.Drawing.Point(6, 117)
        Me.chkPrintDueDate.Name = "chkPrintDueDate"
        Me.chkPrintDueDate.Size = New System.Drawing.Size(167, 17)
        Me.chkPrintDueDate.TabIndex = 4
        Me.chkPrintDueDate.Text = "Print due date on transactions"
        Me.chkPrintDueDate.UseVisualStyleBackColor = True
        '
        'chkPrintbyZIP
        '
        Me.chkPrintbyZIP.AutoSize = True
        Me.chkPrintbyZIP.Location = New System.Drawing.Point(6, 94)
        Me.chkPrintbyZIP.Name = "chkPrintbyZIP"
        Me.chkPrintbyZIP.Size = New System.Drawing.Size(224, 17)
        Me.chkPrintbyZIP.TabIndex = 3
        Me.chkPrintbyZIP.Text = "Print statements by billing address zipcode"
        Me.chkPrintbyZIP.UseVisualStyleBackColor = True
        '
        'chkShowInvoice
        '
        Me.chkShowInvoice.AutoSize = True
        Me.chkShowInvoice.Location = New System.Drawing.Point(6, 71)
        Me.chkShowInvoice.Name = "chkShowInvoice"
        Me.chkShowInvoice.Size = New System.Drawing.Size(214, 17)
        Me.chkShowInvoice.TabIndex = 2
        Me.chkShowInvoice.Text = "Show invoice item details on statements"
        Me.chkShowInvoice.UseVisualStyleBackColor = True
        '
        'cboCreatePer
        '
        Me.cboCreatePer.FormattingEnabled = True
        Me.cboCreatePer.Location = New System.Drawing.Point(9, 45)
        Me.cboCreatePer.Name = "cboCreatePer"
        Me.cboCreatePer.Size = New System.Drawing.Size(181, 21)
        Me.cboCreatePer.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 25)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(115, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Create One Statement "
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(34, 321)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(75, 23)
        Me.btnPreview.TabIndex = 3
        Me.btnPreview.Text = "Prev&iew"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(219, 321)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 23)
        Me.btnPrint.TabIndex = 0
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnEmail
        '
        Me.btnEmail.Location = New System.Drawing.Point(300, 321)
        Me.btnEmail.Name = "btnEmail"
        Me.btnEmail.Size = New System.Drawing.Size(75, 23)
        Me.btnEmail.TabIndex = 4
        Me.btnEmail.Text = "E-mail"
        Me.btnEmail.UseVisualStyleBackColor = True
        '
        'btnHelp
        '
        Me.btnHelp.Location = New System.Drawing.Point(556, 321)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.Size = New System.Drawing.Size(75, 23)
        Me.btnHelp.TabIndex = 6
        Me.btnHelp.Text = "Help"
        Me.btnHelp.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(475, 321)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frm_cust_CreateStatements
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(643, 359)
        Me.Controls.Add(Me.btnHelp)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnEmail)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frm_cust_CreateStatements"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Create Statements"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents dteStatementPeriodFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents dteStatementDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents rbtnStatementPeriod As System.Windows.Forms.RadioButton
    Friend WithEvents dteStatementPeriodTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDays As System.Windows.Forms.TextBox
    Friend WithEvents chkIncludeTrans As System.Windows.Forms.CheckBox
    Friend WithEvents rbtnAsOfStatementDte As System.Windows.Forms.RadioButton
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboCreatePer As System.Windows.Forms.ComboBox
    Friend WithEvents chkShowInvoice As System.Windows.Forms.CheckBox
    Friend WithEvents chkPrintDueDate As System.Windows.Forms.CheckBox
    Friend WithEvents chkPrintbyZIP As System.Windows.Forms.CheckBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtAmount As System.Windows.Forms.TextBox
    Friend WithEvents chkNoAcct As System.Windows.Forms.CheckBox
    Friend WithEvents chkLessBal As System.Windows.Forms.CheckBox
    Friend WithEvents chkZeroBal As System.Windows.Forms.CheckBox
    Friend WithEvents chkInactive As System.Windows.Forms.CheckBox
    Friend WithEvents btnAssessCharges As System.Windows.Forms.Button
    Friend WithEvents rbtnAllCustomer As System.Windows.Forms.RadioButton
    Friend WithEvents btnViewSelected As System.Windows.Forms.Button
    Friend WithEvents rbtnPreferred As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnCustomerType As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnOneCustomer As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnMulCustomer As System.Windows.Forms.RadioButton
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnEmail As System.Windows.Forms.Button
    Friend WithEvents btnHelp As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
End Class
