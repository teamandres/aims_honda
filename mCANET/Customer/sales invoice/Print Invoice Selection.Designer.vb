<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Print_Invoice_Selection
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rbtnSummary = New System.Windows.Forms.RadioButton
        Me.rbtnDetails = New System.Windows.Forms.RadioButton
        Me.btnOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtDeliveryNo = New System.Windows.Forms.TextBox
        Me.txtProject = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.chkAplVAT = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'rbtnSummary
        '
        Me.rbtnSummary.AutoSize = True
        Me.rbtnSummary.Location = New System.Drawing.Point(29, 26)
        Me.rbtnSummary.Name = "rbtnSummary"
        Me.rbtnSummary.Size = New System.Drawing.Size(132, 17)
        Me.rbtnSummary.TabIndex = 0
        Me.rbtnSummary.TabStop = True
        Me.rbtnSummary.Text = "Print Invoice Summary"
        Me.rbtnSummary.UseVisualStyleBackColor = True
        '
        'rbtnDetails
        '
        Me.rbtnDetails.AutoSize = True
        Me.rbtnDetails.Location = New System.Drawing.Point(29, 107)
        Me.rbtnDetails.Name = "rbtnDetails"
        Me.rbtnDetails.Size = New System.Drawing.Size(120, 17)
        Me.rbtnDetails.TabIndex = 1
        Me.rbtnDetails.TabStop = True
        Me.rbtnDetails.Text = "Print Invoice Details"
        Me.rbtnDetails.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(128, 167)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(208, 167)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(56, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Delivery Receipt #"
        '
        'txtDeliveryNo
        '
        Me.txtDeliveryNo.Enabled = False
        Me.txtDeliveryNo.Location = New System.Drawing.Point(154, 52)
        Me.txtDeliveryNo.Name = "txtDeliveryNo"
        Me.txtDeliveryNo.Size = New System.Drawing.Size(129, 21)
        Me.txtDeliveryNo.TabIndex = 5
        '
        'txtProject
        '
        Me.txtProject.Enabled = False
        Me.txtProject.Location = New System.Drawing.Point(154, 132)
        Me.txtProject.Name = "txtProject"
        Me.txtProject.Size = New System.Drawing.Size(129, 21)
        Me.txtProject.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(107, 135)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Project"
        '
        'chkAplVAT
        '
        Me.chkAplVAT.AutoSize = True
        Me.chkAplVAT.Enabled = False
        Me.chkAplVAT.Location = New System.Drawing.Point(154, 79)
        Me.chkAplVAT.Name = "chkAplVAT"
        Me.chkAplVAT.Size = New System.Drawing.Size(114, 17)
        Me.chkAplVAT.TabIndex = 8
        Me.chkAplVAT.Text = "Applied 12% VAT?"
        Me.chkAplVAT.UseVisualStyleBackColor = True
        '
        'Print_Invoice_Selection
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(300, 202)
        Me.Controls.Add(Me.chkAplVAT)
        Me.Controls.Add(Me.txtProject)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtDeliveryNo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.rbtnDetails)
        Me.Controls.Add(Me.rbtnSummary)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Print_Invoice_Selection"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Print Invoice Selection"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents rbtnSummary As System.Windows.Forms.RadioButton
    Friend WithEvents rbtnDetails As System.Windows.Forms.RadioButton
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDeliveryNo As System.Windows.Forms.TextBox
    Friend WithEvents txtProject As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkAplVAT As System.Windows.Forms.CheckBox
End Class
