Public Class Print_Invoice_Selection

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If rbtnSummary.Checked = True Then
            Call sprintinvoicesummary()
        ElseIf rbtnDetails.Checked = True Then
            Call sprintinvoicedetails()
        End If
    End Sub

    Private Sub sprintinvoicesummary()
        If txtDeliveryNo.Text <> "" Then
            gDeliveryNumber = txtDeliveryNo.Text
            Me.Close()
            frmSalesInvoiceSummaryRpt.Show()
        Else
            MsgBox("Please provide provide delivery date...", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub sprintinvoicedetails()
        If txtProject.Text <> "" Then
            gProject = txtProject.Text
            frmSalesInvoiceDetailsRpt.Show()
            Me.Close()
        Else
            MsgBox("Please provide provide project...", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub rbtnSummary_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnSummary.CheckedChanged
        Call verify()
    End Sub

    Private Sub rbtnDetails_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnDetails.CheckedChanged
        Call verify()
    End Sub

    Private Sub verify()
        If rbtnSummary.Checked = True Then
            txtDeliveryNo.Enabled = True
            txtDeliveryNo.Text = ""
            chkAplVAT.Checked = False
            chkAplVAT.Enabled = True
        Else
            txtDeliveryNo.Enabled = False
            txtDeliveryNo.Text = ""
            chkAplVAT.Checked = False
            chkAplVAT.Enabled = False
        End If

        If rbtnDetails.Checked = True Then
            txtProject.Enabled = True
            txtProject.Text = ""
            chkAplVAT.Checked = False
            chkAplVAT.Enabled = False
        Else
            txtProject.Enabled = False
            txtProject.Text = ""
            chkAplVAT.Checked = False
            chkAplVAT.Enabled = True
        End If
    End Sub

    Private Sub Print_Invoice_Selection_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call verify()
    End Sub

    Private Sub chkAplVAT_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAplVAT.CheckedChanged
        If chkAplVAT.Checked = True Then
            gAppliedVAT = "True"
        Else
            gAppliedVAT = "False"
        End If
    End Sub
End Class