<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cust_CreateCreditMemos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cust_CreateCreditMemos))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ts_Previous = New System.Windows.Forms.ToolStripButton
        Me.ts_Next = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripSplitButton
        Me.ts_Preview = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Print = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_PrintShippingLbl = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_PrintEnvelope = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_Email = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.ts_Find = New System.Windows.Forms.ToolStripButton
        Me.ts_Spelling = New System.Windows.Forms.ToolStripButton
        Me.ts_History = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton7 = New System.Windows.Forms.ToolStripSplitButton
        Me.ts_Invoice = New System.Windows.Forms.ToolStripMenuItem
        Me.ts_PurchaseOrder = New System.Windows.Forms.ToolStripMenuItem
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboCustomerID = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtBillingAdd = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.dteCreditMemo = New System.Windows.Forms.DateTimePicker
        Me.txtCreditNo = New System.Windows.Forms.TextBox
        Me.grdCreditMemo = New System.Windows.Forms.DataGridView
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtCustomerMsg = New System.Windows.Forms.TextBox
        Me.chkPrint = New System.Windows.Forms.CheckBox
        Me.chkEmail = New System.Windows.Forms.CheckBox
        Me.btnPreview = New System.Windows.Forms.Button
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtMemo = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.cboTaxCodeID = New System.Windows.Forms.ComboBox
        Me.lblTotal = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.lblTaxAmt = New System.Windows.Forms.Label
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.cboTaxID = New System.Windows.Forms.ComboBox
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtPONo = New System.Windows.Forms.TextBox
        Me.cboCustomerName = New System.Windows.Forms.ComboBox
        Me.cboTaxCodeName = New System.Windows.Forms.ComboBox
        Me.cboTaxName = New System.Windows.Forms.ComboBox
        Me.btnAccountability = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.lblRefund = New System.Windows.Forms.PictureBox
        Me.lblBalance1 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.lblRetDiscount = New System.Windows.Forms.Label
        Me.lblRDPercent = New System.Windows.Forms.Label
        Me.txtRDAmt = New System.Windows.Forms.TextBox
        Me.Label27 = New System.Windows.Forms.Label
        Me.cboRD = New System.Windows.Forms.ComboBox
        Me.lblSaleDiscount = New System.Windows.Forms.Label
        Me.lblSDpercent = New System.Windows.Forms.Label
        Me.txtSDAmt = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.cboSD = New System.Windows.Forms.ComboBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.lblBalance = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.BtnBrowse = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.TxtClassName = New System.Windows.Forms.TextBox
        Me.txtClassRefno = New System.Windows.Forms.TextBox
        Me.LblSDAcntAmt = New System.Windows.Forms.Label
        Me.ToolStrip1.SuspendLayout()
        CType(Me.grdCreditMemo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblRefund, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.Color.White
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_Previous, Me.ts_Next, Me.ToolStripSplitButton1, Me.ts_Email, Me.ToolStripSeparator1, Me.ts_Find, Me.ts_Spelling, Me.ts_History, Me.ToolStripButton7})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(870, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_Previous
        '
        Me.ts_Previous.Image = CType(resources.GetObject("ts_Previous.Image"), System.Drawing.Image)
        Me.ts_Previous.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Previous.Name = "ts_Previous"
        Me.ts_Previous.Size = New System.Drawing.Size(68, 22)
        Me.ts_Previous.Text = "Previous"
        '
        'ts_Next
        '
        Me.ts_Next.Image = CType(resources.GetObject("ts_Next.Image"), System.Drawing.Image)
        Me.ts_Next.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Next.Name = "ts_Next"
        Me.ts_Next.Size = New System.Drawing.Size(50, 22)
        Me.ts_Next.Text = "Next"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripSplitButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_Preview, Me.ts_Print, Me.ts_PrintShippingLbl, Me.ts_PrintEnvelope})
        Me.ToolStripSplitButton1.Image = CType(resources.GetObject("ToolStripSplitButton1.Image"), System.Drawing.Image)
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(32, 22)
        Me.ToolStripSplitButton1.Text = "lope"
        '
        'ts_Preview
        '
        Me.ts_Preview.Name = "ts_Preview"
        Me.ts_Preview.Size = New System.Drawing.Size(178, 22)
        Me.ts_Preview.Text = "Preview"
        '
        'ts_Print
        '
        Me.ts_Print.Name = "ts_Print"
        Me.ts_Print.Size = New System.Drawing.Size(178, 22)
        Me.ts_Print.Text = "Print"
        '
        'ts_PrintShippingLbl
        '
        Me.ts_PrintShippingLbl.Name = "ts_PrintShippingLbl"
        Me.ts_PrintShippingLbl.Size = New System.Drawing.Size(178, 22)
        Me.ts_PrintShippingLbl.Text = "Print Shipping Label"
        '
        'ts_PrintEnvelope
        '
        Me.ts_PrintEnvelope.Name = "ts_PrintEnvelope"
        Me.ts_PrintEnvelope.Size = New System.Drawing.Size(178, 22)
        Me.ts_PrintEnvelope.Text = "Print Envelope"
        '
        'ts_Email
        '
        Me.ts_Email.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ts_Email.Image = CType(resources.GetObject("ts_Email.Image"), System.Drawing.Image)
        Me.ts_Email.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Email.Name = "ts_Email"
        Me.ts_Email.Size = New System.Drawing.Size(23, 22)
        Me.ts_Email.Text = "ToolStripButton3"
        Me.ts_Email.ToolTipText = "Emai"
        Me.ts_Email.Visible = False
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        Me.ToolStripSeparator1.Visible = False
        '
        'ts_Find
        '
        Me.ts_Find.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ts_Find.Image = CType(resources.GetObject("ts_Find.Image"), System.Drawing.Image)
        Me.ts_Find.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Find.Name = "ts_Find"
        Me.ts_Find.Size = New System.Drawing.Size(23, 22)
        Me.ts_Find.Text = "ToolStripButton4"
        Me.ts_Find.Visible = False
        '
        'ts_Spelling
        '
        Me.ts_Spelling.Image = CType(resources.GetObject("ts_Spelling.Image"), System.Drawing.Image)
        Me.ts_Spelling.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_Spelling.Name = "ts_Spelling"
        Me.ts_Spelling.Size = New System.Drawing.Size(63, 22)
        Me.ts_Spelling.Text = "Spelling"
        Me.ts_Spelling.Visible = False
        '
        'ts_History
        '
        Me.ts_History.Image = CType(resources.GetObject("ts_History.Image"), System.Drawing.Image)
        Me.ts_History.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_History.Name = "ts_History"
        Me.ts_History.Size = New System.Drawing.Size(61, 22)
        Me.ts_History.Text = "History"
        Me.ts_History.Visible = False
        '
        'ToolStripButton7
        '
        Me.ToolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton7.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_Invoice, Me.ts_PurchaseOrder})
        Me.ToolStripButton7.Image = CType(resources.GetObject("ToolStripButton7.Image"), System.Drawing.Image)
        Me.ToolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton7.Name = "ToolStripButton7"
        Me.ToolStripButton7.Size = New System.Drawing.Size(32, 22)
        Me.ToolStripButton7.Text = "Journal"
        Me.ToolStripButton7.Visible = False
        '
        'ts_Invoice
        '
        Me.ts_Invoice.Name = "ts_Invoice"
        Me.ts_Invoice.Size = New System.Drawing.Size(160, 22)
        Me.ts_Invoice.Text = "Invoice"
        '
        'ts_PurchaseOrder
        '
        Me.ts_PurchaseOrder.Name = "ts_PurchaseOrder"
        Me.ts_PurchaseOrder.Size = New System.Drawing.Size(160, 22)
        Me.ts_PurchaseOrder.Text = "Purchase Order"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Customer:Job"
        '
        'cboCustomerID
        '
        Me.cboCustomerID.FormattingEnabled = True
        Me.cboCustomerID.Location = New System.Drawing.Point(9, 51)
        Me.cboCustomerID.Name = "cboCustomerID"
        Me.cboCustomerID.Size = New System.Drawing.Size(174, 21)
        Me.cboCustomerID.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 74)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(132, 23)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Credit Memo"
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.BackColor = System.Drawing.Color.DarkBlue
        Me.Label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label4.Location = New System.Drawing.Point(8, 97)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(322, 25)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Customer Address"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBillingAdd
        '
        Me.txtBillingAdd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBillingAdd.Location = New System.Drawing.Point(8, 122)
        Me.txtBillingAdd.Multiline = True
        Me.txtBillingAdd.Name = "txtBillingAdd"
        Me.txtBillingAdd.Size = New System.Drawing.Size(321, 52)
        Me.txtBillingAdd.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.BackColor = System.Drawing.Color.DarkBlue
        Me.Label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label5.Location = New System.Drawing.Point(583, 101)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(129, 15)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Date"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.BackColor = System.Drawing.Color.DarkBlue
        Me.Label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label6.Location = New System.Drawing.Point(718, 101)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(135, 15)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Credit Memo No."
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'dteCreditMemo
        '
        Me.dteCreditMemo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dteCreditMemo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteCreditMemo.Location = New System.Drawing.Point(583, 115)
        Me.dteCreditMemo.Name = "dteCreditMemo"
        Me.dteCreditMemo.Size = New System.Drawing.Size(129, 21)
        Me.dteCreditMemo.TabIndex = 10
        '
        'txtCreditNo
        '
        Me.txtCreditNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCreditNo.Location = New System.Drawing.Point(718, 115)
        Me.txtCreditNo.Name = "txtCreditNo"
        Me.txtCreditNo.Size = New System.Drawing.Size(135, 21)
        Me.txtCreditNo.TabIndex = 11
        '
        'grdCreditMemo
        '
        Me.grdCreditMemo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdCreditMemo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdCreditMemo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6})
        Me.grdCreditMemo.Location = New System.Drawing.Point(10, 180)
        Me.grdCreditMemo.Name = "grdCreditMemo"
        Me.grdCreditMemo.Size = New System.Drawing.Size(848, 167)
        Me.grdCreditMemo.TabIndex = 15
        '
        'Column1
        '
        Me.Column1.HeaderText = "Item"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 200
        '
        'Column2
        '
        Me.Column2.HeaderText = "Decription"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.HeaderText = "Qty"
        Me.Column3.Name = "Column3"
        '
        'Column4
        '
        Me.Column4.HeaderText = "Rate"
        Me.Column4.Name = "Column4"
        '
        'Column5
        '
        Me.Column5.HeaderText = "Amount"
        Me.Column5.Name = "Column5"
        '
        'Column6
        '
        Me.Column6.HeaderText = "Tax"
        Me.Column6.Name = "Column6"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.Location = New System.Drawing.Point(9, 380)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(70, 34)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Customer Message"
        '
        'txtCustomerMsg
        '
        Me.txtCustomerMsg.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtCustomerMsg.Location = New System.Drawing.Point(82, 380)
        Me.txtCustomerMsg.Multiline = True
        Me.txtCustomerMsg.Name = "txtCustomerMsg"
        Me.txtCustomerMsg.Size = New System.Drawing.Size(325, 55)
        Me.txtCustomerMsg.TabIndex = 17
        '
        'chkPrint
        '
        Me.chkPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkPrint.AutoSize = True
        Me.chkPrint.Location = New System.Drawing.Point(12, 514)
        Me.chkPrint.Name = "chkPrint"
        Me.chkPrint.Size = New System.Drawing.Size(102, 17)
        Me.chkPrint.TabIndex = 20
        Me.chkPrint.Text = "To be printed"
        Me.chkPrint.UseVisualStyleBackColor = True
        Me.chkPrint.Visible = False
        '
        'chkEmail
        '
        Me.chkEmail.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkEmail.AutoSize = True
        Me.chkEmail.Location = New System.Drawing.Point(120, 514)
        Me.chkEmail.Name = "chkEmail"
        Me.chkEmail.Size = New System.Drawing.Size(107, 17)
        Me.chkEmail.TabIndex = 21
        Me.chkEmail.Text = "To be emailed"
        Me.chkEmail.UseVisualStyleBackColor = True
        Me.chkEmail.Visible = False
        '
        'btnPreview
        '
        Me.btnPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPreview.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.Location = New System.Drawing.Point(6, 560)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(87, 28)
        Me.btnPreview.TabIndex = 24
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label13.Location = New System.Drawing.Point(9, 444)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(59, 18)
        Me.Label13.TabIndex = 31
        Me.Label13.Text = "Memo"
        '
        'txtMemo
        '
        Me.txtMemo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtMemo.Location = New System.Drawing.Point(82, 444)
        Me.txtMemo.Multiline = True
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.Size = New System.Drawing.Size(325, 61)
        Me.txtMemo.TabIndex = 32
        '
        'Label14
        '
        Me.Label14.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(580, 54)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(122, 13)
        Me.Label14.TabIndex = 34
        Me.Label14.Text = "Customer Tax Code"
        '
        'cboTaxCodeID
        '
        Me.cboTaxCodeID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboTaxCodeID.FormattingEnabled = True
        Me.cboTaxCodeID.Location = New System.Drawing.Point(766, 51)
        Me.cboTaxCodeID.Name = "cboTaxCodeID"
        Me.cboTaxCodeID.Size = New System.Drawing.Size(87, 21)
        Me.cboTaxCodeID.TabIndex = 35
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(742, 378)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(112, 14)
        Me.lblTotal.TabIndex = 56
        Me.lblTotal.Text = "0.00"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label22
        '
        Me.Label22.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label22.Location = New System.Drawing.Point(591, 382)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(44, 18)
        Me.Label22.TabIndex = 55
        Me.Label22.Text = "Total"
        '
        'lblTaxAmt
        '
        Me.lblTaxAmt.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTaxAmt.Location = New System.Drawing.Point(734, 356)
        Me.lblTaxAmt.Name = "lblTaxAmt"
        Me.lblTaxAmt.Size = New System.Drawing.Size(120, 13)
        Me.lblTaxAmt.TabIndex = 54
        Me.lblTaxAmt.Text = "0.00"
        Me.lblTaxAmt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblPercentage
        '
        Me.lblPercentage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPercentage.AutoSize = True
        Me.lblPercentage.Location = New System.Drawing.Point(637, 357)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(25, 13)
        Me.lblPercentage.TabIndex = 53
        Me.lblPercentage.Text = "0.0"
        '
        'cboTaxID
        '
        Me.cboTaxID.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboTaxID.FormattingEnabled = True
        Me.cboTaxID.Location = New System.Drawing.Point(538, 353)
        Me.cboTaxID.Name = "cboTaxID"
        Me.cboTaxID.Size = New System.Drawing.Size(95, 21)
        Me.cboTaxID.TabIndex = 52
        '
        'Label25
        '
        Me.Label25.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label25.Location = New System.Drawing.Point(498, 354)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(59, 18)
        Me.Label25.TabIndex = 51
        Me.Label25.Text = "Tax"
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.BackColor = System.Drawing.Color.DarkBlue
        Me.Label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label7.Location = New System.Drawing.Point(718, 139)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(135, 15)
        Me.Label7.TabIndex = 57
        Me.Label7.Text = "Ref No."
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtPONo
        '
        Me.txtPONo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPONo.Location = New System.Drawing.Point(718, 153)
        Me.txtPONo.Name = "txtPONo"
        Me.txtPONo.Size = New System.Drawing.Size(135, 21)
        Me.txtPONo.TabIndex = 58
        '
        'cboCustomerName
        '
        Me.cboCustomerName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboCustomerName.FormattingEnabled = True
        Me.cboCustomerName.Location = New System.Drawing.Point(9, 51)
        Me.cboCustomerName.Name = "cboCustomerName"
        Me.cboCustomerName.Size = New System.Drawing.Size(322, 21)
        Me.cboCustomerName.TabIndex = 59
        '
        'cboTaxCodeName
        '
        Me.cboTaxCodeName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboTaxCodeName.FormattingEnabled = True
        Me.cboTaxCodeName.Location = New System.Drawing.Point(718, 51)
        Me.cboTaxCodeName.Name = "cboTaxCodeName"
        Me.cboTaxCodeName.Size = New System.Drawing.Size(135, 21)
        Me.cboTaxCodeName.TabIndex = 60
        '
        'cboTaxName
        '
        Me.cboTaxName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboTaxName.FormattingEnabled = True
        Me.cboTaxName.Location = New System.Drawing.Point(536, 353)
        Me.cboTaxName.Name = "cboTaxName"
        Me.cboTaxName.Size = New System.Drawing.Size(99, 21)
        Me.cboTaxName.TabIndex = 61
        '
        'btnAccountability
        '
        Me.btnAccountability.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAccountability.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccountability.Image = Global.CSAcctg.My.Resources.Resources.post
        Me.btnAccountability.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAccountability.Location = New System.Drawing.Point(99, 560)
        Me.btnAccountability.Name = "btnAccountability"
        Me.btnAccountability.Size = New System.Drawing.Size(128, 28)
        Me.btnAccountability.TabIndex = 64
        Me.btnAccountability.Text = "Accountability"
        Me.btnAccountability.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAccountability.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(775, 560)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 28)
        Me.btnClose.TabIndex = 63
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.Location = New System.Drawing.Point(682, 560)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 28)
        Me.btnSave.TabIndex = 62
        Me.btnSave.Text = "S&ave "
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lblRefund
        '
        Me.lblRefund.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRefund.Image = CType(resources.GetObject("lblRefund.Image"), System.Drawing.Image)
        Me.lblRefund.Location = New System.Drawing.Point(341, 78)
        Me.lblRefund.Name = "lblRefund"
        Me.lblRefund.Size = New System.Drawing.Size(236, 96)
        Me.lblRefund.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.lblRefund.TabIndex = 65
        Me.lblRefund.TabStop = False
        Me.lblRefund.Visible = False
        '
        'lblBalance1
        '
        Me.lblBalance1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblBalance1.Location = New System.Drawing.Point(309, 521)
        Me.lblBalance1.Name = "lblBalance1"
        Me.lblBalance1.Size = New System.Drawing.Size(168, 13)
        Me.lblBalance1.TabIndex = 67
        Me.lblBalance1.Text = "0.00"
        Me.lblBalance1.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblBalance1.Visible = False
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label9.Location = New System.Drawing.Point(233, 518)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(94, 36)
        Me.Label9.TabIndex = 66
        Me.Label9.Text = "Remaining Credit"
        Me.Label9.Visible = False
        '
        'lblRetDiscount
        '
        Me.lblRetDiscount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRetDiscount.Location = New System.Drawing.Point(741, 458)
        Me.lblRetDiscount.Name = "lblRetDiscount"
        Me.lblRetDiscount.Size = New System.Drawing.Size(112, 13)
        Me.lblRetDiscount.TabIndex = 129
        Me.lblRetDiscount.Text = "0.00"
        Me.lblRetDiscount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblRDPercent
        '
        Me.lblRDPercent.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRDPercent.AutoSize = True
        Me.lblRDPercent.Location = New System.Drawing.Point(691, 458)
        Me.lblRDPercent.Name = "lblRDPercent"
        Me.lblRDPercent.Size = New System.Drawing.Size(25, 13)
        Me.lblRDPercent.TabIndex = 128
        Me.lblRDPercent.Text = "0.0"
        '
        'txtRDAmt
        '
        Me.txtRDAmt.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRDAmt.Location = New System.Drawing.Point(617, 484)
        Me.txtRDAmt.Name = "txtRDAmt"
        Me.txtRDAmt.Size = New System.Drawing.Size(95, 21)
        Me.txtRDAmt.TabIndex = 127
        Me.txtRDAmt.Text = "0.00"
        Me.txtRDAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRDAmt.Visible = False
        '
        'Label27
        '
        Me.Label27.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(479, 458)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(104, 13)
        Me.Label27.TabIndex = 126
        Me.Label27.Text = "Retailer Discount"
        '
        'cboRD
        '
        Me.cboRD.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboRD.FormattingEnabled = True
        Me.cboRD.Location = New System.Drawing.Point(583, 455)
        Me.cboRD.Name = "cboRD"
        Me.cboRD.Size = New System.Drawing.Size(105, 21)
        Me.cboRD.TabIndex = 125
        '
        'lblSaleDiscount
        '
        Me.lblSaleDiscount.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblSaleDiscount.Location = New System.Drawing.Point(731, 430)
        Me.lblSaleDiscount.Name = "lblSaleDiscount"
        Me.lblSaleDiscount.Size = New System.Drawing.Size(122, 14)
        Me.lblSaleDiscount.TabIndex = 124
        Me.lblSaleDiscount.Text = "0.00"
        Me.lblSaleDiscount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSDpercent
        '
        Me.lblSDpercent.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSDpercent.AutoSize = True
        Me.lblSDpercent.Location = New System.Drawing.Point(691, 431)
        Me.lblSDpercent.Name = "lblSDpercent"
        Me.lblSDpercent.Size = New System.Drawing.Size(25, 13)
        Me.lblSDpercent.TabIndex = 123
        Me.lblSDpercent.Text = "0.0"
        '
        'txtSDAmt
        '
        Me.txtSDAmt.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSDAmt.Location = New System.Drawing.Point(618, 400)
        Me.txtSDAmt.Name = "txtSDAmt"
        Me.txtSDAmt.Size = New System.Drawing.Size(95, 21)
        Me.txtSDAmt.TabIndex = 122
        Me.txtSDAmt.Text = "0.00"
        Me.txtSDAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSDAmt.Visible = False
        '
        'Label18
        '
        Me.Label18.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(479, 432)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(91, 13)
        Me.Label18.TabIndex = 121
        Me.Label18.Text = "Sales Discount"
        '
        'cboSD
        '
        Me.cboSD.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboSD.FormattingEnabled = True
        Me.cboSD.Location = New System.Drawing.Point(583, 427)
        Me.cboSD.Name = "cboSD"
        Me.cboSD.Size = New System.Drawing.Size(105, 21)
        Me.cboSD.TabIndex = 120
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlText
        Me.Panel1.Location = New System.Drawing.Point(717, 400)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(142, 2)
        Me.Panel1.TabIndex = 130
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.SystemColors.ControlText
        Me.Panel2.Location = New System.Drawing.Point(717, 537)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(142, 2)
        Me.Panel2.TabIndex = 132
        '
        'lblBalance
        '
        Me.lblBalance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblBalance.Location = New System.Drawing.Point(710, 521)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(143, 13)
        Me.lblBalance.TabIndex = 133
        Me.lblBalance.Text = "0.00"
        Me.lblBalance.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label10
        '
        Me.Label10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(637, 518)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(74, 13)
        Me.Label10.TabIndex = 131
        Me.Label10.Text = "Total Credit"
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(9, 349)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(158, 21)
        Me.Button1.TabIndex = 134
        Me.Button1.Text = "Delete Selected Rows"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'BtnBrowse
        '
        Me.BtnBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnBrowse.Enabled = False
        Me.BtnBrowse.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBrowse.Location = New System.Drawing.Point(519, 49)
        Me.BtnBrowse.Name = "BtnBrowse"
        Me.BtnBrowse.Size = New System.Drawing.Size(58, 23)
        Me.BtnBrowse.TabIndex = 135
        Me.BtnBrowse.Text = "Browse"
        Me.BtnBrowse.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(338, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 136
        Me.Label2.Text = "Class:"
        '
        'TxtClassName
        '
        Me.TxtClassName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TxtClassName.BackColor = System.Drawing.Color.White
        Me.TxtClassName.Enabled = False
        Me.TxtClassName.Location = New System.Drawing.Point(341, 51)
        Me.TxtClassName.Name = "TxtClassName"
        Me.TxtClassName.ReadOnly = True
        Me.TxtClassName.Size = New System.Drawing.Size(172, 21)
        Me.TxtClassName.TabIndex = 137
        '
        'txtClassRefno
        '
        Me.txtClassRefno.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtClassRefno.Enabled = False
        Me.txtClassRefno.Location = New System.Drawing.Point(341, 74)
        Me.txtClassRefno.Name = "txtClassRefno"
        Me.txtClassRefno.Size = New System.Drawing.Size(172, 21)
        Me.txtClassRefno.TabIndex = 137
        Me.txtClassRefno.Visible = False
        '
        'LblSDAcntAmt
        '
        Me.LblSDAcntAmt.Location = New System.Drawing.Point(482, 403)
        Me.LblSDAcntAmt.Name = "LblSDAcntAmt"
        Me.LblSDAcntAmt.Size = New System.Drawing.Size(130, 13)
        Me.LblSDAcntAmt.TabIndex = 138
        Me.LblSDAcntAmt.Visible = False
        '
        'frm_cust_CreateCreditMemos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(870, 618)
        Me.ControlBox = False
        Me.Controls.Add(Me.LblSDAcntAmt)
        Me.Controls.Add(Me.txtClassRefno)
        Me.Controls.Add(Me.TxtClassName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.BtnBrowse)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.lblBalance)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lblRetDiscount)
        Me.Controls.Add(Me.lblRDPercent)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.cboRD)
        Me.Controls.Add(Me.lblSaleDiscount)
        Me.Controls.Add(Me.lblSDpercent)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.cboSD)
        Me.Controls.Add(Me.lblBalance1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.lblRefund)
        Me.Controls.Add(Me.btnAccountability)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.cboTaxName)
        Me.Controls.Add(Me.cboTaxCodeName)
        Me.Controls.Add(Me.cboCustomerName)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtPONo)
        Me.Controls.Add(Me.txtMemo)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.lblTaxAmt)
        Me.Controls.Add(Me.lblPercentage)
        Me.Controls.Add(Me.cboTaxID)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.cboTaxCodeID)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.chkEmail)
        Me.Controls.Add(Me.chkPrint)
        Me.Controls.Add(Me.txtCustomerMsg)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.grdCreditMemo)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtCreditNo)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dteCreditMemo)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtBillingAdd)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cboCustomerID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.txtSDAmt)
        Me.Controls.Add(Me.txtRDAmt)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(878, 626)
        Me.Name = "frm_cust_CreateCreditMemos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Create Credit Memos/Refunds"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.grdCreditMemo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblRefund, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ts_Previous As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_Next As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSplitButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ts_Preview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Print As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_PrintShippingLbl As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_PrintEnvelope As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_Email As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ts_Find As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_Spelling As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_History As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboCustomerID As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtBillingAdd As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dteCreditMemo As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtCreditNo As System.Windows.Forms.TextBox
    Friend WithEvents grdCreditMemo As System.Windows.Forms.DataGridView
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtCustomerMsg As System.Windows.Forms.TextBox
    Friend WithEvents chkPrint As System.Windows.Forms.CheckBox
    Friend WithEvents chkEmail As System.Windows.Forms.CheckBox
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents ToolStripButton7 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ts_Invoice As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ts_PurchaseOrder As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cboTaxCodeID As System.Windows.Forms.ComboBox
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents lblTaxAmt As System.Windows.Forms.Label
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Friend WithEvents cboTaxID As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtPONo As System.Windows.Forms.TextBox
    Friend WithEvents cboCustomerName As System.Windows.Forms.ComboBox
    Friend WithEvents cboTaxCodeName As System.Windows.Forms.ComboBox
    Friend WithEvents cboTaxName As System.Windows.Forms.ComboBox
    Friend WithEvents btnAccountability As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblRefund As System.Windows.Forms.PictureBox
    Friend WithEvents lblBalance1 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblRetDiscount As System.Windows.Forms.Label
    Friend WithEvents lblRDPercent As System.Windows.Forms.Label
    Friend WithEvents txtRDAmt As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents cboRD As System.Windows.Forms.ComboBox
    Friend WithEvents lblSaleDiscount As System.Windows.Forms.Label
    Friend WithEvents lblSDpercent As System.Windows.Forms.Label
    Friend WithEvents txtSDAmt As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents cboSD As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BtnBrowse As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TxtClassName As System.Windows.Forms.TextBox
    Friend WithEvents txtClassRefno As System.Windows.Forms.TextBox
    Friend WithEvents LblSDAcntAmt As System.Windows.Forms.Label
End Class
