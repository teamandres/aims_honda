<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_cust_CreateSalesReceipt
    Inherits System.Windows.Forms.Form


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_cust_CreateSalesReceipt))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboCustomerName = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtSoldTo = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.dteSale = New System.Windows.Forms.DateTimePicker
        Me.txtSaleNo = New System.Windows.Forms.TextBox
        Me.grdSalesRcpt = New System.Windows.Forms.DataGridView
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtCustomerMsg = New System.Windows.Forms.TextBox
        Me.chkPrint = New System.Windows.Forms.CheckBox
        Me.chkEmail = New System.Windows.Forms.CheckBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtCheckNo = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.cboPaymentMethodName = New System.Windows.Forms.ComboBox
        Me.txtMemo = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.cboTaxName = New System.Windows.Forms.ComboBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.cboTaxCodeName = New System.Windows.Forms.ComboBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.btnAddTimeCost = New System.Windows.Forms.Button
        Me.lblPercentage = New System.Windows.Forms.Label
        Me.lblTaxAmt = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.cboCustomerID = New System.Windows.Forms.ComboBox
        Me.cboTaxCodeID = New System.Windows.Forms.ComboBox
        Me.cboPaymentMethodID = New System.Windows.Forms.ComboBox
        Me.cboTaxID = New System.Windows.Forms.ComboBox
        Me.btnRefresh = New System.Windows.Forms.Button
        Me.lblRetDiscount = New System.Windows.Forms.Label
        Me.lblRDPercent = New System.Windows.Forms.Label
        Me.txtRDAmt = New System.Windows.Forms.TextBox
        Me.Label27 = New System.Windows.Forms.Label
        Me.cboRD = New System.Windows.Forms.ComboBox
        Me.lblSaleDiscount = New System.Windows.Forms.Label
        Me.lblSDpercent = New System.Windows.Forms.Label
        Me.txtSDAmt = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.cboSD = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblBalance = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label24 = New System.Windows.Forms.Label
        Me.txtPercent = New System.Windows.Forms.TextBox
        Me.cboTaxAcnt = New System.Windows.Forms.ComboBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.cboPayAcnt = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.lblSalesPeriod = New System.Windows.Forms.Label
        Me.dteSalesPeriodFROM = New System.Windows.Forms.DateTimePicker
        Me.dteSalesPeriodTo = New System.Windows.Forms.DateTimePicker
        Me.BtnBrowse = New System.Windows.Forms.Button
        Me.Label9 = New System.Windows.Forms.Label
        Me.TxtClassRefno = New System.Windows.Forms.TextBox
        Me.txtClassName = New System.Windows.Forms.TextBox
        Me.LblSalesDiscountAmount = New System.Windows.Forms.Label
        Me.btnAccountability = New System.Windows.Forms.Button
        Me.btnSelDel = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnPreview = New System.Windows.Forms.Button
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripSplitButton
        Me.tsPreview = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton5 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton7 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton8 = New System.Windows.Forms.ToolStripButton
        Me.ToolStrip1.SuspendLayout()
        CType(Me.grdSalesRcpt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.Color.White
        Me.ToolStrip1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1, Me.ToolStripButton2, Me.ToolStripSplitButton1, Me.ToolStripButton3, Me.ToolStripSeparator1, Me.ToolStripButton4, Me.ToolStripButton5, Me.ToolStripButton6, Me.ToolStripButton7, Me.ToolStripButton8})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(840, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        Me.ToolStripSeparator1.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Customer:Job"
        '
        'cboCustomerName
        '
        Me.cboCustomerName.FormattingEnabled = True
        Me.cboCustomerName.Location = New System.Drawing.Point(108, 33)
        Me.cboCustomerName.Name = "cboCustomerName"
        Me.cboCustomerName.Size = New System.Drawing.Size(222, 21)
        Me.cboCustomerName.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(138, 23)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Sales Receipt"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.DarkBlue
        Me.Label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label4.Location = New System.Drawing.Point(12, 81)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(320, 25)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Sold To"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSoldTo
        '
        Me.txtSoldTo.Location = New System.Drawing.Point(12, 105)
        Me.txtSoldTo.Multiline = True
        Me.txtSoldTo.Name = "txtSoldTo"
        Me.txtSoldTo.Size = New System.Drawing.Size(319, 74)
        Me.txtSoldTo.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.BackColor = System.Drawing.Color.DarkBlue
        Me.Label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label5.Location = New System.Drawing.Point(688, 56)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(141, 15)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Date"
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.BackColor = System.Drawing.Color.DarkBlue
        Me.Label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label6.Location = New System.Drawing.Point(557, 56)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(128, 15)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Sale No."
        '
        'dteSale
        '
        Me.dteSale.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dteSale.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteSale.Location = New System.Drawing.Point(688, 70)
        Me.dteSale.Name = "dteSale"
        Me.dteSale.Size = New System.Drawing.Size(140, 21)
        Me.dteSale.TabIndex = 10
        '
        'txtSaleNo
        '
        Me.txtSaleNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSaleNo.Location = New System.Drawing.Point(557, 70)
        Me.txtSaleNo.Name = "txtSaleNo"
        Me.txtSaleNo.Size = New System.Drawing.Size(128, 21)
        Me.txtSaleNo.TabIndex = 11
        '
        'grdSalesRcpt
        '
        Me.grdSalesRcpt.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdSalesRcpt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdSalesRcpt.Location = New System.Drawing.Point(12, 185)
        Me.grdSalesRcpt.Name = "grdSalesRcpt"
        Me.grdSalesRcpt.Size = New System.Drawing.Size(818, 123)
        Me.grdSalesRcpt.TabIndex = 15
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.Location = New System.Drawing.Point(13, 347)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(71, 34)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Customer &Message"
        '
        'txtCustomerMsg
        '
        Me.txtCustomerMsg.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtCustomerMsg.Location = New System.Drawing.Point(79, 343)
        Me.txtCustomerMsg.Multiline = True
        Me.txtCustomerMsg.Name = "txtCustomerMsg"
        Me.txtCustomerMsg.Size = New System.Drawing.Size(294, 41)
        Me.txtCustomerMsg.TabIndex = 17
        '
        'chkPrint
        '
        Me.chkPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkPrint.AutoSize = True
        Me.chkPrint.Location = New System.Drawing.Point(16, 428)
        Me.chkPrint.Name = "chkPrint"
        Me.chkPrint.Size = New System.Drawing.Size(102, 17)
        Me.chkPrint.TabIndex = 20
        Me.chkPrint.Text = "To be printed"
        Me.chkPrint.UseVisualStyleBackColor = True
        Me.chkPrint.Visible = False
        '
        'chkEmail
        '
        Me.chkEmail.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkEmail.AutoSize = True
        Me.chkEmail.Location = New System.Drawing.Point(15, 456)
        Me.chkEmail.Name = "chkEmail"
        Me.chkEmail.Size = New System.Drawing.Size(107, 17)
        Me.chkEmail.TabIndex = 21
        Me.chkEmail.Text = "To be emailed"
        Me.chkEmail.UseVisualStyleBackColor = True
        Me.chkEmail.Visible = False
        '
        'Label17
        '
        Me.Label17.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label17.BackColor = System.Drawing.Color.DarkBlue
        Me.Label17.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label17.Location = New System.Drawing.Point(557, 94)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(128, 19)
        Me.Label17.TabIndex = 18
        Me.Label17.Text = "Check No."
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCheckNo
        '
        Me.txtCheckNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCheckNo.Location = New System.Drawing.Point(557, 112)
        Me.txtCheckNo.Multiline = True
        Me.txtCheckNo.Name = "txtCheckNo"
        Me.txtCheckNo.Size = New System.Drawing.Size(128, 20)
        Me.txtCheckNo.TabIndex = 19
        '
        'Label16
        '
        Me.Label16.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.BackColor = System.Drawing.Color.DarkBlue
        Me.Label16.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label16.Location = New System.Drawing.Point(688, 94)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(141, 19)
        Me.Label16.TabIndex = 16
        Me.Label16.Text = "Payment Method"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPaymentMethodName
        '
        Me.cboPaymentMethodName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboPaymentMethodName.FormattingEnabled = True
        Me.cboPaymentMethodName.Location = New System.Drawing.Point(688, 111)
        Me.cboPaymentMethodName.Name = "cboPaymentMethodName"
        Me.cboPaymentMethodName.Size = New System.Drawing.Size(140, 21)
        Me.cboPaymentMethodName.TabIndex = 17
        '
        'txtMemo
        '
        Me.txtMemo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtMemo.Location = New System.Drawing.Point(79, 391)
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.Size = New System.Drawing.Size(294, 21)
        Me.txtMemo.TabIndex = 36
        '
        'Label11
        '
        Me.Label11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label11.Location = New System.Drawing.Point(13, 394)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(59, 18)
        Me.Label11.TabIndex = 35
        Me.Label11.Text = "Memo"
        '
        'cboTaxName
        '
        Me.cboTaxName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboTaxName.FormattingEnabled = True
        Me.cboTaxName.Location = New System.Drawing.Point(463, 343)
        Me.cboTaxName.Name = "cboTaxName"
        Me.cboTaxName.Size = New System.Drawing.Size(79, 21)
        Me.cboTaxName.TabIndex = 34
        '
        'Label20
        '
        Me.Label20.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label20.Location = New System.Drawing.Point(402, 345)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(59, 18)
        Me.Label20.TabIndex = 33
        Me.Label20.Text = "Tax"
        '
        'cboTaxCodeName
        '
        Me.cboTaxCodeName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboTaxCodeName.FormattingEnabled = True
        Me.cboTaxCodeName.Location = New System.Drawing.Point(740, 28)
        Me.cboTaxCodeName.Name = "cboTaxCodeName"
        Me.cboTaxCodeName.Size = New System.Drawing.Size(89, 21)
        Me.cboTaxCodeName.TabIndex = 38
        '
        'Label21
        '
        Me.Label21.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(614, 31)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(122, 13)
        Me.Label21.TabIndex = 37
        Me.Label21.Text = "Customer Tax Code"
        '
        'btnAddTimeCost
        '
        Me.btnAddTimeCost.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddTimeCost.Location = New System.Drawing.Point(128, 424)
        Me.btnAddTimeCost.Name = "btnAddTimeCost"
        Me.btnAddTimeCost.Size = New System.Drawing.Size(111, 23)
        Me.btnAddTimeCost.TabIndex = 39
        Me.btnAddTimeCost.Text = "Add Time Cost"
        Me.btnAddTimeCost.UseVisualStyleBackColor = True
        Me.btnAddTimeCost.Visible = False
        '
        'lblPercentage
        '
        Me.lblPercentage.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPercentage.AutoSize = True
        Me.lblPercentage.Location = New System.Drawing.Point(566, 346)
        Me.lblPercentage.Name = "lblPercentage"
        Me.lblPercentage.Size = New System.Drawing.Size(47, 13)
        Me.lblPercentage.TabIndex = 41
        Me.lblPercentage.Text = "(0.0%)"
        '
        'lblTaxAmt
        '
        Me.lblTaxAmt.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTaxAmt.Location = New System.Drawing.Point(667, 345)
        Me.lblTaxAmt.Name = "lblTaxAmt"
        Me.lblTaxAmt.Size = New System.Drawing.Size(164, 14)
        Me.lblTaxAmt.TabIndex = 42
        Me.lblTaxAmt.Text = "0.00"
        Me.lblTaxAmt.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Location = New System.Drawing.Point(661, 368)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(170, 13)
        Me.lblTotal.TabIndex = 44
        Me.lblTotal.Text = "0.00"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.Location = New System.Drawing.Point(566, 369)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(59, 18)
        Me.Label13.TabIndex = 43
        Me.Label13.Text = "Total"
        '
        'cboCustomerID
        '
        Me.cboCustomerID.FormattingEnabled = True
        Me.cboCustomerID.Location = New System.Drawing.Point(108, 33)
        Me.cboCustomerID.Name = "cboCustomerID"
        Me.cboCustomerID.Size = New System.Drawing.Size(212, 21)
        Me.cboCustomerID.TabIndex = 104
        '
        'cboTaxCodeID
        '
        Me.cboTaxCodeID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboTaxCodeID.FormattingEnabled = True
        Me.cboTaxCodeID.Location = New System.Drawing.Point(740, 28)
        Me.cboTaxCodeID.Name = "cboTaxCodeID"
        Me.cboTaxCodeID.Size = New System.Drawing.Size(89, 21)
        Me.cboTaxCodeID.TabIndex = 105
        '
        'cboPaymentMethodID
        '
        Me.cboPaymentMethodID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboPaymentMethodID.FormattingEnabled = True
        Me.cboPaymentMethodID.Location = New System.Drawing.Point(689, 111)
        Me.cboPaymentMethodID.Name = "cboPaymentMethodID"
        Me.cboPaymentMethodID.Size = New System.Drawing.Size(140, 21)
        Me.cboPaymentMethodID.TabIndex = 106
        '
        'cboTaxID
        '
        Me.cboTaxID.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboTaxID.FormattingEnabled = True
        Me.cboTaxID.Location = New System.Drawing.Point(463, 343)
        Me.cboTaxID.Name = "cboTaxID"
        Me.cboTaxID.Size = New System.Drawing.Size(79, 21)
        Me.cboTaxID.TabIndex = 107
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRefresh.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Location = New System.Drawing.Point(108, 568)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(87, 23)
        Me.btnRefresh.TabIndex = 108
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = True
        Me.btnRefresh.Visible = False
        '
        'lblRetDiscount
        '
        Me.lblRetDiscount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRetDiscount.Location = New System.Drawing.Point(688, 453)
        Me.lblRetDiscount.Name = "lblRetDiscount"
        Me.lblRetDiscount.Size = New System.Drawing.Size(143, 13)
        Me.lblRetDiscount.TabIndex = 119
        Me.lblRetDiscount.Text = "0.00"
        Me.lblRetDiscount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblRDPercent
        '
        Me.lblRDPercent.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRDPercent.AutoSize = True
        Me.lblRDPercent.Location = New System.Drawing.Point(514, 453)
        Me.lblRDPercent.Name = "lblRDPercent"
        Me.lblRDPercent.Size = New System.Drawing.Size(47, 13)
        Me.lblRDPercent.TabIndex = 118
        Me.lblRDPercent.Text = "(0.0%)"
        '
        'txtRDAmt
        '
        Me.txtRDAmt.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRDAmt.Location = New System.Drawing.Point(569, 449)
        Me.txtRDAmt.Name = "txtRDAmt"
        Me.txtRDAmt.Size = New System.Drawing.Size(95, 21)
        Me.txtRDAmt.TabIndex = 117
        Me.txtRDAmt.Text = "0.00"
        Me.txtRDAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label27
        '
        Me.Label27.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(302, 452)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(104, 13)
        Me.Label27.TabIndex = 116
        Me.Label27.Text = "Retailer Discount"
        '
        'cboRD
        '
        Me.cboRD.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboRD.FormattingEnabled = True
        Me.cboRD.Location = New System.Drawing.Point(406, 449)
        Me.cboRD.Name = "cboRD"
        Me.cboRD.Size = New System.Drawing.Size(105, 21)
        Me.cboRD.TabIndex = 115
        '
        'lblSaleDiscount
        '
        Me.lblSaleDiscount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSaleDiscount.Location = New System.Drawing.Point(688, 429)
        Me.lblSaleDiscount.Name = "lblSaleDiscount"
        Me.lblSaleDiscount.Size = New System.Drawing.Size(143, 13)
        Me.lblSaleDiscount.TabIndex = 114
        Me.lblSaleDiscount.Text = "0.00"
        Me.lblSaleDiscount.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'lblSDpercent
        '
        Me.lblSDpercent.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSDpercent.AutoSize = True
        Me.lblSDpercent.Location = New System.Drawing.Point(514, 428)
        Me.lblSDpercent.Name = "lblSDpercent"
        Me.lblSDpercent.Size = New System.Drawing.Size(47, 13)
        Me.lblSDpercent.TabIndex = 113
        Me.lblSDpercent.Text = "(0.0%)"
        '
        'txtSDAmt
        '
        Me.txtSDAmt.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSDAmt.Location = New System.Drawing.Point(569, 424)
        Me.txtSDAmt.Name = "txtSDAmt"
        Me.txtSDAmt.Size = New System.Drawing.Size(95, 21)
        Me.txtSDAmt.TabIndex = 112
        Me.txtSDAmt.Text = "0.00"
        Me.txtSDAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(302, 429)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(91, 13)
        Me.Label18.TabIndex = 111
        Me.Label18.Text = "Sales Discount"
        '
        'cboSD
        '
        Me.cboSD.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboSD.FormattingEnabled = True
        Me.cboSD.Location = New System.Drawing.Point(406, 424)
        Me.cboSD.Name = "cboSD"
        Me.cboSD.Size = New System.Drawing.Size(105, 21)
        Me.cboSD.TabIndex = 110
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(566, 492)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 13)
        Me.Label2.TabIndex = 120
        Me.Label2.Text = "Balance Due"
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.SystemColors.ControlText
        Me.Panel1.Location = New System.Drawing.Point(687, 394)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(142, 2)
        Me.Panel1.TabIndex = 121
        '
        'lblBalance
        '
        Me.lblBalance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblBalance.Location = New System.Drawing.Point(688, 492)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(143, 13)
        Me.lblBalance.TabIndex = 122
        Me.lblBalance.Text = "0.00"
        Me.lblBalance.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.SystemColors.ControlText
        Me.Panel2.Location = New System.Drawing.Point(687, 508)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(142, 2)
        Me.Panel2.TabIndex = 122
        '
        'Label24
        '
        Me.Label24.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(435, 490)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(19, 13)
        Me.Label24.TabIndex = 125
        Me.Label24.Text = "%"
        Me.Label24.Visible = False
        '
        'txtPercent
        '
        Me.txtPercent.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtPercent.Location = New System.Drawing.Point(393, 484)
        Me.txtPercent.Name = "txtPercent"
        Me.txtPercent.Size = New System.Drawing.Size(40, 21)
        Me.txtPercent.TabIndex = 126
        Me.txtPercent.Text = "0.00"
        Me.txtPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPercent.Visible = False
        '
        'cboTaxAcnt
        '
        Me.cboTaxAcnt.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cboTaxAcnt.FormattingEnabled = True
        Me.cboTaxAcnt.Location = New System.Drawing.Point(164, 484)
        Me.cboTaxAcnt.Name = "cboTaxAcnt"
        Me.cboTaxAcnt.Size = New System.Drawing.Size(209, 21)
        Me.cboTaxAcnt.TabIndex = 128
        Me.cboTaxAcnt.Visible = False
        '
        'Label23
        '
        Me.Label23.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(15, 492)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(116, 13)
        Me.Label23.TabIndex = 127
        Me.Label23.Text = "Tax Credit Account"
        Me.Label23.Visible = False
        '
        'cboPayAcnt
        '
        Me.cboPayAcnt.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cboPayAcnt.FormattingEnabled = True
        Me.cboPayAcnt.Location = New System.Drawing.Point(164, 524)
        Me.cboPayAcnt.Name = "cboPayAcnt"
        Me.cboPayAcnt.Size = New System.Drawing.Size(209, 21)
        Me.cboPayAcnt.TabIndex = 124
        Me.cboPayAcnt.Visible = False
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(13, 527)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(145, 13)
        Me.Label7.TabIndex = 123
        Me.Label7.Text = "Payment Credit Account"
        Me.Label7.Visible = False
        '
        'lblSalesPeriod
        '
        Me.lblSalesPeriod.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSalesPeriod.BackColor = System.Drawing.Color.Navy
        Me.lblSalesPeriod.ForeColor = System.Drawing.Color.White
        Me.lblSalesPeriod.Location = New System.Drawing.Point(557, 137)
        Me.lblSalesPeriod.Name = "lblSalesPeriod"
        Me.lblSalesPeriod.Size = New System.Drawing.Size(271, 18)
        Me.lblSalesPeriod.TabIndex = 130
        Me.lblSalesPeriod.Text = "Sales Period"
        Me.lblSalesPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dteSalesPeriodFROM
        '
        Me.dteSalesPeriodFROM.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dteSalesPeriodFROM.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteSalesPeriodFROM.Location = New System.Drawing.Point(557, 158)
        Me.dteSalesPeriodFROM.Name = "dteSalesPeriodFROM"
        Me.dteSalesPeriodFROM.Size = New System.Drawing.Size(128, 21)
        Me.dteSalesPeriodFROM.TabIndex = 131
        '
        'dteSalesPeriodTo
        '
        Me.dteSalesPeriodTo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dteSalesPeriodTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteSalesPeriodTo.Location = New System.Drawing.Point(688, 158)
        Me.dteSalesPeriodTo.Name = "dteSalesPeriodTo"
        Me.dteSalesPeriodTo.Size = New System.Drawing.Size(140, 21)
        Me.dteSalesPeriodTo.TabIndex = 132
        '
        'BtnBrowse
        '
        Me.BtnBrowse.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBrowse.Location = New System.Drawing.Point(480, 57)
        Me.BtnBrowse.Name = "BtnBrowse"
        Me.BtnBrowse.Size = New System.Drawing.Size(62, 23)
        Me.BtnBrowse.TabIndex = 133
        Me.BtnBrowse.Text = "Browse"
        Me.BtnBrowse.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(336, 36)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(43, 13)
        Me.Label9.TabIndex = 134
        Me.Label9.Text = "Class:"
        '
        'TxtClassRefno
        '
        Me.TxtClassRefno.Enabled = False
        Me.TxtClassRefno.Location = New System.Drawing.Point(375, 57)
        Me.TxtClassRefno.Name = "TxtClassRefno"
        Me.TxtClassRefno.Size = New System.Drawing.Size(115, 21)
        Me.TxtClassRefno.TabIndex = 135
        Me.TxtClassRefno.Visible = False
        '
        'txtClassName
        '
        Me.txtClassName.BackColor = System.Drawing.Color.White
        Me.txtClassName.Location = New System.Drawing.Point(375, 33)
        Me.txtClassName.Name = "txtClassName"
        Me.txtClassName.ReadOnly = True
        Me.txtClassName.Size = New System.Drawing.Size(167, 21)
        Me.txtClassName.TabIndex = 135
        '
        'LblSalesDiscountAmount
        '
        Me.LblSalesDiscountAmount.Location = New System.Drawing.Point(406, 405)
        Me.LblSalesDiscountAmount.Name = "LblSalesDiscountAmount"
        Me.LblSalesDiscountAmount.Size = New System.Drawing.Size(105, 16)
        Me.LblSalesDiscountAmount.TabIndex = 136
        Me.LblSalesDiscountAmount.Visible = False
        '
        'btnAccountability
        '
        Me.btnAccountability.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAccountability.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAccountability.Image = Global.CSAcctg.My.Resources.Resources.post
        Me.btnAccountability.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAccountability.Location = New System.Drawing.Point(108, 568)
        Me.btnAccountability.Name = "btnAccountability"
        Me.btnAccountability.Size = New System.Drawing.Size(146, 28)
        Me.btnAccountability.TabIndex = 129
        Me.btnAccountability.Text = "Accountability"
        Me.btnAccountability.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAccountability.UseVisualStyleBackColor = True
        '
        'btnSelDel
        '
        Me.btnSelDel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSelDel.Image = Global.CSAcctg.My.Resources.Resources.delete
        Me.btnSelDel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSelDel.Location = New System.Drawing.Point(11, 311)
        Me.btnSelDel.Name = "btnSelDel"
        Me.btnSelDel.Size = New System.Drawing.Size(172, 28)
        Me.btnSelDel.TabIndex = 109
        Me.btnSelDel.Text = "Delete Selected Items"
        Me.btnSelDel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSelDel.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.CSAcctg.My.Resources.Resources.close
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(743, 568)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 29)
        Me.btnClose.TabIndex = 103
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Image = Global.CSAcctg.My.Resources.Resources.floppy
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.Location = New System.Drawing.Point(649, 568)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 29)
        Me.btnSave.TabIndex = 102
        Me.btnSave.Text = "S&ave "
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPreview.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPreview.Image = Global.CSAcctg.My.Resources.Resources.Print_Preview
        Me.btnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPreview.Location = New System.Drawing.Point(18, 568)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(87, 28)
        Me.btnPreview.TabIndex = 24
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(84, 22)
        Me.ToolStripButton1.Text = "Previous"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(57, 22)
        Me.ToolStripButton2.Text = "Next"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripSplitButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsPreview})
        Me.ToolStripSplitButton1.Image = CType(resources.GetObject("ToolStripSplitButton1.Image"), System.Drawing.Image)
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(32, 22)
        Me.ToolStripSplitButton1.Text = "lope"
        '
        'tsPreview
        '
        Me.tsPreview.Name = "tsPreview"
        Me.tsPreview.Size = New System.Drawing.Size(137, 22)
        Me.tsPreview.Text = "Preview"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton3.Text = "ToolStripButton3"
        Me.ToolStripButton3.ToolTipText = "Emai"
        Me.ToolStripButton3.Visible = False
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton4.Text = "ToolStripButton4"
        Me.ToolStripButton4.Visible = False
        '
        'ToolStripButton5
        '
        Me.ToolStripButton5.Image = CType(resources.GetObject("ToolStripButton5.Image"), System.Drawing.Image)
        Me.ToolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton5.Name = "ToolStripButton5"
        Me.ToolStripButton5.Size = New System.Drawing.Size(79, 22)
        Me.ToolStripButton5.Text = "Spelling"
        Me.ToolStripButton5.Visible = False
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(74, 22)
        Me.ToolStripButton6.Text = "History"
        Me.ToolStripButton6.Visible = False
        '
        'ToolStripButton7
        '
        Me.ToolStripButton7.Image = CType(resources.GetObject("ToolStripButton7.Image"), System.Drawing.Image)
        Me.ToolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton7.Name = "ToolStripButton7"
        Me.ToolStripButton7.Size = New System.Drawing.Size(75, 22)
        Me.ToolStripButton7.Text = "Journal"
        Me.ToolStripButton7.Visible = False
        '
        'ToolStripButton8
        '
        Me.ToolStripButton8.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton8.Image = CType(resources.GetObject("ToolStripButton8.Image"), System.Drawing.Image)
        Me.ToolStripButton8.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton8.Name = "ToolStripButton8"
        Me.ToolStripButton8.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton8.Text = "ToolStripButton8"
        Me.ToolStripButton8.Visible = False
        '
        'frm_cust_CreateSalesReceipt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.ClientSize = New System.Drawing.Size(840, 599)
        Me.ControlBox = False
        Me.Controls.Add(Me.LblSalesDiscountAmount)
        Me.Controls.Add(Me.txtClassName)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.BtnBrowse)
        Me.Controls.Add(Me.dteSalesPeriodTo)
        Me.Controls.Add(Me.dteSalesPeriodFROM)
        Me.Controls.Add(Me.lblSalesPeriod)
        Me.Controls.Add(Me.btnAccountability)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.txtPercent)
        Me.Controls.Add(Me.cboTaxAcnt)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.cboPayAcnt)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.lblBalance)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblRetDiscount)
        Me.Controls.Add(Me.lblRDPercent)
        Me.Controls.Add(Me.txtRDAmt)
        Me.Controls.Add(Me.Label27)
        Me.Controls.Add(Me.cboRD)
        Me.Controls.Add(Me.lblSaleDiscount)
        Me.Controls.Add(Me.lblSDpercent)
        Me.Controls.Add(Me.txtSDAmt)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.cboSD)
        Me.Controls.Add(Me.btnSelDel)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.cboTaxName)
        Me.Controls.Add(Me.cboTaxID)
        Me.Controls.Add(Me.cboPaymentMethodName)
        Me.Controls.Add(Me.cboPaymentMethodID)
        Me.Controls.Add(Me.cboTaxCodeName)
        Me.Controls.Add(Me.cboTaxCodeID)
        Me.Controls.Add(Me.cboCustomerName)
        Me.Controls.Add(Me.cboCustomerID)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.lblPercentage)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.lblTaxAmt)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.txtCheckNo)
        Me.Controls.Add(Me.btnAddTimeCost)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.txtMemo)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.chkEmail)
        Me.Controls.Add(Me.chkPrint)
        Me.Controls.Add(Me.txtCustomerMsg)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.grdSalesRcpt)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtSaleNo)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dteSale)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtSoldTo)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.TxtClassRefno)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frm_cust_CreateSalesReceipt"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Enter Sales Receipts"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.grdSalesRcpt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSplitButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton5 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton7 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboCustomerName As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtSoldTo As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dteSale As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtSaleNo As System.Windows.Forms.TextBox
    Friend WithEvents grdSalesRcpt As System.Windows.Forms.DataGridView
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtCustomerMsg As System.Windows.Forms.TextBox
    Friend WithEvents chkPrint As System.Windows.Forms.CheckBox
    Friend WithEvents chkEmail As System.Windows.Forms.CheckBox
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtCheckNo As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cboPaymentMethodName As System.Windows.Forms.ComboBox
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cboTaxName As System.Windows.Forms.ComboBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents cboTaxCodeName As System.Windows.Forms.ComboBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents btnAddTimeCost As System.Windows.Forms.Button
    Friend WithEvents lblPercentage As System.Windows.Forms.Label
    Friend WithEvents lblTaxAmt As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents cboCustomerID As System.Windows.Forms.ComboBox
    Friend WithEvents cboTaxCodeID As System.Windows.Forms.ComboBox
    Friend WithEvents cboPaymentMethodID As System.Windows.Forms.ComboBox
    Friend WithEvents cboTaxID As System.Windows.Forms.ComboBox
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents btnSelDel As System.Windows.Forms.Button
    Friend WithEvents lblRetDiscount As System.Windows.Forms.Label
    Friend WithEvents lblRDPercent As System.Windows.Forms.Label
    Friend WithEvents txtRDAmt As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents cboRD As System.Windows.Forms.ComboBox
    Friend WithEvents lblSaleDiscount As System.Windows.Forms.Label
    Friend WithEvents lblSDpercent As System.Windows.Forms.Label
    Friend WithEvents txtSDAmt As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents cboSD As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents tsPreview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripButton8 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtPercent As System.Windows.Forms.TextBox
    Friend WithEvents cboTaxAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents cboPayAcnt As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnAccountability As System.Windows.Forms.Button
    Friend WithEvents lblSalesPeriod As System.Windows.Forms.Label
    Friend WithEvents dteSalesPeriodFROM As System.Windows.Forms.DateTimePicker
    Friend WithEvents dteSalesPeriodTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents BtnBrowse As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TxtClassRefno As System.Windows.Forms.TextBox
    Friend WithEvents txtClassName As System.Windows.Forms.TextBox
    Friend WithEvents LblSalesDiscountAmount As System.Windows.Forms.Label
End Class
