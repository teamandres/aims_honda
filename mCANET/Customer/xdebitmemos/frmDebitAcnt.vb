Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmDebitAcnt

    Private gcon As New Clsappconfiguration

    Private Sub frmDebitAcnt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call m_DisplayAccountsAll(cboPayAcnt)
        Call m_DisplayAccountsAll(cboTaxAcnt)
        Call m_DisplayAccountsAll(cboSalesAcnt)
        Call m_DisplayAccountsAll(cboOutputTax)
        Call m_DisplayAccountsAll(cboCOS)
        Call loadaccountabilitydetails()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If gKeyCredit = "" Then
            Call updateInvoiceAccountability()
            txtPercent.Text = "0.00"
            txtPer1.Text = "0.00"
            Me.Close()
        Else
            Call updateInvoiceAccountability()
            Call updateinvoice()
        End If
    End Sub

    Private Sub loadaccountabilitydetails()
        If gKeyCredit <> "" Then
            Dim sSQLCmd As String = "usp_t_DebitAccountabilityLoad "
            sSQLCmd &= " @fxKeyDebit='" & gKeyDebit & "' "
            sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "
            Try

                Using rd = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                    While rd.Read
                        cboPayAcnt.SelectedItem = rd.Item(0).ToString
                        cboTaxAcnt.SelectedItem = rd.Item(1).ToString
                        cboSalesAcnt.SelectedItem = rd.Item(2).ToString
                        cboOutputTax.SelectedItem = rd.Item(3).ToString
                        txtPercent.Text = rd.Item(4)
                        txtPer1.Text = rd.Item(5)
                        cboCOS.SelectedItem = rd.Item(6).ToString
                    End While
                End Using

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End If
    End Sub

    Private Sub updateInvoiceAccountability()
        Dim pxPayAccount As String = ""
        Dim pxTaxAccount As String = ""
        Dim pxSalesAccount As String = ""
        Dim pxOutTaxAcnt As String = ""
        Dim pxCOSAcnt As String = ""
        If cboSalesAcnt.SelectedItem <> " " Then
            pxSalesAccount = getAccountID(cboSalesAcnt.SelectedItem)
        End If
        If cboPayAcnt.SelectedItem <> " " Then
            pxPayAccount = getAccountID(cboPayAcnt.SelectedItem)
        End If
        If cboTaxAcnt.SelectedItem <> " " Then
            pxTaxAccount = getAccountID(cboTaxAcnt.SelectedItem)
        End If
        If cboOutputTax.SelectedItem <> " " Then
            pxOutTaxAcnt = getAccountID(cboOutputTax.SelectedItem)
        End If
        If cboCOS.SelectedItem <> " " Then
            pxCOSAcnt = getAccountID(cboCOS.SelectedItem)
        End If
        gKeyPayAccount = pxPayAccount
        gKeyTaxAccount = pxTaxAccount
        gKeySalesAccount = pxSalesAccount
        gKeyCOSAccount = pxCOSAcnt
        gTaxPercent = CDec(txtPercent.Text)
        gKeyOutputTax = pxOutTaxAcnt
        gOutputTaxPercent = CDec(txtPer1.Text)
    End Sub

    Private Sub updateinvoice()
        Try
            Dim sSQLCmd As String = "usp_t_DebitAccountability_update "
            sSQLCmd &= " @fxKeyPayAcnt=" & IIf(gKeyPayAccount = "", "NULL", "'" & gKeyPayAccount & "'") & " "
            sSQLCmd &= ",@fxKeyTaxAcnt=" & IIf(gKeyTaxAccount = "", "NULL", "'" & gKeyTaxAccount & "'") & " "
            sSQLCmd &= ",@fxKeySalesTax=" & IIf(gKeySalesAccount = "", "NULL", "'" & gKeySalesAccount & "'") & " "
            sSQLCmd &= ",@fxOutputTax=" & IIf(gKeyOutputTax = "", "NULL", "'" & gKeyOutputTax & "'") & " "
            sSQLCmd &= ",@fxKeyCOSAcnt=" & IIf(gKeyCOSAccount = "", "NULL", "'" & gKeyCOSAccount & "'") & " "
            sSQLCmd &= ",@fdOutputTaxPercent='" & gOutputTaxPercent & "' "
            sSQLCmd &= ",@fdTaxPercent='" & gTaxPercent & "' "
            sSQLCmd &= ",@fxKeyDebit=" & IIf(gKeyDebit = "", "NULL", "'" & gKeyDebit & "'") & " "
            sSQLCmd &= ",@fxKeyCompany=" & IIf(gCompanyID() = "", "NULL", "'" & gCompanyID() & "'") & " "

            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQLCmd)
            'Me.Close()
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Information, Me.Text)
            Me.Close()
        End Try
        Me.Close()
    End Sub

End Class