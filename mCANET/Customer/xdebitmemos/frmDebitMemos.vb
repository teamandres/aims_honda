Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmDebitMemos

    Private gcon As New Clsappconfiguration
    Private sKeyTax As String
    Private dPercentage As Decimal = 0
    Private ClassRefno As String = ""
    Private sKeyDebit As String
    Dim checkDate As New clsRestrictedDate

    Public Property KeyDebitMemo() As String
        Get
            Return sKeyDebit
        End Get
        Set(ByVal value As String)
            sKeyDebit = value
        End Set
    End Property

    Private Sub frmDebitMemos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call loadcombo()
        IsDateRestricted()
        If gKeyDebit = "" Then
            gKeyDebit = Guid.NewGuid.ToString
            txtDebitNo.Text = getjventryid()

        Else
            If frmMain.LblClassActivator.Visible = True Then
                Label2.Visible = True
                TxtClassName.Enabled = True
                TxtClassName.Visible = True
                BtnBrowse.Enabled = True
                BtnBrowse.Visible = True
            Else
                Label2.Visible = False
                TxtClassName.Enabled = False
                TxtClassName.Visible = False
                BtnBrowse.Enabled = False
                BtnBrowse.Visible = False
            End If
            Call load_details()

        End If
    End Sub

    Private Sub load_details()
        Call load_header()
        Call load_item()
    End Sub

    Private Function getjventryid() As String
        Dim stemp As String = ""
        Dim xCnt As Integer = 0
        Dim sstmp() As String
        Dim sSQLCmd As String = "select fcDebitNo from tDebitMemo order by fcDebitNo desc"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    If Not rd.Item(0).ToString Is Nothing Then
                        stemp = rd.Item(0).ToString
                        sstmp = Split(stemp, "-")
                        ' MsgBox(CStr(Format(CDec(Now.Month), "00")) + "     " + sstmp(1), MsgBoxStyle.Information)
                        stemp = "TSA" + "-" + "DM" + CStr(Format(CDec((CDec(sstmp(2)) + 1)), "000000"))
                    End If
                Else
                    stemp = "TSA" + "-" + "DM" + "000001"
                End If
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return stemp
    End Function


    Private Sub load_header()
        Dim sName As String = ""
        Dim sDate As Date = Now
        Dim sDbtNo As String = ""
        Dim sMsg As String = ""
        Dim sTot As Decimal = 0.0
        Dim sRefNo As String = ""
        Dim xClassRefno As String = ""

        Dim sSQLCmd As String = "usp_t_debitmemosload_header "
        sSQLCmd &= " @fxKeyDebit='" & gKeyDebit & "' "
        sSQLCmd &= ",@fxKeyCompany='" & gCompanyID() & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sName = rd.Item(0).ToString
                    sDate = rd.Item(1)
                    sDbtNo = rd.Item(2).ToString
                    sMsg = rd.Item(3).ToString
                    sTot = rd.Item(5)
                    sRefNo = rd.Item(6).ToString
                    xClassRefno = rd.Item("fxClassRefNo").ToString
                    ClassRefno = xClassRefno
                    TxtClassName.Text = GetClassName(ClassRefno)
                End If
            End Using

            cboNames.SelectedItem = sName
            dteDebitMemo.Value = sDate
            txtDebitNo.Text = sDbtNo
            txtCustomerMsg.Text = sMsg
            lblTotal.Text = sTot
            txtPONo.Text = sRefNo
            txtClassRefNo.Text = xClassRefno

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub load_item()
        With Me.grdItem
            Dim sSQLCmd As String = "usp_t_debitmemosload_item "
            sSQLCmd &= " @fxKeyDebit='" & gKeyDebit & "' "
            Dim dtsTable As DataSet = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSQLCmd)
            Dim xRow As Integer = 0
            For xRow = 0 To dtsTable.Tables(0).Rows.Count - 1
                .Rows.Add()
                .Item("cAccounts", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("acnt_desc").ToString
                .Item("cMemo", xRow).Value = dtsTable.Tables(0).Rows(xRow).Item("fcMemo").ToString
                .Item("cDebit", xRow).Value = Format(CDec(dtsTable.Tables(0).Rows(xRow).Item("fdDebitAmt")), "##,##0.00##")
                .Item("cCredit", xRow).Value = Format(CDec(dtsTable.Tables(0).Rows(xRow).Item("fdCreditAmt")), "##,##0.00##")
            Next
        End With
    End Sub

    Private Sub loadcombo()
        Call m_getnames(cboNames)
        Call m_loadSalesTaxCode(cboTaxCodeName, cboTaxCodeID)
        Call createColumn()
    End Sub

    Private Sub createColumn()
        With Me.grdItem
            .Columns.Clear()
            Dim fxKey As New DataGridViewTextBoxColumn
            Dim colAccounts As New DataGridViewComboBoxColumn
            Dim colDebitAmount As New DataGridViewTextBoxColumn
            Dim colCreditAmount As New DataGridViewTextBoxColumn
            Dim colMemo As New DataGridViewTextBoxColumn
            Dim dtAccounts As DataTable = m_displayAccountsDS().Tables(0)
            With colAccounts
                .Items.Clear()
                .HeaderText = "Accounts"
                .DataPropertyName = "fcDescription"
                .Name = "cAccounts"
                .DataSource = dtAccounts.DefaultView
                .DisplayMember = "fcDescription"
                .ValueMember = "fcDescription"
                .DropDownWidth = 350
                .AutoComplete = True
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                .MaxDropDownItems = 10
                .Resizable = DataGridViewTriState.True
            End With
            With colMemo
                .Name = "cMemo"
                .HeaderText = "Memo"
            End With
            With colCreditAmount
                .Name = "cCredit"
                .HeaderText = "Credit"
            End With
            With colDebitAmount
                .Name = "cDebit"
                .HeaderText = "Debit"
            End With
            .Columns.Add(colAccounts)
            .Columns.Add(colDebitAmount)
            .Columns.Add(colCreditAmount)
            .Columns.Add(colMemo)
            .Columns("cDebit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("cDebit").DefaultCellStyle.Format = "##,##0.00"
            .Columns("cCredit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopRight
            .Columns("cCredit").DefaultCellStyle.Format = "##,##0.00"
            .Columns("cAccounts").Width = 300
            .Columns("cMemo").Width = 250
            .Columns("cCredit").Width = 100
            .Columns("cDebit").Width = 100
        End With
    End Sub

    Private Sub cboNames_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboNames.SelectedIndexChanged
        txtBillingAdd.Text = getaddress(getID(cboNames.SelectedItem))
    End Sub

    Private Function getaddress(ByVal sKey As String) As String
        Dim sResult As String = ""

        Dim sSQLCmd As String = "select isnull(t1.fcAddress,'')+' '+isnull(t1.fcStreet,'')+' '+isnull(t1.fcCity,'') as [Address]  "
        sSQLCmd &= " from dbo.loadnames t2 left outer join dbo.mAddress t1 "
        sSQLCmd &= " on t1.fxKeyAddress = t2.fxKeyAddress where t2.fxKeyID ='" & sKey & "' "

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sResult = rd.Item(0).ToString
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return sResult
    End Function

    Private Function getID(ByVal sName As String) As String
        Dim sResult As String = ""
        Dim sSQLCmd As String
        Try
            sSQLCmd = "select fxKeyID from dbo.loadnames where [Name] ='" & sName & "' "
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSQLCmd)
                If rd.Read Then
                    sResult = rd.Item(0).ToString

                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return sResult
    End Function

    Private Function mkDefaultValues(ByVal irow As Integer, ByVal iCol As Integer)
        Return IIf(IsDBNull(grdItem.Rows(irow).Cells(iCol).Value) = True, _
                        Nothing, grdItem.Rows(irow).Cells(iCol).Value)
    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If save_header() = True And save_items() = True Then
            If MsgBox("save successful...", MsgBoxStyle.Information, Me.Text) = MsgBoxResult.Ok Then
                Me.Close()
            End If
        End If
    End Sub

    Private Function save_header() As Boolean
        Dim sSQLCmd As String = "usp_t_debitmemo_save "
        sSQLCmd &= " @fxKeyDebit='" & gKeyDebit & "' "
        sSQLCmd &= ", @fxKeyCompany='" & gCompanyID() & "' "
        sSQLCmd &= ", @fxKeyName='" & getID(Me.cboNames.SelectedItem) & "' "
        sSQLCmd &= ", @fdDateTrans='" & Microsoft.VisualBasic.FormatDateTime(dteDebitMemo.Value, DateFormat.ShortDate) & "' "
        sSQLCmd &= ", @fcDebitNo='" & txtDebitNo.Text & "' "
        sSQLCmd &= ", @fcMessage='" & txtCustomerMsg.Text & "' "
        sSQLCmd &= ", @fdTotal ='" & lblTotal.Text & "' "
        sSQLCmd &= ", @fuCreatedBy='" & gUserName & "' "
        sSQLCmd &= ", @fcRefNo ='" & txtPONo.Text & "' "
        sSQLCmd &= ", @fxClassRefNo ='" & ClassRefno & "'"
        Try
            SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQLCmd)
            Return True
        Catch ex As Exception
            MsgBox(Err.Description, MsgBoxStyle.Information, Me.Text)
            Return False
        End Try
    End Function

    Private Function save_items() As Boolean
        Dim xCnt As Integer
        With Me.grdItem
            For xCnt = 0 To .Rows.Count - 1
                If mkDefaultValues(xCnt, 0) <> Nothing Then
                    Dim sAccount As String = ""
                    Dim sMemo As String = ""
                    Dim dAmount1 As Decimal = 0.0
                    Dim dAmount2 As Decimal = 0.0
                    If mkDefaultValues(xCnt, 0) <> Nothing Then
                        sAccount = m_GetAccountID(.Rows(xCnt).Cells(0).Value.ToString)
                    End If
                    If mkDefaultValues(xCnt, 3) <> Nothing Then
                        sMemo = grdItem.Rows(xCnt).Cells(3).Value
                    End If
                    If mkDefaultValues(xCnt, 1) <> Nothing Then
                        dAmount1 = grdItem.Rows(xCnt).Cells(1).Value
                    End If
                    If mkDefaultValues(xCnt, 2) <> Nothing Then
                        dAmount2 = grdItem.Rows(xCnt).Cells(2).Value
                    End If

                    Dim sSQLCmd As String = "usp_t_debitmemoitem_save "
                    sSQLCmd &= " @fxKeyDebit='" & gKeyDebit & "' "
                    sSQLCmd &= ",@fxKeyAccount='" & sAccount & "' "
                    sSQLCmd &= ",@fcMemo='" & sMemo & "' "
                    sSQLCmd &= ",@fdCreditAmount='" & dAmount2 & "' "
                    sSQLCmd &= ",@fdDebitAmount='" & dAmount1 & "' "
                    Try
                        SqlHelper.ExecuteScalar(gcon.cnstring, CommandType.Text, sSQLCmd)
                    Catch ex As Exception
                        MsgBox(Err.Description, MsgBoxStyle.Information, Me.Text)
                        Return False
                    End Try
                End If
            Next
            Return True
        End With
    End Function

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        gKeyDebit = ""
        Me.Close()
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmDebitAcnt.Show()
    End Sub

    Private Sub debittotal()
        Try
            With Me.grdItem
                Dim xCnt As Integer = 0
                Dim xDebit As Decimal = 0
                For xCnt = 0 To .RowCount - 1
                    If .Item("cAccounts", xCnt).Value IsNot Nothing Then
                        If .Item("cDebit", xCnt).Value IsNot Nothing Then
                            xDebit += CDec(.Item("cDebit", xCnt).Value)
                        Else
                            xDebit += 0
                        End If
                    End If
                Next
                txtTDebit.Text = Format(CDec(xDebit), "##,##0.00")
            End With
        Catch ex As Exception
        End Try
    End Sub

    Private Sub credittotal()
        Try
            With Me.grdItem
                Dim xCnt As Integer = 0
                Dim xCredit As Decimal = 0
                For xCnt = 0 To .RowCount - 1
                    If .Item("cAccounts", xCnt).Value IsNot Nothing Then
                        If .Item("cCredit", xCnt).Value IsNot Nothing Then
                            xCredit += CDec(.Item("cCredit", xCnt).Value)
                        Else
                            xCredit += 0
                        End If
                    End If
                Next
                txtCredit.Text = Format(CDec(xCredit), "##,##0.00")
            End With
        Catch ex As Exception
        End Try
    End Sub

    Private Sub grdItem_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles grdItem.CellPainting
        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center
        If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < grdItem.Rows.Count Then
            e.PaintBackground(e.ClipBounds, True)
            e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
            e.Handled = True
        End If
    End Sub

    Private Sub grdItem_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdItem.CellValueChanged
        Call credittotal()
        Call debittotal()
    End Sub

    Private Sub txtCredit_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCredit.TextChanged
        Try
            If txtTDebit.Text = txtCredit.Text Then
                lblTotal.Text = CDec(txtCredit.Text)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub txtTDebit_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTDebit.TextChanged
        Try
            If txtTDebit.Text = txtCredit.Text Then
                lblTotal.Text = CDec(txtCredit.Text)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        frmDebitMemo.ShowDialog()
    End Sub


    Private Sub grdItem_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles grdItem.EditingControlShowing
        Dim comboBoxItem As DataGridViewComboBoxColumn = Me.grdItem.Columns("cAccounts")
        If (Me.grdItem.CurrentCellAddress.X = comboBoxItem.DisplayIndex) Then
            Dim cb As ComboBox = e.Control
            If (cb IsNot Nothing) Then
                cb.DropDownStyle = ComboBoxStyle.DropDown
                cb.AutoCompleteMode = AutoCompleteMode.Suggest
                cb.AutoCompleteSource = AutoCompleteSource.ListItems
            End If
        End If
    End Sub

    Private Sub grdItem_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdItem.CellContentClick

    End Sub

    Private Sub IsDateRestricted()
        'Validate Date
        If checkDate.checkIfRestricted(dteDebitMemo.Value) Then
            btnSave.Enabled = False 'true if date is NOT within range of period restricted
        Else
            btnSave.Enabled = True 'false if date is within range of period restricted
        End If
    End Sub

    Private Sub dteDebitMemo_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dteDebitMemo.ValueChanged
        IsDateRestricted()
    End Sub

    Private Sub BtnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBrowse.Click
        FrmBrowseClass.Mode = "BROWSE MODE"
        FrmBrowseClass.ShowDialog()
        TxtClassName.Text = FrmBrowseClass.Data1
        txtClassRefNo.Text = FrmBrowseClass.Data2
        ClassRefno = FrmBrowseClass.Data2
    End Sub
End Class